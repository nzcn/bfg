# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-29 22:06
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('appa', '0004_printer'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_date', models.DateTimeField(db_index=True, null=True)),
                ('modified_date', models.DateTimeField(null=True)),
                ('end_date', models.DateTimeField(db_index=True, null=True)),
                ('process_date', models.DateTimeField(null=True)),
                ('duration', models.SmallIntegerField(default=7)),
                ('content', models.TextField(blank=True, default=None, null=True)),
                ('priority', models.SmallIntegerField(default=3)),
                ('is_faq', models.BooleanField(default=False)),
                ('company', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='appa.Company')),
                ('parent', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ticket.Ticket')),
                ('processor', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='processor', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TicketStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
                ('description', models.CharField(max_length=100)),
                ('position', models.SmallIntegerField(default=10)),
                ('short_code', models.CharField(max_length=2, null=True)),
                ('style', models.CharField(max_length=20, null=True)),
                ('send_email', models.BooleanField(default=True)),
                ('is_system', models.BooleanField(default=False)),
                ('company', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='appa.Company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TicketType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20)),
                ('pos', models.PositiveSmallIntegerField(default=100)),
                ('company', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='appa.Company')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='ticket',
            name='status',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ticket.TicketStatus'),
        ),
        migrations.AddField(
            model_name='ticket',
            name='type',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ticket.TicketType'),
        ),
        migrations.AddField(
            model_name='ticket',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ticket_author', to=settings.AUTH_USER_MODEL),
        ),
    ]
