# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-02-27 08:43
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ticket', '0004_auto_20200210_1829'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticket',
            name='company',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='appa.Company'),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='parent',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='ticket.Ticket'),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='processor',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, related_name='processor', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='status',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='ticket.TicketStatus'),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='type',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='ticket.TicketType'),
        ),
        migrations.AlterField(
            model_name='ticketstatus',
            name='company',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='appa.Company'),
        ),
        migrations.AlterField(
            model_name='tickettype',
            name='company',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='appa.Company'),
        ),
    ]
