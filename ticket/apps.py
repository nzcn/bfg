# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# noinspection PyPackageRequirements
from django.apps import AppConfig


class PromotionConfig(AppConfig):
    name = 'ticket'
