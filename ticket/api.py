# noinspection PyPackageRequirements
import rest_framework.pagination
from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets,permissions
from rest_framework import serializers, viewsets
from bfg.ticket.models import *
from rest_framework.response import Response
from rest_framework.decorators import action
import django_filters.rest_framework
from landing.core.api import *
from landing.api import FileSerializer, ImageSerializer, ImageAbstractSerializer, UserSerializer
from bfg.ticket.appa import *
from bfg.ticket.controllers.ticket import *
from bfg.ticket.models import Ticket

def register_api(router):
    router.register(r'tickets', TicketViewSet, base_name="tickets")


class TicketResponseSerializer(serializers.ModelSerializer):
    files = FileSerializer(many=True, read_only=True)
    user = UserSerializer(many=False, read_only=True)
    class Meta:
        model = TicketResponse
        fields = '__all__'

class TicketTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TicketType
        fields = '__all__'

class TicketTypeListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TicketType
        fields = ['id','title','short_name','style','icon','group']

class TicketTypeListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TicketType
        fields = ['id','title','short_name','icon','description','group']

class TicketStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = TicketStatus
        fields = '__all__'


# Serializers define the API representation.
class TicketSerializer(serializers.ModelSerializer):
    files = FileSerializer(many=True, read_only=True)
    type = TicketTypeListSerializer(many=False, read_only=True)
    replys = serializers.SerializerMethodField()
    status = TicketStatusSerializer(many=False, read_only=True)
    state = serializers.SerializerMethodField()
    class Meta:
        model = Ticket
        fields = '__all__'

    def get_state(self, obj):
        result = 'Open'
        if obj.end_date:
            result = 'Close'
        if obj.is_archived:
            result = 'Archieved'
        if obj.is_deleted:
            result = 'Deleted'
        return result

    def get_replys(self, obj):
        replies = obj.ticketresponse_set.all().order_by('id')
        return TicketResponseSerializer(replies, many=True).data

class TicketViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    pagination_class = rest_framework.pagination.LimitOffsetPagination



    def get_queryset(self):
        tm = TicketManager(self.request, ticket_class=self.get_ticket_class())
        type = get_req_value(self.request, 'type')
        return tm.get_tickets(only_for_user=True, filter_by_group=False, show_all=(type == 'all'))

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_serializer_class(self):
        return TicketSerializer

    # serializer_class = TicketSerializer
    search_fields = ('content')

    def get_ticket_form(self):
        return TicketForm

    def get_ticket_response_form(self):
        return TicketResponseForm

    def get_ticket_class(self):
        return Ticket

    def get_ticket_response_class(self):
        return TicketResponse

    def pre_save(self, request, form):
        pass

    @action(methods=['post'], detail=False)
    def new(self, request, **kwargs):
        id = get_req_value(request, 'id')
        if id:
            id = int(id)
            ticket = self.get_ticket_class().objects.get(id=id)
            form = self.get_ticket_form()(request.POST, request.FILES, instance=ticket)
        else:
            form = self.get_ticket_form()(request.POST, request.FILES)
        if form.is_valid():
            if not form.instance.user:
                form.instance.user = request.user
            if not form.instance.company:
                cm = AppaCompanyManager(request)
                if request.user.customer:
                    form.instance.company = request.user.customer.company
                else:
                    form.instance.company = cm.get_company()
            self.pre_save(request, form)
            form.instance.save()
            files = request.FILES.getlist("file")
            if files:
                path = "pack/tickets/{id}/".format(id=form.instance.id)
                for file in files:
                    image = save_post_file(file, path=path, is_image=True, thumb_size=[200, 200], max_size=[1920, 1920])
                    if image:
                        form.instance.files.add(image)
            return api_result(message="Create Ticket Successfully",
                              obj=TicketSerializer(form.instance, many=False).data)
        else:
            return api_result(message="Save failed. Reason:" + form.errors.__str__(), code=2)


    @action(methods=['post'], detail=True)
    def reply(self, request, **kwargs):
        ticket = self.get_object()
        id = get_req_value(request, 'id')
        try:
            if id:
                id = int(id)
                reply = self.get_ticket_response_class().objects.get(id=id)
                form = self.get_ticket_response_form()(request.POST, request.FILES, instance=reply)
            else:
                reply = None
                form = self.get_ticket_response_form()(request.POST, request.FILES)
            form.instance.modified_date = timezone.now()
            form.instance.ticket = ticket
            form.instance.user = request.user
            form.instance.content = get_req_value(request,'content')
            form.instance.create_date = timezone.now()
            form.instance.save()
            files = request.FILES.getlist("file")
            if files:
                path = "pack/tickets/{id}/".format(id=ticket.id)
                for file in files:
                    image = save_post_file(file, path=path, is_image=True, thumb_size=[200, 200], max_size=[1920, 1920])
                    if image:
                        form.instance.files.add(image)
            return api_result(message="Create Ticket Response Successfully",
                              obj=TicketResponseSerializer(form.instance, many=False).data)
        except Exception as ex:
            return api_result(message="Save failed. Reason:" + form.errors.__str__(), code=2)


    @action(methods=['post'], detail=True)
    def finish(self, request, **kwargs):
        ticket = self.get_object()
        form = self.get_ticket_form()(request.POST, request.FILES, instance=ticket)
        if form.is_valid():
            form.instance.end_date = timezone.now()
            form.save()
            return api_result(message="Finish Ticket Successfully",
                              obj=TicketSerializer(form.instance, many=False).data)
        else:
            return api_result(message="Finish Ticket failed. Reason:" + form.errors.__str__(), code=2)


    @action(methods=['get','post'], detail=False)
    def types(self, request, **kwargs):
        tm = TicketManager(self.request)
        translation.activate(get_req_value(self.request, "lang", settings.DEFAULT_LANGUAGE))
        group = get_req_value(request, "group")
        if not group:
            return api_result(message="Group is needed", code=2)
        types = tm.get_ticket_type(group=group)
        if types:
            return api_result(message="Get TicketType Successfully",
                              obj=TicketTypeListSerializer(types, many=True).data)
        else:
            return api_result(message="Get TicketType Failed", code=1)

