# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from bfg.appa.models import CompanyModelBase
from landing.models import File
from django.contrib.auth import models as AdminModels

# Create your models here.
class TicketType(CompanyModelBase):
    title = models.CharField(max_length=20)
    short_name = models.CharField(max_length=20, null=True, blank=True, default=None)
    style = models.CharField(max_length=20, null=True, blank=True, default=None)
    icon = models.CharField(max_length=30, null=True, blank=True, default=None)
    description = models.CharField(max_length=200, null=True, blank=True, default=None)
    group = models.CharField(max_length=20, null=True, blank=True, default=None)
    targets = models.ManyToManyField(AdminModels.Group, blank=True)
    default_options = models.TextField(null=True, blank=True, default=None)
    script_create = models.TextField(null=True, blank=True, default=None)
    expire_hours = models.PositiveIntegerField(default=0,blank=True)
    pos = models.PositiveSmallIntegerField(default=100,blank=True)
    to_customer = models.BooleanField(default=False,blank=True)
    init_status = models.ForeignKey('TicketStatus', null=True, blank=True, default=None,related_name='init_status',on_delete=models.SET_DEFAULT)
    close_status = models.ForeignKey('TicketStatus', null=True, blank=True, default=None,related_name='close_status',on_delete=models.SET_DEFAULT)

    def __str__(self):
        return self.title if self.title else 'NoName Type'

class TicketStatus(CompanyModelBase):
    title = models.CharField(max_length=20)
    description = models.CharField(max_length=200,null=True,blank=True,default=None)
    position = models.SmallIntegerField(default=10)
    short_code = models.CharField(max_length=2,null=True,blank=True,default=None)
    style = models.CharField(max_length=20,null=True,blank=True,default=None)
    send_email = models.BooleanField(default=True)
    is_system = models.BooleanField(default=False)
    type = models.ForeignKey(TicketType, null=True,blank=True,default=None)


class Ticket(CompanyModelBase):
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(null=True, blank=True, default=None)
    process_date = models.DateTimeField(null=True, blank=True, default=None)
    end_date = models.DateTimeField(null=True, blank=True, default=None)
    duration = models.SmallIntegerField(default=7, blank=True)
    status = models.ForeignKey(TicketStatus, default=None, null=True,blank=True, on_delete=models.SET_DEFAULT)
    type = models.ForeignKey(TicketType, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    parent = models.ForeignKey('self', null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    user = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, related_name='ticket_author', on_delete=models.CASCADE)
    processor = models.ForeignKey(AdminModels.User,related_name='processor',default=None,null=True,blank=True, on_delete=models.SET_DEFAULT)
    content = models.TextField(null=True, default=None, blank=True)
    priority = models.SmallIntegerField(default=3, blank=True)
    is_faq = models.BooleanField(default=False, blank=True)
    is_deleted = models.BooleanField(default=False, blank=True)
    is_archived = models.BooleanField(default=False, blank=True)
    files = models.ManyToManyField(File, blank=True, related_name='ticket_files')


class TicketResponse(models.Model):
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    create_date = models.DateTimeField(null=True, db_index=True)
    modified_date = models.DateTimeField(null=True)
    user = models.ForeignKey(AdminModels.User, related_name='ticket_replyer', on_delete=models.CASCADE)
    content = models.TextField(null=True, default=None, blank=True)
    is_deleted = models.BooleanField(default=False)
    is_public = models.BooleanField(default=True)
    request_close = models.BooleanField(default=False)
    files = models.ManyToManyField(File, blank=True, related_name='ticket_response_files')


