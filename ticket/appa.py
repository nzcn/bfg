from django.utils.translation import ugettext_lazy as _
from django.http import *
from bfg.appa.controllers.core import *
from rest_framework import routers, serializers, viewsets, permissions
from django.db.models.aggregates import Avg, Sum, Count
from django.views.decorators.csrf import csrf_exempt
from django.db.models.functions import Coalesce
from django import forms
from bfg.ticket.models import *
import time


class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = "__all__"


class TicketResponseForm(forms.ModelForm):
    class Meta:
        model = TicketResponse
        fields = "__all__"


class TicketAppa(CompanyBaseAdmin):
    list_display = ['content', 'type', 'status', 'priority', 'process_date', 'end_date', 'is_faq']
    search_fields = ['content', 'processor', 'user']
    list_filter = ['type', 'status', 'priority', 'is_faq', 'create_date']


class TicketTypeAppa(CompanyBaseAdmin):
    list_display = ['title', 'short_name', 'group', 'to_customer', 'targets_field', 'pos', 'expire_hours']
    list_filter = ['group', 'to_customer', ]
    search_fields = ['title', 'short_name']
    fieldsets = [
        (None, {'fields': ['title', 'short_name', 'description', 'group', 'targets', 'to_customer',
                           'default_options', 'script_create',
                           'pos', 'style', 'icon', 'expire_hours']}),
    ]

    def targets_field(self, obj):
        return get_html_from_template("ticket/appa/template/ticket_target_field.html",
                                      {'ticket': obj, 'request': self.request})

    targets_field.allow_tags = True
    targets_field.short_description = _("Targets")


class TicketStatusAppa(CompanyBaseAdmin):
    list_display = ['title', 'position', 'short_code', 'style', 'send_email']
    allow_system_delete = False
    allow_system_edit = False
    fieldsets = [
        (None,
         {'fields': ['title', 'description', 'position', 'short_code', 'type', 'style', 'send_email', 'is_system']}),
    ]
