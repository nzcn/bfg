from landing.core.singal import BfgSignal

after_create_ticket = BfgSignal(providing_args=["request", "ticket"])
after_reply_ticket = BfgSignal(providing_args=["request", "ticket"])
