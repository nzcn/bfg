# noinspection PyPackageRequirements
import os
from django.shortcuts import render,reverse
from django.db.models import Q
from bfg.ticket.models import *
from landing.core.common import *
from bfg.appa.controllers.company import AppaCompanyManager
from django.utils.translation import ugettext as _

class TicketManager(object):
    def __init__(self,request = None, ticket_class=Ticket):
        self.request = request
        self.ticket_class = ticket_class

    def get_tickets(self,user=None, processor=None, company=None, filter_by_group=True, only_for_user=False,show_all=False):
        cm = AppaCompanyManager(request=self.request)
        if not user:
            user = self.request.user

        if not company:
            if hasattr(user,'customer') and user.customer:
                company = user.customer.company
            else:
                company = cm.get_company()

        filter = None
        if user:
            filter = Q(user=user)
        if processor:
            filter = combine_filter(filter,Q(processor=processor))
        if company:
            filter = combine_filter(filter, Q(company=company))
        if not only_for_user:
            filter = combine_filter(filter, Q(processor__isnull=True),logic_and=False)
        if filter_by_group:
            groups = list(user.groups.values_list('id',flat=True))
            if groups:
                filter = combine_filter(filter, Q(type__targets__in=groups))

        if not show_all:
            filter = combine_filter(filter, Q(end_date__isnull=True))
        return self.ticket_class.objects.filter(filter).order_by('-priority','create_date')

    def create_ticket_from_request(self):
        pass

    def create_ticket(self,fo,title,type,user=None,content=None):
        cm = AppaCompanyManager(request=self.request)
        company = cm.get_company()
        ticket = Ticket(company=company, user=user)
        ticket.content = title
        ticket.save()

    def get_ticket_type(self, group=None, company=None, order_by='pos'):
        if not company:
            cm = AppaCompanyManager(self.request)
            company = cm.get_company()
        q = Q(company=company)
        if group:
            q = q & Q(group=group)
        return TicketType.objects.filter(q).order_by(order_by)

