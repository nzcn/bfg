from __future__ import unicode_literals

from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth import models as AdminModels
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
class Variable(models.Model):
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    key = models.CharField(max_length=30)
    content = RichTextUploadingField(default=None, null=True, blank=True)
    description = models.TextField(default=True, null=True, blank=True)

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = _("Variable")
        verbose_name_plural = _("Variables")


HISTORY_TYPE = (
    ('C', 'Credit'),
    ('A', 'Action'),
    ('S', 'System'),
)


class Address(models.Model):
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    firstname = models.CharField(max_length=30, default=None, null=True, blank=True)
    lastname = models.CharField(max_length=30, default=None, null=True, blank=True)
    middlename = models.CharField(max_length=30, default=None, null=True, blank=True)
    email = models.CharField(max_length=100, default=None, null=True, blank=True)
    tel = models.CharField(max_length=100, default=None, null=True, blank=True)
    fax = models.CharField(max_length=100, default=None, null=True, blank=True)
    mobile = models.CharField(max_length=50, default=None, null=True, blank=True)
    tax = models.FloatField(default=0, blank=True)
    building = models.CharField(max_length=100, default=None, null=True, blank=True)
    address = models.CharField(max_length=100, default=None, null=True, blank=True)
    suburb = models.CharField(max_length=100, default=None, null=True, blank=True)
    city = models.CharField(max_length=30, default=None, null=True, blank=True)
    state = models.CharField(max_length=30, default=None, null=True, blank=True)
    country = models.CharField(max_length=30, default=None, null=True, blank=True)
    postcode = models.CharField(max_length=20, default=None, null=True, blank=True)
    lat = models.FloatField(default=0, blank=True)
    lng = models.FloatField(default=0, blank=True)
    geocode = models.CharField(max_length=150, default=None, null=True, blank=True)

    def __str__(self):
        return self.address or u'No Address'

    @property
    def full_name(self):
        return "{firstname}{middlename}{lastname}".format(
            firstname=self.firstname,
            middlename=(' ' + self.middlename) if (self.middlename and self.middlename != 'null') else '',
            lastname=(' ' + self.lastname) if (self.lastname and self.lastname != 'null') else '')

    @property
    def full_address(self):
        return "{firstname} {lastname} {address},{suburb},{city},{state}".format(
            firstname=self.firstname,
            lastname=self.lastname if self.lastname else '',
            state=self.state if self.state else '',
            city=self.city,
            suburb=self.suburb,
            address=self.address)

    @property
    def address_cn(self):
        return "{country}{state}{city}{suburb}{address}".format(
            state=self.state if self.state else '',
            city=self.city if self.city else '',
            suburb=self.suburb if self.suburb else '',
            address=self.address if self.address else '',
            country=self.country if self.country else '',
        )

    class Meta:
        verbose_name = _("Address")
        verbose_name_plural = _("Addresses")


EmailType = (
    ('H', 'HTML'),
    ('P', 'Plain Text'),
)


class Language(models.Model):
    title = models.CharField(max_length=50, null=True, default=None, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    logo = models.ImageField(max_length=255, null=True, default=None, blank=True)
    enable = models.BooleanField(default=True)
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)

    class Meta:
        verbose_name = _("Language")
        verbose_name_plural = _("Languages")


GenderType = (
    ('M', 'Male'),
    ('F', 'Female'),
    ('U', 'Unknown'),
)

UserRequestOperationType = (
    ('F', 'Forgot Password'),
)


class OperationRequest(models.Model):
    user_account = models.CharField(max_length=50)
    created_time = models.DateTimeField(auto_now_add=True)
    key = models.CharField(max_length=32, unique=True)
    type = models.CharField(max_length=1, default='F', choices=UserRequestOperationType, blank=True)


class Profile(models.Model):
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    user = models.OneToOneField(AdminModels.User)
    balance = models.FloatField(default=0, blank=True)
    credit = models.PositiveIntegerField(default=0, blank=True)
    used_credit = models.PositiveIntegerField(default=0, blank=True)
    experience = models.PositiveIntegerField(default=0, blank=True)
    feedback = models.FloatField(default=3.0)

    message = models.CharField(max_length=200, default=None, null=True, blank=True)

    account_facebook = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_googleplus = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_twitter = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_instgram = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_pintrest = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_wechat = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_qq = models.CharField(max_length=50, default=None, null=True, blank=True)

    social_wechat_openid = models.CharField(max_length=50, default=None, null=True, blank=True, unique=True)
    social_wechat_settings = models.TextField(default=None, null=True, blank=True)
    social_google_openid = models.CharField(max_length=50, default=None, null=True, blank=True, unique=True)
    social_google_settings = models.TextField(default=None, null=True, blank=True)
    social_facebook_openid = models.CharField(max_length=50, default=None, null=True, blank=True, unique=True)
    social_facebook_settings = models.TextField(default=None, null=True, blank=True)
    social_twitter_openid = models.CharField(max_length=50, default=None, null=True, blank=True, unique=True)
    social_twitter_settings = models.TextField(default=None, null=True, blank=True)
    social_qq_openid = models.CharField(max_length=50, default=None, null=True, blank=True, unique=True)
    social_qq_settings = models.TextField(default=None, null=True, blank=True)

    activiate_key = models.CharField(max_length=100, null=True, default=None, blank=True)
    activiated = models.BooleanField(default=False)
    activiated_date = models.DateTimeField(null=True, default=None, blank=True)

    addresses = models.ManyToManyField(Address)
    picture = models.ImageField(default=None, null=True, blank=True)

    timezone = models.SmallIntegerField(default=0, blank=True)
    gender = models.CharField(max_length=1, default='M', choices=GenderType)
    birthday = models.DateField(default=None, null=True, blank=True)
    default_address = models.ForeignKey(Address, default=None, null=True, blank=True, related_name='default_address',
                                        on_delete=models.SET_DEFAULT)
    default_language = models.ForeignKey(Language, default=None, null=True, blank=True, related_name='default_language',
                                         on_delete=models.SET_DEFAULT)
    request_language = models.ForeignKey(Language, default=None, null=True, blank=True, related_name='request_language',
                                         on_delete=models.SET_DEFAULT)
    auth_mobile = models.CharField(max_length=20, default=None, null=True, blank=True)
    auth_email = models.CharField(max_length=30, default=None, null=True, blank=True)
    confirmed_mobile = models.BooleanField(default=False)
    confirmed_email = models.BooleanField(default=False)
    email_type = models.CharField(max_length=1, default='H', choices=EmailType)
    closed = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")


class ProfileHistory(models.Model):
    user = models.ForeignKey(AdminModels.User, on_delete=models.CASCADE)
    type = models.CharField(max_length=1, default='C', choices=HISTORY_TYPE)
    created_time = models.DateTimeField(auto_now_add=True)
    operator = models.ForeignKey(AdminModels.User, related_name='operator', null=True, default=None, blank=True,
                                 on_delete=models.SET_DEFAULT)
    content = RichTextUploadingField(default=None, null=True, blank=True)

    class Meta:
        verbose_name = _("Profile History")
        verbose_name_plural = _("Profile Histories")


class Site(models.Model):
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    name = models.CharField(max_length=20, default=None, blank=True, null=True)
    title = models.CharField(max_length=50)
    subtitle = models.CharField(blank=True, max_length=100, null=True, default=None)

    meta_title = models.CharField(max_length=200, default=None, null=True, blank=True)
    description = models.CharField(max_length=200, default=None, null=True, blank=True)
    keywords = models.CharField(max_length=200, default=None, null=True, blank=True)

    logo = models.CharField(blank=True, max_length=255, null=True, default=None)
    reverse_logo = models.CharField(blank=True, max_length=255, null=True, default=None)
    home_page = models.CharField(blank=True, max_length=255, null=True, default=None)
    page_icon = models.CharField(blank=True, max_length=255, null=True, default=None)
    template = models.CharField(blank=True, max_length=255, null=True, default=None)

    address = models.CharField(blank=True, max_length=50, null=True, default=None)
    country = models.CharField(blank=True, max_length=20, null=True, default=None)
    state = models.CharField(blank=True, max_length=50, null=True, default=None)
    city = models.CharField(blank=True, max_length=50, null=True, default=None)
    tel1 = models.CharField(blank=True, max_length=50, null=True, default=None)
    fax = models.CharField(blank=True, max_length=50, null=True, default=None)
    email = models.CharField(blank=True, max_length=50, null=True, default=None)
    mobile = models.CharField(blank=True, max_length=20, null=True, default=None)
    contact_person = models.CharField(blank=True, max_length=20, null=True, default=None)
    contact_time = models.TextField(blank=True, null=True, default=None)

    author = models.ForeignKey(AdminModels.User, blank=True, null=True, default=None, on_delete=models.SET_DEFAULT)
    create_time = models.DateTimeField(auto_created=True)
    last_modified_time = models.DateTimeField(auto_now=True)

    email_server = models.CharField(blank=True, max_length=50, null=True, default=None)
    email_port = models.PositiveIntegerField(default=465)
    email_username = models.CharField(blank=True, max_length=100, null=True, default=None)
    email_password = models.CharField(blank=True, max_length=50, null=True, default=None)
    email_user_auth = models.BooleanField(default=True)
    email_sender = models.CharField(blank=True, max_length=50, null=True, default=None)
    email_replyto = models.CharField(blank=True, max_length=100, null=True, default=None)
    email_use_tls = models.BooleanField(default=False)
    email_use_ssl = models.BooleanField(default=True)

    show_login = models.BooleanField(default=True)
    show_register = models.BooleanField(default=True)

    home_top_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_slide_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_feature_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_service_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_info_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_price_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_action_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_testimonial_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_contact_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_request_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_newsbox_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_subscription_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_mail_html = RichTextUploadingField(blank=True, default=None, null=True)
    home_about_html = RichTextUploadingField(blank=True, default=None, null=True)

    terms = RichTextUploadingField(blank=True, default=None, null=True)

    social_facebook = models.CharField(blank=True, max_length=100, null=True, default=None)
    social_twitter = models.CharField(blank=True, max_length=100, null=True, default=None)
    social_pinterest = models.CharField(blank=True, max_length=100, null=True, default=None)
    social_instagram = models.CharField(blank=True, max_length=100, null=True, default=None)
    social_linkedin = models.CharField(blank=True, max_length=100, null=True, default=None)
    social_sina = models.CharField(blank=True, max_length=100, null=True, default=None)
    social_wechat = models.CharField(blank=True, max_length=100, null=True, default=None)
    social_qq = models.CharField(blank=True, max_length=100, null=True, default=None)

    header = RichTextUploadingField(blank=True, default=None, null=True)
    footer = RichTextUploadingField(blank=True, default=None, null=True)
    top_html = RichTextUploadingField(blank=True, default=None, null=True)
    footer_html = RichTextUploadingField(blank=True, default=None, null=True)

    google_map_apikey = models.CharField(blank=True, max_length=50, null=True, default=None)
    google_analysis_apikey = models.CharField(blank=True, max_length=50, null=True, default=None)

    enable = models.BooleanField(default=True)

    paypal_site = models.BooleanField(default=True, help_text="Use normal paypal site or sandbox site.")
    pay_paypal = models.CharField(blank=True, max_length=100, null=True, default=None)
    free_user_period = models.PositiveIntegerField(default=0)

    settings = models.TextField(default=None, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Site")
        verbose_name_plural = _("Sites")


class ContentType(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.CASCADE)
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    title = models.CharField(max_length=50)
    key = models.CharField(max_length=50, default=None, null=True, blank=True)
    subtitle = models.TextField(null=True, default=None, blank=True)
    content = RichTextUploadingField(default=None, null=True, blank=True)

    meta_title = models.CharField(max_length=200, default=None, null=True, blank=True)
    description = models.CharField(max_length=200, default=None, null=True, blank=True)
    keywords = models.CharField(max_length=200, default=None, null=True, blank=True)

    picture = models.ImageField(max_length=255, null=True, default=None, blank=True)
    logo = models.ImageField(max_length=255, null=True, default=None, blank=True)
    icon_class = models.CharField(max_length=255, null=True, default=None, blank=True)
    header_class = models.CharField(max_length=255, null=True, default=None, blank=True)
    show_children = models.BooleanField(default=True)
    parent = models.ForeignKey('self', null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    path = models.CharField(max_length=200, default=None, null=True, blank=True, db_index=True)

    pos = models.PositiveSmallIntegerField(default=0)

    href = models.CharField(max_length=255, null=True, default=None, blank=True)
    url_name = models.CharField(max_length=255, null=True, default=None, blank=True)
    url_param1 = models.CharField(max_length=255, null=True, default=None, blank=True)
    url_param2 = models.CharField(max_length=255, null=True, default=None, blank=True)
    url_param3 = models.CharField(max_length=255, null=True, default=None, blank=True)
    url_param4 = models.CharField(max_length=255, null=True, default=None, blank=True)
    open_in_new_window = models.BooleanField(default=False)
    shown_content_in_lists_page = models.BooleanField(default=False)
    template = models.CharField(max_length=255, null=True, default=None, blank=True)

    author = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    create_time = models.DateTimeField(default=None, null=True, auto_created=True, blank=True)
    last_modified_time = models.DateTimeField(auto_now=True, blank=True)
    enable = models.BooleanField(default=True)

    @property
    def children(self):
        if not hasattr(self, "_children"):
            self._children = []
        return self._children

    class Meta:
        unique_together = ('site', 'lang', 'key')
        verbose_name = _("ContentType")
        verbose_name_plural = _("ContentTypes")

    def __str__(self):
        if self.site:
            return "{s}:{k}".format(s=self.site.name, k=self.title)
        else:
            return "{k}".format(k=self.title)


# Site Related
class Tag(models.Model):
    group = models.CharField(max_length=10, null=True, default=None, blank=True)
    key = models.CharField(max_length=10, null=True, default=None, blank=True, unique=True)
    title = models.CharField(max_length=50, null=True, default=None, blank=True)
    subtitle = models.TextField(blank=True, null=True, default=None)
    content = RichTextUploadingField(blank=True, null=True, default=None)
    pos = models.PositiveSmallIntegerField(default=0)

    meta_title = models.CharField(max_length=200, default=None, null=True, blank=True)
    description = models.CharField(max_length=200, default=None, null=True, blank=True)
    keywords = models.CharField(max_length=200, default=None, null=True, blank=True)

    picture = models.ImageField(max_length=255, null=True, default=None, blank=True)
    logo = models.ImageField(max_length=255, null=True, default=None, blank=True)
    icon_class = models.CharField(max_length=255, null=True, default=None, blank=True)
    header_class = models.CharField(max_length=255, null=True, default=None, blank=True)
    show_children = models.BooleanField(default=True)
    parent = models.ForeignKey('self', null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    path = models.CharField(max_length=200, default=None, null=True, blank=True, db_index=True)

    author = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    create_time = models.DateTimeField(default=None, null=True, auto_created=True, blank=True)
    last_modified_time = models.DateTimeField(auto_now=True, blank=True)
    enable = models.BooleanField(default=True)

    def __str__(self):
        if self.title:
            return self.title
        else:
            return 'Unkown Tag'

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")


class Post(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.CASCADE)
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    title = models.CharField(max_length=50)
    type = models.ManyToManyField(ContentType)
    tags = models.ManyToManyField(Tag, blank=True)
    comments = models.ManyToManyField('Comment', blank=True)
    url_slug = models.SlugField(unique=True, default=None, null=True, blank=True)

    meta_title = models.CharField(max_length=200, default=None, null=True, blank=True)
    description = models.CharField(max_length=200, default=None, null=True, blank=True)
    keywords = models.CharField(max_length=200, default=None, null=True, blank=True)

    path = models.CharField(max_length=200, default=None, null=True, blank=True, db_index=True)

    subtitle = models.TextField(null=True, default=None, blank=True)
    content = RichTextUploadingField(default=None, null=True, blank=True)
    picture = models.ImageField(max_length=255, null=True, default=None, blank=True)
    logo = models.ImageField(max_length=255, null=True, default=None, blank=True)
    icon = models.CharField(max_length=255, null=True, default=None, blank=True)
    images = models.ManyToManyField('Image', blank=True)
    attachment = models.FileField(max_length=255, null=True, default=None, blank=True)
    show_children = models.BooleanField(default=True)
    parent = models.ForeignKey('self', null=True, blank=True, default=None, on_delete=models.CASCADE)
    pos = models.PositiveSmallIntegerField(default=100)

    template = models.CharField(max_length=255, null=True, default=None, blank=True)

    view_count = models.PositiveIntegerField(default=0)
    like_count = models.PositiveIntegerField(default=0)
    dislike_count = models.PositiveIntegerField(default=0)
    star = models.PositiveIntegerField(default=3)

    author = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, related_name='author',
                               on_delete=models.SET_DEFAULT)
    create_time = models.DateTimeField(auto_created=True, blank=True)
    last_modified_time = models.DateTimeField(auto_now=True, blank=True)
    enable = models.BooleanField(default=True)

    def __str__(self):
        return "{s}-{t}".format(s=self.site, t=self.title)

    def get_events(self):
        return ['BeforeSave', 'AfterSave', 'BeforeCreate', 'AfterCreate']

    class Meta:
        verbose_name = _("Post")
        verbose_name_plural = _("Posts")


class Link(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.CASCADE)
    url = models.URLField(max_length=255, null=True, blank=True, default=None, unique=True)
    post = models.ForeignKey(Post, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)

    class Meta:
        verbose_name = _("Link")
        verbose_name_plural = _("Links")


class Testimonial(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.CASCADE)
    user = models.CharField(max_length=50)
    company = models.CharField(max_length=50, null=True, default=None)
    title = models.CharField(max_length=50, null=True, default=None)
    content = RichTextUploadingField(null=True, default=None)
    picture = models.URLField(max_length=255, null=True, default=None)
    create_time = models.DateTimeField(auto_created=True)
    last_modified_time = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("Testimonial")
        verbose_name_plural = _("Testimonials")


COMMENT_STATUS = (
    ('O', 'Open'),
    ('P', 'Processing'),
    ('C', 'Commplete'),
    ('A', 'Cancel'),
    ('I', 'Ignore'),
)


class Comment(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.CASCADE)
    author = models.ForeignKey(AdminModels.User, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    name = models.CharField(max_length=50, default=None, null=True, blank=True)
    email = models.CharField(max_length=100, default=None, null=True, blank=True)
    phone = models.CharField(max_length=100, default=None, null=True, blank=True)
    content = RichTextUploadingField(null=True, default=None)
    create_time = models.DateTimeField(auto_created=True)
    last_modified_time = models.DateTimeField(auto_now=True)
    parent = models.ForeignKey('self', null=True, blank=True, default=None, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, default='O', choices=COMMENT_STATUS)

    def get_events(self):
        return ['BeforeSave', 'AfterSave', 'BeforeCreate', 'AfterCreate']

    class Meta:
        verbose_name = _("Comment")
        verbose_name_plural = _("Comments")


class Event(models.Model):
    title = models.CharField(max_length=150, default=None, null=True, blank=True)
    author = models.ForeignKey(AdminModels.User, default=None, null=True, blank=True, related_name='event_author',
                               on_delete=models.SET_DEFAULT)
    receivers = models.ManyToManyField(AdminModels.User, blank=True, related_name='event_receivers')
    place = models.CharField(max_length=100, default=None, null=True, blank=True)
    content = RichTextUploadingField(null=True, default=None, blank=True)
    create_time = models.DateTimeField(auto_created=True)
    last_modified_time = models.DateTimeField(auto_now=True, null=True, blank=True)
    start_time = models.DateTimeField(null=True, default=None, blank=True)
    end_time = models.DateTimeField(null=True, default=None, blank=True)
    alert_time = models.DateTimeField(null=True, default=None, blank=True)

    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")


class MailSubscription(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    email = models.CharField(max_length=100)
    create_time = models.DateTimeField(auto_created=True)
    enable = models.BooleanField(default=True)

    class Meta:
        verbose_name = _("MailSubscription")
        verbose_name_plural = _("MailSubscriptions")


class MailTemplate(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    key = models.CharField(max_length=50, default=None, null=True, unique=True,
                           help_text='Template identifier. Must be unique.')
    subject = models.CharField(max_length=200, default=None, null=True)
    body = models.TextField(default=None, null=True, blank=True)
    is_html = models.BooleanField(default=True)

    class Meta:
        verbose_name = _("MailTemplate")
        verbose_name_plural = _("MailTemplates")


class SEOTemplate(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    key = models.CharField(max_length=50, default=None, null=True, unique=True,
                           help_text='Template identifier. Must be unique.')
    title = models.CharField(max_length=300, default=None, null=True, blank=True)
    description = models.TextField(default=None, null=True, blank=True)
    keywords = models.CharField(max_length=500, default=None, null=True, blank=True)
    author = models.CharField(max_length=300, default=None, null=True, blank=True)

    class Meta:
        verbose_name = _("SEOTemplate")
        verbose_name_plural = _("SEOTemplates")


class Message(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    title = models.CharField(max_length=200)
    name = models.CharField(max_length=200, default=None, null=True, blank=True)
    email = models.CharField(max_length=200, default=None, null=True, blank=True)
    content = RichTextUploadingField(default=None, null=True, blank=True)
    sender = models.ForeignKey(AdminModels.User, null=True, default=None, blank=True, related_name='sender',
                               on_delete=models.SET_DEFAULT)
    reader = models.ForeignKey(AdminModels.User, null=True, default=None, blank=True, related_name='reader',
                               on_delete=models.SET_DEFAULT)
    create_time = models.DateTimeField(auto_now_add=True)
    read_time = models.DateTimeField(default=None, null=True, blank=True)
    is_system = models.BooleanField(default=True)
    priority = models.PositiveSmallIntegerField(default=3)
    page = models.URLField(default=None, blank=True, null=True)
    ip = models.GenericIPAddressField(default=None, null=True, blank=True)

    def get_events(self):
        return ['BeforeSave', 'AfterSave', 'BeforeCreate', 'AfterCreate']

    class Meta:
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")


class Action(models.Model):
    site = models.ForeignKey(Site, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    object = models.CharField(max_length=30, db_index=True)
    event = models.CharField(max_length=30, db_index=True)
    action = models.CharField(max_length=100)
    parameters = models.TextField(default=None, null=True, blank=True)

    class Meta:
        verbose_name = _("Action")
        verbose_name_plural = _("Actions")

    def __str__(self):
        return '{obj}.{event} {action}'.format(obj=self.object, event=self.event, action=self.action)


class File(models.Model):
    title = models.CharField(max_length=500, blank=True, null=True)
    size = models.PositiveIntegerField()
    content_type = models.CharField(max_length=100, null=True, blank=True, default=None)
    path = models.FilePathField(max_length=500, null=True, blank=True, default=None)
    url = models.CharField(max_length=500, null=True, blank=True)
    create_time = models.DateTimeField(auto_now_add=True)
    uploader = models.ForeignKey(AdminModels.User, null=True, default=None, blank=True, related_name='uploader',
                                 on_delete=models.SET_DEFAULT)
    download_count = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = _("File")
        verbose_name_plural = _("Files")

    @property
    def ext(self):
        if self.content_type and '/' in self.content_type:
            types = self.content_type.split('/')
            if len(types) > 1:
                return types[1]
        return 'unknown'


class Image(File):
    thumb = models.CharField(max_length=500, null=True, blank=True)
    video = models.URLField(null=True, blank=True, default=None)
    video_short = models.URLField(null=True, blank=True, default=None)
    width = models.PositiveSmallIntegerField()
    height = models.PositiveSmallIntegerField()
    out_id = models.PositiveIntegerField(null=True, blank=True, default=None)
    pos = models.PositiveSmallIntegerField(default=0)
    is_secret = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")


class ChangeHistory(models.Model):
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    version = models.CharField(max_length=30, default=None, null=True, blank=True)
    version_number = models.PositiveIntegerField(default=0, null=True, blank=True)
    download_count = models.PositiveIntegerField(default=0, null=True, blank=True)
    content = RichTextUploadingField(default=None, null=True, blank=True)
    author = models.ForeignKey(AdminModels.User, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    create_time = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=3, default=None, null=True, blank=True)
    force_update = models.BooleanField(default=False)
    download_file = models.FileField(default=None, null=True, blank=True)
    priority = models.PositiveSmallIntegerField(default=3)

    class Meta:
        verbose_name = _("ChangeHistory")
        verbose_name_plural = _("ChangeHistories")
