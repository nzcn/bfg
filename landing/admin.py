# noinspection PyPackageRequirements
from django.contrib import admin
from landing.models import *
from landing.admin_classes import *

# Register your models here.
admin.site.register(Site, SiteAdmin)
admin.site.register(ContentType, ContentTypeAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Testimonial, TestimonialAdmin)
admin.site.register(Comment)
admin.site.register(Variable, VairableAdmin)
admin.site.register(MailTemplate, MailTemplateAdmin)
admin.site.register(SEOTemplate, SEOTemplateAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Address)
admin.site.register(Action)
admin.site.register(Tag, TagAdmin)
admin.site.register(ChangeHistory, ChangeHisotryAdmin)
