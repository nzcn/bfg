from datetime import *

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.text import slugify

from landing.models import Site, ContentType


class Command(BaseCommand):
    help = 'Init landing data'

    def handle(self, *args, **options):
        # Check whether the site exitst:
        if Site.objects.all().count() > 0:
            print("Aleady has a website confirgration. Init process will terminate soon.")
            return
        print("Create a admin user...")
        new_user = False
        user = None
        while (not new_user):
            username = raw_input("What's the name of the user:")
            new_user = not User.objects.filter(username=username).exists()
            if new_user:
                password = raw_input("Please input the password of the user:")
                email = raw_input("What's the email of the user:")
                user = User.objects.create_superuser(username=username, password=password, email=email)
                print("Create super user finished.")
            else:
                use_existing_user = raw_input("The user already exists. Do you want use this user? (Y/n)") == 'Y'
                if use_existing_user:
                    user = User.objects.get(username=username)
                    password = raw_input("Please inpu the password of the user:")
                    if not user.check_password(password):
                        print("Wrong password")
                    else:
                        print("User selected")
                    break

        print("Init Landing Site Data....")
        site = Site()
        site.name = raw_input("What's the name of the site:")
        site.title = raw_input("Add a title on the homepage:")
        site.address = raw_input("Address:")
        site.tel1 = raw_input("Telphone:")
        site.email = raw_input("Email:")
        site.create_time = timezone.now()
        site.author = user
        site.save()
        if ContentType.objects.count() == 0:
            result = raw_input("Do you need init ContentTypes? (Y/n):")
            if result == 'Y':
                titles = raw_input("Input the titles for sections(seperate with ','):")
                i = 10
                for title in titles.split(','):
                    ct = ContentType()
                    ct.pos = i
                    ct.title = title
                    ct.key = slugify(title).lower()
                    if ct.key == 'home':
                        ct.url_name = 'home'
                    elif ct.key == 'contact-us':
                        ct.url_name = 'landing_contactus'
                    else:
                        ct.url_name = 'landing_content_page'
                        ct.url_param1 = ct.key
                    ct.save()
                    i += 10
                    print("{title} content type created.".format(title=title))
        print("Finish init site command.")
