from django.core.management.base import BaseCommand, CommandError
import datetime
from landing.models import Post,ContentType,Site
# noinspection PyPackageRequirements
from django.utils.text import slugify
from django.contrib.auth.models import User
# noinspection PyPackageRequirements
from django.utils import translation
from django.conf import settings

class Command(BaseCommand):
    help = 'Clone landing contenttype and post to locale file'

    def handle(self, *args, **options):
        available_languages = list(settings.LANGUAGES)
        for lang in available_languages:
            self.clone_content(lang[0])

    def clone_content(self,lang):
        # Close Site
        sites = Site.objects.all()
        for site in sites:
            if site.lang != lang:
                try:
                    site.id = None
                    site.title = site.title + " " + lang
                    site.lang = lang
                    site.save()
                except Exception as ex:
                    pass

        cts = ContentType.objects.all()
        for ct in cts:
            if ct.lang != lang:
                try:
                    posts = Post.objects.filter(type__in=[ct])
                    ct.id = None
                    ct.title = ct.title + "" + lang
                    ct.lang = lang
                    ct.save()

                    for post in posts:
                        if post.lang != lang:
                            try:
                                post.id = None
                                post.title = post.title + "" + lang
                                post.lang = lang
                                post.save()
                                post.type.add(ct)
                            except Exception as ex:
                                pass

                except Exception as ex:
                    pass
        print("Clone {lang} Contents Finished".format(lang=lang))