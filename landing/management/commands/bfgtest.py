from django.core.management.base import BaseCommand
from landing.core.common import *

class Command(BaseCommand):
    help = 'Test BFG Configuration'

    def handle(self, *args, **options):
        self.send_email()

    def send_email(self):
        print('Sending test email...')
        try:
            send_email("Test Email","This is a test")
            print('Successfully')
        except Exception as ex:
            print(ex)

    def check_notifications(self):
        pass


