from __future__ import division

from datetime import *

from PIL import Image as PLImage
from django.apps import apps
from django.contrib.auth.models import Group
from django.core.files.uploadedfile import UploadedFile
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.shortcuts import *
from django.utils import translation
from django.views.decorators.csrf import csrf_exempt

from landing.core.common import *
from landing.controller.contents import ContentManager
from landing.models import *


# Create your views here.
# @cache_page(60 * 60 * 12)
def index(request):
    homepage = True
    return template_response(request, 'landing/index.html', locals())


def unsubscribe(request):
    return template_response(request, 'landing/index.html', locals())


def show_result(request):
    if request.method == 'POST':
        title = request.POST.get('title', 'Operation Result')
    return template_response(request, 'landing/operation_result.html', locals())


def contact_email(request):
    if request.method == 'POST':
        site = get_site(request)
        context = {"name": request.POST.get("name"),
                   "email": request.POST.get("email"),
                   "message": request.POST.get("message"),
                   "ip": get_client_ip(request)}
        send_email_from_template("New Inquiry From Your Website",
                                 context,
                                 'landing/mail/html/inquery_from_website.html',
                                 [request.POST.get("email")],
                                 [site.email]
                                 )
        return JsonResponse(get_action_result(True, 'Email sent successfully'))
    else:
        return JsonResponse(get_action_result(False, 'GET is not accepted'))


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def contact_us(request):
    if request.method == 'POST':
        if 'email' in request.POST:
            context = {}
            context.update(request.META)
            context.update(request.POST.dict())
            send_email_from_template('New inquiry from your website', context,
                                     'landing/mail/html/inquery_from_website.html', [context['email']])
            message = 'Thanks for you inquiry. We will contact you soon.'
        else:
            message = 'Email is needed.'
        return template_response(request, 'landing/action_result.html', locals())
    else:
        q = ContentType.objects.filter(key='contact', lang__in=get_lang_array())
        if q.exists():
            cat = q.first()
        if request.method == 'POST':
            request.session['last_contact'] = timezone.now
            comment = Comment()
            comment.name = request.POST.get('name', '')
            comment.email = request.POST.get('email', '')
            comment.phone = request.POST.get('phone', '')
            comment.content = request.POST.get('message', '')
            comment.save()

            message = 'Thanks for your message. We will contact you soon.'
            return template_response(request, 'landing/action_result.html', locals())
        else:
            return template_response(request, 'landing/contact_us.html', locals())


@login_required
def common_action(request, action='', app_label='landing', model_name=''):
    if len(action) == 0 or not action or action.lower() == 'none':
        raise ValueError('Action type invalid')
    try:
        model = apps.get_model(app_label=app_label, model_name=model_name)
        ids = get_ids(request)
        for id in ids:
            if action.lower() == 'delete':
                # model.objects.filter(pk=id).delete()
                pass
            elif action.lower() == 'export':
                pass
        return JsonResponse(get_action_result(0, 'Operation completed'))
    except Exception as ex:
        raise ValueError(ex.message)


@login_required
def side_notice(request):
    groups = request.user.groups.all()
    if Group.objects.get(name='Free') in groups:
        free_user = True
        days_left = 30 - (timezone.now() - request.user.date_joined).days
    else:
        free_user = False
    return template_response(request, 'landing/include/inc_user_status.html', locals())


@login_required
def inner_page(request, action=None):
    if action == 'password':
        return template_response(request, 'landing/account/change_password.html', locals())
    elif action == 'account':
        return template_response(request, 'landing/account/details.html', locals())
    elif action == 'payment':
        return template_response(request, 'landing/purchase/payment.html', locals())
    elif action == 'subscription':
        return template_response(request, 'landing/purchase/subscription.html', locals())
    elif action == 'faq':
        return template_response(request, 'landing/article/faq.html', locals())
    elif action == 'terms':
        return template_response(request, 'landing/article/term.html', locals())
    elif action == 'suggestion':
        return template_response(request, 'landing/suggestion.html', locals())
    else:
        pass


def content_page(request, cat1=None, cat2=None, page=None):
    result = get_contenttype_info(request, cat1, cat2, page, include_post=True)
    template = 'landing/article/content_page.html'
    if result['cat']:
        cat = result['cat']
        if cat.template:
            template = result['cat'].template

        if cat.meta_title:
            result['title'] = cat.meta_title

    return template_response(request, template, result)


def post_page(request, cat1=None, cat2=None, page=None, template=None):
    result = get_contenttype_info(request, cat1, cat2, page, include_post=True, order='pos')
    return template_response(request, 'landing/{page}.html'.format(page=template), result)


def post(request, id=None, cat1=None, cat2=None):
    post = Post.objects.get(id=id)

    post.view_count += 1
    post.save()

    if post and post.type:
        category = post.type.all()
    q1 = Post.objects.filter(type__in=category, id__gt=id, lang__in=get_lang_array())
    q2 = Post.objects.filter(type__in=category, id__lt=id, lang__in=get_lang_array())
    if q1.exists():
        next_post = q1.first()
    if q2.exists():
        previous_post = q2.first()
    # Set the custome template
    template = post.template if post.template else 'landing/article/post1.html'
    # get next 5 posts
    next_posts = Post.objects.filter(last_modified_time__gt=post.last_modified_time, lang__in=get_lang_array())[:5]
    return template_response(request, template, locals())


def posts(request, cat1=None, cat2=None, page=None):
    result = get_contenttype_info(request, cat1, cat2, page, include_post=True)
    if 'template' in result and result['template'] != '':
        template = result['template']
    else:
        template = 'landing/article/content_page.html'
    return template_response(request, template, result)


def get_contenttype_info(request, cat1, cat2, page, include_post=False, order='-last_modified_time'):
    cm = ContentManager(request)
    site = cm.get_site_by_domain()
    if cat2:
        cat = cm.get_content_type_by_key(key=cat2, site=site)
    else:
        cat = cm.get_content_type_by_key(key=cat1, site=site)
    c = cat
    if cat:
        title = cat.title
        template = cat.template
        sub_categories = cm.get_content_types(parent=cat)
        recent_posts = cm.get_posts(content_type=cat, order_by='-last_modified_time', count=5)
        if include_post:
            result = get_pager(cm.get_posts(content_type=cat, order_by=order), page)
            result.update(locals())
            result.__delitem__('result')
            return result
        else:
            return locals()
    else:
        recent_posts = cm.get_posts(content_type=cat, order_by='-last_modified_time', count=5)
        if include_post:
            result = get_pager(cm.get_posts(content_type=cat, order_by='-last_modified_time'), page)
            result.update(locals())
            result.__delitem__('result')
            return result
        else:
            return locals()


def static_file(request, page):
    return template_response(request, page, locals())


def sitemap(request):
    urls = []
    # Extract urls from all apps
    for app in settings.INSTALLED_APPS:
        app_module = get_module("{app}.apps".format(app=app))
        if app_module and "register_sitemap" in dir(app_module):
            urls = app_module.register_sitemap(request, urls)

    # Load external sitemaps
    if hasattr(settings, 'SITEMAP'):
        add_sitemap(urls, settings.SITEMAPS)
    return template_response(request, 'landing/sitemap.xml', locals())


def change_language(request):
    if request.method == 'GET' and 'lang' in request.GET:
        lang = request.GET.get('lang')
        set_lang(lang)
        request.session[translation.LANGUAGE_SESSION_KEY] = lang
        response = HttpResponseRedirect(reverse('home'))
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang)
        return response
    return redirect(reverse('home'))


@csrf_exempt
def multiuploader_delete(request, pk):
    """
    View for deleting photos with multiuploader AJAX plugin.
    made from trademe_api on:
    https://github.com/blueimp/jQuery-File-Upload
    """
    if request.method == 'POST':
        image = get_object_or_404(Image, pk=pk)
        image.delete()
        return HttpResponse(str(pk))
    else:
        return HttpResponseBadRequest('Only POST accepted')


def install(request):
    return template_response(request, 'install/index.html')


def test(request):
    return HttpResponse(settings.LOCALE_PATHS)


@csrf_exempt
def multiuploader(request):
    if request.method == 'POST':
        if request.FILES == None:
            return HttpResponseBadRequest('Must have files attached!')

        result = []

        if len(request.FILES) == 0:
            return result

        # getting file data for farther manipulations
        if 'files[]' in request.FILES:
            file = request.FILES['files[]']
        elif 'file' in request.FILES:
            file = request.FILES['file']
        else:
            file = request.FILES[0]

        wrapped_file = UploadedFile(file)
        file_size = wrapped_file.file.size
        filename = wrapped_file.name
        filename_noext, file_ext = os.path.splitext(filename)
        is_image = False
        if is_image_file(filename):
            is_image = True
            file_directory_url = 'upload/images/'
        else:
            file_directory_url = 'upload/'
        folder = get_req_value(request, "folder")
        if folder:
            # Add / for folder
            if not folder.endswith("/"):
                folder += "/"
            # Check wheathrer
            if folder.startswith("/"):
                file_directory_url = folder
            else:
                file_directory_url += folder

        fileurl = os.path.join(file_directory_url, str(filename))
        filepath = os.path.join(settings.MEDIA_ROOT, fileurl)

        if not os.path.exists(os.path.dirname(filepath)):
            os.makedirs(os.path.dirname(filepath))

        with open(filepath, 'wb+') as destination:
            for chunk in wrapped_file.chunks():
                destination.write(chunk)

        # getting file url here
        file_url = settings.MEDIA_PATH + fileurl

        # getting url for photo deletion
        file_delete_url = 'delete/'

        if is_image:
            thumb_url = os.path.join(file_directory_url, filename_noext + '_thumb' + file_ext)
            thumb_path = os.path.join(settings.MEDIA_ROOT, thumb_url)

            img = PLImage.open(filepath)
            width, height = img.size
            img.thumbnail((200, 200), PLImage.ANTIALIAS)
            img.save(thumb_path)
            file = Image()
            file.title = str(filename)
            file.url = fileurl
            file.thumb = thumb_url
            file.width = width
            file.height = height
            file.size = file_size

            # getting thumbnail url using sorl-thumbnail
            # thumb_url = file_url
        else:
            thumb_url = None
            file = File()
            file.title = str(filename)
            file.size = file_size
            file.path = file_size
            file.url = fileurl
        file.content_type = wrapped_file.content_type
        file.uploader = request.user
        file.save()
        # Add file to object property or properties.

        # generating json response array

        result.append({"name": filename,
                       "size": file_size,
                       "url": file_url,
                       "thumbnailUrl": thumb_url,
                       "pk": file.pk,
                       "creator": file.uploader.get_full_name() if file.uploader else 'anonymous',
                       "create_time": file.create_time.strftime("%Y-%m-%d %H:%M:%S"),
                       "is_valid": True,
                       "deleteUrl": file_delete_url + str(file.pk) + '/',
                       "deleteType": "POST", })

        return JsonResponse({'files': result}, safe=False)
    else:  # GET
        return template_response(request, 'multiuploader_main.html',
                                 {'static_url': settings.MEDIA_PATH,
                                  'open_tv': u'{{',
                                  'close_tv': u'}}'}, )
