from landing.models import *
from landing.core.common import get_random_string
class UserManager(object):

    def __init__(self,request = None):
        self.request = request

    def create_user(self,useraccount,password,email=None,is_staff=False,is_admin=False,extra=None):
        pass

    def get_profile(self,user = None,create=False):
        if not user and self.request.user.is_authenticated:
            user = self.request.user
        q = Profile.objects.filter(user=user)
        if q.exists():
            return q.first()
        else:
            if create:
                return Profile.objects.create(user=user,activiate_key=get_random_string())
            else:
                return None