from landing.core.common import get_action_result,get_html_from_template
from landing.models import Message
from django.db.models import Q
from django.core.exceptions import *
from django.contrib.auth import models as AdminModels
from django.db.models.aggregates import Sum,Count


class MessageManager:

    def __init__(self,request=None):
        self.request = request

    def __get_reader(self,reader):
        if isinstance(reader,int):
            try:
                return AdminModels.User.objects.get(id=reader)
            except:
                return None
        elif isinstance(reader,AdminModels.User):
            return reader
        else:
            return None

    def add_message(self, title, content, reader=None, sender=None, is_system=False, priority=3, page=None, ip=None,
                    name=None, email=None):
        message = Message(title=title)
        message.reader = self.__get_reader(reader)
        message.sender = sender
        message.name = name
        message.email = email
        message.content = content
        message.ip = ip
        message.page = page
        message.is_system = is_system
        message.save()
        return message

    def add_message_by_request(self,reader=None, request=None):
        if not request:
            request = self.request
        if request and 'name' in request.POST and 'email' in request.POST and 'message_content' in request.POST:
            message = Message(title='User Feedback ')
            message.reader = self.__get_reader(reader)
            message.sender = request.user
            message.name = request.POST.get('name')
            message.email = request.POST.get('email')
            message.content = request.POST.get('message_content')
            message.ip = request.META['REMOTE_ADDR']
            message.page = request.META['HTTP_REFERER']
            message.save()
            return message
        else:
            return None

    def add_template_message(self, title, template, reader=None, context=None, sender=None, is_system=False, priority=3, page=None, ip=None,
                    name=None, email=None):
        content = get_html_from_template(template=template,context=context)
        return self.add_message(title,content,reader,sender,is_system,priority,page,ip,name,email)


    def get_messages(self,count=10,request=None,empty_user=False,stat=False):
        if not request and self.request:
            request = self.request
        else:
            raise ValueError('Request object needed.')

        q = Q(reader=request.user)
        if empty_user:
            q = q | Q(reader=None)
        if stat:
            result = Message.objects.filter(q).aggregate(count=Count("id"))
            return Message.objects.filter(q).order_by('-create_time')[:count],result
        else:
            return Message.objects.filter(q).order_by('-create_time')[:count]

    def get_message(self,id):
        q = (Q(reader=self.request.user) | Q(reader=None)) & Q(id = id)
        result = Message.objects.filter(q)
        if result.exists():
            return result.first()
        else:
            raise ObjectDoesNotExist()

    def delete(self,user=None, ids=[]):
        if not user:
            user = self.request.user
        result = Message.objects.filter(reader=user,id__in=ids)
        if result.exists():
            return result.delete()
        else:
            return 0,[]

    def clean(self,user=None):
        if not user:
            user = self.request.user
        result = Message.objects.filter(reader=user)
        if result.exists():
            return result.delete()
        else:
            return 0,[]
