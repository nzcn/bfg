from landing.core.common import get_action_result
from landing.models import *
from landing.core.common import *
from django.db.models import Q

class ContentManager:

    def __init__(self,request):
        self.request = request


    def get_sites(self):
        return Site.objects.all()

    def get_site_by_id(self,id):
        try:
            return Site.objects.get(pk=id)
        except Exception as ex:
            return None

    def get_site_cache_key(self):
        return CONST_LANDING_SITE_VARS + self.get_domain() + translation.get_language()

    def get_site_by_domain(self,domain=None):
        if not domain and self.request:
            domain = self.get_domain()

        lang = translation.get_language()
        if domain:
            result = Site.objects.filter(home_page=domain,lang = lang)
            if result.exists():
                return result.first()
            else:
                result = Site.objects.filter(home_page=domain)
                if result.exists():
                    return result.first()

        # The default site should be the one with 0.0.0.0

        q = Site.objects.filter(home_page="0.0.0.0",lang=lang)
        if q.exists():
            return q.first()
        else:
            q = Site.objects.filter(home_page="0.0.0.0")
            if q.exists():
                return q.first()
            else:
                # Or if there is only one site available then select it as the default site
                if Site.objects.count() > 0:
                    return Site.objects.first()
                else:
                    return None

    def get_domain(self):
        if 'HTTP_ORIGIN' in self.request.META:
            host = self.request.META['HTTP_ORIGIN']
        elif 'HTTP_HOST' in self.request.META:
            host = self.request.META['HTTP_HOST']
        else:
            host = ''
        domain = host.lower()
        if domain.startswith("www."):
            domain = domain.replace("www.","")
        return domain

    def get_posts(self,site=None, content_type=None, order_by='-create_time',count=20,start=0):
        q = Q(enable=True)
        if site:
            q = q & Q(site=site)
        if content_type:
            q = q & Q(type__in = [content_type])

        result = Post.objects.filter(q).order_by(order_by)
        if count > 0:
            return result[start:count]
        else:
            return result

    def get_content_type_by_key(self, key, site=None):
        q = Q(key = key) & get_lang_filter()
        if site:
            q = q & Q(site=site)
        result = ContentType.objects.filter(q).order_by('pos')
        if result.exists():
            return result.first()
        else:
            return None

    def get_post(self,site=None,slug=None):
        q = Q(enable=True)
        if site:
            q = q & Q(site=site)
        if slug:
            q = q & Q(url_slug=slug)
        if ContentType.objects.filter(q).exists():
            return ContentType.objects.filter(q).first()
        else:
            return None


    def get_content_types(self,site=None,parent=None):
        q = Q(enable=True)
        if site:
            q = q & Q(site=site)
        if parent:
            q = q & Q(parent=parent)
        return ContentType.objects.filter(q).order_by('pos')

    def get_all_content_types(self,site=None,order_by='pos'):
        q = get_lang_filter()
        if site:
            q = q & (Q(site=site))
        result = ContentType.objects.filter(q).order_by(order_by)
        if len(result) == 0:
            if site:
                q = Q(site=site)
                result = ContentType.objects.filter(q).order_by(order_by)
        return result

    def get_or_sign_up_account(self, request=None):
        '''
        Get current account or sign up new account with custoized AccountType
        :param request:
        :return:
        '''
        pass

    def get_address(self, request=None):
        '''
        Get Address Object from Request
        :param request:
        :return:
        '''
        pass

    def get_tags(self,group = None):
        if group:
            return Tag.objects.filter(group = group).order_by('pos')
        else:
            return Tag.objects.all().order_by('group,pos')

    def get_tag_by_key(self,key):
        try:
            return Tag.objects.get(key=key)
        except:
            return None




