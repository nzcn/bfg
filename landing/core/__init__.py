import sys
from django.conf import settings

def get_module(name):
    try:
        if not name in sys.modules:
            __import__(name)
        module = sys.modules[name]
        return module
    except:
        return None

def init_modules():
    for app in settings.INSTALLED_APPS:
        app_module = get_module("{app}.apps".format(app=app))
        if app_module and "init" in dir(app_module):
            app_module.init()

settings_env = {'prod': 'bfg_template.prod',
                'test': 'bfg_template.test',
                'dev': 'bfg_template.settings'}