import datetime
import json
import rest_framework.pagination
import django_filters.rest_framework
from django.contrib.auth import (
    login as auth_login, authenticate,
)
from django.http import Http404
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import routers, serializers, viewsets, permissions, generics, mixins, views
from django.utils import translation
from django.db.models import Q
from django.conf.urls import url, include
from django.http import HttpResponse, JsonResponse


class RULModelViewSet(mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.ListModelMixin,
                      viewsets.GenericViewSet):
    """
    A viewset that provides default `retrieve()`, `update()`,
    `partial_update()` and `list()` actions.
    """
    pass


CODE_SUCCESS = 0
CODE_AUTHERROR = 1
CODE_DATANOTFOUND = 2
CODE_PARAMNOTFOUND = 3
CODE_DATAINVALID = 4
CODE_UNKNOWN = 100


def api_result(message, code=0, obj=None, exception=None, cookies=None, headers=None):
    result = {"message": message, "code": code}
    if exception:
        if isinstance(exception, Exception):
            result.update({"error": str(exception)})
        elif isinstance(exception, list):
            result.update({"error": exception})
    if obj:
        result.update({"object": obj})
    resp = JsonResponse(result, safe=False)
    if cookies:
        for k in cookies:
            resp.set_cookie(key=k, value=cookies[k])
    if headers:
        for k in headers:
            resp[k] = headers[k]
    return resp
