from html2text import html2text

class MailProviderBase(object):
    def send(self,title,body,reply_to=None, receivers=None,sender = None,
                cc=None,
                bcc=None,
                html_body=None,
                attachments = [],
                mail_account=None):
        pass

class FakeMailProvider(MailProviderBase):
    def send(self,title,body,reply_to = None, receivers=None,sender = None,
                 cc=None,
                 bcc=None,
                 html_body=None,
                 attachments = [],
                 mail_account=None):
        print("Pretend to send email '{t}' ".format(t=title))

class SmtpMailProvider(MailProviderBase):
    def send(self,title, body, reply_to=None, receivers=None, sender=None,
             cc=None,
             bcc=None,
             html_body=None,
             attachments=[],
             mail_account=None):
        from landing.core.common import send_email_background
        if mail_account:
            send_email_background(subject=title,
                                  body=body,
                                  reply_to=reply_to,
                                  receivers=receivers,
                                  sender=sender,
                                  attachments=attachments,
                                  server=mail_account['server'],
                                  port=mail_account['port'],
                                  user=mail_account['user'],
                                  password=mail_account['password'],
                                  ssl=mail_account['ssl'])
        else:
            send_email_background(subject=title,
                                  body=body,
                                  reply_to=reply_to,
                                  receivers=receivers,
                                  sender=sender,
                                  attachments=attachments)

class MailGunMailProvider(MailProviderBase):
    def send(self,title, body, reply_to=None, receivers=None, sender=None,
             cc=None,
             bcc=None,
             html_body=None,
             attachments=[],
             mail_account=None):
        import requests
        from landing.core.common import get_value
        data = {"from": sender,
                  "to": receivers,
                  "subject": title,
                  "text":  html2text(body),
                  "html": body,
                }
        if html_body:
            data.update({"html":html_body})
        if cc:
            data.update({"cc":cc})
        if bcc:
            data.update({"bcc":bcc})

        if len(attachments) > 0:
            files = []
            for attachment in attachments:
                file_name = attachment.split('/')[-1]
                files.append(("attachment", (file_name, open(attachment,"rb").read())));
            return requests.post(
                get_value(mail_account,'url'),
                auth=("api", get_value(mail_account,'key')),
                data=data,
                files=files
            )
        else:
            return requests.post(
                get_value(mail_account, 'url'),
                auth=("api", get_value(mail_account, 'key')),
                data=data)
