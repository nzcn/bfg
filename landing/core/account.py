from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.utils.translation import gettext as _
from landing.core.common import *
from django.shortcuts import render,redirect,reverse
from landing.models import *
from datetime import *

from store.models import Channel

class Account:
    def __init__(self,request=None,default_model=None):
        if default_model:
            self.default_model = default_model
        else:
            if 'store' in settings.INSTALLED_APPS:
                default_model = "store.models.Customer"
            else:
                default_model = 'django.contrib.auth.models.User'
            self.default_model = get_bfg_settings("customer_model", default_model)
        self.request = request

    def resetpwd_mail(self, email):
        customer_cls = get_class_by_path(self.default_model)
        if self.request.user.is_authenticated:
            customer = customer_cls.objects.get(id = self.request.user.id)
        else:
            customers = customer_cls.objects.filter(email=email)
            if customers.exists():
                customer = customers.first()
            else:
                raise Exception('Customer not found.')
        p = generate_profile(customer, need_active=True)
        self.send_resetpwd_email(customer, lang=p.lang)
        return True


    def register_user(self,username,email,password,first_name=None,last_name=None,tel=None, lang='en'):
        # Get class from settings
        customer_cls = get_class_by_path(self.default_model)
        if customer_cls:

            customer = customer_cls.objects.create_user(username=username,
                                                        email=email,
                                                        password=password)

            if first_name:
                customer.first_name = first_name

            if last_name:
                customer.last_name = last_name

            if tel:
                customer.tel = tel

            # Create profile
            p = generate_profile(customer, lang=lang)
            if hasattr(customer,'settings'):
                customer.settings = p
                customer.save()

            # Execute code after create customer
            if 'create_request' in dir(customer):
                try:
                    customer.create_request(self.request)
                except Exception as ex:
                    print(ex.message)

            if AdminModels.Group.objects.filter(name='Free').exists():
                freeGroup = AdminModels.Group.objects.get(name='Free')
            else:
                freeGroup = AdminModels.Group(name='Free')
                freeGroup.save()
            # Test Comment
            customer.groups.add(freeGroup)

            return customer
        else:
            return None

    def auth(self,username,password):
        user = authenticate(username=username,password=password)
        if user is not None:
            # the password verified for the user
            if user.is_active:
                pass
            else:
                raise ValueError("The password is valid, but the account has been disabled!")
        else:
            # the authentication system was unable to verify the username and password
            raise  ValueError("The username and password were incorrect.")
        return user


    def send_verify_email(self,customer,request=None):
        customer_cls = get_class_by_path(self.default_model)
        if not customer:
            raise Exception('Customer is None')

        if  not isinstance(customer, customer_cls):
            raise Exception('Customer type not right')

        if not request and self.request:
            request = self.request

        if request and 'HTTP_ORIGIN' in request.META:
            server = request.META['HTTP_ORIGIN']
        else:
            server = get_bfg_settings("server")

        lang = customer.settings.lang if customer.settings else 'en'
        profile = customer.settings
        context = {
            'server': server,
            'customer': customer,
            'profile': profile,
            'company': customer.company,
            'url':'{server}{url}'.format(server=server,url=reverse('active_account',kwargs={'id':profile.activiate_key}))
        }
        email = profile.user.email
        title = translate_to("Your account - Please verify your email", lang)
        send_email_from_template(title,context,template=get_local_template_file("backend/mail/user_not_activitated.html", lang),receivers=[email],lang=lang,)

    def verify_code(self, activiate_code):
        q = Profile.objects.filter(activiate_key=activiate_code)
        if q.exists():
            return True
        else:
            # raise ValueError('Invalid activiated code')
            return False

    def verify_profile(self,activiate_code):
        q = Profile.objects.filter(activiate_key=activiate_code)
        if q.exists():
            profile = q.first()
            profile.activiated = True
            profile.activiated_date = timezone.localtime()
            profile.save()

            self.after_verify_success(profile)

            return True
        else:
            raise ValueError('Invalid activiated code')

    def after_verify_success(self,profile):
        # Trigger After Active Procedure
        try:
            customer_cls = get_class_by_path(self.default_model)
            customer = customer_cls.objects.get(id=profile.user.id)
            # Execute code after create customer
            if 'after_verify' in dir(customer):
                customer.after_verify(self.request)
        except Exception as ex:
            print(ex)

    def check_username(self,username):
        return User.objects.filter(username=username).exists()

    def get_user_profile(self, user):
        if user and isinstance(user, AdminModels.User):
            return user.profile
        else:
            return None

    def request_user_operation(self, user, type='F'):
        dtEnd = timezone.now()
        dtStart = dtEnd - timedelta(minutes=10)
        # Check Same Account request to prevent attact.
        if OperationRequest.objects.filter(user_account=user.username, type=type, created_time__in=[dtStart, dtEnd]).count() > 10:
            raise Exception('Too many requests.')
        key = get_random_string(32)
        while(OperationRequest.objects.filter(key=key).exists()):
            key = get_random_string(32)
        op = OperationRequest(user_account=user.username, type=type, key=key)
        op.save()
        return op

    def get_user_operation(self, code):
        try:
            return OperationRequest.objects.get(key=code)
        except:
            return None

    def check_user_operation_expire(self, op):
        dtStart = timezone.now() - timedelta(days=1)
        return op.created_time > dtStart

    def clear_user_operation(self, op):
        return OperationRequest.objects.filter(user_account=op.user_account, type=op.type).delete()


    def send_resetpwd_email(self, user, lang=None, mail_account=None):
        import base64
        from django.utils.encoding import force_bytes
        from django.contrib.sites.shortcuts import get_current_site
        from django.contrib.auth.tokens import PasswordResetTokenGenerator
        def urlsafe_base64_encode(s):
            return base64.urlsafe_b64encode(s).rstrip(b'\n=')

        token_generator = PasswordResetTokenGenerator()
        if not lang:
            if self.request:
                lang = self.request.LANGUAGE_CODE
            elif user:
                lang = user.profile.lang or (user.profile and user.profile.lang)
            else:
                lang = settings.DEFAULT_LANGUAGE
        request = self.request
        site_name = ''
        domain = ''
        if request:
            if 'HTTP_ORIGIN' in request.META:
                server = request.META['HTTP_ORIGIN']
            else:
                server = get_bfg_settings("server")
            try:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            except Exception as ex:
                print(ex.__str__())
        user_email = user.email
        title = translate_to("Forgot password", lang)
        token = token_generator.make_token(user)
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        use_https = False
        context = {
            'email': user_email,
            'domain': domain,
            'site_name': site_name,
            'uid': uid,
            'user': user,
            'token': token,
            'server': server,
            'protocol': 'https' if use_https else 'http',
        }
        send_email_from_template(title, context, template=get_local_template_file("backend/mail/resetpwd.html", lang),
                                 receivers=[user_email], lang=lang, mail_account=mail_account)
