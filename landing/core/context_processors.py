from landing.models import *
from landing.core.common import *
from landing.controller.contents import ContentManager
from django.core.cache import cache
from django.utils import translation
from django.conf import settings
from django.template.loader import get_template



def landing_var(request):
    cm = ContentManager(request)
    key = cm.get_site_cache_key()
    content = cache.get(key,None)
    # print("get {key} in cache, {content}".format(key=key,content=content))
    if not content:
        content = get_content(cm)
        cache.set(key,content)
    return {'landing':content}


def get_content(cm):
    site = cm.get_site_by_domain()
    content_types = cm.get_all_content_types(site=site)
    # Convert array to a tree
    # ct - all content types
    ct = {}
    # cta - All enabled first level content types. Will be used in
    cta = []
    for content_type in content_types:
        if not content_type.key in ct.keys():
            ct[content_type.key] = content_type
            # Add first level menu items
            if (not content_type.parent) and content_type.enable:
                cta.append(content_type)

        # Add sub menu item
        if content_type.parent and content_type.enable:
            parent_node = None
            for cat in content_types:
                if cat.id == content_type.parent.id:
                    parent_node = cat
                    break
            if parent_node:
                parent_node.children.append(content_type)
    languages = []
    available_languages = list(settings.LANGUAGES)
    current_language = translation.get_language_info(translation.get_language())
    for language in available_languages:
        info = translation.get_language_info(language[0])
        if info['code'] != current_language['code']:
            languages.append(info)
    try:
        version = settings.VERSION
    except:
        version = None
    if hasattr(settings, "BFG"):
        bfg_settings.update(settings.BFG)
    # Load Left menus
    if hasattr(bfg_settings, "account_menus"):
        menus = bfg_settings.account_menus
    else:
        menus = []
        for app in settings.INSTALLED_APPS:
            if app == "landing":
                continue
            menu = "{app}/account/inc_menu.html".format(app=app)
            try:
                get_teplate(menu, None)
                menus.append(menu)
            except:
                pass
    content = {'site': site,
               'ct': ct,
               'cta': cta,
               'languages': languages,
               'language': current_language,
               'version': version,
               'menus': menus,
               'settings': bfg_settings}
    if hasattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_SECRET'):
        content.__setitem__('google_plus_login', True)
    if hasattr(settings, 'SOCIAL_AUTH_WEIXIN_KEY'):
        content.__setitem__('weixin_login', True)
    if hasattr(settings, 'SOCIAL_AUTH_FACEBOOK_KEY'):
        content.__setitem__('facebook_login', True)
    return content