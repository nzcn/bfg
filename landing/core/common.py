from __future__ import division
import math
from datetime import *
from landing.models import *
from django.template import Template, loader, Context
from django.core.cache import cache
from django.shortcuts import render, redirect, reverse
from django.conf import settings
from lxml import etree
import requests
import django.core.mail as mail
from django.utils.crypto import get_random_string
from django.core.mail import EmailMessage
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
from django.utils import translation
from django.utils.translation import ugettext
from django.db.models import Q
from landing.core import *
from landing.tasks import send_background_email
from django.template.response import TemplateResponse
from django.http import HttpResponse, JsonResponse
from django.core.cache import cache
import re
import json
import logging
import calendar
import os
import sys
from django.apps import apps
from rest_framework import routers
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.translation import gettext as _
from PIL import Image as PLImage
from django.views.decorators.cache import cache_page

try:
    from django.db.transaction import atomic
except ImportError:
    def atomic(func):
        return func

CONST_LANDING_SITE_VARS = 'LANDING_SITE_VARS'

logger = logging.getLogger("error")
bfg_settings = \
    {
        'hide_mobile_account_header': True,
        'hide_my_header': True,
        'hide_my_footer': True,
        'navbar_theme': 'light',
        'hide_top_bar': False,
        'lang': 'en-us',
        'hide_bottom_bar': False,
        'mail': {
            'provider': 'landing.core.mail.SmtpMailProvider',
            'settings': {}
        }
    }


def testing_mode():
    return sys.argv[1:2] == ['test']


def get_site(request=None):
    from landing.controller.contents import ContentManager
    cm = ContentManager(request)
    return cm.get_site_by_domain()


def get_value(obj, key, default_value=None):
    if obj:
        if isinstance(obj, dict) and key in obj:
            v = obj[key]
        elif hasattr(obj, key):
            v = getattr(obj, key)
        else:
            v = default_value
    else:
        v = default_value

    if isinstance(v, basestring) and v.isdigit():
        if "." in v:
            return float(v)
        else:
            return int(v)
    else:
        return v


def get_bfg_settings(key=None, default=None):
    if hasattr(settings, 'BFG'):
        bfg_settings.update(settings.BFG)
    if key:
        if key in bfg_settings:
            return bfg_settings[key]
        else:
            return default
    else:
        return bfg_settings


def get_paypal_url(site):
    if not site.paypal_site:
        return 'https://www.sandbox.paypal.com/cgi-bin/webscr'
    else:
        return 'https://www.paypal.com/cgi-bin/webscr'


def get_settings(site=None, request=None):
    if not site:
        site = get_site(request)
    if type(site) is Site:
        return json.loads(site.settings)
    else:
        return None


def get_pager(q, page, page_count=10, whole_page=False):
    if (not page) or page < 1 or page == '0':
        page = 1
    else:
        page = int(page)
    start = (page - 1) * page_count
    end = start + page_count
    show_page = 5
    total = int(math.ceil(q.count() / page_count))
    list = q[start:end]
    pagers = []
    pagers.append({'title': '<', 'href': 0 if page == 1 else page - 1})
    pagers.append({'title': '1', 'href': 1})
    if whole_page:
        for i in range(1, total):
            pagers.append({'title': str(i), 'href': i})
    else:
        if total > show_page:
            if page - show_page > 2:
                pagers.append({'title': '...', 'href': page - show_page - 2})
                pagers.append({'title': str(page - show_page - 1), 'href': page - show_page - 1})
                for i in range(page - show_page, page):
                    pagers.append({'title': str(i), 'href': i})
                if page < total:
                    pagers.append({'title': str(page), 'href': page})
            elif page > 1:
                for i in range(2, page):
                    pagers.append({'title': str(i), 'href': i})
                if page < total:
                    pagers.append({'title': str(page), 'href': page})

            if page + show_page < total - 1:
                for i in range(page + 1, page + show_page + 1):
                    pagers.append({'title': str(i), 'href': i})
                pagers.append({'title': '...', 'href': page + show_page + 1})
            else:
                for i in range(page + 1, total):
                    pagers.append({'title': str(i), 'href': i})
        else:
            for i in range(2, total):
                pagers.append({'title': str(i), 'href': i})
    if total > 1:
        pagers.append({'title': total, 'href': total})
    pagers.append({'title': '>', 'href': 0 if page >= total else page + 1})
    return locals()


def get_url(url, last_update=None, frequency='monthly', priority=0.5):
    if not last_update:
        last_update = timezone.now()
    return {'loc': url, 'lastmod': last_update, 'changefreq': frequency, 'priority': priority}


def if_exists(key, context):
    return key in context and context[key]


def get_teplate(template_name, context):
    '''
    Get Rendered String from a given template name.
    :param template_name:
    :param context:
    :return:
    '''
    template = loader.get_template(template_name)
    if template:
        return template.render(context)
    else:
        return None


def send_email(subject, body, reply=None, receivers=None, sender=None, attachments=[],
               mail_account=None, cc=None, bcc=None):
    from landing.core.mail import MailProviderBase

    mail_settings = get_bfg_settings("mail")
    provider_name = get_value(mail_settings, "provider", "landing.core.mail.SmtpMailProvider")
    if mail_account:
        setting = mail_account
    else:
        setting = get_value(mail_settings, "settings")
    cls = get_class_by_path(provider_name)
    if cls and issubclass(cls, MailProviderBase):
        class_obj = cls()
        if get_bfg_settings('test_mode', False):
            receivers = settings.ADMIN_EMAIL
        if not sender:
            sender = setting['sender']
        if not reply:
            reply = setting['sender']
        result = class_obj.send(title=subject, body=body, receivers=receivers, sender=sender,
                                attachments=attachments, mail_account=setting, reply_to=reply, cc=cc, bcc=bcc)
        return result
    else:
        raise Exception('Wrong mail provider')


def send_email_background(subject, body, reply_to=None, receivers=None, sender=None, attachments=[],
                          server=None, port=465, user=None, password=None, ssl=True):
    '''
    Send email at background
    It will check mail_record_id first. then using site configuration. if all failed it will use globle settings
    :param subject:
    :param body:
    :param reply_to:
    :param receivers:
    :param sender:
    :param attachments:
    :param mail_record_id:
    :return:
    '''

    if server:
        connection = mail.get_connection(host=server,
                                         port=port,
                                         username=user,
                                         password=password,
                                         use_ssl=ssl)
        if not sender:
            sender = user

        if not receivers:
            receivers = [user];
    else:
        site = get_site()
        if site.email_server and site.email_port and site.email_username and site.email_password:
            connection = mail.get_connection(host=site.email_server,
                                             port=site.email_port,
                                             username=site.email_username,
                                             password=site.email_password,
                                             use_ssl=site.email_use_ssl)
            if not sender:
                sender = site.email_sender
            if not receivers:
                receivers = [site.email_replyto];
        else:
            if settings.EMAIL_HOST and settings.EMAIL_PORT and settings.EMAIL_HOST_USER and settings.EMAIL_HOST_PASSWORD:
                connection = mail.get_connection(host=settings.EMAIL_HOST,
                                                 port=settings.EMAIL_PORT,
                                                 username=settings.EMAIL_HOST_USER,
                                                 password=settings.EMAIL_HOST_PASSWORD,
                                                 use_tls=settings.EMAIL_USE_TLS,
                                                 use_ssl=settings.EMAIL_USE_SSL)
                if not sender:
                    sender = settings.EMAIL_HOST_USER

                if not receivers:
                    receivers = [settings.EMAIL_HOST_USER];
            else:
                raise ValueError('No configration for sending email')

    if get_bfg_settings('test_mode', False):
        receivers = settings.ADMIN_EMAIL

    email = EmailMessage(subject, body,
                         reply_to=reply_to,
                         to=receivers,
                         from_email=sender,
                         connection=connection)
    for attatchment in attachments:
        email.attach_file(attatchment, 'application/pdf')
    email.send(fail_silently=True)


def send_email_from_template(subject, context, template, reply_to=None,
                             receivers=[], sender=None, attachments=[],
                             mail_account=None, lang=None, cc=None, bcc=None, background=False):
    '''
    Send email from template
    :param subject:
    :param context:
    :param template:
    :param receivers:
    :param sender:
    :return:
    '''
    # if lang and (not lang in template):
    #     lang_template = template.replace(".html", "_{lang}.html".format(lang=lang))
    #     templates = [lang_template, template]
    # else:
    #     templates = [template]
    subject = get_html(subject, context)
    body = get_html_from_template(template, context)
    if background:
        send_background_email(subject, body,
                              reply_to=reply_to, receivers=receivers, sender=sender, attachments=attachments,
                              mail_account=mail_account, cc=cc, bcc=bcc)
    else:
        send_email(subject, body,
                   reply_to, receivers, sender, attachments, mail_account=mail_account, cc=cc, bcc=bcc)


def get_variable(key, context=None, using_cache=True):
    '''
    Get variable and render
    :param key:
    :param context:
    :param using_cache:
    :return:
    '''
    try:
        if using_cache:
            var = cache.get(key, None)
            if not var:
                var = Variable.objects.get(key=key)
                cache.set(key, var)
        else:
            var = Variable.objects.get(key=key)
            if using_cache:
                cache.set(key, var)
        if context:
            return Template(var.content).render(context)
        else:
            return var.content
    except Exception as ex:
        print(ex.message)
        return None


def preprocess_html(content):
    regex_static = r"{%\sstatic\s"
    regex_i18n = r"{%\strans\s"
    if len(re.findall(regex_static, content, re.MULTILINE | re.IGNORECASE)) > 0:
        content = "{% load staticfiles %}\n" + content
    if len(re.findall(regex_i18n, content, re.MULTILINE | re.IGNORECASE)) > 0:
        content = "{% load i18n %}\n" + content
    return content


def get_html(content, context=None):
    try:
        if context:
            content = preprocess_html(content)
            if not isinstance(context, Context):
                context = Context(context)
            return Template(content).render(context)
        else:
            return content
    except Exception as ex:
        return ex.message


def get_html_from_template(template, context=None):
    from django.template.exceptions import TemplateDoesNotExist
    if isinstance(template, str) or isinstance(template, unicode):
        t = loader.get_template(template)
        return t.render(context)
    elif isinstance(template, list):
        for temp in template:
            try:
                t = loader.get_template(temp)
                return t.render(context)
            except:
                continue
        if len(template) > 0:
            raise TemplateDoesNotExist(template[0])
        else:
            raise Exception(_("Template not found"))
    elif isinstance(template, Template):
        return template.render(context)
    else:
        return None


def add_sitemap(urls, sitemap_urls):
    for sitemap_url in sitemap_urls:
        tree = etree.parse(sitemap_url)
        ns = 'http://www.sitemaps.org/schemas/sitemap/0.9'
        url_nodes = tree.getroot().xpath("//atom:url", namespaces={'atom': ns})
        for url in url_nodes:
            loc = url.xpath("atom:loc/text()", namespaces={'atom': ns})[0]
            lastmod = datetime.strptime(url.xpath("atom:lastmod/text()", namespaces={'atom': ns})[0],
                                        "%Y-%m-%dT%H:%M:%S+00:00")
            changefreq = url.xpath("atom:changefreq/text()", namespaces={'atom': ns})[0]
            priority = url.xpath("atom:priority/text()", namespaces={'atom': ns})[0]
            urls.append(get_url(loc, lastmod, changefreq, priority))
    return urls


def get_posts_by_category(key, sort='-last_modified_time'):
    if ContentType.objects.filter(key=key).exists():
        return Post.objects.filter(type=ContentType.objects.get(key=key)).order_by(sort)
    else:
        return None


def show_result(request, message, success, title=_('Operation Result'), icon_class='', return_url=None):
    return template_response(request, 'landing/operation_result.html', locals())


def action_result(result, message=None, ids=[], return_url=None, obj=None):
    return JsonResponse(get_action_result(
        result=result, message=message, ids=ids, return_url=return_url, obj=obj
    ), safe=False)


def get_action_result(result, message=None, ids=[], return_url=None, obj=None):
    result = {'code': 0 if result else 1,
              'message': message,
              'ids': ids,
              }
    if return_url:
        result.__setitem__("return_url", return_url)
    if obj:
        result.__setitem__("obj", obj)
    return result


def return_to(request, default_url):
    return_url = request.POST.get('return_url')
    if return_url:
        return redirect(return_url)
    else:
        return redirect(default_url)


def get_ids(request):
    if 'ids' in request.POST:
        return str(request.POST.get('ids', '')).split(';')
    elif 'id' in request.POST:
        return [request.POST.get('id', 0)]
    else:
        return []


def get_date_range(date=timezone.now(), interval=-30):
    if interval > 0:
        dtFrom = date
        dtTo = dtFrom + timedelta(days=interval)
    else:
        dtTo = date
        dtFrom = dtTo + timedelta(days=interval)
    return dtFrom, dtTo


def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + month / 12)
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month, day)


def add_days(duration_in, count, dt=None):
    if duration_in == 'months' or duration_in == "M":
        result = 30 * count
    elif duration_in == 'days' or duration_in == "D":
        result = count
    elif duration_in == 'years' or duration_in == "Y":
        result = 365 * count
    elif duration_in == 'weeks' or duration_in == "W":
        result = 7 * count
    else:
        result = count
    if dt:
        if isinstance(dt, datetime.date):
            dt = datetime.combine(date.today(), datetime.min.time())
        return dt + timedelta(days=result)
    else:
        return result


def push_slack(message, username=None, icon=None):
    settings = get_bfg_settings("slack")
    if settings and isinstance(settings, dict):
        if 'url' in settings:
            slack_url = settings['url']
        else:
            slack_url = None
        if not username:
            username = settings['user']
    else:
        slack_url = settings.SLACK_URL if hasattr('SLACK_URL', settings) else None
        if not username:
            username = settings.SLACK_USER if hasattr('SLACK_USER', settings) else None

    if not slack_url:
        logger.error('Slack not configured')
        return False
    if icon is None:
        icon = ":ghost:"

    if not username:
        username = "bfg"
    payload = {"text": message, "icon_emoji": icon, 'username': username}
    r = requests.post(slack_url, json=payload)
    if r.status_code == 200:
        print(r.content)
    else:
        logger.error('Send slack error. ' + r.text)
    return r.status_code == 200


def set_cookie(response, key, value, days_expire=7):
    if days_expire is None:
        max_age = 365 * 24 * 60 * 60  # one year
    else:
        max_age = days_expire * 24 * 60 * 60
    expires = datetime.strftime(timezone.now() + timedelta(seconds=max_age),
                                "%a, %d-%b-%Y %H:%M:%S GMT")
    response.set_cookie(key, value, max_age=max_age, expires=expires, domain=settings.SESSION_COOKIE_DOMAIN,
                        secure=settings.SESSION_COOKIE_SECURE or None)


def parse_dict(value):
    try:
        return json.loads(value)
    except:
        return {}


def dict_to_string(dict):
    return json.dumps(dict)


def dict_to_list(dict, key, distinct):
    dictlist = []
    for key, value in dict.iteritems():
        temp = [key, value]
        dictlist.append(temp)
    return dictlist


def getvalue(v, key):
    if key in v and v[key]:
        return v[key]
    else:
        return None


# ----- Messages Related ---------

def add_message(title, request=None, name=None, email=None, content=None, reader=None, priority=3, is_system=False):
    msg = Message()
    msg.title = title
    msg.name = name
    msg.email = email
    msg.content = content
    msg.is_system = is_system
    msg.priority = priority
    msg.reader = reader
    if request:
        if name is None and request.user.is_authenticated():
            msg.name = request.user.username
        if email is None and request.user.is_authenticated():
            msg.email = request.user.email
        msg.sender = request.user
        msg.ip = request.META['REMOTE_ADDR']
        msg.page = request.META['HTTP_REFERER']
    msg.save()
    return msg


def add_operation_log(action, content, request=None):
    user = request.user if request else "anonymous"
    title = "{user} {action}".format(user=user, action=action)
    return add_message(title, request=request, content=content, is_system=True)


def tuple2dict(t):
    return dict((y, x) for x, y in t)


def turple2dict(t):
    return tuple(t)


def turple2array(t, key='key', value='value'):
    result = []
    for item in t:
        if len(item) > 1:
            result.append({key: item[0], value: item[1]})
        else:
            result.append({key: item[0], value: item[0]})
    return result


def check_dir(filepath, makedir=True):
    dir_path = os.path.dirname(filepath)
    result = os.path.exists(dir_path)
    if (not result) and makedir:
        os.makedirs(dir_path)
    return result


def check_filename(filepath):
    result = os.path.exists(filepath)
    if result:
        filename = os.path.basename(filepath)
        filedir = os.path.dirname(filepath)
        filename_noext, file_ext = os.path.splitext(filename)
        new_filename = '{noext}_{random}{ext}'.format(noext=filename_noext, random=get_random_string(length=6),
                                                      ext=file_ext)
        while (os.path.exists(os.path.join(filedir, new_filename))):
            new_filename = '{noext}_{random}{ext}'.format(noext=filename_noext, random=get_random_string(length=6),
                                                          ext=file_ext)
        return os.path.join(filedir, new_filename)
    else:
        return filepath


def generate_profile(user, need_active=True, lang='zh_CN'):
    p, create = Profile.objects.get_or_create(user=user)
    p.lang = lang
    if need_active:
        p.expire_date = timezone.now() + timedelta(days=30)
        p.activiate_key = get_random_string(length=32)
    else:
        p.activiated = True
        p.activiated_date = timezone.now()
    p.save()
    return p


def deactive_profile(p):
    p.expire_date = timezone.now() + timedelta(days=30)
    p.activiate_key = get_random_string(length=32)
    p.save()
    return p


def get_module(name):
    try:
        if not name in sys.modules:
            __import__(name)
        module = sys.modules[name]
        return module
    except:
        return None


def get_class_by_name(module_name, class_name):
    module = get_module(module_name)
    if module is None:
        raise ValueError("Can not find module.")
    try:
        class_obj = getattr(module, class_name)
        return class_obj
    except:
        return None


def get_class_by_path(class_path):
    class_name = class_path.split('.')[-1]
    module_name = class_path.replace('.' + class_name, "")
    return get_class_by_name(module_name, class_name)


def execute_action(class_path, context):
    from landing.core.action import ActionBase
    cls = get_class_by_path(class_path)
    if cls and issubclass(cls, ActionBase):
        class_obj = cls()
        return class_obj.execute(context)
    else:
        raise Exception('Wrong action object')


def get_app_model(app_anme, class_name):
    return apps.get_model(app_label=app_anme, model_name=class_name)


def get_field(cls, name):
    try:
        field = cls._meta.get_field(name)
        return field
    except:
        return None


def get_profile(request):
    q = Profile.objects.filter(user=request.user)
    if q.exists():
        return q.first()
    else:
        return None


def is_in_request(request, key):
    result = False
    if request:
        if request.method == "POST" or request.method == 'DELETE':
            if key in request.POST:
                result = True
            elif hasattr(request, 'data') and key in request.data:
                result = True
        elif request.method == "GET":
            if key in request.GET:
                result = True
    return result


def get_req_value(request, key, default=None, trim=None, type=None):
    if request:
        if request.method == "POST" or request.method == 'PUT' or request.method == 'PATCH' or request.method == 'DELETE':
            if key in request.POST:
                result = request.POST.get(key)
            elif hasattr(request, 'data') and key in request.data:
                result = request.data[key]
            else:
                if key in request.GET:
                    result = request.GET.get(key, default)
                else:
                    result = default
        elif request.method == "GET":
            result = request.GET.get(key, default)
        else:
            result = default
    else:
        result = default
    if trim:
        result = result.strip(trim)
    if type:
        if result:
            try:
                if type == float:
                    result = float(result)
                elif type == bool:
                    if isinstance(result, bool):
                        return result
                    elif isinstance(result, str):
                        return result.lower().strip(" ") == "true"
                    else:
                        return result
                elif type == int:
                    result = int(result)
            except Exception as ex:
                result = default
        else:
            result = default
    if result:
        return result
    else:
        return default


def get_req_list(request, key, default=[], parseInt=False, ignore_blank=False):
    def str2json(value):
        try:
            result = json.loads(value)
        except:
            result = [value]
        if not isinstance(result, list):
            result = [result]
        return result

    result = default
    if '[]' in key:
        pure_key = key.rstrip('[]')
    else:
        pure_key = None
    if request.method == 'POST' or request.method == 'DELETE':
        if key in request.POST:
            result = request.POST.getlist(key)
            if len(result) == 1:
                result = str2json(result[0])
        elif pure_key and pure_key in request.POST:
            result = request.POST.getlist(pure_key)
            if len(result) == 1:
                result = str2json(result[0])

        elif hasattr(request, 'data'):
            if key in request.data:
                result = request.data[key]
            elif pure_key in request.data:
                result = request.data[pure_key]

    elif request.method == 'GET':
        if key in request.GET:
            result = request.GET.get(key)
            if isinstance(result, str):
                if '{' in result or '[' in result:
                    result = str2json(result)
                elif ',' in result:
                    result = result.split(',')

    if ignore_blank:
        new_result = []
        for item in result:
            if item:
                new_result.append(item)
        result = new_result

    if parseInt:
        pos = 0
        new_result = []
        for item in result:
            if isinstance(item, str):
                if '.' in item:
                    new_result.append(float(item))
                else:
                    if item.isdigit():
                        new_result.append(int(item))
            elif isinstance(item, int):
                new_result.append(item)
            elif isinstance(item, float):
                new_result.append(item)
            pos += 1
        return new_result
    else:
        return result


def get_filename_from_url(url):
    return url[url.rfind("/") + 1:]


def swap_filename(filename, new_filename):
    ext = filename[filename.rfind("."):]
    return new_filename + ext


def save_file_to_media(request, filename, path=None):
    if request.method == 'POST' and filename in request.FILES:
        myfile = request.FILES[filename]
        return save_fileobject_to_media(myfile, path)
    else:
        return None


def save_fileobject_to_media(file, path):
    if file:
        fs = FileSystemStorage()
        if path:
            check_dir(path)
            if not '.' in path.split('/')[-1]:
                if path.endswith('/'):
                    path += file.name
                else:
                    path += '/' + file.name
            filename = fs.save(path, file)
        else:
            filename = fs.save(file.name, file)
        return filename
    else:
        return None


def save_url_to_path(url, path):
    local_filename = url.split('/')[-1]
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return local_filename


def save_url_to_media(url, filename=None, path=None):
    file = requests.get(url, allow_redirects=True)
    if not filename:
        filename = url[url.rfind("/") + 1:]
    else:
        original_filename = url[url.rfind("/") + 1:]
        if "." in original_filename:
            # Only for normal fileanme with extension
            filename = swap_filename(original_filename, filename)
    if path:
        image_path = os.path.join(path, filename)
    else:
        image_path = filename
    filepath = os.path.join(settings.MEDIA_ROOT, image_path)
    check_dir(filepath)
    open(filepath, 'wb').write(file.content)
    return image_path


def save_image_path_to_image(image_path, thumb_size=None, pos=0):
    filepath = os.path.join(settings.MEDIA_ROOT, image_path)
    image = Image()
    img = PLImage.open(filepath)
    width, height = img.size
    image.size = os.stat(filepath).st_size
    image.width = width
    image.height = height
    image.title = image_path[image_path.rfind("/") + 1:]
    image.path = filepath
    image.url = image_path
    image.pos = pos
    filename_noext, file_ext = os.path.splitext(image.title)
    path = os.path.dirname(image_path)
    if thumb_size:
        thumb_url = os.path.join(path, filename_noext + '_thumb' + file_ext)
        thumbpath = os.path.join(settings.MEDIA_ROOT, thumb_url)
        img.thumbnail(thumb_size, PLImage.ANTIALIAS)
        img.save(thumbpath)
        image.thumb = thumb_url
    image.save()
    return image


def save_url_to_file(url, filename=None, path=None):
    file_path = save_url_to_media(url=url, filename=filename, path=path)
    filename = get_filename_from_url(file_path)
    file = File(title=filename)
    file.path = os.path.join(settings.MEDIA_ROOT, file_path)
    file.size = os.stat(file.path).st_size
    file.url = file_path
    file.save()
    return file


def save_url_to_image(url, filename=None, path=None, thumb_size=None, pos=0):
    image_path = save_url_to_media(url=url, filename=filename, path=path)
    return save_image_path_to_image(image_path, thumb_size, pos)


def save_base64_file(content, name, path=None, is_image=False, thumb_size=None, max_size=None, override=False):
    import base64
    from PIL import Image as PLImage
    if is_image:
        f = Image()
    else:
        f = File()
    parts = content.split(',')
    file_type_exception_description = 'Invalid file content. It must be start with data:image/png;base64,xxxx '
    if len(parts) != 2:
        raise Exception(file_type_exception_description)
    header = parts[0]
    file_type = header.split(';')
    if len(file_type) != 2:
        raise Exception(file_type_exception_description)
    file_encode = file_type[1]
    file_content_type = file_type[0].replace('data:', '')
    file_body = parts[1]
    fileurl = path + name
    filepath = os.path.join(settings.MEDIA_ROOT, fileurl)
    filepath = check_filename(filepath)
    name = os.path.basename(filepath)
    filename_noext, file_ext = os.path.splitext(name)
    check_dir(filepath)
    decodeit = open(filepath, 'wb')
    decodeit.write(base64.b64decode(file_body))
    decodeit.close()
    f.title = name
    f.size = os.path.getsize(filepath)
    f.path = filepath
    f.url = fileurl
    f.content_type = file_content_type
    if is_image:
        create_thumb(f, file_ext, filename_noext, filepath, max_size, path, thumb_size)
    f.save()
    return f


def save_post_file(file, name=None, path=None, is_image=False, thumb_size=None, max_size=None, override=False,
                   uploader=None):
    from PIL import Image as PLImage
    from django.core.files.uploadedfile import UploadedFile
    '''
    Save HttpPost file
    :param file:
    :param path:
    :param is_image:
    :param thumb_size:
    :return:
    '''
    if is_image:
        f = Image()
    else:
        f = File()
    uf = UploadedFile(file)
    f.size = uf.file.size
    if uploader:
        f.uploader = uploader
    if len(uf.name) > 60:
        f.title = uf.name[-50:]
    else:
        f.title = uf.name
    filename_noext, file_ext = os.path.splitext(f.title)
    if not path:
        path = ''

    if not name:
        name = u"{filename}{ext}".format(filename=filename_noext,
                                         ext=file_ext).strip()
    else:
        if (not "." in name) and file_ext:
            filename_noext = name
            name = filename_noext + file_ext

    fileurl = path + name
    filepath = os.path.join(settings.MEDIA_ROOT, fileurl)
    while os.path.exists(filepath):
        if override:
            os.remove(filepath)
        else:
            name = "{filename}_{random}{ext}".format(filename=filename_noext,
                                                     random=get_random_string(6),
                                                     ext=file_ext)
            fileurl = path + name
            filepath = os.path.join(settings.MEDIA_ROOT, fileurl)

    check_dir(filepath)

    with open(filepath, 'wb+') as destination:
        for chunk in uf.chunks():
            destination.write(chunk)

    f.url = fileurl
    f.path = filepath
    f.content_type = uf.file.content_type

    if is_image:
        create_thumb(f, file_ext, filename_noext, filepath, max_size, path, thumb_size)
    f.save()
    return f


def create_thumb(f, file_ext, filename_noext, filepath, max_size, path, thumb_size):
    img = PLImage.open(filepath)
    width, height = img.size
    if max_size:
        img.thumbnail(max_size, PLImage.ANTIALIAS)
        img.save(filepath)
        img = PLImage.open(filepath)
        width, height = img.size
    f.width = width
    f.height = height
    if thumb_size:
        thumb_url = path + filename_noext + '_thumb' + file_ext
        thumbpath = os.path.join(settings.MEDIA_ROOT, thumb_url)
        img.thumbnail(thumb_size, PLImage.ANTIALIAS)
        img.save(thumbpath)
        f.thumb = thumb_url


def save_string_to_file(content, filepath, close=True, append=False):
    check_dir(filepath)
    type = "w"
    if append:
        type = "w+"
    file = open(filepath, type)
    file.write(content)
    if close:
        file.close()
    return file


def read_string_from_file(filepath):
    with open(filepath, 'r') as file:
        content = file.read()
    return content


def get_lang():
    return translation.get_language()


def get_lang_filter():
    return Q(lang=get_lang()) | Q(lang=None)


def get_lang_array():
    return [get_lang()]


def set_lang(lang, change_Cache=True):
    translation.activate(lang)
    if change_Cache:
        cache.set(CONST_LANDING_SITE_VARS, None)


def translate_to(string, lang='en'):
    if lang:
        orig_lang = translation.get_language()
        translation.activate(lang)
        result = ugettext(string)
        translation.activate(orig_lang)
        return result
    else:
        return string


def get_api_router():
    router = routers.DefaultRouter()
    for app in settings.INSTALLED_APPS:
        api_module = get_module("{app}.api".format(app=app))
        if api_module and "register_api" in dir(api_module):
            api_module.register_api(router)
    return router.get_urls()


def message_user(request, message, level=messages.INFO, extra_tags='',
                 fail_silently=False, store=False):
    """
    Send a message to the user. The default implementation
    posts a message using the django.contrib.messages backend.

    Exposes almost the same API as messages.add_message(), but accepts the
    positional arguments in a different order to maintain backwards
    compatibility. For convenience, it accepts the `level` argument as
    a string rather than the usual level number.
    """
    if request:
        if not isinstance(level, int):
            # attempt to get the level if passed a string
            try:
                level = getattr(messages.constants, level.upper())
            except AttributeError:
                levels = messages.constants.DEFAULT_TAGS.values()
                levels_repr = ', '.join('`%s`' % l for l in levels)
                raise ValueError(
                    'Bad message level string: `%s`. Possible values are: %s'
                    % (level, levels_repr)
                )

        messages.add_message(request, level, message, extra_tags=extra_tags, fail_silently=fail_silently)


def template_response(request, template, context=None, content_type=None,
                      status=None, charset=None, using=None):
    '''
    Generate template response
    :param request:
    :param template:
    :param context:
    :param content_type:
    :param status:
    :param charset:
    :param using:
    :return:
    '''
    pages = [template]
    theme = request.session['theme'] if 'theme' in request.session else (
        settings.THEME if hasattr(settings, 'THEME') else None)
    if theme:
        if '/default/' in template:
            theme_page = template.replace("/default/", "/{theme}/".format(theme=theme))
            pages.insert(0, theme_page)
        pages.insert(0, "themes/{theme}/{page}".format(theme=theme, page=template))
        pages.insert(0, "{theme}/{page}".format(theme=theme, page=template))
    return TemplateResponse(request, pages, context, content_type, status, charset, using).render()


def list_to_tree(raw_nodes, parent_name, order_name=None, id_name='id', root_parent=0):
    def sort_item(item):
        if isinstance(item, dict) and 'children' in item:
            item['children'] = sorted(item['children'], key=lambda k: get_value(k, order_name), reverse=False)
            for child in item['children']:
                sort_item(child)
        elif hasattr(item, "children") and isinstance(item.children, list):
            item.children = sorted(item.children, key=lambda k: get_value(k, order_name), reverse=False)
            for child in item.children:
                sort_item(child)

    nodes = {}
    for i in raw_nodes:
        id, obj = (get_value(i, id_name), i)
        nodes[id] = obj
    forest = []
    for i in raw_nodes:
        id, parent_id, obj = (get_value(i, id_name), get_value(i, parent_name), i)
        node = nodes[id]
        if parent_id == root_parent:
            forest.append(node)
        else:
            parent = nodes[parent_id]
            if isinstance(parent, dict) and not 'children' in parent:
                parent['children'] = []
            elif not hasattr(parent, 'children'):
                parent.children = []

            if isinstance(parent, dict) and 'children' in parent:
                parent['children'].append(node)
            elif hasattr(parent, 'children') and isinstance(parent.children, list):
                parent.children.append(node)

    if order_name:
        # Sort forest
        forest = sorted(forest, key=lambda k: get_value(k, order_name), reverse=False)
        for i in forest:
            sort_item(i)

    return forest


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_request_path(request):
    if 'HTTP_ORIGIN' in request.META:
        return request.META['HTTP_ORIGIN']
    else:
        return ''


def match_regex(string, regex):
    pattern = re.compile(regex, flags=re.IGNORECASE)
    return not (pattern.match(string) is None)


def get_regex_group(string, regex):
    pattern = re.search(regex, string, re.IGNORECASE)
    if pattern:
        return pattern.group(1)
    else:
        return None


def replace_regex(string, regex, to_str):
    pattern = re.sub(regex, to_str, string, flags=re.IGNORECASE)
    return pattern


def sort_list(original_list, attr, order_list):
    if isinstance(original_list, list) and isinstance(order_list, list):
        result = []
        for id in order_list:
            for o in original_list:
                if hasattr(o, attr):
                    if getattr(o, attr) == id:
                        result.append(o)
                        break
        return result
    else:
        raise Exception("Wrong list type")


def get_local_template_file(filepath, language_code):
    from django.template.loader import get_template
    if (not language_code) or "en" in language_code:
        return filepath
    else:
        if language_code == 'zh-hans' or language_code == 'zh_TW' or language_code == 'zh_HK':
            language_code = 'zh_CN'
        temp_path = filepath.replace(".html", "-{code}.html".format(code=language_code))
        try:
            if get_template(temp_path):
                return temp_path
            else:
                return filepath
        except:
            logger.error('Template file {tmp} not found'.format(tmp=temp_path))
            return filepath


def list_to_dict(list, key):
    new_dict = {}
    for item in list:
        if hasattr(item, key):
            new_dict.update({getattr(item, key): item})
        elif isinstance(item, dict) and key in item.keys():
            new_dict.update({item[key]: item})
    return new_dict


def get_time_range(request, default_start_time=timezone.now() + timedelta(days=-30),
                   default_end_time=timezone.now(), date_format='%Y/%m/%d %H:%M:%S',
                   end_key='end_time', start_key='start_time'):
    end_time = get_req_value(request, end_key)
    start_time = get_req_value(request, start_key)
    return get_time_range_from_text(start_time, end_time,
                                    default_start_time=default_start_time,
                                    default_end_time=default_end_time,
                                    date_format=date_format,
                                    )


def get_time_range_from_single_text(param, default_start_time=timezone.now() + timedelta(days=-30),
                                    default_end_time=timezone.now(), date_format='%Y/%m/%d %H:%M:%S',
                                    end_key='end_time', start_key='start_time'):
    params = param.split('~')
    if len(params) == 2:
        end_time = params[1]
        start_time = params[0]
    else:
        end_time = ''
        start_time = params[0]
    return get_time_range_from_text(start_time, end_time,
                                    default_start_time=default_start_time,
                                    default_end_time=default_end_time,
                                    date_format=date_format,
                                    )


def get_time_range_from_text(start_time, end_time, default_start_time=timezone.now() + timedelta(days=-30),
                             default_end_time=timezone.now(), date_format='%Y/%m/%d %H:%M:%S'):
    if end_time and '_' in end_time:
        end_time = end_time.replace('_', ' ')
    if start_time and '_' in start_time:
        start_time = start_time.replace('_', ' ')
    if end_time and '-' in end_time:
        end_time = end_time.replace('-', '/')
    if start_time and '-' in start_time:
        start_time = start_time.replace('-', '/')
    if not end_time:
        end_time = default_end_time
    else:
        try:
            end_time = datetime.strptime(end_time, date_format)
        except:
            raise Exception('Invalid time expression')
    if not start_time:
        start_time = default_start_time
    else:
        try:
            start_time = datetime.strptime(start_time, date_format)
        except:
            raise Exception('Invalid time expression')
    return start_time, end_time


def get_google_map_key():
    return settings.BFG['google']['map_api_key']


def address_geo(address):
    try:
        results = requests.get(
            'https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={key}'.format(address=address,
                                                                                                   key=get_google_map_key())).json()
        return results
    except:
        return None


def address2geo(address):
    result = address_geo(address)
    geos = []
    if result and 'status' in result and result['status'] == 'OK':
        for data in result['results']:
            final = {}
            for item in data['address_components']:
                for category in item['types']:
                    data[category] = {}
                    data[category] = item['long_name']
            final['street'] = data.get("route", None)
            final['state'] = data.get("administrative_area_level_1", None)
            final['city'] = data.get("locality", None)
            final['county'] = data.get("administrative_area_level_2", None)
            final['country'] = data.get("country", None)
            final['postal_code'] = data.get("postal_code", None)
            final['neighborhood'] = data.get("neighborhood", None)
            final['sublocality'] = data.get("sublocality", None)
            final['housenumber'] = data.get("housenumber", None)
            final['postal_town'] = data.get("postal_town", None)
            final['subpremise'] = data.get("subpremise", None)
            final['latitude'] = data.get("geometry", {}).get("location", {}).get("lat", None)
            final['longitude'] = data.get("geometry", {}).get("location", {}).get("lng", None)
            final['code'] = data.get('place_id')
            final['location_type'] = data.get("geometry", {}).get("location_type", None)
            final['postal_code_suffix'] = data.get("postal_code_suffix", None)
            final['street_number'] = data.get('street_number', None)
            final['building'] = None
            geos.append(final)
    return geos


def geo2address(geo, address):
    street_number = geo['street_number']
    if geo['subpremise']:
        street_number = '{sub}/{num}'.format(sub=geo['subpremise'], num=street_number)
    address.address = '{number} {street}'.format(number=street_number, street=geo['street'])
    address.suburb = geo['sublocality']
    address.state = geo['state']
    address.city = geo['city']
    address.country = geo['country']
    address.postcode = geo['postal_code']
    address.building = geo['building']
    address.lat = geo['latitude']
    address.lng = geo['longitude']
    address.geocode = geo['code']


def address2point(address):
    geos = address2geo(address)
    if geos:
        return (geos[0]['latitude'], geos[0]['longitude'])
    else:
        return (0, 0)


def nearest_road(points):
    '''
    Find the nearest road of pointes
    :param points:
    :return:
    '''
    points_str = ""
    for point in points:
        points_str += "{lat},{lng}|".format(lat=point['lat'], lng=point['lng'])
    points_str = points_str.rstrip("|")
    results = requests.get(
        'https://roads.googleapis.com/v1/nearestRoads?points={address}&key={key}'.format(address=points_str,
                                                                                         key=get_google_map_key())).json()
    if 'snappedPoints' in results:
        index = 0
        for snappedPoint in results['snappedPoints']:
            points[snappedPoint['originalIndex']].update({'pos': index})
            index += 1
    return points


def isnull(value):
    if value:
        return value if value.lower() != 'null' else ''
    else:
        return False


def in_group(user, group_name):
    return user.groups.filter(name=group_name).exists()


def combine_filter(filter, q, logic_and=True):
    if not q or not isinstance(q, Q):
        return filter
    if filter:
        if logic_and:
            filter = filter & q
        else:
            filter = filter | q
    else:
        filter = q
    return filter


def clean_str(content):
    return content.replace('None', '').replace('null', '').replace('Null', '')


def round_up_digits(number, decimals=2):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.ceil(number)

    factor = 10 ** decimals
    return math.ceil(number * factor) / factor


def round_up_float(number, min_float=0.01):
    """
    Returns a value rounded up to a specific number of decimal places.
    """
    if not isinstance(min_float, float):
        raise TypeError("min_float places must be an float")
    elif min_float < 0:
        raise ValueError("min_float places has to be 0 or more")
    elif min_float == 0:
        return math.ceil(number)

    return math.ceil(number / min_float) * min_float


def is_image_file(filename):
    exts = ['png', 'jpeg', 'jpg', 'bmp']
    filename = filename.lower()
    for ext in exts:
        if filename.endswith(ext):
            return True
    return False


def create_excel(filename):
    import xlwt
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename={filename}'.format(
        filename=filename)
    wb = xlwt.Workbook(encoding='utf-8')
    return wb, response


def md5(string):
    import hashlib
    if string:
        md5 = hashlib.md5()
        md5.update(string.encode('utf-8'))
        return md5.hexdigest()
    else:
        return None
