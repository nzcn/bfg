# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2023-09-21 12:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0015_auto_20230730_2252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='changehistory',
            name='download_file',
            field=models.FileField(blank=True, default=None, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='contenttype',
            name='logo',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='contenttype',
            name='picture',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='language',
            name='logo',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='post',
            name='attachment',
            field=models.FileField(blank=True, default=None, max_length=255, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='post',
            name='logo',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='post',
            name='picture',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='profile',
            name='picture',
            field=models.ImageField(blank=True, default=None, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='tag',
            name='logo',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='tag',
            name='picture',
            field=models.ImageField(blank=True, default=None, max_length=255, null=True, upload_to=''),
        ),
    ]
