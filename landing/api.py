# noinspection PyPackageRequirements
from django.conf.urls import url, include
from landing.models import *
from rest_framework import routers, serializers, viewsets, permissions
from landing.controller.contents import ContentManager
from landing.core.common import *
from rest_framework.decorators import action
from rest_framework_jwt.settings import api_settings
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
from rest_framework import status
from django.db.models import Q
from django.utils.translation import gettext as _
from landing.core.api import *


def register_api(router):
    router.register(r'content_types', ContentTypeViewSet, base_name='contenttype')
    router.register(r'posts', PostViewSet, base_name='post')
    router.register(r'tags', TagViewSet, base_name="tag")
    router.register(r'profiles', ProfileViewSet, base_name="profile")
    router.register(r'users', UserViewSet, base_name="user")


# Serializers define the API representation.
class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = ('id', 'title', 'key', 'enable')


class MessageSerializer(serializers.ModelSerializer):
    type = ContentTypeSerializer(many=True, read_only=True)

    class Meta:
        model = Message
        fields = ('id', 'title', 'content', 'is_system', 'create_time', 'priority', 'sender', 'reader')


class PostSerializer(serializers.ModelSerializer):
    type = ContentTypeSerializer(many=True, read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'subtitle', 'picture', 'create_time', 'type', 'url_slug', 'description')


class PostDetailSerializer(serializers.ModelSerializer):
    type = ContentTypeSerializer(many=True, read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'subtitle', 'content', 'picture', 'url_slug',
                  'logo',
                  'icon',
                  'attachment',
                  'show_children',
                  'pos',
                  'template',
                  'create_time',
                  'view_count',
                  'star',
                  'last_modified_time',
                  'enable',
                  'meta_title',
                  'description',
                  'keywords',
                  'path',
                  'lang',
                  'type')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminModels.User
        fields = ("id", "username", "first_name", "last_name", "email", "is_active")


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = "__all__"


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    addresses = AddressSerializer(many=True)

    class Meta:
        model = Profile
        fields = "__all__"


class ProfileSerializerPatch(serializers.ModelSerializer):
    user = UserSerializer(many=False)

    class Meta:
        model = Profile
        exclude = ('credit', 'used_credit', 'feedback', 'experience', 'closed', 'activiated', 'activiated_date',
                   'confirmed_mobile', 'confirmed_email', 'activiate_key')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'title', 'key', 'group', 'description', 'content')


class SiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = "__all__"


class TestimonialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Testimonial
        fields = "__all__"


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"


class MailTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailTemplate
        fields = "__all__"


class FileSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = File
        fields = "__all__"


class ImageSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField('get_full_url')
    thumb = serializers.SerializerMethodField('get_full_thumb')

    def get_full_url(self, obj):
        return settings.MEDIA_PATH + obj.url

    def get_full_thumb(self, obj):
        return settings.MEDIA_PATH + obj.thumb

    class Meta:
        model = Image
        fields = "__all__"


class ImageAbstractSerializer(ImageSerializer):
    class Meta:
        model = Image
        fields = ("url", "thumb")


class MailSubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailSubscription
        fields = "__all__"


# ViewSets define the view behavior.
class PostViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        cm = ContentManager(self.request)
        site = cm.get_site_by_domain()
        q = Q(site=site) & Q(enable=True)
        if 'type' in self.request.GET:
            type_key = self.request.GET.get("type")
            if type_key:
                q = q & Q(type__key__exact=type_key)

        if 'url_slug' in self.request.GET:
            url_slug = self.request.GET.get("url_slug")
            if url_slug:
                q = q & Q(url_slug=url_slug)
        # return Post.objects.filter(type__key__exact='album')
        return Post.objects.filter(q).order_by('pos', '-create_time')

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return PostDetailSerializer
        else:
            return PostSerializer

    serializer_class = PostSerializer


class ContentTypeViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = ContentType.objects.all()
    serializer_class = ContentTypeSerializer


class TagViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        q = Tag.objects.all().order_by('group', 'pos')
        group = self.request.GET.get('group', None)
        if group:
            q = Tag.objects.filter(group=group).order_by('pos')
        key = self.request.GET.get('key', None)
        if key:
            q = Tag.objects.filter(key=key).order_by('pos')
        return q

    serializer_class = TagSerializer


class ProfileViewSet(RULModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ProfileSerializer

    def get_queryset(self):
        return Profile.objects.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.request.method == 'PATCH' or self.request.method == 'PUT':
            return ProfileSerializerPatch
        else:
            return self.serializer_class

    @action(methods=['patch'], detail=True)
    def change_password(self, request, **kwargs):
        if 'original_password' in self.request.POST and 'new_password' in self.request.POST:
            original_password = self.request.POST.get("original_password")
            new_password = self.request.POST.get("new_password")
            if request.user.check_password(original_password):
                request.user.set_password(new_password)
                request.user.save()
                return api_result(_("Change password successfully"), CODE_SUCCESS)
            else:
                return api_result(_("Wrong password"), CODE_AUTHERROR)
        else:
            return api_result(_("new_password and original_password are needed"), CODE_PARAMNOTFOUND)

    @action(methods=['put', 'patch', 'delete'], detail=True)
    def edit_address(self, request, **kwargs):
        def save_address(address, request):
            for key in request.POST:
                if hasattr(address, key):
                    setattr(address, key, request.POST[key])
            address.save()

        q = Profile.objects.filter(user=request.user)
        if q.exists():
            profile = q.first()
        else:
            profile = Profile(user=request.user)
            profile.save()

        addr = None
        if 'id' in request.POST:
            address_id = int(request.POST.get('id'))
            for address in profile.addresses.all():
                if address.id == address_id:
                    addr = address
            if addr:
                if request.method == 'DELETE':
                    addr.delete()
                    return api_result(_("Delete address successfully."), obj=addr.id)
                elif request.method == "PATCH":
                    # Change Address
                    save_address(addr, request)
                    return api_result(_("Update address successfully"), obj=addr.id)
                else:
                    return api_result(_("Only PATCH or DELETE method allowed."), CODE_PARAMNOTFOUND)
            else:
                return api_result(_("Can not find the address"), CODE_DATANOTFOUND)
        else:
            if 'address' in request.POST:
                addr = Address()
                save_address(addr, request)
                profile.addresses.add(addr)
                return api_result(_("Create address successfully"), CODE_SUCCESS, obj=addr.id)
            else:
                return api_result(_("Address field is needed when create an address"), CODE_DATANOTFOUND)


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = UserSerializer

    def get_queryset(self):
        '''
        Super User can access all users. Other people only can assess himself
        :return:
        '''
        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                return AdminModels.User.objects.all()
            else:
                return AdminModels.User.objects.filter(id=self.request.user.id)
        else:
            return []

    def get_serializer(self, *args, **kwargs):
        from rest_framework_jwt.serializers import JSONWebTokenSerializer
        serializer_class = JSONWebTokenSerializer
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    @action(methods=['post'], detail=False, permission_classes=[permissions.AllowAny])
    def login(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer_cls = get_req_value(request, 'serializer')
        if serializer_cls:
            serializer_class = get_class_by_path(serializer_cls)
        else:
            serializer_class = UserSerializer
        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            profile, create = Profile.objects.get_or_create(user=user)
            response_data = {
                'token': {
                    'id': user.id,
                    'name': user.username,
                    'email': user.email,
                    'api_token': token,
                    'device_token': '',
                },
                'user': serializer_class(user, many=False).data if user else {},
                'data': ProfileSerializer(profile, many=False).data
            }
            response = Response(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = (datetime.utcnow() +
                              api_settings.JWT_EXPIRATION_DELTA)
                response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                    token,
                                    expires=expiration,
                                    httponly=True)
            return response
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['get', 'post'], detail=False, permission_classes=[permissions.AllowAny])
    def register(self, request, **kwargs):
        '''
        Register User
        :param request:
        :param kwargs:
        :return:
        '''
        from landing.core.account import Account
        if request.method == 'POST':
            email = get_req_value(request, 'email')
            password = get_req_value(request, 'password')
            tel = get_req_value(request, 'tel')
            first_name = get_req_value(request, 'first_name')
            last_name = get_req_value(request, 'last_name')
            lang = get_req_value(request, 'lang')
            cls = get_req_value(request, 'cls')
            serializer_cls = get_req_value(request, 'serializer')
            if serializer_cls:
                serializer_class = get_class_by_path(serializer_cls)
            else:
                serializer_class = UserSerializer
            am = Account(request, default_model=cls)
            try:
                if am.check_username(email):
                    return api_result(_("Username already exists."), code=2)
                user = am.register_user(email, email, password, first_name=first_name, last_name=last_name, tel=tel,
                                        lang=lang)
            except Exception as ex:
                return action_result(False, ex.message)
            return api_result(_("Register user successfully"), obj=serializer_class(user, many=False).data)
        else:
            return api_result(_("GET method not allowed"), code=1)

    @action(methods=['post'], detail=False)
    def resetpwd(self, request):
        code = get_req_value(request, "code", 0)
        pwd = get_req_value(request, "pwd", 0)
        result = Account(request).verify_code(code)
        if (result):
            customer = Profile.objects.get(activiate_key=code).user
            customer.set_password(pwd)
            customer.save()
        return JsonResponse(result, safe=False)

    @action(methods=['post'], detail=False, permission_classes=[permissions.AllowAny])
    def forgot_password(self, request, **kwargs):
        from landing.core.account import Account
        email = get_req_value(request, 'email')
        am = Account(request)
        try:
            if request.user.is_authenticated:
                user = request.user
            else:
                users = AdminModels.User.objects.filter(username=email)
                user = users.first()
            am.send_resetpwd_email(user)
            return api_result(_("Forgot password successfully."))
        except Exception as ex:
            return api_result(_("Forgot password exception."), exception=ex, code=1)
