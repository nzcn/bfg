from landing.models import *
from django import forms
from django.forms import ModelForm, PasswordInput
from datetime import *
import logging
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.core.cache import cache
from landing.core.common import *
from uuslug import slugify
# noinspection PyPackageRequirements
from django.utils.translation import gettext
from landing.controller.contents import ContentManager
from django.db import transaction


class SiteForm(forms.ModelForm):
    email_password = forms.CharField(widget=PasswordInput(), required=False)

    class Meta:
        model = Site
        exclude = ('last_modified_time', 'author', 'create_time')
        fields = '__all__'


def clear_cache(modeladmin, request, queryset):
    cm = ContentManager(request)
    cache.set(cm.get_site_cache_key(), None)


def copy_site(modeladmin, request, queryset):
    i = 0
    cm = ContentManager(request)
    for site in queryset:
        i += 1
        cts = cm.get_content_types(site=site)
        site.id = None
        site.name = "{name}-{i}".format(name=site.name, i=i)
        site.home_page = "{server}-{i}".format(server=site.home_page, i=i)
        site.save()
        for ct in cts:
            ct.id = None
            ct.site = site
            ct.save()


@transaction.atomic()
def copy_post(modeladmin, request, queryset):
    for post in queryset:
        types = post.type
        post.id = None
        post.title = "{t}(clone)".format(t=post.title)
        post.url_slug = post.url_slug + "_" + get_random_string(6)
        post.save()
        for t in types.all():
            post.type.add(t)


clear_cache.short_description = gettext("Clear Item Cache")
copy_site.short_description = gettext("Copy Site")
copy_post.short_description = gettext("Copy Post")


class SiteAdmin(admin.ModelAdmin):
    list_display = ('title', 'name', 'home_page', 'lang', 'enable')
    form = SiteForm
    actions = [clear_cache, copy_site]
    fieldsets = [
        (None, {'fields': ['lang', 'name', 'title', 'home_page', 'subtitle',
                           'logo', 'reverse_logo', 'page_icon', 'enable']}),
        ('Contact Information', {
            'fields': ['address', 'city', 'state', 'country', 'tel1', 'fax', 'email', 'mobile', 'contact_person',
                       'contact_time']}
         ),
        ('Content', {
            'classes': ('collapse',),
            'fields': ['header', 'footer', 'top_html', 'footer_html',
                       'home_top_html', 'home_slide_html', 'home_feature_html', 'home_service_html',
                       'home_info_html', 'home_price_html', 'home_action_html', 'home_testimonial_html',
                       'home_contact_html',
                       'home_request_html', 'home_newsbox_html', 'home_subscription_html', 'home_mail_html',
                       'home_about_html']}
         ),
        ('Settings', {
            'classes': ('collapse',),
            'fields': ['show_login', 'show_register', 'google_map_apikey', 'google_analysis_apikey']}
         ),
        ('SEO', {
            'classes': ('collapse',),
            'fields': ['meta_title', 'description', 'keywords']}
         ),
        ('Social Media', {
            'classes': ('collapse',),
            'fields': ['social_facebook', 'social_twitter', 'social_pinterest', 'social_instagram', 'social_linkedin',
                       'social_sina', 'social_wechat', 'social_qq', ]}
         ),
        ('Email', {
            'classes': ('collapse',),
            'fields': ['email_server', 'email_port', 'email_username', 'email_password', 'email_user_auth',
                       'email_sender',
                       'email_replyto', 'email_use_tls', 'email_use_ssl']}
         ),
        ('Advanced Settings', {
            'classes': ('collapse',),
            'fields': ['template', 'paypal_site', 'pay_paypal', 'free_user_period', 'settings', 'terms']}
         ),
    ]

    def save_model(self, request, obj, form, change):
        if not change or not obj.create_time:
            obj.create_time = timezone.now()
            obj.author = request.user
        obj.last_modified_time = timezone.now()
        obj.save()
        logger.info("Site Saved")
        cm = ContentManager(request)
        cache.set(cm.get_site_cache_key(), None)


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('title', 'abstraction')


class TestimonialAdmin(admin.ModelAdmin):
    list_display = ('user', 'title', 'content', 'create_time')


class PricePlanAdmin(admin.ModelAdmin):
    list_display = ('title', 'price', 'currency', 'unit', 'enable')


class PlanPromotionAdmin(admin.ModelAdmin):
    list_display = ('plan', 'title', 'discount', 'price_off', 'min_quantity')


class PostForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget(), required=False)

    # lang = forms.CharField(widget=forms.Select(choices=settings.LANGUAGES),required=False)
    class Meta:
        model = Post
        exclude = ('last_modified_time', 'create_time', 'seo', 'author')
        fields = '__all__'


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_types', 'url_slug', 'site', 'lang', 'last_modified_time')
    list_filter = ('type', 'author', 'site', 'lang', 'last_modified_time')
    search_fields = ('title', 'content', 'subtitle')
    ordering = ('-last_modified_time',)
    actions = [copy_post]
    form = PostForm

    def save_model(self, request, obj, form, change):
        if not change:
            obj.create_time = timezone.now()
            obj.author = request.user
        if not obj.create_time:
            obj.create_time = timezone.now()
            obj.author = request.user
        if obj.url_slug is None:
            obj.url_slug = orig = slugify(obj.title)
        obj.last_modified_time = timezone.now()
        obj.save()
        logger.info("Content Type Saved")

    def get_types(self, obj):
        return "\n".join([p.title for p in obj.type.all()])


class ContentTypeAdmin(admin.ModelAdmin):
    exclude = ("create_time", "author", "last_modified_time")
    list_filter = ('site', "key", "lang")
    list_display = ('title', 'site', 'key', 'parent', 'url_name', 'pos', 'lang', 'enable')
    ordering = ['key']

    # form = ContentTypeForm
    def save_model(self, request, obj, form, change):
        if not change:
            obj.create_time = timezone.now()
            obj.author = request.user
        obj.last_modified_time = timezone.now()
        if obj.parent == obj:
            obj.parent = None
        obj.save()
        cache.set(CONST_LANDING_SITE_VARS, None)


class VairableAdmin(admin.ModelAdmin):
    list_display = ('key', 'content', 'description')
    search_fields = ('key',)
    ordering = ['key']


class MailTemplateAdmin(admin.ModelAdmin):
    list_display = ('key', 'subject', 'is_html')
    search_fields = ('key',)
    ordering = ['key']


class SEOTemplateAdmin(admin.ModelAdmin):
    list_display = ('key', 'title', 'keywords')
    search_fields = ('key', 'keywords',)
    ordering = ['key']


class MessageAdmin(admin.ModelAdmin):
    list_display = ('title', 'content')
    search_fields = ('keywords',)
    ordering = ['-create_time']


class TagAdmin(admin.ModelAdmin):
    list_display = ('title', 'group', 'key', 'pos')
    search_fields = ('title', 'key')
    list_filter = ('group',)
    ordering = ['pos']
    exclude = ['create_time', 'author']

    def save_model(self, request, obj, form, change):
        if not change:
            obj.create_time = timezone.now()
            obj.author = request.user
        obj.save()


class ChangeHisotryAdmin(admin.ModelAdmin):
    list_display = ("lang", "version", "content", "create_time")
