from huey.contrib.djhuey import task
from landing.core.mail import MailProviderBase
from django.conf import settings

@task()
def send_background_email(subject, body,
                          reply_to=None,
                          receivers=None, sender=None,
                          cc=None,
                          bcc=None,
                          html_body=None,
                          attachments=[],
                          mail_account=None,
                          cls=None):
    from landing.core.common import get_bfg_settings,get_value,get_class_by_path

    if not cls:
        mail_settings = get_bfg_settings("mail")
        provider_name = get_value(mail_settings, "provider", "landing.core.mail.SmtpMailProvider")
        if mail_account:
            setting = mail_account
        else:
            setting = get_value(mail_settings, "settings")
        cls = get_class_by_path(provider_name)
    if cls and issubclass(cls, MailProviderBase):
        class_obj = cls()
        if get_bfg_settings('test_mode', False):
            receivers = settings.ADMIN_EMAIL
        if not sender:
            sender = setting['sender']
        if not reply_to:
            reply_to = setting['sender']
        result = class_obj.send(title=subject, body=body, receivers=receivers, sender=sender,
                                attachments=attachments, mail_account=setting, reply_to=reply_to, cc=cc, bcc=bcc)
        if result.status_code != 200:
            print(result.text)
        return result
    else:
        print('Mail Provider invalid.' + cls.__str__())
