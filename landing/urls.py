from django.conf.urls import url, include
# noinspection PyPackageRequirements
from django.contrib import admin
import landing.views as views
import landing.account.views as account_view

urlpatterns = [
    url(r'^$', views.index, name="home"),
    url(r'^unsubscribe$', views.index, name="unsubscribe"),
    url(r'^login/$', account_view.login, name='login'),
    # url(r'^', include('django.contrib.auth.urls')),
    url(r'^(?P<page>\w+.html)', views.static_file, name='landing_static'),
    url(r'^(?P<page>\w+.txt)', views.static_file),
    url(r'^(?P<page>robots.txt)', views.static_file),
    url(r'^account/', include('landing.account.urls')),
    url(r'^test/', views.test, name='landing_test'),
    url(r'^install/', views.install, name='landing_install'),
    url(r'^result/', views.show_result, name='landing_result'),
    url(r'^contactus/', views.contact_us, name='landing_contactus'),
    url(r'^contact_email/', views.contact_email, name='landing_contact_email'),
    url(r'^change_language', views.change_language, name='landing_change_language'),
    url(r'^upload_image/$', views.multiuploader, name='landing_uploadimage'),
    url(r'^delete_image/$', views.multiuploader_delete, name='landing_deleteimage'),
    url(r'^sitemap.xml', views.sitemap, name='landing_sitemap'),
    url(r'^side_notice/', views.side_notice, name='landing_side_notice'),
    url(r'^common_action/(?P<action>\w+)/(?P<app_label>\w+)/(?P<model_name>\w+)/', views.common_action,
        name='landing_common_action'),
    url(r'^landing/(?P<action>\w+)/', views.inner_page, name='landing_inner_page'),
    url(r'^(?P<cat1>\w+)/content.html$', views.content_page, name='landing_content_page'),
    url(r'^(?P<cat1>\w+)/(?P<cat2>\w+)/content.html$', views.content_page, name='landing_content_page'),
    url(r'^(?P<cat1>\w+)/(?P<template>\w+)_t.html', views.post_page, name='landing_post_page'),
    url(r'^(?P<cat1>\w+)/(?P<cat2>\w+)/(?P<template>\w+)_t.html', views.post_page, name='landing_post_page'),
    url(r'^posts(?P<page>\d+).html$', views.posts, name='landing_posts'),
    url(r'^(?P<cat1>\w+)/posts(?P<page>\d+).html$', views.posts, name='landing_posts'),
    url(r'^(?P<cat1>\w+)/(?P<cat2>\w+)/posts(?P<page>\d+).html$', views.posts, name='landing_posts'),
    url(r'^(?P<cat1>\w+)/posts/(?P<id>\d+).html$', views.post, name='landing_post'),
    url(r'^(?P<cat1>\w+)/(?P<cat2>\w+)/posts/(?P<id>\d+).html$', views.post, name='landing_post'),
]
