from __future__ import unicode_literals

import os

from django.apps import AppConfig
from django.conf import settings

from landing.controller.contents import ContentManager
from landing.core.common import *


def register_sitemap(request, urls):
    cm = ContentManager
    # remove lang filter
    cts = ContentType.objects.filter()
    posts = Post.objects.filter(enable=True)
    for ct in cts:
        if ct.href:
            urls.append(get_url(ct.href, ct.last_modified_time, 'daily', 1))
        elif ct.url_name:
            args = []
            if ct.url_param1:
                args.append(ct.url_param1)
            if ct.url_param2:
                args.append(ct.url_param2)
            if ct.url_param3:
                args.append(ct.url_param3)
            if ct.url_param4:
                args.append(ct.url_param4)
            try:
                url = reverse(ct.url_name, args=args)
                urls.append(get_url(url, ct.last_modified_time, 'daily', 1))
            except Exception as ex:
                print(ex)
    # Add Post URLs
    for post in posts:
        if post.type.exists():
            urls.append(get_url(reverse('landing_post', kwargs={'cat1': post.type.first().key, 'id': post.id}),
                                post.last_modified_time))
    return urls


class LandingConfig(AppConfig):
    name = 'landing'


def init():
    if 'landing.core.context_processors.landing_var' not in settings.TEMPLATES[0]['OPTIONS']['context_processors']:
        settings.TEMPLATES[0]['OPTIONS']['context_processors'].append('landing.core.context_processors.landing_var')

    if 'ckeditor' not in settings.INSTALLED_APPS:
        # print "- Add CKEditor Support"
        try:
            settings.CKEDITOR_UPLOAD_PATH
        except:
            settings.CKEDITOR_UPLOAD_PATH = 'upload/'
        try:
            settings.CKEDITOR_JQUERY_URL
        except:
            settings.CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

        try:
            settings.CKEDITOR_BROWSE_SHOW_DIRS
        except:
            settings.CKEDITOR_BROWSE_SHOW_DIRS = True

        settings.INSTALLED_APPS.append('ckeditor')

    if 'ckeditor_uploader' not in settings.INSTALLED_APPS:
        settings.INSTALLED_APPS.append('ckeditor_uploader')
    try:
        settings.CKEDITOR_CONFIGS
    except:
        settings.CKEDITOR_CONFIGS = {
            'awesome_ckeditor': {
                'toolbar': 'Basic',
            },
            "default": {
                "removePlugins": "stylesheetparser",
                "allowedContent": True,
            },
            "small": {
                "height": 120,
            },
            "large": {
                "width": 900,
            }
        }

    try:
        if settings.STATIC_ROOT == None:
            settings.STATIC_ROOT = os.path.join(settings.BASE_DIR, 'static')
            # print '- Add Static Root to {path}'.format(path=settings.STATIC_ROOT)
    except:
        settings.STATIC_ROOT = os.path.join(settings.BASE_DIR, 'static')
        # print '- Add Static Root to {path}'.format(path=settings.STATIC_ROOT)

    if settings.LOGGING and 'loggers' in settings.LOGGING:
        pass
    else:
        # print "- Add Loggin Configration"
        settings.LOGGING = {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'verbose': {
                    'format': '%(levelname)s %(asctime)s %(pathname)s %(funcName)s \n%(message)s'
                },
                'simple': {
                    'format': '%(levelname)s %(message)s'
                },
            },
            'handlers': {
                'file': {
                    'level': 'DEBUG',
                    'class': 'logging.FileHandler',
                    'filename': os.path.join(settings.BASE_DIR, 'landing.log'),
                    'formatter': 'verbose',
                },
            },
            'loggers': {
                'default': {
                    'handlers': ['file'],
                    'level': 'DEBUG',
                    'propagate': True,
                },
            },
        }
