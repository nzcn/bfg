// CONTACT MAP

var PageContact = function() {

	var _init = function() {

		var mapbg = new GMaps({
			div: '#gmapbg',
			lat: -36.782975,
			lng: 174.739137,
			scrollwheel: false,
		});


		mapbg.addMarker({
			lat: -36.782975,
			lng: 174.739137,
			title: 'Your Location',
			infoWindow: {
				content: '<h3>Jango Inc.</h3><p>25, Lorem Lis Street, Orange C, California, US</p>'
			}
		});
	}

    return {
        //main function to initiate the module
        init: function() {

            _init();

        }

    };
}();

$(document).ready(function() {
    PageContact.init();
    $( window ).resize(function() {
		PageContact.init();
	});
});