// CONTACT MAP

var PageContact2 = function() {

	var _init = function() {

		var mapbg = GMaps.createPanorama({
			el: '#gmapbg',
			lat: -36.7831977,
			lng: 174.7400605,
			scrollwheel: false
		});
	}

    return {
        //main function to initiate the module
        init: function() {
            _init();

        }

    };
}();

$(document).ready(function() {
    PageContact2.init();
    $( window ).resize(function() {
		PageContact2.init();
	});
});