$(function () {
    $(".delete-picture").click(function () {
        var btn = $(this);
        $(btn.attr("data-target")).val("");
        $(btn.attr("image-target")).attr("src","");
        $("#picture_status").val("deleted")
        btn.hide();
    });

    $(".toggle_hidden").click(function () {
       $(".adv").toggleClass("hidden");
    });
})