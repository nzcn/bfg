
show_message = function (message) {
    Swal.fire({
        title:message,
        icon:'info',
    });
}

get_value_from_element = function (obj) {
    if (obj) {
        if (obj.is("input"))
            if (obj.is(':checkbox')) {
                if(obj.is(":checked")){
                    if(obj.attr("value")){
                        return obj.attr("value")
                    }
                    else{
                        return "true"
                    }
                }
                else
                {
                    return ""
                }
            } else
                return obj.val();

        else if (obj.is("select")) {
            return obj.find(":selected").val();
        } else if (obj.is("textarea")) {
            return obj.val();
        }
    } else
        return ""
}

//extract key value from form_fields selectors
get_data_from_container = function (form_fields, data) {
    $.each(form_fields, function (i, v) {
        var container = $(v);
        container.find("input,select,checkbox,textarea").each(function () {
            var ele = $(this);
            var key;
            if (ele.attr("name"))
                key = ele.attr("name");
            else if (ele.attr("id")) {
                key = ele.attr("id");
            } else
                key = null;
            if (key) {
                if(key in data){
                    //Make an array
                    var result = data[key];
                    if(!$.isArray(result))
                        result = [result]
                    result.push(get_value_from_element(ele));
                    data[key] = result
                }
                else{
                    data[key] = get_value_from_element(ele);
                }
            }
        });
    });
}

/* Pager related */
updateParam = function (k, v) {
    var params = $.url().param();
    params[k] = v;
    return "?" + $.param(params);
}

hasAttr = function (obj, attrName) {
    var attr = obj.attr(attrName);
    return (typeof attr !== typeof undefined && attr !== false);
}

executeAction = function (url, opt_name) {
    var ids = "";
    $.each($("input[name='" + opt_name + "']:checked"), function () {
        if($(this).val() != 'on'){
            ids += $(this).val() + ",";
        }
    });
    window.open(url + "&ids=" + ids, '_blank');
}


isBoolean = function (value, defaultValue) {
    if (value == "false")
        return false;
    else if (value == "true")
        return true;
    else
        return defaultValue;
}

var getUrlParam = function getUrlParameter(k) {
    var params = $.url().param();
    if (k in params)
        return params[k];
    else {
        return "";
    }
};

go_back =function (result) {
    var return_url = getUrlParam('return_url');
    if(result && result.return_url)
        return_url = result.return_url;

    if(return_url)
        location.href = return_url;
    else
        history.back(1);
}

executeFunctionByName = function (functionName, context) {
  var args = [].slice.call(arguments).splice(2);
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for(var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  return context[func].apply(context, args);
}


get_modal_option = function (item, ids) {
    if (!ids || typeof ids == 'undefined')
        ids = [item.attr("data-field-id")];
    return {
        url:item.attr('data-url'),
        title:item.attr('data-title'),
        message: item.attr('data-message'),
        ids: ids,
        action_url: item.attr("data-action"),
        ok_button_title: item.attr('button_title'),
        after_load: item.attr('data-after-load'),
        refresh: isBoolean(item.attr('refresh'),true),
        return: isBoolean(item.attr('return'),false),
        need_confirm: isBoolean(item.attr('confirm'),true),
        data_form_id: item.attr('data-form-id')
    };
}


execute_batch_action = function (dlg, opt) {
    var i = 0;
    var total_item = opt.ids.length;
    var confirm_button = dlg.find('#confirm_button');
    var progress_bar = dlg.find('#progress .progress-bar');
    var inclued_form = dlg.find('.included_form');
    var detail_box = dlg.find('#model_failure');
    var tips_label = dlg.find('#modal_tips');
    var final_result = true;
    executeAction = function ( element,index) {
        var post_data = {
            'csrfmiddlewaretoken': $('#csrfmiddlewaretoken').val(),
            'id': element
        }
        //add extra data to the post——data
        if(inclued_form){
            get_data_from_container(inclued_form,post_data);
        }

        $.post(opt.action_url,
            post_data,
            function (result) {
                i++;
                if (result) {
                    if (result.code == 0) {
                        final_result = final_result && true;
                        if (detail_box.length > 0) detail_box.prepend("<li>Process " + result.message + " successfully.</li>");
                    }
                    else {
                        final_result = false;
                        if (detail_box.length > 0) detail_box.prepend("<li class='has-error'>Process " + result.message + " failure.</li>");
                    }
                }
                if (progress_bar.length > 0) {
                    var progress = parseInt(i / total_item * 100, 10);
                    if (progress >= 100) {
                        confirm_button.prop('disabled', false);
                        if (opt.autoclose > 0) {
                            setTimeout(function () {
                                $('#modal_close').click();
                                if (modal_refresh_parent) {
                                    location.reload();
                                }
                            }, opt.autoclose);
                        }
                    }
                    progress_bar.css(
                        'width',
                        progress + '%'
                    );
                }
            });
    }
    opt.ids.forEach(executeAction);

    if ($("#icon_question").length > 0) {
        $("#icon_question").hide();
        if (final_result) {
            $("#icon_success").removeClass("hidden");
            tips_label.html("Operation execuetd successfully.");
            if (opt.after_success)
                opt.after_success(final_result);
        } else {
            $("#icon_failue").removeClass("hidden");
            tips_label.html("Operation execuetd fail.");
        }
    }
}


open_dialog = function (dlg, opt, obj, func) {
    switch_icon = function (t) {
        $(".fa-5x").addClass("hidden");
        $("#icon_" + t).removeClass("hidden");
    }

    show_model = function(data){
        // alert(data);
        dlg.removeData('bs.modal');
        dlg.find('.modal-content').html(data);
        dlg.modal("show")
            .on('shown.bs.modal', function(){
            dlg.off('shown.bs.modal');

            var title = dlg.find(".modal-title");
            var confirm_button = dlg.find("#confirm_button");
            var tips = dlg.find("#modal_tips");

            if(opt.title) title.html(opt.title);
            tips.html(opt.message);
            confirm_button.hide();
            confirm_button.click(function (e) {
                $(this).prop('disabled', true);
                if(func !=undefined){
                    func(obj);
                }
                opt.executed = true;
            });

            if (opt.ok_button_title) {
                confirm_button.html(opt.ok_button_title);
            }

            if (opt.preaction_url) {
                tips.html("Retrieving data ...");
                confirm_button.prop('disabled', true);
                $.post(opt.preaction_url, {'csrfmiddlewaretoken': $('#csrfmiddlewaretoken').val()}, function (result) {
                    if (result.code == 0) {
                        opt.ids = result.ids;
                        tips.html('You have ' + opt.ids.length + ' tasks to be processed.');
                        if (result.url)
                            opt.action_url = result.url;
                        confirm_button.prop('disabled', false);
                    }
                    else {
                        tips.html("Operation failed.");
                    }
                });
            }

            if (opt.need_confirm) {
                //Single page with tips
                confirm_button.show();
                switch_icon("question");
            }
            else {
                confirm_button.hide();
                confirm_button.click();
            }

            if(opt.after_load)
                executeFunctionByName(opt.after_load,window);

            init(dlg);

            confirm_button.prop('disabled', false);
        }).on('hidden.bs.modal', function () {
            if (opt.executed && opt.refresh && !func)
                location.reload();
        });
    }

    if (typeof dlg != 'undefined') dlg = $("#modal-form");
    if (!opt || typeof opt == 'undefined') opt = {};
    if (!opt.message) opt.message = 'Dialog operation result';
    if (!opt.url) opt.url = '/dialog/none/';
    if (!opt.execute_once) opt.execute_once = true;
    if (typeof opt.need_confirm == 'undefined') opt.need_confirm = true;
    if (typeof opt.autoclose == 'undefined') opt.autoclose = 0;
    if (typeof opt.refresh == 'undefined') opt.refresh = false;
    if (typeof opt.backdrop == 'undefined') opt.backdrop = false;
    if (typeof opt.return == 'undefined') opt.return = false;
    opt.executed = false;
    if (typeof opt.data_form_id == 'undefined') opt.data_form_id = '';
    if(opt.data_form_id != ''){
        var data = {"csrfmiddlewaretoken":$("input[name='csrfmiddlewaretoken']").val()};
        get_data_from_container(opt.data_form_id.split(','),data);
        $.post(opt.url,data,function(data){
            show_model(data);
        });
    }else{
        $.get(opt.url, function(data){
            show_model(data);
        })
    }
}

//Custom Extentions
init = function (obj) {
    if(!obj)
        obj = $( ":root" );
    //Auto remove the modal cache
    obj.find('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });

    //Action Button
    obj.find("[data-toggle='sl_modal']").click(function (e) {
        e.preventDefault();
        open_dialog(obj.find("#modal-form"), get_modal_option($(this)));
    });

    //Batch Operation
    obj.find('.batch_action').click(function (e) {
        e.preventDefault();
        var btn = $(this);
        var ids = obj.find('input[name^="post"]:checked').map(function () {
            return this.value;
        }).get();
        if (ids.length == 0)
        {
            show_message('Please select items and action type.');
            return;
        }
        var action_obj = null;
        if(hasAttr(btn,"data-action"))
            action_obj = btn;
        else
        {
            action_obj = obj.find('.batch_action_list option:selected');
            if(!hasAttr(action_obj,"data-action"))
            {
                obj.find('.batch_action_list').addClass("has-error");
            }
        }
        open_dialog(
            obj.find("#modal-form"),
            get_modal_option(action_obj, ids),
            btn,
            function(){
                execute_batch_action(obj.find("#modal-form"),get_modal_option(action_obj, ids))
            });
    });

    //Attach submit buttons
    obj.find(".submit-btn").click(function (e) {
        e.preventDefault();
        if($(this).attr("data-url")){
            open_dialog(
                obj.find("#modal-form"),
                get_modal_option($(this)),
                $(this),
                function (obj) {
                    execute_submit_button(obj);
                });
        }else{
            execute_submit_button($(this));
        }
    });

    //Attach submit buttons
    obj.find(".dlg-submit-btn").click(function (e) {
        e.preventDefault();
        execute_submit_button($(this));
    });

    obj.find(".language_list li").click(function () {
        var form = $(".language_form")
        var href = form.attr("value");
        var code = $(this).attr("href");
        $.post(
            href,
            {
                csrfmiddlewaretoken:form.find("input[name='csrfmiddlewaretoken']").val(),
                next:form.find("input[name='next']").val(),
                language:code
            },
            function (data) {
                window.location.reload();
            }).fail(function (data) {
                alert(data);
        });
    });

    obj.find('.btn-cancel').click(function () {
        go_back();
    });

    obj.find('.input').keypress(function (e) {
      if (e.which == 13) {
        obj.find('form').submit();
        return false;
      }
    });

    obj.find("select").each(function (e) {
       if($(this).attr("selected_id")){
           var id = $(this).attr("selected_id");
           $(this).find("option[value='"+id+"']").prop('selected',true);
       }
    });

    obj.find(".input-source").change(function () {
        var source = $(this);
        if(source.attr("copy-to")){
            var dest = $(source.attr("copy-to"));
            if(dest.val() == "" || dest.val() == source.val()){
                dest.val(source.val());
                dest.change();
            }
        }
    })

    obj.find(".switch").on('change', function (event) {
        var input = $(this).find("input[type='checkbox']");
        var input_hidden = $(this).find("input[type='hidden']");
        var attr = input.attr("checked");
        if(typeof attr !== typeof undefined && attr !== false){
            input.removeAttr("checked");
            input_hidden.attr("value","off");
        }
        else
        {
            input.attr("checked","true");
            input_hidden.attr("value","on");
        }
    })

    obj.find(".radio_switch").click(function () {
        var input = $(this);
        var group_class = input.attr("name");
        var for_id = input.attr("for");
        $("." + group_class).hide();
        $("#" + for_id).show();
    })

    obj.find(".toggle-switch").click(function () {
        var input = $(this);
        var duration = input.attr("duration") ? input.attr("duration") : 500;
        var for_ele = $(input.attr("for"));
        if(for_ele.is(":visible"))
        {
            for_ele.hide(duration).trigger('change');
        }
        else{
            for_ele.show(duration).trigger('change');
        }
    })

    obj.find(".btn-cancel,.cancel-btn").click(function () {
        var return_url = getUrlParam('return_url');
        if (return_url)
            location.href = return_url;
        else
            history.back(1);
    });

    obj.find(".delete-tr").click(function () {
        $(this).closest("tr").remove();
    });

    obj.find(".editable-table").each(function () {
        var table = $(this);
        table.find(".add-item").click(function () {
            var line = $(this).closest("tr");
            var newLine = line.clone(true);
            newLine.find("input[name='detail_ids']").val("");
            newLine.insertAfter(line);
        });

        table.find(".append-item").click(function () {
            var line = $(this).closest("tr").prev();
            var newLine = line.clone(true);
            newLine.find("input[name='detail_ids']").val(0);
            let btn = newLine.find("[data-toggle='sl_modal']");
            let oldId = btn.attr("id");
            let newId = oldId + '_dup';
            btn.attr("id",newId);
            btn.attr("data-url",btn.attr("data-url").replace(oldId,newId));
            newLine.insertAfter(line);
        });

        table.find(".del-item").click(function () {
            var line = $(this).closest("tr");
            if (table.find(".del-item").length > 1)
                line.remove();
        });

        table.find(".change").on("change", function () {
            var line = $(this).closest("tr");
            calc(line, $(this));
        });

        table.find("tr").each(function () {
            if ($(this).find(".change").length > 0) {
                calc($(this), $(this).find(".change"));
            }
        });
    });


    obj.find(".copy_button,.copy-btn").click(function () {
        var btn = $(this);
        var target = document.getElementById(btn.attr("data-target"));
        var message = $("#copy_message");
        var el = document.createElement('textarea');
        el.value = target.innerText;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        btn.removeClass("btn-primary");
        btn.addClass("btn-success");
        message.html(btn.attr("data-message"));
    });

    obj.find(".import_file").click(function (){
        var fileForm = $(this).closest("form");
        if(fileForm.length > 0){
            var fileInput=fileForm.find("input[type=file]");

            if(fileInput.length > 0)
            {
                fileInput.trigger('click').on('change',function (){
                    if(fileInput.val().length > 0){
                        fileForm.submit();
                    }
                });
            }
        }
    });

    obj.find('button[value="bulk_action"]').click(function (){
        e.preventDefault();
        let btn = $(this);
        let form = btn.closest("form");
        let action_selected = form.find('select[name="bulk_action"]').find(":selected");
        if(action_selected.val()=="")
        {
            Swal.fire(
              'Result',
              "Please select the option first.",
              'info'
            )
        }
        else
        {
            let attr = action_selected.attr('confirm');
            if (typeof attr !== 'undefined' && attr !== false) {
                Swal.fire({
                    title: attr,
                    icon: 'question',
                    confirmButtonText: 'Yes',
                    showCancelButton: true,
                }).then((result) => {
                    if(result.value){
                        form.submit();
                    }
                });
            }
            else
            {
                form.submit();
            }
        }
    });

    obj.find("#parcel_number").on('keypress',function(e) {
        if(e.keyCode == 13) {
            var number = $(this).val();
            $.post('/api/parcels/info/',
                {
                    'csrfmiddlewaretoken': $('#csrfmiddlewaretoken').val(),
                    'parcel_id':number,
                },
                function (result) {
                    console.log(result);
                    if(result.code == 0){
                        var panel = $("#content_panel");
                        var btnParcel = panel.find("#btn_parcel")
                        var btnFo = panel.find("#btn_fo")
                        var btnFreight = panel.find("#btn_freight")
                        panel.show();

                        btnParcel.html(result.object.local_courier_name + ' ' + result.object.local_courier_number).on('click',function (){
                            window.open('/appa/packgo/parcel/'+ result.object.id +'/change/');
                        });

                        if(result.object.dform != null && result.object.dform.tracking_number != null)
                        {
                            btnFo.html(result.object.dform.tracking_number).on('click',function (){
                                window.open('/appa/packgo/declarationform/' + result.object.dform.id +'/change/');
                            });
                        }
                        else
                        {
                            btnFo.removeClass("bg-info");
                            btnFo.addClass("bg-white");
                        }


                        if(result.object.in_freight != null && result.object.in_freight.tracking_number)
                        {
                            btnFreight.html(result.object.in_freight.tracking_number).on('click',function (){
                                window.open('/appa/packgo/freight/'+ result.object.in_freight.id +'/change/');
                            });
                        }
                        else
                        {
                            btnFreight.removeClass("bg-info");
                            btnFreight.addClass("bg-white");
                        }

                    }
                });
        }
    });
}
sl_init = init;

var execute_submit_button = function (obj,onResult) {
    function show_top_message(obj,result) {
        Swal.fire(
          'Result',
          result.message,
          'info'
        )
    }

    execute_submit = function (obj,onResult) {
        obj.prop('disabled', true);
        var data = {"csrfmiddlewaretoken":$("input[name='csrfmiddlewaretoken']").val()};
        if(obj.attr("data-field-id")){
            var fields = obj.attr("data-field-id").split(',');
            $.each(fields,function (i,v) {
                data[v] = get_value_from_element($("#"+v));
            });
        }
        else if(obj.attr("data-form-id"))
        {
            get_data_from_container(obj.attr("data-form-id").split(","),data)
        }
        if(obj.attr("data-key")){
            data['data-key'] = obj.attr("data-key");
        }
        $.post(
            obj.attr("data-action"),
            data,
            function (result) {
                obj.result = result;
                obj.prop('disabled', false);
                if(typeof(onResult) != "undefined")
                {
                    onResult(result);
                }
                else {
                    if(result.code == 0)
                    {
                        if(obj.attr("close")){
                            $('#modal_close').click();
                        }
                        if(obj.attr("refresh-selector"))
                        {
                            var refresh_obj = $(obj.attr("refresh-selector"));
                            if(refresh_obj){
                                var innerHtml = result.obj || result.object;
                                if(innerHtml){
                                    refresh_obj.html(innerHtml);
                                    init(refresh_obj);
                                }
                                else if(refresh_obj.attr("data-url")){
                                    //Load from data-url
                                    refresh_obj.load(refresh_obj.attr("data-url"),null,function () {
                                        init(refresh_obj);
                                    });
                                }
                            }
                        }
                        if(result.message != null && result.message.length > 0)
                        {
                            Swal.fire({
                                title:result.message,
                                icon:'success'
                            }).then(result => {
                                if(obj.attr("data-refresh"))
                                {
                                    location.reload();
                                }
                            });
                        }
                    }
                    else{
                        if(obj.attr("onerror")){
                            execute_result = result;
                            obj.trigger("onerror");
                        }
                        else
                        {
                            show_top_message(obj,result);
                        }
                    }
                }
            }
        )
    }

    if(obj.attr("data-confirm")){
        Swal.fire({
            title: obj.attr("data-confirm"),
            icon: 'question',
            confirmButtonText: 'Yes',
            showCancelButton: true,
        }).then((result) => {
            if(result.value){
                Swal.showLoading();
                execute_submit(obj,onResult);
            }
        });
    }
    else{
        execute_submit(obj,onResult);
    }
}


findObject = function (btn, type, value){
    let href = '/pack/find/' + type + "/" + value;
    $.get(href, function (data) {
        let container = $("#content_panel");
        container.html("");
        $.each(data, function (index, e) {
            $("<a class='btn btn-info' style='margin: 5px' target='_blank'></a>").html(e.text).attr("href", e.link).appendTo(container);
        });
        if(data.length == 1){
            let tform = $("#ticketForm");
            let tFor = tform.find("#ticketFor");
            let tType = tform.find("#ticketType");
            let tId = tform.find("#ticketObjectId");
            tFor.html(btn.html() + ' ' + data[0].text);
            tType.val(data[0].type);
            tId.val(data[0].value);
            tform.show();
        }
    });
}