from __future__ import division

from django.contrib import messages
from django.contrib.admin.forms import AuthenticationForm
from datetime import *
from django.contrib.auth import (
    login as auth_login, authenticate,
    update_session_auth_hash,
)
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User, Group
from django.http import JsonResponse
from django.shortcuts import reverse
from django.utils.translation import gettext as _

from landing.controller.mesages import MessageManager
from landing.controller.users import UserManager
from landing.core.account import Account
from landing.core.common import *


# Create your views here.
@login_required()
def index(request):
    um = UserManager(request)
    profile = um.get_profile()
    return template_response(request, 'landing/account/index.html',locals())

def active_account(request,id):
    if id and id != 'None':
        result = Account(request).verify_profile(id)
    else:
        result = False
    return template_response(request, 'landing/account/active_account_result.html',locals())

def login(request):
    title = _("Sign In")
    subtitle = _("Login with your own account")
    if request.method == 'POST':
        form = AuthenticationForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                auth_login(request, user)
                if 'remember_me' in request.POST:
                    request.session.set_expiry(60 * 60 * 24 * 180)
                else:
                    request.session.set_expiry(60 * 60 * 24)
                return redirect(request.GET.get('next',reverse('account_index')))
        else:
            messages.error(request,_('username or password not correct'))
            return redirect('login')
    else:
        form = AuthenticationForm()

    if request.user.is_authenticated() and request.user.id is not None:
        return redirect(request.GET.get('next', '/'))
    return template_response(request, 'registration/login.html', locals())

@login_required
def resend_verify_email(request):
    if request.method == 'POST':
        q = Profile.objects.filter(user=request.user)
        if q.exists():
            p = q.first()
        else:
            p = generate_profile(request.user)
        result = Account(request).send_verify_email(p.customer_set.first(), request)
        return show_result(request,message= _('Verification Email has beens sent to your mailbox. Please activate your account in 24 hours.'),success=result)
    else:
        return template_response(request, 'landing/account/resend_verify_email.html')

@login_required()
def landing_logout(request):
    logout(request)
    if 'next' in request.GET:
        return redirect(reverse('login') + '?next=' + request.GET.get('next'))
    else:
        return redirect('login')

def resetpwd(request, key):
    from django.contrib.auth.forms import PasswordResetForm
    am = Account(request)
    op = am.get_user_operation(key)
    if request.method == 'POST':
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'landing/account/result.html',locals())
    if op:
        if am.check_user_operation_expire(op):
            # Switch to Change Password page
            return render(request, 'registration/password_reset_confirm.html', locals())
        else:
            message_user(request, message=_('Request operation expired.'), level=messages.ERROR)
    else:
        message_user(request, message=_('Request operation not available.'), level=messages.ERROR)

def signup(request):
    AGREE_TERMS = 'agree_terms'
    title = _("Sign up")
    subtitle = _("Create new account")
    if request.method == 'POST':
        if 'action' in request.POST:
            action = get_req_value(request,"action")
            if action == 'agree':
                request.session[AGREE_TERMS] = 'yes'
                return template_response(request, 'landing/account/register.html', locals())

        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        next = request.POST.get('next')
        lang = request.POST.get('lang')
        try:
            if User.objects.filter(email=email).exists():
                raise ValueError(_("Email already been used by others"))
            user = Account(request).register_user(username,email,password, lang=lang)

            # Auto login the user
            auth_user = authenticate(username=username,password=password)
            auth_login(request,auth_user)
            return redirect('account_result')
        except Exception as ex:
            if ex.args[0] == 1062:
                error = _('The account has been used.')
            else:
                error = ex
            return template_response(request, 'landing/account/register.html', locals())
    else:
        site = get_site(request)
        if not (AGREE_TERMS in request.session) and site.terms:
            subtitle = _("Agree terms and conditions")
            return template_response(request, 'landing/account/agree.html', locals())
        else:
            return template_response(request, 'landing/account/register.html',locals())

def check_email_available(request):
    if request.method != 'POST':
        return JsonResponse(get_action_result(False, 'Post needed'))

    if 'email' in request.POST:
        exist = User.objects.filter(email=request.POST.get("email")).exists()
        if exist:
            return JsonResponse(get_action_result(False,'Email exists'))
        else:
            return JsonResponse(get_action_result(True, 'Email available'))
    else:
        return JsonResponse(get_action_result(False, 'Email not in request'))

def generate_random_password(request):
    return JsonResponse(get_action_result(True, get_random_string(6)))

def result(request):
    title = _("Operation Result")
    return template_response(request,'landing/account/result.html',locals())


@login_required()
def settings(request):
    return template_response(request, 'landing/account/settings.html', locals())

@login_required()
def profile(request):
    profile = get_profile(request)
    if profile and profile.addresses.count() > 0:
        address = profile.addresses.all()[0]

    if request.method == 'POST':
        if not profile:
            profile = generate_profile(request.user,need_active=False)
        if "lastname" in request.POST:
            profile.user.first_name = get_req_value(request,"firstname")
            profile.user.last_name = get_req_value(request,"lastname","")
            profile.user.save()
            profile.gender = get_req_value(request, "gender", "U")
            profile.auth_mobile = get_req_value(request, "auth_mobile")
            profile.message = get_req_value(request, "message")
        new = False
        if profile.addresses.count() > 0:
            address = profile.addresses.all()[0]
        else:
            new = True
            address = Address()
        address.firstname = get_req_value(request,"firstname")
        address.city = get_req_value(request,"city")
        address.suburb = get_req_value(request,"suburb")
        address.state = get_req_value(request,"state")
        address.address = get_req_value(request,"street")
        address.country = get_req_value(request, "country")
        address.save()
        if new:
            profile.addresses.add(address)
        profile.account_facebook = get_req_value(request,"facebook")
        profile.account_twitter = get_req_value(request, "twitter")
        profile.account_wechat = get_req_value(request, "wechat")
        profile.account_googleplus = get_req_value(request, "googleplus")

        if 'picture_status' in request.POST and request.POST.get("picture_status") == "deleted":
            profile.picture = None
        else:
            picture_url = save_file_to_media(request,'picture')
            if picture_url:
                profile.picture = picture_url
        profile.save()
        messages.success(request,'Save profile successfully.')
        return redirect(reverse('account_index'))

    return template_response(request, 'landing/account/profile.html',locals())

@login_required()
def balance(request):
    return template_response(request, 'landing/account/balance.html',locals())


@login_required()
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, _('Your password was successfully updated!'))
            return redirect('account_index')
        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        form = PasswordChangeForm(request.user)
    return template_response(request, 'landing/account/change_password.html',{'form': form})

@login_required()
def message(request):
    mm = MessageManager(request)
    if request.method == 'POST':
        if 'action' in request.POST:
            action = request.POST.get('action')
            if action == 'delete':
                ids = request.POST.getlist("items[]")
                if len(ids) > 0:
                    d = mm.delete(ids=ids)
                    if d[0] == 0:
                        message_user(request, _("Delete messages fail. No items deleted. "), level=messages.ERROR)
                    elif d[0] < len(ids):
                        message_user(request, _("$(count)s items deleted. Some items have not been deleted" % (d[0])), level=messages.WARNING)
                    else:
                        message_user(request,_("Delete messages successfully"),level=messages.SUCCESS)
                else:
                    message_user(request, _("You have not selected items."), level=messages.WARNING)
            elif action == 'clean':
                d = mm.clean()
                if d[0] == 0:
                    message_user(request, _("Clean messages fail. No items deleted. "), level=messages.ERROR)
                else:
                    message_user(request, _("Clean messages successfully"), level=messages.SUCCESS)
    result = get_pager(mm.get_messages(),request.POST.get('p',0))
    result.update(locals())
    result.__delitem__('result')
    return template_response(request, 'landing/account/messages.html',result)

@login_required()
def message_detail(request,id):
    if request.method == 'POST':
        if 'back' in request.POST:
            if 'return' in request.POST:
                return redirect(request.POST.get('return'))
            else:
                return redirect(reverse('account_message'))
    else:
        if id and id != 'None' and id.isdigit():
            mm = MessageManager(request)
            message = mm.get_message(int(id))
            if not message.read_time and not message.is_system:
                message.read_time = timezone.now()
                message.ip = request.META['REMOTE_ADDR']
                message.save()
        return template_response(request, 'landing/account/message_details.html',locals())

@login_required()
def account_header(request):
    um = UserManager(request)
    mm = MessageManager(request)
    messages,stat = mm.get_messages(count=3,stat=True)
    profile = um.get_profile()
    return template_response(request,"landing/account/inc_account_summury.html",locals())
