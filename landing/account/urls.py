"""digger URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# noinspection PyPackageRequirements
from django.conf.urls import url, include
# noinspection PyPackageRequirements
from django.contrib import admin
# noinspection PyPackageRequirements
from django.contrib.auth import views as authViews
import landing.account.views as views

urlpatterns = [
    url(r'^index/', views.index, name="account_index"),
    url(r'^login/$', authViews.LoginView.as_view(), name='account_login'),
    url(r'^logout/', views.landing_logout, name="account_logout"),
    url(r'^signup/$', views.signup, name="account_signup"),
    url(r'^result/$', views.result, name="account_result"),
    url(r'^settings/$', views.settings, name="account_settings"),
    url(r'^profile/$', views.profile, name="account_profile"),
    url(r'^balance/$', views.balance, name="account_balance"),
    url(r'^change_password/$', views.change_password, name="account_change_password"),
    url(r'^messages/$', views.message, name="account_message"),
    url(r'^messages/(?P<id>\w+)$', views.message_detail, name='account_message_detail'),

    url(r'active_account/(?P<id>\w+)$', views.active_account, name='active_account'),
    url(r'resend_verify_email?$', views.resend_verify_email, name='resend_verify_email'),
    url(r'check_email$', views.check_email_available, name='check_email'),
    url(r'generate_password$', views.generate_random_password, name='generate_password'),
]
