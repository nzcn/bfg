from __future__ import division
from datetime import *
from django import template
from landing.core.common import *
from landing.models import ContentType, Post
from django.core.urlresolvers import reverse
from django.contrib.messages import add_message, get_messages
from django.template import Template, loader
from django.db.models import Q
from django.db.models.query import QuerySet
from django.utils.translation import gettext as _, ngettext

register = template.Library()


@register.filter
def startwith(value, prefix):
    result = (str)(value).startswith(prefix)
    if result:
        return True
    else:
        return False


@register.filter
def div(value, arg):
    '''
    Divides the value; argument is the divisor.
    Returns empty string on any error.
    '''
    try:
        value = int(value)
        arg = int(arg)
        if arg: return value / arg
    except:
        pass
    return ''


@register.filter
def divf(value, arg):
    '''
    Divides the value; argument is the divisor.
    Returns empty string on any error.
    '''
    try:
        value = value
        arg = arg
        if arg: return value / arg
    except:
        pass
    return ''


@register.simple_tag
def current_time(format_string):
    return timezone.now().strftime(format_string)


@register.simple_tag
def ct_url(ct):
    if ct and type(ct) is ContentType:
        if ct.href:
            return ct.href
        elif ct.url_name:
            try:
                if ct.url_param1:
                    if ct.url_param4:
                        return reverse(ct.url_name, args=[ct.url_param1, ct.url_param2, ct.url_param3, ct.url_param4])
                    elif ct.url_param3:
                        return reverse(ct.url_name, args=[ct.url_param1, ct.url_param2, ct.url_param3])
                    elif ct.url_param2:
                        return reverse(ct.url_name, args=[ct.url_param1, ct.url_param2])
                    else:
                        return reverse(ct.url_name, args=[ct.url_param1, ])
                else:
                    return reverse(ct.url_name)
            except Exception as ex:
                print('Content type {ct} URL pattern {url} is not right. Exception:{ex}'.format(
                    ct=ct.title, url=ct.url_name, ex=ex))
                return 'error'
        else:
            return '#' + ct.key
    else:
        return '-'


@register.simple_tag
def loop_val(values, index):
    if isinstance(values, basestring):
        vals = values.split(",")
    elif isinstance(values, list):
        vals = values
    else:
        vals = [values]
    if index < len(vals):
        return vals[index]
    else:
        return vals[index % len(vals)]


@register.simple_tag(takes_context=True)
def var(context, key):
    return get_variable(key, context=context)


@register.simple_tag(takes_context=True)
def html(context, content):
    return get_html(content, context=context)


@register.simple_tag(takes_context=True)
def menu_active(context, link, active_class='active'):
    request = context['request']
    if type(link) is ContentType:
        if 'cat1' in context and context['cat1'] == link.key:
            return active_class
        else:
            url = ct_url(link)
    else:
        url = link
    if url == request.path:
        return active_class
    else:
        return ''


@register.simple_tag(takes_context=True)
def check_active(context, param_name, value, active_class='active'):
    request = context['request']
    if param_name in request.GET and request.GET.get(param_name) == value:
        return active_class
    else:
        return ''


@register.simple_tag(takes_context=True)
def cookie(context, value, cookie_key, default=''):
    if value:
        return value
    else:
        request = context['request']
        if cookie_key in request.COOKIES:
            return request.COOKIES.get(cookie_key)
        else:
            return default


@register.simple_tag(takes_context=True)
def reqval(context, key, default=''):
    request = context['request']
    if key in request.GET:
        return request.GET.get(key)
    else:
        if key in request.POST:
            return request.POST.get(key)
        else:
            return default


@register.simple_tag(takes_context=True)
def post_url(context, post):
    if type(post) is Post:
        if if_exists('cat2', context) and if_exists('cat1', context) in context:
            url = reverse('landing_post', kwargs={'cat1': context['cat1'], 'cat2': context['cat2'], 'id': post.id})
        elif if_exists('cat1', context):
            url = reverse('landing_post', kwargs={'cat1': context['cat1'], 'id': post.id})
        elif if_exists('cat', context):
            url = reverse('landing_post', kwargs={'cat1': context['cat'], 'id': post.id})
        else:
            url = ''
    elif type(post) is str:
        try:
            page = int(post)
        except:
            return '#'

        if if_exists('cat2', context) and if_exists('cat1', context) in context:
            url = reverse('landing_posts', kwargs={'cat1': context['cat1'], 'cat2': context['cat2'], 'page': page})
        elif if_exists('cat1', context):
            url = reverse('landing_posts', kwargs={'cat1': context['cat1'], 'page': page})
        elif if_exists('cat', context):
            url = reverse('landing_posts', kwargs={'cat1': context['cat'], 'page': page})
        else:
            url = ''
    else:
        url = ''
    return url


@register.simple_tag(takes_context=True)
def alert(context):
    t = loader.get_template('landing/include/inc_messages.html')
    messages = get_messages(context.request)
    return t.render({'messages': messages})


@register.simple_tag(takes_context=True)
def relative_url(context, value, field_name, urlencode=None):
    if not urlencode:
        urlencode = context.request.GET.urlencode()
    url = '?{}={}'.format(field_name, value)
    if urlencode:
        querystring = urlencode.split('&')
        filtered_querystring = filter(lambda p: p.split('=')[0] != field_name, querystring)
        encoded_querystring = '&'.join(filtered_querystring)
        url = '{}&{}'.format(url, encoded_querystring)
    return url


@register.simple_tag
def stars(value):
    t = loader.get_template('landing/include/inc_rate_score.html')
    scores = []
    for i in range(1, 6):
        if value < i:
            delta = i - value
            if delta <= 0.5:
                scores.append(0.5)
            else:
                scores.append(0)
        else:
            scores.append(1)
    return t.render({'scores': scores})


@register.simple_tag
def country_flag(code):
    if code == 'zh-hans':
        flag = 'cn'
    elif code == 'en':
        flag = 'us'
    elif code == 'ko':
        flag = 'kr'
    else:
        flag = 'nz'
    return "assets/base/img/flags-mini/{country}.png".format(country=flag)


# Filters ----------------------


@register.filter('none_empty')
def none_empty(value):
    return '' if not value else value


@register.filter('null_empty')
def null_empty(value):
    return '' if not value or value.lower() == 'null' else value


@register.filter('line2br')
def line2br(value):
    return value.replace('\r\n', '<br/>')


@register.filter
def none_default(value, default):
    return default if not value else value


@register.filter
def replace(value, arg):
    """
    Replacing filter
    Use `{{ "aaa"|replace:"a|b" }}`
    """
    if len(arg.split('|')) != 2:
        return value

    what, to = arg.split('|')
    return value.replace(what, to)


@register.filter
def rel_now(value):
    if isinstance(value, datetime):
        now = timezone.now()
        if value > now:
            return str((value - now).days) + _(" days left")
        else:
            return str((value - now).days * -1) + _(" days passed")
    else:
        return value


@register.filter('range')
def get_range(value):
    return range(value) if value else []


@register.filter('bool_checked')
def bool_checked(value):
    return 'checked' if value else ''


@register.filter('multiply')
def multiply(value, arg):
    if not value:
        return 0
    else:
        return value * arg


@register.filter
def timepassed(value, format='d'):
    if value:
        td = timezone.now() - value
        if format == 'd':
            return td.days
        elif format == 'h':
            return td.seconds / 3600
        elif format == 'm':
            return td.seconds / 60
        else:
            return td.microseconds
    else:
        return None


@register.filter('zero_default')
def zero_default(value, default):
    return default if value == 0 or not value else value


@register.filter(is_safe=False)
def add_float(value, arg):
    """Adds the arg to the value."""
    try:
        return value + arg
    except (ValueError, TypeError):
        try:
            return value + arg
        except Exception:
            return ''


@register.filter
def indexof(list, index):
    if index >= len(list) or index < 0:
        return None
    else:
        return list[index]


@register.filter
def contains(parent, sub):
    if parent:
        return sub in parent
    else:
        return False


@register.filter('posts')
def posts(value, count=0, order='pos'):
    if value and type(value) is ContentType:
        filter = Q(type=value) & get_lang_filter()
        if count > 0:
            return Post.objects.filter(filter).order_by(order)[:count]
        else:
            return Post.objects.filter(filter).order_by(order)
    else:
        return ""


@register.filter()
def tags(value):
    tags = []
    if value and (isinstance(value, list) or isinstance(value, QuerySet)):
        for post in value:
            if isinstance(post, Post):
                for tag in post.tags.all():
                    if not tag in tags:
                        tags.append(tag)
    elif isinstance(value, Post):
        for tag in value.tags.all():
            if not tag in tags:
                tags.append(tag)
    return tags


@register.filter
def mmcontains(value, target):
    fld = None
    for field in value.model._meta.fields:
        if field.related_model == target._meta.model:
            fld = field
            break
    for obj in value:
        if getattr(obj, fld.name) == target:
            return True
    return False


@register.filter
def url_remove_param(value, key):
    from urllib.parse import parse_qs, urlparse
    parsed_url = urlparse(value)
    qs = parse_qs(parsed_url.query)
    if key in qs.keys():
        del qs[key]
    url = parsed_url.scheme + "://" + parsed_url.netloc + parsed_url.path + "?"
    for key in qs:
        url += '{key}={value}&'.format(key=key, value=qs[key][0])
    return url.rstrip('&')


@register.filter
def percentage(value, str_format=".2%"):
    return format(value, str_format)


@register.filter(is_safe=False)
def modby(value, arg):
    """Returns True if the value is divisible by the argument."""
    return int(value) % int(arg)


@register.filter('foreign_values')
def foreign_values(obj, key):
    field = obj._meta.get_field(key)
    if field.related_model:
        return field.related_model.objects.all().order_by('pos', 'id')
    elif field.choices:
        return turple2array(field.choices)


@register.filter(is_safe=False)
def class_name(value):
    return value.__class__.__name__


@register.filter(is_safe=False)
def key_value(value, key, default_value=None):
    if "." in key:
        keys = key.split(".")
        v = value
        for k in keys:
            v = key_value(v, k)
            if not v:
                return default_value
        return v
    else:
        if isinstance(value, object) and hasattr(value, key):
            return getattr(value, key, default_value)
        elif isinstance(value, dict) and key in value:
            return value[key]
        else:
            return default_value


@register.filter(is_safe=False)
def json_dumps(value):
    try:
        return json.dumps(value)
    except:
        return ""


@register.filter(is_safe=False)
def json_loads(value):
    try:
        return json.loads(value)
    except:
        return []


@register.filter(is_safe=False)
def contact(value, key=None, default_value='0', seperator=','):
    if isinstance(value, list):
        if key:
            result = []
            for v in value:
                value = key_value(v, key, default_value)
                result.append(str(value))
            return seperator.join(result)
        else:
            return seperator.join(value)
    else:
        return value


@register.filter(name='has_group')
def has_group(user, group_name):
    if ',' in group_name:
        groups = group_name.split(',')
        return user.groups.filter(name__in=groups).exists()
    else:
        return user.groups.filter(name=group_name).exists()
