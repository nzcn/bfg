# noinspection PyPackageRequirements
from django.test import TestCase,SimpleTestCase
from django.test.runner import DiscoverRunner
from landing.core.common import *
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory,APIClient

class NoDbTestRunner(DiscoverRunner):
  """ A test runner to test without database creation """

  def setup_databases(self, **kwargs):
    """ Override the database creation defined in parent class """
    pass

  def teardown_databases(self, old_config, **kwargs):
    """ Override the database teardown defined in parent class """
    pass

class LandingTest(TestCase):
    def test_slack(self):
        self.assertTrue(push_slack('Test Server Message', username='Test Useer'))


class LandingApiTest(APITestCase):
    server = '127.0.0.1:8098'
    user = {
        'admin':{
            'username':'markyin',
            'password': 'Pass@word1',
        }
    }

    def login(self,role):
        self.client = APIClient(HTTP_HOST=self.server)
        admin = self.user[role]
        self.client.login(username=admin['username'], password=admin['password'])

    def test_post(self):
        self.login('admin')
        response = self.client.get("/api/posts/")
        self.assertEqual(response.status_code,status.HTTP_200_OK)

    def test_profile(self):
        self.login('admin')
        response = self.client.get("/api/profiles/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data[0]
        self.assertTrue('credit' in data,msg='credit not in profile')
        self.assertTrue('addresses' in data)
        self.assertTrue('user' in data)
        # change profile
        account_qq = data['account_qq']
        new_value = 'test'
        id = data['id']
        response_detail = self.client.patch("/api/profiles/{id}/".format(id=id),data={'account_qq':new_value})
        self.assertEqual(response_detail.data['account_qq'],new_value)

        response_detail = self.client.put("/api/profiles/{id}/".format(id=id), data={'account_qq': account_qq})
        self.assertEqual(response_detail.status_code,status.HTTP_400_BAD_REQUEST,msg='PUT is not supported')
        response_detail = self.client.post("/api/profiles/{id}/".format(id=id), data={'account_qq': account_qq})
        self.assertEqual(response_detail.status_code, status.HTTP_405_METHOD_NOT_ALLOWED, msg='POST is not supported')

        # Change Back
        response_detail = self.client.patch("/api/profiles/{id}/".format(id=id), data={'account_qq': account_qq})
        if account_qq:
            self.assertEqual(response_detail.data['account_qq'], account_qq)
        else:
            self.assertEqual(response_detail.data['account_qq'],'None')

        # Edit Not
        response_detail = self.client.patch("/api/profiles/{id}/".format(id=id), data={'credit': new_value})
        self.assertFalse('credit' in response_detail.data)

    def test_address(self):
        self.login('admin')
        response = self.client.get("/api/profiles/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data[0]
        addresses = data['addresses']
        # Add New Address
        response_address = self.client.patch("/api/profiles/edit_address/",data={'address':'Test Address',
                                                                               'suburb':'test suburb',
                                                                               })
        self.assertEqual(len(response_address.data['addresses']),len(addresses) + 1)
        # Delte the New Address



    def test_login(self):
        pass

    def test_address_geo(self):
        result = address_geo('9 Galaxy Dr, Mairangi Bay')
        print(result)
