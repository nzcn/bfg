# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from bfg.appa.admin_classes import *

# Register your models here.
admin.site.register(Staff, StaffAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Printer, PrinterAdmin)
