# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase,SimpleTestCase
from bfg.appa.core.common import print_label
from bfg.appa.models import *

# Create your tests here.
class TestAppa(TestCase):

    def test_print(self):
        printer = Printer.objects.first()
        context = {'code':'123456'}
        print_label(printer,"appa/back/label/test.html",context)