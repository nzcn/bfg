# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.conf import settings
from bfg.appa.controllers.core import get_staff_company

# Create your views here.
@login_required(login_url='/login/')
def index(request):
    return render(request, 'appa/index.html')

@login_required()
def sidebar(request):
    # pm = PlanManager(request)
    # profile = get_user_profile(request)
    company = get_staff_company(request)
    # plan = pm.get_user_plan(company.administrator)
    # is_admin = is_user_admin(request,company)
    site = settings.APPA_SITE
    return render(request, 'appa/includes/sidebar.html', locals())