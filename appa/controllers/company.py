# noinspection PyPackageRequirements
import qrcode
import os
from django.db import transaction
from django.shortcuts import render, reverse
from landing.core.common import get_action_result, get_req_value, check_dir
from django.utils.crypto import get_random_string
from django.db.models import Q
from bfg.appa.models import *
from store.models import *
from bfg.appa.controllers.core import *


class AppaCompanyManager(object):
    def __init__(self, request=None):
        self.request = request

    def create_company(self, data):
        # Create the company
        company = Company()
        company.title = data['title']
        company.save()

    def get_company(self, id=0):
        if id == 0:
            return get_staff_company(self.request)
        else:
            try:
                return get_company_class().objects.get(id=id)
            except:
                return None

    def get_company_by_homepage(self):
        '''
        Find Company from headers in Wechat App Access
        :param use_default:
        :return:
        '''
        company = None
        if self.request:
            if 'HTTP_ORIGIN' in self.request.META:
                host = self.request.META['HTTP_ORIGIN']
                qs = get_company_class().objects.filter(homepage__icontains=host)
                if qs.exists():
                    company = qs.first()
            elif 'HTTP_HOST' in self.request.META:
                host = self.request.META['HTTP_HOST']
                qs = get_company_class().objects.filter(homepage__icontains=host)
                if qs.exists():
                    company = qs.first()
        return company

    def set_company(self, company):
        if not company:
            company = get_staff_company(self.request)
        return company

    def get_staffs(self, company=None):
        company = self.set_company(company)
        return Staff.objects.filter(company=company)

    def get_files(self, path):
        from os import listdir
        from os.path import isfile, join
        files = [f for f in listdir(path) if isfile(join(path, f))]
        return files

    def sync_mail_template(self, template_path):
        filename = os.path.basename(template_path)
        parts = os.path.splitext(filename)
        if len(parts) == 2 and parts[1] == '.html':
            filenames = parts[0].split('-')

    def send_resetpwd_mail(self, email):
        from landing.core.account import Account
        if self.request.user.is_authenticated:
            customer = self.request.user
        else:
            customers = AdminModels.User.objects.filter(email=email)
            if customers.exists():
                customer = customers.first()
            else:
                raise Exception('User not found.')
        am = Account(self.request)
        am.send_resetpwd_email(customer, mail_account=self.get_mail_account())
        return True

    def get_mail_account(self, company=None):
        if not company:
            company = self.get_company_by_homepage()
        else:
            if not isinstance(company, Company):
                raise Exception('Company is not available')
        if company and company.mailaccount_set.exists():
            return company.mailaccount_set.first()
        else:
            settings = get_bfg_settings('mail')
            return MailAccount(cls=settings['provider'],
                               url=settings['settings']['url'],
                               key=settings['settings']['key'],
                               user=settings['settings']['sender'])

    def get_customer(self, id):
        try:
            return Customer.objects.get(id=id)
        except:
            return None

    def get_variable(self, company, key):
        key = key.upper()
        try:
            variable = company.variables.get(key=key)
            return variable.content
        except:
            return None
