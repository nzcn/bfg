# noinspection PyPackageRequirements
from django.db.models import Q
from bfg.appa.core.model import AppaAdmin,AppaForm
from bfg.appa.models import Company
from landing.core.common import *
from django.core.exceptions import PermissionDenied
from django.utils.translation import ugettext_lazy as _
from django.template import RequestContext
from functools import wraps
from django.shortcuts import render_to_response

USER_COMPANY = "USER_COMPANY"

def get_company_class():
    class_path = get_bfg_settings("company_class","bfg.appa.models.Company")
    return get_class_by_path(class_path)


def get_staff_companies(request):
    if request:
        user = request.user
        if request.user.is_authenticated:
            companies = get_company_class().objects.filter(Q(administrator=user) | Q(staffs=user))
            return companies
        else:
            raise PermissionDenied()
    else:
        return get_company_class().objects.none()



def get_customer_companies(request):
    user = request.user
    companies = get_company_class().objects.filter(users__in=[user])
    return companies



def get_staff_company(request):
    companies = get_staff_companies(request)
    if companies.exists():
        company = companies.first()
    else:
        company = None
    return company


def get_staff_company_id(request):
    if not USER_COMPANY in request.session:
        company = get_staff_company(request)
        if company:
            request.session[USER_COMPANY] = company.id
            return company.id
        else:
            return None
    else:
        return request.session[USER_COMPANY]


class CompanyBaseAdmin(AppaAdmin):
    company_filter_fields = []
    company_none_filter_fields = []
    show_none_data = False
    is_user = False
    '''
    Model saved by company
    '''
    def get_custom_filter(self,request=None):
        # Check whether the model has company property
        if hasattr(self.model,'company'):
            self.company = get_staff_company(request)
            if self.show_none_data:
                return Q(company=self.company) | Q(company=None)
            else:
                return Q(company=self.company)
        else:
            return None

    def get_field_query(self, db, db_field, request):
        return None

    def get_field_customized_queryset(self,db, db_field, request):
        return None

    def get_field_queryset(self, db, db_field, request):
        """
        If the ModelAdmin specifies ordering, the queryset should respect that
        ordering.  Otherwise don't specify the queryset, let the field decide
        (returns None in that case).
        """
        result = self.get_field_customized_queryset(db, db_field, request)
        if not result:

            # Using Customized Query
            q = self.get_field_query(db, db_field, request)
            if not q and (db_field.name in self.company_none_filter_fields or db_field.name in self.company_filter_fields) \
                    and hasattr(db_field.related_model,'company'):
                if db_field.name in self.company_none_filter_fields:
                    q = Q(company=get_staff_company(request)) | Q(company = None)
                elif  db_field.name in self.company_filter_fields:
                    q = Q(company=get_staff_company(request))
                else:
                    q = None

            if q:
                result = db_field.remote_field.model._default_manager.using(db).filter(q)
            else:
                result = db_field.remote_field.model._default_manager.using(db)

        # Reorder the result
        related_admin = self.admin_site._registry.get(db_field.remote_field.model)
        if related_admin is not None:
            ordering = related_admin.get_ordering(request)
            if ordering is not None and ordering != ():
                result = result.order_by(*ordering)

        return result


    def pre_save(self, request, obj, form, change):
        pass

    def after_save(self, request, obj, form, change):
        pass

    def save_model(self, request, obj, form, change):
        if not change:
            obj.company = get_staff_company(request)
        if self.is_user:
            password = form.cleaned_data['password']
            if password:
                obj.set_password(password)
            else:
                obj.password = form.initial['password']
        self.pre_save(request,obj,form,change)
        obj.save()
        self.after_save(request, obj, form, change)