import xlwt
import xlrd
from django.http import HttpResponse,FileResponse

title_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;font: colour white, bold True;')
header_style = xlwt.easyxf('font: bold True;')
text_style = xlwt.easyxf('align: wrap yes')
number_style = xlwt.easyxf('align: wrap yes')

def get_excel_response(filename):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename={filename}'.format(filename=filename)
    return response

def get_workbook():
    return xlwt.Workbook(encoding='utf-8')

def get_column_index(columns, key):
    result = 0
    for column in columns:
        if column[0] == key:
            return result
        result += 1
    return -1