from __future__ import unicode_literals

import copy
from collections import OrderedDict
from functools import partial

from django.contrib.admin import ModelAdmin
from django.contrib.admin.filters import SimpleListFilter, ListFilter
from django.contrib.admin.options import get_content_type_for_model
from functools import partial, reduce, update_wrapper
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.contrib.admin.utils import (
    flatten_fieldsets,
    quote,
    unquote,
)
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper
from django.core.exceptions import (
    FieldError, )
from django.core.exceptions import (
    FieldDoesNotExist, FieldError, PermissionDenied, ValidationError,
)
from django.core.files.uploadedfile import UploadedFile
from django.forms import *
from django.forms.models import (
    modelform_defines_fields,
    modelform_factory, )
from django.contrib.admin import helpers
from django.http import HttpResponseRedirect
from django.utils import six
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.http import urlencode, urlquote
from django.utils.translation import ugettext as _
from django.views.generic import RedirectView
from django.contrib.admin.models import LogEntry
from django.contrib.admin.options import csrf_protect_m, IncorrectLookupParameters, SimpleTemplateResponse, ungettext
from django.contrib.contenttypes.models import ContentType, ContentTypeManager
from bfg.appa.templatetags.appa_urls import add_preserved_filters
from landing.core.common import *
from datetime import *

IS_POPUP_VAR = '_popup'
TO_FIELD_VAR = '_to_field'


def freeview(name):
    def decorator(view_func):
        def wrapped_func(request, *args, **kwargs):
            print("Running {view_func.__name__} with name={name}")
            response = view_func(request, qs="Hello, World!", *args, **kwargs)
            return response

        return wrapped_func

    return decorator


class AppaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AppaForm, self).__init__(*args, **kwargs)
        try:
            self.init(len(args) == 0)
            for visible in self.visible_fields():
                if 'class' in visible.field.widget.attrs:
                    visible.field.widget.attrs['class'] = 'form-control ' + visible.field.widget.attrs['class']
                else:
                    visible.field.widget.attrs['class'] = 'form-control'
        except Exception as ex:
            print(ex.message)

    def init(self, is_new):
        pass

    def save_related(self, attribute, sort_field=None):
        mm = ModelManager()


class AppaRelatedFieldWrapper(RelatedFieldWidgetWrapper):
    def get_related_url(self, info, action, *args):
        return reverse("appa:%s_%s_%s" % (info + (action,)),
                       current_app=self.admin_site.name, args=args)


class AppaAdmin(ModelAdmin):
    form = AppaForm

    change_list_template = 'appa/change_list.html'
    report_template = 'appa/report.html'
    change_form_template = 'appa/change_form.html'
    add_form_template = 'appa/change_form.html'
    object_history_template = 'appa/object_history.html'
    delete_confirmation_template = 'appa/delete_confirmation.html'
    delete_selected_confirmation_template = 'appa/delete_selected_confirmation.html'

    allow_delete = True
    allow_change = True
    allow_system_delete = False
    allow_system_edit = False
    field_templates_cache = {}
    list_buttons = None
    freeviews = {}

    def __init__(self, model, admin_site):
        # if isinstance(admin_site,AppaSite):
        super(AppaAdmin, self).__init__(model, admin_site)
        self.init_appa()

    def init_appa(self):
        pass

    def template_output(self, template, context):
        if template not in self.field_templates_cache:
            t = loader.get_template(template)
            self.field_templates_cache.update({template: t})
        else:
            t = self.field_templates_cache[template]
        return t.render(context)

    def register_view(self, name, func):
        self.freeviews.update({
            name: func,
        })

    def get_urls(self):
        from django.conf.urls import url

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = [
            url(r'^$', wrap(self.changelist_view), name='%s_%s_changelist' % info),
            url(r'^report/$', wrap(self.report_view), name='%s_%s_report' % info),
            url(r'^export/$', wrap(self.export_view), name='%s_%s_export' % info),
            url(r'^add/$', wrap(self.add_view), name='%s_%s_add' % info),
            url(r'^(.+)/history/$', wrap(self.history_view), name='%s_%s_history' % info),
            url(r'^(.+)/delete/$', wrap(self.delete_view), name='%s_%s_delete' % info),
            url(r'^(.+)/change/$', wrap(self.change_view), name='%s_%s_change' % info),
            url(r'^(.+)/action/$', wrap(self.action_view), name='%s_%s_action' % info),
            url(r'^action/$', wrap(self.action_view_all), name='%s_%s_action' % info),
            # For backwards compatibility (was the change url before 1.9)
            url(r'^(.+)/$', wrap(RedirectView.as_view(
                pattern_name='%s:%s_%s_change' % ((self.admin_site.name,) + info)
            ))),
        ]
        # Add free views
        for key in self.freeviews.keys():
            urlpatterns.insert(0,
                               url(r'^views/{name}/$'.format(name=key), wrap(self.freeviews[key]),
                                   name='%s_%s_view_{name}'.format(
                                       name=key
                                   ) % info))
        return urlpatterns

    def do_action(self, request, object_id, form_url='', extra_context=None):
        t = request.GET.get('type')
        action = request.GET.get('action')
        if action:
            if t:
                method_name = 'action_{type}_{action}'.format(type=t, action=action)
            else:
                method_name = 'action_{action}'.format(type=t, action=action)
            invert_op = getattr(self, method_name, None)
            if callable(invert_op):
                try:
                    if object_id:
                        obj = self.get_object(request, object_id)
                        return invert_op(request, obj)
                    else:
                        obj = None
                        return invert_op(request)
                except Exception as ex:
                    return action_result(False, ex.__str__(), )
            return action_result(False, '{name} not found in class {cls}.'.format(name=method_name,
                                                                                  cls=self.__class__.__name__))
        else:
            return action_result(False, 'Action name not valid in class {cls}.'.format(cls=self.__class__.__name__))

    def action_view(self, request, object_id, form_url='', extra_context=None):
        return self.do_action(request=request, object_id=object_id, form_url=form_url, extra_context=extra_context)

    def action_view_all(self, request, form_url='', extra_context=None):
        return self.do_action(request=request, object_id=None, form_url=form_url, extra_context=extra_context)

    def get_form(self, request, obj=None, **kwargs):
        """
        Returns a Form class for use in the admin add view. This is used by
        add_view and change_view.
        """
        if 'fields' in kwargs:
            fields = kwargs.pop('fields')
        else:
            fields = flatten_fieldsets(self.get_fieldsets(request, obj))
        excluded = self.get_exclude(request, obj)
        exclude = [] if excluded is None else list(excluded)
        readonly_fields = self.get_readonly_fields(request, obj)
        exclude.extend(readonly_fields)
        if excluded is None and hasattr(self.form, '_meta') and self.form._meta.exclude:
            # Take the custom ModelForm's Meta.exclude into account only if the
            # ModelAdmin doesn't define its own.
            exclude.extend(self.form._meta.exclude)
        # if exclude is an empty list we pass None to be consistent with the
        # default on modelform_factory
        exclude = exclude or None

        # Remove declared form fields which are in readonly_fields.
        new_attrs = OrderedDict(
            (f, None) for f in readonly_fields
            if f in self.form.declared_fields
        )
        new_attrs['request'] = request
        form = type(self.form.__name__, (self.form,), new_attrs)

        defaults = {
            "form": form,
            "fields": fields,
            "exclude": exclude,
            "formfield_callback": partial(self.formfield_for_dbfield, request=request),
        }
        defaults.update(kwargs)

        if defaults['fields'] is None and not modelform_defines_fields(defaults['form']):
            defaults['fields'] = ALL_FIELDS

        try:
            return modelform_factory(self.model, **defaults)
        except FieldError as e:
            raise FieldError(
                '%s. Check fields/fieldsets/exclude attributes of class %s.'
                % (e, self.__class__.__name__)
            )

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        """
        Hook for specifying the form Field instance for a given database Field
        instance.

        If kwargs are given, they're passed to the form Field's constructor.
        """
        # If the field specifies choices, we don't need to look for special
        # admin widgets - we just need to use a select widget of some kind.
        if db_field.choices:
            return self.formfield_for_choice_field(db_field, request, **kwargs)

        # ForeignKey or ManyToManyFields
        if isinstance(db_field, models.ManyToManyField) or isinstance(db_field, models.ForeignKey):
            # Combine the field kwargs with any options for formfield_overrides.
            # Make sure the passed in **kwargs override anything in
            # formfield_overrides because **kwargs is more specific, and should
            # always win.
            if db_field.__class__ in self.formfield_overrides:
                kwargs = dict(self.formfield_overrides[db_field.__class__], **kwargs)

            # Get the correct formfield.
            if isinstance(db_field, models.ForeignKey):
                formfield = self.formfield_for_foreignkey(db_field, request, **kwargs)
            elif isinstance(db_field, models.ManyToManyField):
                formfield = self.formfield_for_manytomany(db_field, request, **kwargs)

            # For non-raw_id fields, wrap the widget with a wrapper that adds
            # extra HTML -- the "add other" interface -- to the end of the
            # rendered output. formfield can be None if it came from a
            # OneToOneField with parent_link=True or a M2M intermediary.
            if formfield and db_field.name not in self.raw_id_fields:
                related_modeladmin = self.admin_site._registry.get(db_field.remote_field.model)
                wrapper_kwargs = {}
                if related_modeladmin:
                    wrapper_kwargs.update(
                        can_add_related=related_modeladmin.has_add_permission(request),
                        can_change_related=related_modeladmin.has_change_permission(request),
                        can_delete_related=related_modeladmin.has_delete_permission(request),
                    )
                formfield.widget = AppaRelatedFieldWrapper(
                    formfield.widget, db_field.remote_field, self.admin_site, **wrapper_kwargs
                )
            return formfield

        # If we've got overrides for the formfield defined, use 'em. **kwargs
        # passed to formfield_for_dbfield override the defaults.
        for klass in db_field.__class__.mro():
            if klass in self.formfield_overrides:
                kwargs = dict(copy.deepcopy(self.formfield_overrides[klass]), **kwargs)
                return db_field.formfield(**kwargs)

        # For any other type of field, just call its formfield() method.
        return db_field.formfield(**kwargs)

    def stat_list(self, request, queryset):
        '''
        Override this method to perform static method on the selected method
        :param request:
        :param queryset:
        :return:
        '''
        self.stat = None

    def pre_open(self, request, object_id, form_url='', extra_context=None):
        pass

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.pre_open(request, object_id, form_url, extra_context)
        return super(AppaAdmin, self).change_view(request, object_id, form_url, extra_context)

    def get_paginator(self, request, queryset, per_page, orphans=0, allow_empty_first_page=True):
        self.stat_list(request, queryset)
        return self.paginator(queryset, per_page, orphans, allow_empty_first_page)

    def response_add(self, request, obj, post_url_continue=None):
        """
        Determines the HttpResponse for the add_view stage.
        """
        opts = obj._meta
        pk_value = obj._get_pk_val()
        preserved_filters = self.get_preserved_filters(request)
        obj_url = reverse(
            'appa:%s_%s_change' % (opts.app_label, opts.model_name),
            args=(quote(pk_value),),
            current_app=self.admin_site.name,
        )
        # Add a link to the object's change form if the user can edit the obj.
        if self.has_change_permission(request, obj):
            obj_repr = format_html('<a href="{}">{}</a>', urlquote(obj_url), obj)
        else:
            obj_repr = force_text(obj)
        msg_dict = {
            'name': force_text(opts.verbose_name),
            'obj': obj_repr,
        }
        # Here, we distinguish between different save types by checking for
        # the presence of keys in request.POST.

        if IS_POPUP_VAR in request.POST:
            to_field = request.POST.get(TO_FIELD_VAR)
            if to_field:
                attr = str(to_field)
            else:
                attr = obj._meta.pk.attname
            value = obj.serializable_value(attr)
            popup_response_data = json.dumps({
                'value': six.text_type(value),
                'obj': six.text_type(obj),
            })
            return TemplateResponse(request, self.popup_response_template or [
                'admin/%s/%s/popup_response.html' % (opts.app_label, opts.model_name),
                'admin/%s/popup_response.html' % opts.app_label,
                'admin/popup_response.html',
            ], {
                                        'popup_response_data': popup_response_data,
                                    })

        elif "_continue" in request.POST or (
                # Redirecting after "Save as new".
                "_saveasnew" in request.POST and self.save_as_continue and
                self.has_change_permission(request, obj)
        ):
            msg = format_html(
                _('The {name} "{obj}" was added successfully. You may edit it again below.'),
                **msg_dict
            )
            self.message_user(request, msg, messages.SUCCESS)
            if post_url_continue is None:
                post_url_continue = obj_url
            post_url_continue = add_preserved_filters(
                {'preserved_filters': preserved_filters, 'opts': opts},
                post_url_continue
            )
            return HttpResponseRedirect(post_url_continue)

        elif "_addanother" in request.POST:
            msg = format_html(
                _('The {name} "{obj}" was added successfully. You may add another {name} below.'),
                **msg_dict
            )
            self.message_user(request, msg, messages.SUCCESS)
            redirect_url = request.path
            redirect_url = add_preserved_filters({'preserved_filters': preserved_filters, 'opts': opts}, redirect_url)
            return HttpResponseRedirect(redirect_url)

        else:
            msg = format_html(
                _('The {name} "{obj}" was added successfully.'),
                **msg_dict
            )
            self.message_user(request, msg, messages.SUCCESS)
            return self.response_post_save_add(request, obj)

    def get_preserved_filters(self, request):
        """
        Returns the preserved filters querystring.
        """
        match = request.resolver_match
        if self.preserve_filters and match:
            opts = self.model._meta
            current_url = '%s:%s' % (match.app_name, match.url_name)
            changelist_url = 'appa:%s_%s_changelist' % (opts.app_label, opts.model_name)
            if current_url == changelist_url:
                preserved_filters = request.GET.urlencode()
            else:
                preserved_filters = request.GET.get('_changelist_filters')

            if preserved_filters:
                return urlencode({'_changelist_filters': preserved_filters})
        return ''

    def get_custom_filter(self, request=None):
        return None

    def get_prefetch(self, qs, request=None):
        return qs

    def get_queryset(self, request):
        """
        Returns a QuerySet of all model instances that can be edited by the
        admin site. This is used by changelist_view.
        """
        self.request = request
        q = self.get_custom_filter(request)
        if q:
            qs = self.model._default_manager.filter(q)
        else:
            qs = self.model._default_manager.get_queryset()

        qs = self.get_prefetch(qs, request)
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

    def get_cl_queryset(self, request):
        from django.contrib.admin.views.main import ERROR_FLAG
        opts = self.model._meta
        app_label = opts.app_label
        if not self.has_report_permission(request, None):
            raise PermissionDenied

        ChangeList = self.get_changelist(request)
        try:
            cl = ChangeList(
                request, self.model, None,
                None, None, self.date_hierarchy,
                None, None, self.list_per_page,
                self.list_max_show_all, self.list_editable, self,
            )
        except IncorrectLookupParameters:
            # Wacky lookup parameters were given, so redirect to the main
            # changelist page, without parameters, and pass an 'invalid=1'
            # parameter via the query string. If wacky parameters were given
            # and the 'invalid=1' parameter was already in the query string,
            # something is screwed up with the database, so display an error
            # page.
            if ERROR_FLAG in request.GET.keys():
                return SimpleTemplateResponse('admin/invalid_setup.html', {
                    'title': _('Database error'),
                })
            return HttpResponseRedirect(request.path + '?' + ERROR_FLAG + '=1')
        return cl.queryset

    @csrf_protect_m
    def export_view(self, request, extra_context=None):
        from django.contrib.admin.views.main import ERROR_FLAG
        opts = self.model._meta
        app_label = opts.app_label
        if not self.has_export_permission(request, None):
            raise PermissionDenied

        ChangeList = self.get_changelist(request)
        try:
            cl = ChangeList(
                request, self.model, None,
                None, None, self.date_hierarchy,
                None, None, self.list_per_page,
                self.list_max_show_all, self.list_editable, self,
            )
        except IncorrectLookupParameters:
            # Wacky lookup parameters were given, so redirect to the main
            # changelist page, without parameters, and pass an 'invalid=1'
            # parameter via the query string. If wacky parameters were given
            # and the 'invalid=1' parameter was already in the query string,
            # something is screwed up with the database, so display an error
            # page.
            if ERROR_FLAG in request.GET.keys():
                return SimpleTemplateResponse('admin/invalid_setup.html', {
                    'title': _('Database error'),
                })
            return HttpResponseRedirect(request.path + '?' + ERROR_FLAG + '=1')

        action_failed = False
        selected = request.POST.getlist(helpers.ACTION_CHECKBOX_NAME)

        # Actions with no confirmation
        if request.method == 'POST':
            if selected:
                action_failed = True
            else:
                msg = _("Items must be selected in order to perform "
                        "actions on them. No items have been changed.")
                self.message_user(request, msg, messages.WARNING)
                action_failed = True

        if action_failed:
            # Redirect back to the changelist page to avoid resubmitting the
            # form if the user refreshes the browser or uses the "No, take
            # me back" button on the action confirmation page.
            return HttpResponseRedirect(request.get_full_path())

        context = dict(
            self.admin_site.each_context(request),
            module_name=force_text(opts.verbose_name_plural),
            actions_on_top=self.actions_on_top,
            actions_on_bottom=self.actions_on_bottom,
            actions_selection_counter=self.actions_selection_counter,
            preserved_filters=self.get_preserved_filters(request),
            data=cl.get_queryset(request),
        )
        context.update(extra_context or {})

        request.current_app = self.admin_site.name

        return self.get_export_result(request, context)

    def get_export_result(self, request, context):
        return HttpResponse()

    def get_xls_response(self, filename, sheet='Sheet1', columns=None, rows=None, encode='utf-8'):
        from bfg.appa.core.common import get_xls_response
        return get_xls_response(filename, sheet, columns, rows, encode)

    def import_xls(self, columes, sheet, start_row=0, start_col=0, retry_empty_rows=5):
        from bfg.appa.core.common import import_xls
        return import_xls(columes=columes, sheet=sheet, start_row=start_row, start_col=start_col,
                          retry_empty_rows=retry_empty_rows)

    @csrf_protect_m
    def report_view(self, request, extra_context=None):
        from django.contrib.admin.views.main import ERROR_FLAG
        opts = self.model._meta
        app_label = opts.app_label
        if not self.has_report_permission(request, None):
            raise PermissionDenied

        ChangeList = self.get_changelist(request)
        try:
            cl = ChangeList(
                request, self.model, None,
                None, None, self.date_hierarchy,
                None, None, self.list_per_page,
                self.list_max_show_all, self.list_editable, self,
            )
        except IncorrectLookupParameters:
            # Wacky lookup parameters were given, so redirect to the main
            # changelist page, without parameters, and pass an 'invalid=1'
            # parameter via the query string. If wacky parameters were given
            # and the 'invalid=1' parameter was already in the query string,
            # something is screwed up with the database, so display an error
            # page.
            if ERROR_FLAG in request.GET.keys():
                return SimpleTemplateResponse('admin/invalid_setup.html', {
                    'title': _('Database error'),
                })
            return HttpResponseRedirect(request.path + '?' + ERROR_FLAG + '=1')

        action_failed = False
        selected = request.POST.getlist(helpers.ACTION_CHECKBOX_NAME)

        # Actions with no confirmation
        if request.method == 'POST':
            if selected:
                action_failed = True
            else:
                msg = _("Items must be selected in order to perform "
                        "actions on them. No items have been changed.")
                self.message_user(request, msg, messages.WARNING)
                action_failed = True

        if action_failed:
            # Redirect back to the changelist page to avoid resubmitting the
            # form if the user refreshes the browser or uses the "No, take
            # me back" button on the action confirmation page.
            return HttpResponseRedirect(request.get_full_path())

        context = dict(
            self.admin_site.each_context(request),
            module_name=force_text(opts.verbose_name_plural),
            actions_on_top=self.actions_on_top,
            actions_on_bottom=self.actions_on_bottom,
            actions_selection_counter=self.actions_selection_counter,
            preserved_filters=self.get_preserved_filters(request),
            data=self.get_report_queryset(cl.queryset),
        )
        context.update(extra_context or {})

        request.current_app = self.admin_site.name

        return TemplateResponse(request, self.report_template or [
            '%s/appa/%s/report.html' % (app_label, opts.model_name),
        ], context)

    def get_report_queryset(self, qs):
        return qs

    def _get_obj_does_not_exist_redirect(self, request, opts, object_id):
        """
        Create a message informing the user that the object doesn't exist
        and return a redirect to the admin index page.
        """
        msg = _("""%(name)s with ID "%(key)s" doesn't exist. Perhaps it was deleted?""") % {
            'name': force_text(opts.verbose_name),
            'key': unquote(object_id),
        }
        self.message_user(request, msg, messages.WARNING)
        url = reverse('appa:index', current_app=self.admin_site.name)
        return HttpResponseRedirect(url)

    def response_post_save_add(self, request, obj):
        """
        Figure out where to redirect after the 'Save' button has been pressed
        when adding a new object.
        """
        opts = self.model._meta
        if self.has_change_permission(request, None):
            post_url = reverse('appa:%s_%s_changelist' %
                               (opts.app_label, opts.model_name),
                               current_app=self.admin_site.name)
            preserved_filters = self.get_preserved_filters(request)
            post_url = add_preserved_filters({'preserved_filters': preserved_filters, 'opts': opts}, post_url)
        else:
            post_url = reverse('admin:index',
                               current_app=self.admin_site.name)
        return HttpResponseRedirect(post_url)

    def response_post_save_change(self, request, obj):
        """
        Figure out where to redirect after the 'Save' button has been pressed
        when editing an existing object.
        """
        opts = self.model._meta

        if self.has_change_permission(request, None):
            post_url = reverse('appa:%s_%s_changelist' %
                               (opts.app_label, opts.model_name),
                               current_app=self.admin_site.name)
            preserved_filters = self.get_preserved_filters(request)
            post_url = add_preserved_filters({'preserved_filters': preserved_filters, 'opts': opts}, post_url)
        else:
            post_url = reverse('appa:index',
                               current_app=self.admin_site.name)
        return HttpResponseRedirect(post_url)

    def get_view_on_site_url(self, obj=None):
        if obj is None or not self.view_on_site:
            return None

        if callable(self.view_on_site):
            return self.view_on_site(obj)
        elif self.view_on_site and hasattr(obj, 'get_absolute_url'):
            # use the ContentType lookup if view_on_site is True
            return reverse('appa:view_on_site', kwargs={
                'content_type_id': get_content_type_for_model(obj).pk,
                'object_id': obj.pk
            })

    def has_add_permission(self, request):
        return True

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        else:
            if hasattr(obj, 'is_system'):
                return not getattr(obj, 'is_system')
            else:
                return self.allow_change

    def has_report_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        else:
            return True

    def has_export_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        else:
            return True

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True and self.allow_delete
        else:
            if hasattr(obj, 'is_system'):
                return not getattr(obj, 'is_system')
            else:
                return self.allow_delete

    def has_module_permission(self, request):
        return True


class ModelManager(object):
    def __init__(self, request, object_class):
        self.request = request
        self.object_class = object_class
        self.admin_class = None

    def set_admin_class(self, v):
        self.admin_class = v

    def get_admin_class(self):
        return self.admin_class

    def get_object(self, id):
        try:
            if id > 0:
                return self.object_class.objects.get(pk=id)
            else:
                return self.object_class()
        except Exception as ex:
            print(ex)
            return None

    def __set_obj_attr(self, obj, key, value, do_nothing_when_none=False):
        try:
            field = obj._meta.get_field(key)
            if field.auto_created:
                return

            # Process the company field
            if field.name == 'company' or field.name == 'company_id':
                setattr(obj, 'company_id', int(value[0]))

            ftype = field.get_internal_type()
            v = None
            if ftype == 'CharField' or ftype == 'TextField':
                v = value[0]
                if field.null and len(v) == 0:
                    v = None
            elif 'Boolean' in ftype:
                v = value[0] == 'on'
            elif 'Integer' in ftype:
                v = int(value[0])
            elif 'Float' in ftype:
                v = float(value[0])
            elif 'DateTime' in ftype:
                if len(value[0]) == 0:
                    v = None
                else:
                    v = datetime.strptime(value[0], "%Y-%m-%d")
            elif 'ForeignKey' in ftype:
                if len(value) == 1:
                    v = int(value[0])
                    key = field.attname
            if (not v) and do_nothing_when_none:
                pass
            else:
                setattr(obj, key, v)
        except:
            return None

    def __set_obj_list(self, obj, key, list_values):
        self.get_or_create(obj)
        value = getattr(obj, key)
        if value:
            value.clear()
            for v in list_values:
                try:
                    v = int(v)
                    value.add(v)
                except:
                    pass
        else:
            raise ValueError("Can not find {name} field".format(name=key))

    def __set_obj_filelist(self, obj, key, list_values):
        self.get_or_create(obj)
        value = getattr(obj, key)
        if value:
            if len(list_values) > 0:
                value.clear()
                for v in list_values:
                    try:
                        if hasattr(obj, 'file_path'):
                            path = obj.file_path
                        else:
                            path = None
                        is_image = value.model == Image
                        f = self.save_post_file(v, path=path, is_image=is_image)
                        value.add(f)
                    except Exception as ex:
                        print(ex.message)
        else:
            raise ValueError("Can not find {name} field".format(name=key))

    def save_post_file(self, file, name=None, path=None, is_image=False, thumb_size=None):
        '''
        Save HttpPost file
        :param file:
        :param path:
        :param is_image:
        :param thumb_size:
        :return:
        '''
        if is_image:
            f = Image()
        else:
            f = File()
        uf = UploadedFile(file)
        f.size = uf.file.size
        f.title = uf.name
        filename_noext, file_ext = os.path.splitext(uf.name)
        if not path:
            path = ''

        if not name:
            name = u"{filename}_{random}{ext}".format(filename=filename_noext,
                                                      random=get_random_string(length=12),
                                                      ext=file_ext).strip()
        fileurl = path + '/' + name
        filepath = settings.MEDIA_ROOT + fileurl
        while (os.path.exists(filepath)):
            name = "{filename}_{random}{ext}".format(filename=filename_noext,
                                                     random=get_random_string(length=12),
                                                     ext=file_ext)
            fileurl = path + '/' + name
            filepath = settings.MEDIA_ROOT + fileurl

        if not os.path.exists(os.path.dirname(filepath)):
            os.makedirs(os.path.dirname(filepath))
        with open(filepath, 'wb+') as destination:
            for chunk in uf.chunks():
                destination.write(chunk)

        if is_image:
            f.url = fileurl
            img = PLImage.open(filepath)
            width, height = img.size
            f.width = width
            f.height = height
            if thumb_size:
                thumb_url = path + '/' + filename_noext + '_thumb' + file_ext
                thumbpath = settings.MEDIA_ROOT + thumb_url
                img.thumbnail(thumb_size, PLImage.ANTIALIAS)
                img.save(thumbpath)
                f.thumb = thumb_url
        f.save()
        return f

    def get_or_create(self, obj):
        if not obj.id:
            obj.create_time = timezone.now()
            if self.request and self.request.user:
                obj.user = self.request.user
            obj.save()
        return obj

    def save_model(self, m=None, save=True, do_nothing_when_none=False):
        '''
        Save model from request
        :return:
        '''
        if self.request and self.request.method == "POST" and self.object_class:
            if not m:
                m = self.get_model(self.request.POST.get("id"))
            fields = []
            for k, v in self.request.POST.lists():
                if k.startswith("fld_"):
                    # Check weather it is a list:
                    if k.endswith("[]"):
                        list = self.request.POST.getlist(k)
                        k = k.replace("fld_", "").replace("[]", "")
                        self.__set_obj_list(m, k, list)
                    else:
                        k = k.replace("fld_", "")
                        self.__set_obj_attr(m, k, v, do_nothing_when_none=do_nothing_when_none)
                    fields.append(k)

            for k in self.request.FILES:
                if k.startswith("fld_"):
                    if k.endswith("[]"):
                        list = self.request.FILES.getlist(k)
                        k = k.replace("fld_", "").replace("[]", "")
                        self.__set_obj_filelist(m, k, list)
                    else:
                        list = self.request.FILES[k]
                        k = k.replace("fld_", "")
                        self.__set_obj_filelist(m, k, list)
                    fields.append(k)
            if save:
                m.save()
                try:
                    cm = ContentTypeManager()
                    ct = cm.get_for_model(m)
                    LogEntry.objects.log_action(self.request.user.id, content_type_id=ct.id, object_id=m.id,
                                                object_repr=str(m),
                                                action_flag=2, change_message=json.dump(
                            [{"changed": {"fields": fields}}]
                        ))
                except:
                    print("Log model save error")
            return m
        else:
            return None

    def get_model(self, id):
        '''
        Get model from request and id
        :param id:
        :return:
        '''
        try:
            id = int(id)
        except:
            id = None
        return self.get_object(id)

    def get_meta(self):
        if self.object_class:
            class_obj = self.object_class
            app_name = self.object_class._meta.app_label
            class_name = self.object_class._meta.object_name
            if self.admin_class:
                admin_class = self.admin_class
            else:
                admin_class = get_class_by_name(app_name + ".admin_classes", class_name + "Admin")
            # using the default ModelAdmin
            if not admin_class:
                admin_class = get_class_by_name("django.contrib.admin", "ModelAdmin")
            fields = [xs for xs in admin_class.list_display]
            model_fields = admin_class.form.Meta.fields
            return locals()
        else:
            return None


class AppaFilter(SimpleListFilter):
    pass


class AppaInputFilter(SimpleListFilter):
    template = 'appa/input_filter.html'

    def lookups(self, request, model_admin):
        # Dummy, required to show the filter.
        return ((),)

    def choices(self, changelist):
        # Grab only the "all" option.
        all_choice = next(super(AppaInputFilter, self).choices(changelist))
        yield all_choice


class EmptyFilter(ListFilter):
    parameter_name = None
    title = None

    def has_output(self):
        return False
