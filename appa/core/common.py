from django.core.exceptions import *
from bfg.appa.models import Printer
from websocket import create_connection
from landing.core.common import get_html, get_html_from_template, logger, push_slack, check_dir
from django.http import JsonResponse, HttpResponseRedirect, HttpResponseForbidden, HttpResponse
from django.shortcuts import redirect
from django.conf import settings
import json, os, ssl
from datetime import *

try:
    import StringIO

    StringIO = StringIO.StringIO
except Exception:
    from io import StringIO


def print_label(printer, template, context, copy=1):
    '''
    Connect to CloudPrint
    :param printer:
    :param template:
    :param context:
    :return:
    '''
    if not printer or not isinstance(printer, Printer):
        logger.error('Printer not configured')
        return
    ws_uri = "wss://{server}/ws/printer/{sn}/".format(server=printer.server, sn=printer.sn)
    ws = create_connection(ws_uri, header={'username': printer.username, 'password': printer.password},
                           sslopt={"cert_reqs": ssl.CERT_NONE})
    try:
        printer_filename = "_{width}x{length}.html".format(width=printer.width, length=printer.length)
        if not printer_filename in template:
            real_template = template.replace(".html", printer_filename)
            if os.path.exists(os.path.join(settings.BASE_DIR, "packgo/templates/", real_template)):
                template = real_template
            else:
                # Keep using default template name
                logger.error("Label template {filename} not found.".format(filename=real_template))

        message = {'message': get_html('Print label {{ code }} ', context),
                   'task': get_html_from_template(template, context)}
        content = json.dumps(message)
        if copy > 0:
            content = content.replace("PRINT 1", "PRINT {copy}".format(copy=copy))
        ws.send(content)
        result = ws.recv()
        print("Received '%s'" % result)
    except Exception as ex:
        logger.error(ex)
        push_slack('Printer Server Connection Error {err}'.format(err=ex))
    finally:
        if ws:
            ws.close()


def download_pdf(template, context, filename, download=False):
    pdf, result = pdf_to_stream(context, template, stream=StringIO())
    if download:
        type = "attachment"
    else:
        type = "inline"

    if not pdf.err:
        response = HttpResponse(result.getvalue(), content_type='application/pdf')
        response['Content-Disposition'] = '{type}; filename="{f}"'.format(type=type, f=filename)
        return response
    else:
        return HttpResponse('Download PDF Error.' + pdf.err)


def download_txt(template, context, filename, download=False):
    content = get_html_from_template(template, context)
    response = HttpResponse(content=content, content_type='txt/csv')
    type = 'attachment' if download else 'inline'
    response['Content-Disposition'] = '{type}; filename="{f}"'.format(type=type, f=filename)
    return response


def save_pdf_to_file(template, context, filepath, close=True):
    check_dir(filepath)
    file = open(filepath, "w+b")
    pdf_to_stream(context, template, stream=file)
    if close:
        file.close()
    return file


def pdf_to_stream(context, template, stream):
    import xhtml2pdf.pisa as pisa
    from reportlab.pdfbase.ttfonts import TTFont
    from reportlab.pdfbase import pdfmetrics
    from xhtml2pdf.default import DEFAULT_FONT
    pdfmetrics.registerFont(TTFont('yh', '{root}/bfg/appa/static/appa/fonts/msyh.ttf'.format(root=settings.BASE_DIR)))
    DEFAULT_FONT['helvetica'] = 'yh'
    pdf = pisa.CreatePDF(
        StringIO(get_html_from_template(template, context).encode("UTF-8")),
        stream
    )
    return pdf, stream


def get_xls_response(filename, sheet='Sheet1', columns=None, rows=None, encode='utf-8'):
    import xlwt
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename={filename}'.format(
        filename=filename.encode(encode))
    wb = xlwt.Workbook(encoding=encode)
    ws = wb.add_sheet(sheet)
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], font_style)
        ws.col(col_num).width = columns[col_num][1]
    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1

    for row in rows:
        row_num += 1
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response


def save_xls_file(filename, sheet='Sheet1', columns=None, rows=None, encode='utf-8'):
    import xlwt
    wb = xlwt.Workbook(encoding=encode)
    ws = wb.add_sheet(sheet)
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], font_style)
        ws.col(col_num).width = columns[col_num][1]
    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1

    for row in rows:
        row_num += 1
        for col_num in range(len(columns)):
            ws.write(row_num, col_num, row[col_num], font_style)
    filepath = os.path.join(settings.MEDIA_ROOT, filename)
    wb.save(filepath)
    return filepath


def op_result(request, result, title, message=None):
    from django.shortcuts import render, render_to_response
    return render(request, 'op_result.html', locals())


def import_xls(columes, sheet, start_row=0, start_col=0, retry_empty_rows=5):
    '''
    Import Data from Excel
    :param columes:
    :param sheet:
    :param start_row: Start Row
    :param start_col: Start Column
    :return:
    '''

    def minimalist_xldate_as_datetime(xldate, datemode=0):
        # datemode: 0 for 1900-based, 1 for 1904-based
        return (
                datetime(1899, 12, 30)
                + timedelta(days=xldate + 1462 * datemode)
        )

    def process_content(content, column):
        content = str(content)
        if 'remove' in column:
            content = content.replace(column['remove'], '')

        if 'type' in column:
            try:
                if column['type'] == str:
                    pass
                elif column['type'] == int:
                    content = int(content)
                elif column['type'] == float:
                    content = float(content)
                elif column['type'] == datetime:
                    if 'format' in column:
                        if column['format'] == 'int':
                            content = int(float(content))
                            content = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + content - 2)
                            return content
                    content = minimalist_xldate_as_datetime(content)
                elif column['type'] == date:
                    content = datetime.strptime(content, column['format'])
            except Exception as ex:
                print(ex.__str__())
        return content

    row_index = start_row
    for col in range(start_col, max(20, len(columes)) + start_col):
        try:
            content = sheet.cell(row_index, col).value
            content = content.lower()
        except IndexError:
            break
        if isinstance(content, str):
            for column in columes:
                if 'title' in column:
                    if not 'index' in column:
                        for title in column['title']:
                            if title in content:
                                column.update({'index': col})
                                break
                    else:
                        pass
                else:
                    print('title not set')
    rows = []
    row_index += 1
    empty_rows = 0
    content = sheet.cell(row_index, start_col).value
    while empty_rows < retry_empty_rows:
        if content:
            empty_rows = 0
            row = {}
            for column in columes:
                if 'index' in column:
                    col_index = column['index']
                    content = process_content(sheet.cell(row_index, col_index).value, column)
                    row.update({column['property']: content})
            rows.append(row)
        else:
            empty_rows += 1
        row_index += 1
        try:
            content = sheet.cell(row_index, start_col).value
        except:
            break
    return rows
