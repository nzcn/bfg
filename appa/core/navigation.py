from functools import update_wrapper
from django.apps import apps
from django.contrib.admin.sites import AdminSite, actions
from django.contrib.admin import ModelAdmin
from django.shortcuts import render, redirect, reverse, render_to_response
from django.urls import NoReverseMatch
from django.utils.text import capfirst
from django.conf.urls import url, include
from bfg.appa.views import index as appa_index
from bfg.appa.core.model import AppaAdmin
from landing.core.common import *
from landing.controller.mesages import MessageManager
from django.utils.translation import ugettext as _
from django.http import HttpResponse, JsonResponse

site_name = "appa"


class AppaSite(AdminSite):

    def __init__(self, name):
        self.name = name
        self.menu_items = []
        self._actions = {'delete_selected': actions.delete_selected}
        self._global_actions = self._actions.copy()
        self._registry = {}
        self.nav_class = "bg-dark aside-sm"
        self.nav_popmenu = True

    def _safe_registry(self, model_class, admin_class):
        if model_class in self._registry:
            pass
        else:
            self.register(model_class, admin_class)

    def add_menu_item(self, item, add=True):
        if add:
            self.menu_items.append(item)

        if isinstance(item, (AdminNavigation)):
            self._safe_registry(item.model_class, item.model_admin)
        else:
            for child in item.children:
                self.add_menu_item(child, add=False)

    def add_model(self, model_or_iterable, admin_class=None):
        # Find the model
        self._safe_registry(model_or_iterable, admin_class)

    def get_view(self, request, url_or_view, args=[], kwargs={}):
        from django.core import urlresolvers
        # url_or_view = Variable(self.url_or_view).resolve(context)
        try:
            urlconf = getattr(request, "urlconf", settings.ROOT_URLCONF)
            resolver = urlresolvers.RegexURLResolver(r'^/', urlconf)
            view, args, kwargs = resolver.resolve(url_or_view)
        except:
            view = urlresolvers.get_callable(url_or_view)
        try:
            if callable(view):
                return view(request, *args, **kwargs)
            raise "%r is not callable" % view
        except Exception as ex:
            if settings.TEMPLATES[0]['OPTIONS']['debug']:
                raise ex
        return ""

    def dialog(self, request, action=None):
        include = get_req_value(request, 'include')
        p1 = get_req_value(request, 'p1')
        p2 = get_req_value(request, 'p2')
        if not action:
            action = get_req_value(request, 'action')
        if action == 'none':
            return render_to_response('appa/includes/dialog_tips.html', locals())
        elif action == 'process':
            return render_to_response('appa/includes/dialog_process.html', locals())
        elif action == 'message':
            return render_to_response('appa/includes/dialog_message.html', locals())
        else:
            return self.get_view(request, action)

    def execute(self, request, action=None):
        if action == 'message':
            mm = MessageManager(request)
            admin_user = AdminModels.User.objects.filter(is_superuser=True).first()
            message = mm.add_message_by_request(request=request, reader=admin_user)
            send_email_from_template(_("New Feedback about {{ request.get_full_path }}"), locals(),
                                     "appa/back/mail/feedback.html",
                                     reply_to=message.email,
                                     receivers=[admin_user.email])
            if message:
                return JsonResponse(get_action_result(True, 'Thanks for your feedback. Message has been sent.'),
                                    safe=False)
            else:
                return JsonResponse(get_action_result(False, 'Submit message failed.'), safe=False)
        else:
            return JsonResponse(get_action_result(False, 'Unknown command'))

    def get_urls(self):
        # Since this module gets imported in the application's root package,
        # it cannot import models from other applications at the module level,
        # and django.contrib.contenttypes.views imports ContentType.
        from django.contrib.contenttypes import views as contenttype_views

        def wrap(view, cacheable=False):
            def wrapper(*args, **kwargs):
                return self.admin_view(view, cacheable)(*args, **kwargs)

            wrapper.admin_site = self
            return update_wrapper(wrapper, view)

        urlpatterns = [
            url(r'^$', appa_index, name='home'),
            url(r'^$', appa_index, name='index'),
            url(r'^dialog/(?P<action>[\w|.]+)/$', self.dialog, name='dialog'),
            url(r'^execute/(?P<action>[\w|.]+)/$', self.execute, name='execute'),
            url(r'^login/$', self.login, name='login'),
            url(r'^logout/$', wrap(self.logout), name='logout'),
            url(r'^password_change/$', wrap(self.password_change, cacheable=True), name='password_change'),
            url(r'^password_change/done/$', wrap(self.password_change_done, cacheable=True),
                name='password_change_done'),
            url(r'^jsi18n/$', wrap(self.i18n_javascript, cacheable=True), name='jsi18n'),
            url(r'^r/(?P<content_type_id>\d+)/(?P<object_id>.+)/$', wrap(contenttype_views.shortcut),
                name='view_on_site'),
        ]
        valid_app_labels = []
        for model, model_admin in self._registry.items():
            urlpatterns += [
                url(r'^%s/%s/' % (model._meta.app_label, model._meta.model_name), include(model_admin.urls)),
            ]
            if model._meta.app_label not in valid_app_labels:
                valid_app_labels.append(model._meta.app_label)

        # If there were ModelAdmins registered, we should have a list of app
        # labels for which we need to allow access to the app_index view,
        if valid_app_labels:
            regex = r'^(?P<app_label>' + '|'.join(valid_app_labels) + ')/$'
            urlpatterns += [
                url(regex, wrap(self.app_index), name='app_list'),
            ]

        return urlpatterns

    def _build_app_dict(self, request, label=None):
        """
        Builds the app dictionary. Takes an optional label parameters to filter
        models of a specific app.
        """
        app_dict = {}

        if label:
            models = {
                m: m_a for m, m_a in self._registry.items()
                if m._meta.app_label == label
            }
        else:
            models = self._registry

        for model, model_admin in models.items():
            app_label = model._meta.app_label

            has_module_perms = model_admin.has_module_permission(request)
            if not has_module_perms:
                continue

            perms = model_admin.get_model_perms(request)

            # Check whether user has any perm for this module.
            # If so, add the module to the model_list.
            if True not in perms.values():
                continue

            info = (site_name, app_label, model._meta.model_name)
            model_dict = {
                'name': capfirst(model._meta.verbose_name_plural),
                'object_name': model._meta.object_name,
                'perms': perms,
            }
            if perms.get('change'):
                try:
                    model_dict['admin_url'] = reverse('%s:%s_%s_changelist' % info, current_app=self.name)
                except NoReverseMatch:
                    pass
            if perms.get('add'):
                try:
                    model_dict['add_url'] = reverse('%s:%s_%s_add' % info, current_app=self.name)
                except NoReverseMatch:
                    pass

            if app_label in app_dict:
                app_dict[app_label]['models'].append(model_dict)
            else:
                app_dict[app_label] = {
                    'name': apps.get_app_config(app_label).verbose_name,
                    'app_label': app_label,
                    'app_url': reverse(
                        site_name + ':app_list',
                        kwargs={'app_label': app_label},
                        current_app=self.name,
                    ),
                    'has_module_perms': has_module_perms,
                    'models': [model_dict],
                }

        if label:
            return app_dict.get(label)
        return app_dict

    @property
    def urls(self):
        return self.get_urls(), site_name, self.name

    def each_context(self, request):
        """
        Returns a dictionary of variables to put in the template context for
        *every* page in the admin site.

        For sites running on a subpath, use the SCRIPT_NAME value if site_url
        hasn't been customized.
        """
        script_name = request.META['SCRIPT_NAME']
        site_url = script_name if self.site_url == '/' and script_name else self.site_url
        return {
            'site': site_name,
            'site_title': self.site_title,
            'site_header': self.site_header,
            'site_url': site_url,
            'has_permission': self.has_permission(request),
            'available_apps': self.get_app_list(request),
        }


class NavigationBase(object):
    def __init__(self, title, id=None, url=None, icon=None, group=None, target=None, children=[], viewname=None,
                 viewkargs=None):
        self.title = title
        self.id = id
        self._url = url
        self.icon = icon
        self.target = target
        self.group = group
        self.children = children
        self.viewname = viewname
        self.viewkargs = viewkargs

    @property
    def url(self):
        if self._url:
            return self._url
        elif self.viewname:
            return reverse(viewname=self.viewname, kwargs=self.viewkargs)
        else:
            return "#"


class MenuSection(NavigationBase):
    pass


class LinkNavigation(NavigationBase):
    pass


class AdminNavigation(NavigationBase):
    def __init__(self, model_class=None, model_admin=None, title=None, query=None, icon=None, help=None, children=[]):
        self.model_class = model_class
        if model_admin:
            self.model_admin = model_admin
        else:
            self.model_admin = AppaAdmin
        if title:
            self.title = title
        else:
            self.title = self.model_class._meta.object_name
        self.query = query
        self.icon = icon
        self.help = help
        self.children = children

    @property
    def url(self):
        url = reverse("%s:%s_%s_changelist" % (site_name, self.model_class._meta.app_label,
                                               self.model_class._meta.model_name))
        url = url + (self.query if self.query else '')
        return url


class CustomNavigation(NavigationBase):
    pass
