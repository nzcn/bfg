from django.core.management.base import BaseCommand, CommandError
import datetime
import json

class Command(BaseCommand):
    help = 'Sync MailTemplate'

    def add_arguments(self, parser):
        parser.add_argument("freight_id",nargs='+',type=int)

    def handle(self, *args, **options):

        return