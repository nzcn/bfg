# noinspection PyPackageRequirements
from django.contrib.admin import ModelAdmin

from landing.core.common import *
from bfg.appa.models import *
from django import forms

class CompanyAdmin(ModelAdmin):
    list_display = ('title','administrator','homepage')
    search_fields = ['title',]
    fieldsets = [
        (None, {'fields': ['title','administrator','homepage']}),
        ('Basic Information', {
            'fields': ['administrator','staffs',]}
         ),
        ('Advance Information', {
            'fields': ['privacy', 'terms', 'invoice_terms','content',
                       'account_facebook','account_googleplus','account_twitter','account_instgram','account_pintrest']}
         ),
        ('Payement Information', {
            'classes': ('collapse',),
            'fields': ['tax_number', 'bank_account', 'bank_account_name','bank_name','stripe_account',
                       'paypal_account','qr_check']}
         ),
    ]

class StaffForm(forms.ModelForm):
    password = forms.CharField(required=False,widget=forms.PasswordInput(render_value=False))

class StaffAdmin(ModelAdmin):
    list_display = ('username','first_name','last_name','email','company','title')
    search_fields = ['username','email','title']
    fieldsets = [
        (None, {'fields': ['username','email','first_name','last_name','company']}),
        ('Basic Information', {
            'fields': ['password']}
         ),
    ]
    form = StaffForm

    def save_model(self, request, obj, form, change):
        password = form.cleaned_data['password']
        if password:
            obj.set_password(password)
        else:
            obj.password = form.initial['password']
        obj.save()

class PrinterAdmin(ModelAdmin):
    list_display = ('title','sn','server','username')

