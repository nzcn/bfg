# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import models as AdminModels
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.translation import gettext_lazy as _
from django.db import models
from landing.models import Variable


# Create your models here.
class Company(models.Model):
    title = models.CharField(max_length=100)
    code = models.CharField(max_length=10, unique=True, default=None, null=True, blank=True)
    administrator = models.ForeignKey('Staff', null=True, blank=True, default=None, related_name='administrator',
                                      on_delete=models.SET_DEFAULT)
    staffs = models.ManyToManyField('Staff', related_name="staffs", blank=True, default=None)
    variables = models.ManyToManyField(Variable, related_name="variables", blank=True, default=None)
    tax_number = models.CharField(max_length=50, default=None, null=True, blank=True)
    tax_name = models.CharField(max_length=10, default='GST', blank=True)
    tax_rate = models.FloatField(default=0, blank=True)
    bank_account = models.CharField(max_length=50, default=None, null=True, blank=True)
    bank_account_name = models.CharField(max_length=50, default=None, null=True, blank=True)
    bank_name = models.CharField(max_length=100, default=None, null=True, blank=True)
    size_level = models.CharField(max_length=1, default=None, null=True, blank=True)
    stripe_account = models.CharField(max_length=50, default=None, null=True, blank=True)
    paypal_account = models.CharField(max_length=50, default=None, null=True, blank=True)

    qr_check = models.BooleanField(default=True)
    confirm_email = models.BooleanField(default=True)
    email_as_username = models.BooleanField(default=False)

    last_active = models.DateTimeField(auto_now=True)
    settings = models.TextField(default=None, null=True, blank=True)
    active_index = models.PositiveIntegerField(default=0)
    logo = models.ImageField(blank=True, null=True, default=None)
    lat = models.FloatField(default=0)
    lng = models.FloatField(default=0)
    credit = models.PositiveIntegerField(default=0)
    used_credit = models.PositiveIntegerField(default=0)
    feedback_rate = models.FloatField(default=0)
    content = RichTextUploadingField(null=True, default=None, blank=True)
    terms = RichTextUploadingField(null=True, default=None, blank=True)
    invoice_terms = RichTextUploadingField(null=True, default=None, blank=True)
    privacy = RichTextUploadingField(null=True, default=None, blank=True)
    homepage = models.CharField(max_length=500, null=True, default=None, blank=True)

    account_facebook = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_googleplus = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_twitter = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_instgram = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_pintrest = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_wechat = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_qq = models.CharField(max_length=50, default=None, null=True, blank=True)

    mail_account = models.ForeignKey('MailAccount', default=None, null=True, blank=True, on_delete=models.SET_DEFAULT,
                                     related_name="main_mailaccount")
    mail_header = RichTextUploadingField(default=None, null=True, blank=True)
    mail_footer = RichTextUploadingField(default=None, null=True, blank=True)

    def get_variable_list(self):
        return [
            {'key': 'private_policy', 'value': None, 'description': 'Privacy Policy'},
            {'key': 'terms', 'value': None, 'description': 'Terms and Conditions'},
        ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Company")
        verbose_name_plural = _("Companies")


class CompanyModelBase(models.Model):
    company = models.ForeignKey(Company, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)

    class Meta:
        abstract = True


class Printer(CompanyModelBase):
    title = models.CharField(max_length=50, null=True, blank=True, default=None)
    sn = models.CharField(max_length=32)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=200)
    server = models.CharField(max_length=100)
    gap = models.SmallIntegerField(default=2)
    width = models.SmallIntegerField(default=40)
    length = models.SmallIntegerField(default=50)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Printer")
        verbose_name_plural = _("Printers")


class Staff(AdminModels.User):
    company = models.ForeignKey(Company, null=True, blank=True, default=None, related_name='appa_staff_company',
                                on_delete=models.SET_DEFAULT)
    title = models.CharField(max_length=30, null=True, blank=True, default=None)
    printer = models.ForeignKey(Printer, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)

    def in_group(self, group_name):
        return self.groups.filter(name=group_name).exists()

    def __str__(self):
        return self.title if self.title else self.get_full_name()

    class Meta:
        verbose_name = _("Staff")
        verbose_name_plural = _("Staffs")


DeviceType = (
    ("I", "iOS"),
    ("A", "Amdrpod"),
)


class Device(models.Model):
    title = models.CharField(max_length=50, null=True, blank=True, default=None)
    user = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, on_delete=models.CASCADE)
    device_id = models.CharField(max_length=100, null=True, blank=True, default=None)
    enable = models.BooleanField(default=True)
    type = models.CharField(max_length=1, default='I', choices=DeviceType)
    last_login = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("Device")
        verbose_name_plural = _("Devices")


MailAccountType = (
    ("landing.core.mail.SmtpMailProvider", "Smtp Client"),
    ("landing.core.mail.MailGunMailProvider", "MailGun Client"),
)


class MailAccount(CompanyModelBase):
    cls = models.CharField(max_length=50, null=True, blank=True, default=None, choices=MailAccountType)
    server = models.CharField(max_length=50, null=True, blank=True, default=None)
    user = models.CharField(max_length=50, null=True, blank=True, default=None)
    password = models.CharField(max_length=50, null=True, blank=True, default=None)
    port = models.PositiveIntegerField(default=465, blank=True)
    ssl = models.BooleanField(default=True, blank=True)
    url = models.CharField(max_length=250, null=True, blank=True, default=None)
    key = models.CharField(max_length=250, null=True, blank=True, default=None)


MailTemplateType = (
    ("M", "Mail"),
    ("WXAPP", "Wechat App"),
    ("IM", "Internal Message"),

)


class CompanyMailTemplate(CompanyModelBase):
    lang = models.CharField(max_length=10, default=None, null=True, blank=True)
    key = models.CharField(max_length=50, default=None, null=True,
                           help_text='Template identifier. Must be unique.')
    subject = models.CharField(max_length=200, default=None, null=True)
    body = RichTextUploadingField(default=None, null=True, blank=True)
    type = models.CharField(max_length=5, default=None, null=True, blank=True)
    is_html = models.BooleanField(default=True, blank=True)
    last_sync_time = models.DateTimeField(default=None, null=True, blank=True)
    enable = models.BooleanField(default=True, blank=True)

    class Meta:
        unique_together = ('company', 'lang', 'key')
        verbose_name = _("Mail Template")
        verbose_name_plural = _("Mail Templates")
