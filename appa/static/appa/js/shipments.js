get_package_list = function (packages) {
    var package_list = [];
    var i = 0;
    packages.each(function (e) {
        var package = $(this);
        package_list[i] = {
            "Height": parseInt(package.find(".height").val()),
            "Length": parseInt(package.find(".length").val()),
            "Id": i,
            "Width": parseInt(package.find(".width").val()),
            "Kg": parseFloat(package.find(".weight").val()),
            "Name": package.find(".name").text().trim(),
            "PackageCode": package.find(".name").val(),
            "Type": "Box"
        }
        i++;
    });
    return package_list;
}

get_shipment = function (event) {
    var courier_id = $("#couriers").val();
    var package_list = get_package_list($(".package"))

    var address = {
        "DeliveryReference": "ORDER123",
        "Destination": {
            "Id": 0,
            "Name": "DestinationName",
            "Address": {
                "BuildingName": $("#shipping_building").val(),
                "StreetAddress": $("#shipping_street").val(),
                "Suburb": $("#shipping_suburb").val(),
                "City": $("#shipping_city").val(),
                "PostCode": $("#shipping_postcode").val(),
                "CountryCode": "NZ"
            },
        },
        "IsSaturdayDelivery": $("#Saturday").is(':checked'),
        "IsSignatureRequired": $("#Signature").is(':checked'),
        "Packages": package_list
    };
    get_quote(courier_id, address);
}

select_quote = function (courier_id, quote_id) {
    var url =  + courier_id;
    var package_list = get_package_list($(".package"))
    var address = {
        "QuoteId": quote_id,
        "Destination": {
            "Name": "DestinationName",
            "Address": {
                "BuildingName": $("#shipping_building").val(),
                "StreetAddress": $("#shipping_street").val(),
                "Suburb": $("#shipping_suburb").val(),
                "City": $("#shipping_city").val(),
                "PostCode": $("#shipping_postcode").val(),
                "CountryCode": "NZ"
            },
            "Email": "{{ item.buyer_email }}",
            "ContactPerson": "{{ item.buyer_fname }} {{ item.buyer_lname }}",
            "PhoneNumber": "{{ item.shipping_addr_phone|none_empty }}",
            "DeliveryInstructions": $("#shipping_instruction").val(),
            "SendTrackingEmail": false,
            "ExplicitNotRural": false
        },
        "Packages": package_list,
        "Commodities": null,
        "IsSaturdayDelivery": $("#Saturday").is(':checked'),
        "IsSignatureRequired": $("#Signature").is(':checked'),
        "IsUrgentCouriers": false,
        "DutiesAndTaxesByReceiver": false,
        "RuralOverride": false,
        "DeliveryReference": "{{ item.reference }}",
        "PrintToPrinter": "false",
    }

    $.post(url,
        {'csrfmiddlewaretoken': $('#csrfmiddlewaretoken').val(), 'body': JSON.stringify(address)},
        function (result) {
            alert(result);
        });
}

get_quote = function (courier_id, address) {
    var url = $("#quote").attr("data-action") + courier_id;
    $.post(url,
        {'csrfmiddlewaretoken': $('#csrfmiddlewaretoken').val(), 'body': JSON.stringify(address)},
        function (result) {
            $("#available_quotes").html("");
            if(result.Available && result.Available.length > 0){
                quotes = result.Available;

                for (var i in quotes) {
                    var q = quotes[i];
                    var container = $("<div class='row'>");
                    $("<div class='col-lg-6'>").append(q.CarrierName).appendTo($(container));
                    $("<div class='col-lg-3'>").append("$" + q.Cost).appendTo(container);
                    $("<div class='col-lg-3'>").append($("<button class='btn btn-sm btn-default selectQuote pull-right' data-id='" +
                        q.QuoteId + "' data-action='" + $("#available_quotes").attr("data-action") +"'>").append("Select"))
                        .appendTo(container);
                    $("<div class='col-lg-12'>").append(q.ServiceStandard).appendTo(container);

                    container.appendTo($("#available_quotes"));
                }
                resetSelectQuote();
            }
            else
            {
                $("#available_quotes").html(result.ValidationErrors.Destination);
            }
        });
}

resetPackage = function () {
    $('.del_package').click(function (e) {
        if ($(".package").length > 1)
            $(this).parent().parent().remove();
    });
}

resetSelectQuote = function () {
    $('.selectQuote').click(function (e) {
        var courier_id = $("#couriers").val();
        select_quote(courier_id, $(this).attr("data-id"));
    });
}

initShipment = function () {
    $('#quote').click(get_shipment);
    $('.add_package').click(function (e) {
        var pack = $(".package");
        $(pack[0].outerHTML).appendTo(pack.parent());
        resetPackage();
    });
};