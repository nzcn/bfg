/* Page initialize */
$(function () {
    createPager($('.pagination'));
    init();
    //Find current menu
    active_menuitem();

    $(".table-responsive td").click(function (e) {
        if($(this).closest("tr").hasClass("bg-selected")){
            $(this).closest("tr").removeClass("bg-selected");
        }
        else{
            $(this).closest("table").find(".bg-selected").removeClass("bg-selected");
            $(this).closest("tr").addClass("bg-selected");
        }
    });

    var $chkboxes = $('.chkbox');
    var lastChecked = null;
    $chkboxes.click(function(e) {
        if (!lastChecked) {
            lastChecked = this;
            return;
        }

        if (e.shiftKey) {
            var start = $chkboxes.index(this);
            var end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
        }

        lastChecked = this;
    });
});

function active_menuitem(){
    function expandMenuItem(item) {
        var ul = item.closest("ul .nav");
        var li = ul.closest("li");
        var li_a = li.find("a .dropdown-toggle");
        item.addClass("menu_active")
        ul.attr("style","display:block;");
        li.addClass("active");
        li_a.addClass("active");
    }
    var path = $(location).attr('href').replace($(location).attr('origin'),"");
    var links = $("li a[href^='" + path + "']");
    if(links.length > 0)
    {
        links.each(function (e) {
            expandMenuItem($(this));
        });
    }
    else{
        var return_url = getUrlParam('return_url');
        if(return_url != "")
            path = return_url;
        $(".nav li a[href]").each(function () {
            if(path.indexOf($(this).attr("href")) >= 0){
                expandMenuItem($(this));
            }
        });
    }
}

//Create a page number list
createPager = function (ul) {
    if (ul.length == 0) return;
    if(ul.attr("render")=="server")
        return;
    page = parseInt(ul.attr("page"));
    total = parseInt(ul.attr("total"));
    //Pager related
    var a = [10, 20, 30, 50, 100]
    for (i = 0; i < a.length; i++) {
        var p = a[i];
        $("<li>").append($("<a>", {'href': updateParam('pcount', p)}).html(p)).appendTo($(".pageCount"));
    }

    var show_page = 5;
    var key = 'page';
    $("<li>").append($("<a>", {'href': page == 1 ? '#' : updateParam(key, page - 1)}).html("<i class='fa fa-chevron-left' />")).appendTo(ul);
    $("<li>").append($("<a>", {'href': updateParam(key, 1)}).html('1')).appendTo(ul);
    if (total > show_page) {
        if (page - show_page > 2) {
            $("<li>").append($("<a>", {'href': updateParam(key, page - show_page - 1)}).html('...')).appendTo(ul);
            for (x = page - show_page; x < page; x++) {
                $("<li>").append($("<a>", {'href': updateParam(key, x)}).html(x)).appendTo(ul);
            }
            if (page < total)
                $("<li>").append($("<a>", {'href': '#'}).html(page)).appendTo(ul);
        }
        else if (page > 1) {
            for (x = 2; x < page; x++) {
                $("<li>").append($("<a>", {'href': updateParam(key, x)}).html(x)).appendTo(ul);
            }
            if (page < total)
                $("<li>").append($("<a>", {'href': '#'}).html(page)).appendTo(ul);
        }
        if (page + show_page < total - 1) {
            for (x = page + 1; x <= page + show_page; x++) {
                $("<li>").append($("<a>", {'href': updateParam(key, x)}).html(x)).appendTo(ul);
            }
            $("<li>").append($("<a>", {'href': updateParam(key, page + show_page + 1)}).html('...')).appendTo(ul);
        }
        else {
            for (x = page + 1; x <= total - 1; x++) {
                $("<li>").append($("<a>", {'href': updateParam(key, x)}).html(x)).appendTo(ul);
            }
        }
    }
    else {
        for (x = 2; x <= total - 1; x++) {
            $("<li>").append($("<a>", {'href': updateParam(key, x)}).html(x)).appendTo(ul);
        }
    }
    if (total > 1)
        $("<li>").append($("<a>", {'href': updateParam(key, total)}).html(total)).appendTo(ul);
    $("<li>").append($("<a>", {'href': page == total ? '#' : updateParam(key, page + 1)}).html("<i class='fa fa-chevron-right' />")).appendTo(ul);
}


/* Modal realated functions */
setModalParameters = function (url, actionUrl, isBatchAction, ids, title, confirm, buttonTitle, refresh, isPreaction) {
    if (typeof isBatchAction == 'undefined')
        isBatchAction = false;
    if (typeof ids == 'undefined')
        ids = [];
    if (typeof title == 'undefined')
        title = 'Action';
    if (typeof confirm == 'undefined')
        confirm = true;
    if (typeof buttonTitle == 'undefined')
        buttonTitle = 'Confirm';
    if (typeof refresh == 'undefined')
        refresh = false;
    if (typeof isPreaction == 'undefined')
        isPreaction = false;
    modal_url = actionUrl;
    modal_title = title;
    modal_confirm = confirm;
    modal_refresh_parent = refresh;
    modal_button_title = buttonTitle;
    modal_needPreaction = isPreaction;
    modal_is_batch_action = isBatchAction;
    selectedIds = ids;
}

ajaxModal = function(remote)
{
    $('#ajaxModal').remove();
    var $this = $(this)
        , $remote = remote
        , $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
    $('body').append($modal);
    $modal.modal();
    $modal.load($remote);
}

showSingleModal = function (url, refresh, backdrop) {
    if (typeof refresh == 'undefined')
        refresh = true;
    if (typeof backdrop == 'undefined')
        backdrop = false;
    $('#modal-form').modal({
        remote: url,
        refresh: refresh,
        backdrop: backdrop
    });
}

showModal = function (url, actionUrl, isBatchAction, ids, title, confirm, buttonTitle, refresh) {
    setModalParameters(url, actionUrl, ids, title, confirm, buttonTitle, refresh, isBatchAction);
    showSingleModal(url);
}

//Show modal
setSingleClickModal = function (obj, url) {
    obj.click(function (event) {
        showModal(url);
    });
}

setClickModal = function (obj, url, actionUrl, isBatchAction, ids, title, confirm, buttonTitle, refresh) {
    setModalParameters(url, actionUrl, ids, title, confirm, buttonTitle, refresh, isBatchAction);
    setSingleClickModal(obj, url);
}

showDialog = function (actionUrl, isBatchAction, ids, title, confirm, buttonTitle, refresh) {
    showModal('/dialog/none/', actionUrl, isBatchAction, ids, title, confirm, buttonTitle, refresh)
}

//Show dialog with the url to rectrive the ids then execute action one by one
setClickDialog = function (obj, actionUrl, isBatchAction, ids, title, confirm, buttonTitle, refresh) {
    setClickModal(obj, '/dialog/none/', actionUrl, ids, title, confirm, buttonTitle, refresh, isBatchAction);
}

/* File upload related functions  */

init_file_upload = function (obj, url) {
    $('.img_remove').click((function () {
        var $this = $(this);
        $this.parent().remove();
    }));

    var closeButton = $('<button/>')
        .addClass('btn btn-xs')
        .text('x')
        .click(function () {
            var $this = $(this);
            $this.parent().remove();
        });
    obj.fileupload({
        url: url,
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        if ($('#sortable').length > 0) {
            data.context = $('#sortable');
        }
        else {
            data.context = $('<ul/>').appendTo('#files');
            data.context.attr('id', 'sortable');
            data.context.sortable();
            data.context.disableSelection();
            data.context.addClass('sortable');
        }
        $.each(data.files, function (index, file) {
            var node = $('<li/>').append($('<span/>').text(file.name));
            node.addClass('ui-state-default');
            closeButton.clone(true).appendTo(node);
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = data.context.find("li:contains('" + file.name + "')");
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress').removeClass("hidden");
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $('#progress').addClass("hidden");
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                        .attr('target', '_blank')
                        .prop('href', file.url),

                    node = data.context.find("li:contains('" + file.name + "')");
                node.attr('pk', file.pk);
                node.find('canvas')
                    .wrap(link);
                var input = $("<input>")
                    .attr('type', 'hidden').attr('name', 'images').attr('value', file.pk)
                    .appendTo(node);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            node = data.context.find("li:contains('" + data.files[index].name + "')");
            if (node) {
                node
                    .append('<br/>')
                    .append(error);
            }
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

show_top_message = function(obj,result){
    var use_old_title = obj.attr("show_old_title") ? bool(obj.attr("show_old_title")) : false;
    var reload = obj.attr("refresh") ? obj.attr("refresh") == "true" : false;
    var need_return = obj.attr("return") ? obj.attr("return") == "true" : false;
    var success = (obj.result.code == 0);
    var header = $(".message_area");
    var messager = header.find(".message");
    var old_title = messager.text();
    var message_class = success ? "bg-success" : "bg-danger";
    var message = obj.result.message ? obj.result.message : obj.attr("success-msg");
    var refresh_time = obj.attr("refresh_time")?obj.attr("refresh_time"):5000
    messager.text(message);
    header.switchClass("bg-white",message_class,1000,"easeInOutQuad");
    window.setTimeout(function ease() {
        header.switchClass(message_class,"bg-white",2000,"easeInOutQuad");
        if(reload)
            window.location.reload();
        if(need_return)
            go_back(result);
    },refresh_time);
    if(use_old_title) messager.text(old_title);
}

