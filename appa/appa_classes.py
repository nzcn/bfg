from django.db.models.aggregates import Sum
from django.utils.translation import gettext
from django.utils.translation import ugettext as _, ugettext_lazy
from bfg.appa.controllers.core import *
from bfg.appa.core.model import *
from packgo.models import Company as FreightCompany
from bfg.appa.models import *
from django import forms


class CompanyAppa(CompanyBaseAdmin):
    list_display = ('title', 'administrator', 'tax_number')
    company_filter_fields = ['administrator']
    fieldsets = [
        (None, {'fields': ['title', ]}),
        ('Basic Information', {
            'fields': ['administrator']}
         ),
        ('Payement Information', {
            'fields': ['tax_number', 'bank_account', 'bank_account_name', 'bank_name', 'stripe_account',
                       'paypal_account', 'qr_check']}
         ),
    ]

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        company = get_staff_company(request)
        return FreightCompany.objects.filter(id=company.id)


class StaffForm(AppaForm):
    password = forms.CharField(required=False, widget=PasswordInput(render_value=False))

    class Meta:
        model = Staff
        fields = '__all__'


class StaffAppa(CompanyBaseAdmin):
    list_display = ('username', 'first_name', 'last_name', 'email', 'title')
    search_fields = ['username', 'email', 'title']
    form = StaffForm
    is_user = True
    company_filter_fields = ['warehouse']
    fieldsets = [
        (None, {'fields': ['username', 'email', 'first_name', 'last_name', 'title']}),
        ('Basic Information', {
            'fields': ['password']}
         ),
    ]

    def pre_save(self, request, obj, form, change):
        if not change:
            obj.is_staff = True
            obj.is_active = True
            obj.company.staffs.add(obj)


class MailAccountAppa(CompanyBaseAdmin):
    list_display = ('cls', 'server', 'user', 'port')
    search_fields = ['server']
    fieldsets = [
        (None, {'fields': ['cls', 'server', 'user', 'password', 'port', 'ssl', 'url', 'key']}),
    ]


class CompanyMailTemplateAppa(CompanyBaseAdmin):
    list_display = ('key', 'lang', 'subject', 'is_html')
    search_fields = ['key', 'subject', 'body']
    list_filter = ['lang', 'is_html']
    fieldsets = [
        (None, {'fields': ['key', 'lang', 'subject', 'body', 'is_html']}),
    ]
