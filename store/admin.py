# noinspection PyPackageRequirements
from django.contrib import admin
from store.models import *
from store.admin_classes import *
from store.courier.admin import *

# Register your models here.
if not admin.site.is_registered(Product):
    admin.site.register(Product, ProductAdmin)

admin.site.register(Order, OrderAdmin)
admin.site.register(Currency, CurrencyAdmin)
admin.site.register(PricePlan, PricePlanAdmin)
admin.site.register(Function)
admin.site.register(PaymentGateway)
admin.site.register(PlanPromotion, PlanPromotionAdmin)
admin.site.register(City)
admin.site.register(State)
admin.site.register(Country)
admin.site.register(Suburb)
admin.site.register(Channel)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ShippingArea)
