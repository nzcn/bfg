# noinspection PyPackageRequirements
from django.core.management.base import BaseCommand, CommandError
from landing.core.common import *
import datetime
from django.utils.text import slugify
from django.contrib.auth.models import User
from django_cart.cscart import CSCart
from store.models import *
import urlparse
import urllib
from django.db.transaction import atomic


class Command(BaseCommand):
    help = 'Import Cart data from different suppliers'

    def add_arguments(self, parser):
        parser.add_argument('source', nargs='+', type=str)

    def handle(self, *args, **options):
        if options['source'][0] == 'cscart':
            if len(options['source']) == 4:
                self.import_cscart(options['source'][1], options['source'][2], options['source'][3])
            else:
                print('Wrong source parameters numbers.')
        else:
            print('Wrong source parameters')

    def import_cscart(self, url, user, api_key):
        '''
        Import Data from CSCart
        :return:
        '''

        @atomic()
        def create_category(category, pos, store, parent=None):
            q = Category.objects.filter(slug=get_value(category['object'], 'seo_name'))
            if not q.exists():
                cat = Category()
                cat.company = None
                cat.store = store
                cat.parent = parent
                cat.title = get_value(category, 'title')
                cat.slug = get_value(category['object'], 'seo_name')
                if parent:
                    cat.path = parent.slug + "/" + cat.slug
                else:
                    cat.path = cat.slug
                cat.pos = pos
                cat.save()
                print("Imported category {name}".format(name=cat.title))
            else:
                cat = q.first()
            child_pos = 0
            if 'children' in category:
                for child_category in category['children']:
                    child_pos += 1
                    child_cat = create_category(child_category, child_pos, store, parent=cat)
            return cat

        @atomic()
        def create_mainpair(pair, product, pos=0):
            def __urlEncodeNonAscii(b):
                return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

            def __iriToUri(iri):
                parts = urlparse.urlparse(iri)
                return urlparse.urlunparse(
                    part.encode('idna') if parti == 1 else __urlEncodeNonAscii(part)
                    for parti, part in enumerate(parts)
                )

            if not 'detailed' in pair:
                return None
            detail = pair['detailed']
            if len(detail['image_path']) > 0:
                fild_directory_url = 'product_images/{product_id}'.format(product_id=product.id)
                image_url = __iriToUri(detail['image_path'])
                image = save_url_to_image(image_url, path=fild_directory_url, thumb_size=(500, 500), pos=pos)
                return image
            else:
                return None

        def get_category(category_id):
            def find_category(cat, target_id):
                if get_value(cat, 'id') == target_id:
                    return cat
                else:
                    if 'children' in cat:
                        for sub_category in cat['children']:
                            sub_cat = find_category(sub_category, target_id)
                            if sub_cat:
                                break
                        return sub_cat
                    else:
                        return None

            for category in categoroies:
                cat = find_category(category, category_id)
                if cat:
                    return cat
            return None

        def get_cat(slug):
            if slug:
                q = Category.objects.filter(slug=slug)
                if q.exists():
                    return q.first()
            return None

        @atomic()
        def create_product(product):
            q = Product.objects.filter(code=get_value(product, "product_code"))
            if not q.exists():
                product = cscart.get_product_detail(get_value(product, 'product_id'))
                p = Product(title=get_value(product, 'product'))
                p.quantity = get_value(product, 'amount')
                p.content = get_value(product, 'full_description')
                p.subtitle = get_value(product, 'short_description')
                p.meta_title = get_value(product, 'page_title')
                p.keywords = get_value(product, 'meta_keywords')
                p.slug = get_value(product, 'seo_name')
                p.description = get_value(product, "meta_description")
                p.code = get_value(product, 'product_code')
                p.price = get_value(product, 'price')
                p.is_free_shipping = (get_value(product, 'free_shipping') == 'Y')
                p.weight = get_value(product, "weight")

                if 'main_category' in product:
                    category = get_category(product['main_category'])
                    slug = get_value(category['object'], "seo_name")
                    cat = get_cat(slug)
                    if cat:
                        p.category = cat
                p.save()

                # Images Main pair
                if 'main_pair' in product:
                    image = create_mainpair(product['main_pair'], p)
                    if image:
                        p.image = image.url
                        p.thumb = image.thumb
                        p.save()

                if 'image_pairs' in product:
                    pos = 0
                    for image_pair in product['image_pairs']:
                        pos += 1
                        image = create_mainpair(product['image_pairs'][image_pair], p, pos)
                        if image:
                            p.images.add(image)

                if 'category_ids' in product:
                    for cat_id in product['category_ids']:
                        category = get_category(cat_id)
                        cat = get_cat(get_value(category['object'], 'seo_name'))
                        if cat:
                            ProductCategory(product=p, category=cat).save()
                print("Imported product {name}".format(name=p.title))
                return p
            else:
                q.first()

        cscart = CSCart({
            "url": url,
            "email": user,
            "api_key": api_key,
        })
        stores = cscart.get_stores()
        last_store = None
        for store in stores:
            q = Store.objects.filter(url=get_value(store, 'storefront'))
            if not q.exists():
                new_store = Store()
                new_store.title = get_value(store, 'company')
                new_store.url = get_value(store, 'storefront')
                new_store.secure_url = get_value(store, 'secure_storefront')
                new_store.save()
                last_store = new_store
            else:
                last_store = q.first()
        categoroies = cscart.get_categories()
        pos = 0
        for category in categoroies:
            pos += 1
            create_category(category, pos, last_store)

        # Import products
        products = cscart.get_products()
        for product in products:
            try:
                create_product(product)
            except Exception as ex:
                print(ex)
