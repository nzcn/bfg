from django.core.management.base import BaseCommand
from store.models import *
import requests

class Command(BaseCommand):
    help = 'Init NZ Country Data'

    def handle(self, *args, **options):
        # Check whether the site exitst:
        q = Country.objects.filter(abstraction='nz')
        if q.exists():
            country = q.first()
        else:
            country = Country(abstraction='nz')
            country.name = 'New Zealand'
            country.phonecode = '64'
            country.slug = 'nz'
            country.save()
        url = 'https://platform.realestate.co.nz/search/v1/locations/regions'
        data = requests.get(url).json()

        included = data['included']
        for inc in included:
            if inc['type'] == 'districts':
                qcity = City.objects.filter(out_id=int(inc['id']))
                if qcity.exists():
                    city = qcity.first()
                else:
                    city = City(out_id=int(inc['id']))
                    city.name = inc['attributes']['title']
                    city.slug = inc['attributes']['slug']
                    city.save()
                for sub in included:
                    suburb_index = 0
                    if sub['type'] == 'suburbs' and sub['attributes']['parent-id'] == city.out_id:
                        suburb_index += 1
                        qsuburb = Suburb.objects.filter(out_id=int(sub['id']))
                        try:
                            if qsuburb.exists():
                                suburb = qsuburb.first()
                            else:
                                suburb = Suburb(out_id=int(sub['id']))
                                suburb.name = sub['attributes']['title']
                                suburb.slug = sub['attributes']['slug']
                                suburb.city = city
                            suburb.pos = suburb_index
                            suburb.save()
                        except Exception as ex:
                            print(ex.message)
                    else:
                        continue
            else:
                continue

        regions = data['data']
        state_index = 0
        for region in regions:
            # State
            state_index += 1
            if region['type'] == 'regions':
                qstate = State.objects.filter(slug=region['attributes']['slug'],country=country)
                if qstate.exists():
                    state = qstate.first()
                else:
                    state = State(country=country,slug=region['attributes']['slug'])
                    state.name = region['attributes']['title']
                    state.pos = state_index
                    state.out_id = region['id']
                    state.save()

                if 'relationships' in region:
                    # Connect State to City
                    if 'districts' in region['relationships'] and 'data' in region['relationships']['districts']:
                        districts = region['relationships']['districts']['data']
                        district_index = 0
                        for district in districts:
                            district_index += 1
                            qcity = City.objects.filter(out_id=int(district['id']))
                            if qcity.exists():
                                city = qcity.first()
                                city.state = state
                                city.pos = district_index
                                city.save()









