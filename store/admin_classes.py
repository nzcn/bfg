from store.models import *
from django import forms
from django.forms import ModelForm, PasswordInput
from datetime import *
import logging
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.core.cache import cache
from landing.core.common import *
from django.utils.crypto import get_random_string
from uuslug import slugify


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        exclude = ('last_modified_time', 'author', 'create_time')
        # fields = '__all__'


class TagAdmin(admin.ModelAdmin):
    list_display = ('title', 'parent', 'pos', 'key', 'group')
    list_filter = ['group']
    ordering = ['pos']
    search_fields = ('key', 'title')
    form = TagForm

    def save_model(self, request, obj, form, change):
        if not change:
            obj.create_time = timezone.now()
            obj.author = request.user
        if obj.key is None:
            obj.key = orig = slugify(obj.title)
        obj.last_modified = timezone.now()
        obj.save()


class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'code', 'quantity', 'price', 'status', 'company')
    list_filter = ['company', 'category', 'status']
    search_fields = ['title', 'content', 'subtitle']
    fieldsets = [
        (None, {'fields': ['title', 'category', 'code', 'quantity', 'price', 'status', 'company']}),
        ('Image', {'fields': ['image', 'video', 'thumb']}),
        ('Price', {'fields': ['was_price', 'price_text', ]}),
        ('Content', {'fields': ['subtitle', 'content']}),
        ('SEO', {
            'classes': ('collapse',),
            'fields': ['meta_title',
                       'description',
                       'keywords', ]
        }),
        ('Advanced Options', {
            'classes': ('collapse',),
            'fields': ['site', 'user',
                       'slug',
                       'sku', 'pos',
                       'publish_time',
                       'enable',
                       'active_index',
                       'is_used', 'is_clearance', 'is_hot', 'is_service',
                       'is_online_product', 'is_free_shipping',
                       'success_action', 'template',
                       'weight', 'height', 'width', 'length',
                       'box_weight', 'box_height', 'box_length', 'box_width',
                       ]
        }),
    ]

    def save_model(self, request, obj, form, change):
        if not change:
            obj.create_time = timezone.now()
            obj.user = request.user
            obj.slug = slugify(obj.title)
        obj.last_modified = timezone.now()
        obj.save()


class OrderDetailInline(admin.TabularInline):
    model = OrderDetail
    fields = ['title', 'quantity', 'price']


class OrderAdmin(admin.ModelAdmin):
    list_display = ('title', 'random_link', 'buyer_fullname', 'total', 'status', 'pay_time')
    fieldsets = [
        (None, {'fields': ['title']}),
        ('Date information', {'fields': ['buyer_fname', 'buyer_lname', 'buyer_email']}),
        ('Price', {'fields': ['total', 'shipping_cost', 'currency']}),
        ('Security', {'fields': ['access_code']}),
        ('Advanced Options', {
            'classes': ('collapse',),
            'fields': ['shipping_addr_building',
                       'shipping_addr_street',
                       'shipping_addr_suburb',
                       'shipping_addr_city',
                       'shipping_addr_state',
                       'shipping_addr_postcode',
                       'shipping_addr_country', ]
        }),
    ]
    inlines = [OrderDetailInline]

    def save_model(self, request, obj, form, change):
        if not change:
            obj.create_time = timezone.now()
            obj.user = request.user
            obj.random = get_random_string(length=16)
        obj.last_modified = timezone.now()
        obj.access_expire = timezone.now() + timedelta(days=7)
        obj.save()
        logger.info("Content Type Saved")


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('title', 'abstraction', 'one_USD', 'sign', 'enable')
    ordering = ['pos']


class PricePlanFunctionInline(admin.TabularInline):
    model = PlanFunction


class PricePlanAdmin(admin.ModelAdmin):
    list_display = ('title', 'price', 'currency', 'unit', 'enable')
    inlines = [
        PricePlanFunctionInline,
    ]


class PlanPromotionAdmin(admin.ModelAdmin):
    list_display = ('plan', 'title', 'discount', 'price_off', 'min_quantity')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'parent', 'company', 'store', 'status')
    ordering = ['pos']
    list_filter = ['company', 'store']
    fieldsets = [
        (None, {'fields': ['title', 'pos', 'parent']}),
        ('Content', {'fields': ['content', 'company', 'store', 'status', 'picture']}),
        ('SEO', {
            'classes': ('collapse',),
            'fields': ['slug',
                       'path',
                       'meta_title',
                       'description',
                       'keywords',
                       ]
        }),
        ('Advanced', {
            'classes': ('collapse',),
            'fields': ['show_header',
                       'in_menu',
                       'age_limit',
                       'age_verification',
                       'keywords',
                       ]
        }),
    ]

    def save_model(self, request, obj, form, change):
        if not change:
            obj.create_time = timezone.now()
        obj.save()
