# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2022-05-02 10:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0102_auto_20220502_1846'),
    ]

    operations = [
        migrations.AddField(
            model_name='shipmentgroup',
            name='pieces',
            field=models.PositiveIntegerField(blank=True, default=0),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='volume',
            field=models.FloatField(blank=True, default=0),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='weight',
            field=models.FloatField(blank=True, default=0),
        ),
    ]
