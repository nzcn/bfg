# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-06-28 01:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0045_bankstatement'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shipmentgroup',
            name='driver',
        ),
        migrations.AddField(
            model_name='courieraccount',
            name='contact_info',
            field=models.TextField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='courieraccount',
            name='pos',
            field=models.PositiveSmallIntegerField(blank=True, default=100),
        ),
        migrations.AddField(
            model_name='courieraccount',
            name='send_email',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='shipment',
            name='courier_account',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='store.CourierAccount'),
        ),
        migrations.AddField(
            model_name='shipment',
            name='group',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='store.ShipmentGroup'),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='comments',
            field=models.TextField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='contact_info',
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='courier_account',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='store.CourierAccount'),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='driver_note',
            field=models.TextField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='has_pallet_jack',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='has_tail_lift',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='shipmentgroup',
            name='paid_time',
            field=models.DateTimeField(blank=True, default=None, null=True),
        ),
    ]
