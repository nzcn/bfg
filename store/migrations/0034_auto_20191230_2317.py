# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-12-30 10:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0033_packtype'),
    ]

    operations = [
        migrations.AddField(
            model_name='shipment',
            name='tracking_numbers',
            field=models.TextField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='shipmentitem',
            name='content',
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
    ]
