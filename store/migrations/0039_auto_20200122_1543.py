# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-01-22 02:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0038_shipment_tracking_link'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderdetail',
            name='seller',
        ),
        migrations.RemoveField(
            model_name='orderdetail',
            name='snapshot',
        ),
        migrations.RemoveField(
            model_name='orderdetail',
            name='status',
        ),
        migrations.RemoveField(
            model_name='orderdetail',
            name='user',
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='brand',
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='material',
            field=models.CharField(blank=True, default=None, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='material_en',
            field=models.CharField(blank=True, default=None, max_length=120, null=True),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='source_hs_code',
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='target_hs_code',
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='usage',
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='variant',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='store.Variant'),
        ),
    ]
