# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-12-20 05:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0063_shipment_shipping_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='shipment',
            name='reference',
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
    ]
