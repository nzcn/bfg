# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-01-11 00:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0065_auto_20210111_1257'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='courieraccount',
            name='shipping_areas',
        ),
    ]
