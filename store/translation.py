from modeltranslation.translator import translator, TranslationOptions
from store.models import *


class ProductTranslationOptions(TranslationOptions):
    fields = ('title', 'meta_title', 'description', 'subtitle', 'price_text', 'content')


class CategoryTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


class OrderTranslationOptions(TranslationOptions):
    fields = ['title']


class OrderDetailTranslationOptions(TranslationOptions):
    fields = ['title', 'reference']


translator.register(Product, ProductTranslationOptions)
translator.register(Category, CategoryTranslationOptions)
translator.register(Order, OrderTranslationOptions)
translator.register(OrderDetail, OrderDetailTranslationOptions)
