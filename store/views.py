# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import *
from landing.core.common import *
from landing.core.account import Account
from django.utils.translation import gettext as _
from django.shortcuts import render_to_response, get_object_or_404
from store.models import *
from store import *
from django.contrib.auth.decorators import login_required
from store.controller.cart import CartManager
from store.controller.order import OrderManager
from store.controller.plan import PlanManager
from store.controller.product import ProductManager
from store.controller.payment import PaymentManager
from store.controller.store_manager import StoreManager
from django.contrib.auth import (
    login as auth_login, authenticate,
    update_session_auth_hash,
)

try:
    from django.db.transaction import atomic
except ImportError:
    def atomic(func):
        return func

# Create your views here.
def index(request):
    '''
    Store Home Page
    :param request:
    :return:
    '''
    return template_response(request,'store/frontend/default/index.html',locals())

def category(request):
    products = Product.objects.filter(enable=True).order_by('last_modified_time')[0:20]
    return template_response(request,'store/frontend/default/product/category.html',locals())

def plan_detail(request,id=0):
    plan = get_object_or_404(PricePlan,pk=id)
    return template_response(request,'store/frontend/default/product/detail.html',locals())

def product(request,id=0):
    if request.method == 'POST':
        if 'action' in request.POST:
            action = request.POST.get('action')
            if action == 'add_cart':
                # Add to cart
                cm = CartManager(request)
                resp = HttpResponseRedirect(redirect_to=reverse('store:cart'))
                cm.add_product(resp,id,request.POST.get('qty'))
                return resp
    else:
        pm = ProductManager(request)
        product = pm.get_product(id)
        return template_response(request,'store/frontend/default/product/detail.html',locals())

def cart_add_product(request,id):
    if request.method == "POST":
        if 'action' in request.POST:
            cm = CartManager(request)
            action = request.POST.get("action")
            if action == 'edit':
                pass
            elif action == 'delete':
                cm.del_product()
    else:
        cm = CartManager(request)
        resp = HttpResponseRedirect(reverse('store:cart'))
        cm.add_product(resp,id,1)
        return resp

def cart_add_plan(request,id):
    cm = CartManager(request)
    resp = HttpResponseRedirect(reverse('store:cart'))
    cm.add_plan(resp,id)
    return resp

def cart(request):
    '''
        Shopping Cart Page
        :param request:
        :return:
        '''
    # Override default cart if needed
    if hasattr(settings, "CART_URL"):
        return redirect(settings.CART_URL)

    title = _("Shopping Cart")

    cm = CartManager(request)
    if request.method == 'POST':
        if 'checkout' in request.POST:
            # Checkout
            return redirect(reverse('store:checkout'))
        elif 'shopping' in request.POST:
            # Continue Shopping
            if 'next' in request.GET:
                next = request.GET.get("next")
                return redirect(next)
            else:
                return redirect(reverse('store:home'))
        elif 'remove_detail' in request.POST:
            # Remove Item
            id = request.POST.get('remove_detail')
            resp = JsonResponse(data={}, safe=False)
            cm.del_product(resp, id, 0)
            return resp
        elif 'update_shopping_cart' in request.POST:
            # Adjust item quantity
            id = request.POST.get('id')
            price = float(request.POST.get('price'))
            qty = int(request.POST.get('update_shopping_cart'))
            cm.del_product(None, id, qty)
            cart = cm.get_cart()
            resp = JsonResponse(data={
                'count': cart['count'],
                'subtotal': '{:.2f}'.format(price * qty),
                'total': '{:.2f}'.format(cart['total']),
            }, safe=False)
            cm.set_cookie_str(resp, using_session=True, values=cm.get_cart_array())
            return resp
    else:
        cart = cm.get_cart()
        return template_response(request, 'store/frontend/default/cart/index.html', locals())

@atomic
def checkout(request):
    cm = CartManager(request)
    sm = StoreManager(request)
    pm = PaymentManager(request)
    if request.method == 'POST':
        try:
            if cm.availabble_cart():
                auto_checkout = False
                if 'login' in request.POST or request.POST.get('account') == 'login':
                    email = request.POST.get("fld_buyer_email")
                    username = request.POST.get("username")
                    password = request.POST.get("password")
                    user = authenticate(username=username, password=password)
                    if user is not None:
                        if user.is_active:
                            auth_login(request, user, backend="django.contrib.auth.backends.ModelBackend")
                        auto_checkout = True
                    else:
                        # Check wethere the user account exists
                        if AdminModels.User.objects.filter(username=username).exists():
                            message_user(_("User account already exists. Please try other account name"), messages.WARNING)
                        else:
                            # Create a new account and login directly
                            user = Account(request=request).register_user(
                                                    username=username,
                                                    email=email,
                                                    password=password)
                            auth_login(request, user, backend="django.contrib.auth.backends.ModelBackend")
                            auto_checkout = True

                if 'checkout' in request.POST or auto_checkout:
                    order = cm.checkout()
                    if order:
                        message_user(request, _("Check out successfully."), messages.SUCCESS)
                        # Get the payment gateway
                        gateway = pm.get_payment_gateway(int(request.POST.get("payment_gateway")))
                        payment = pm.create_payment_by_gateway(order,gateway=gateway)
                        if payment.variant:
                            resp = HttpResponseRedirect(reverse('store:pay:pay_payment',
                                                                kwargs={'payment_id':payment.id}))
                        else:
                            resp = HttpResponseRedirect(reverse('store:pay:result',
                                                                kwargs={'token':payment.token}))
                        cm.clean_cart(resp)
                        return resp
                    else:
                        message_user(request,_("Check out error."),messages.ERROR)
                elif 'cancel' in request.POST:
                    return redirect(reverse("store:cart"))
            else:
                message_user(request,_("No available products or plan found in shopping cart."),messages.ERROR)
        except Exception as ex:
            logger.error('payment error ' + ex.message)
            raise ex
    cart = cm.get_cart()
    shippings = sm.get_shipping_areas()
    payment_gateways = pm.get_payment_gateways()
    return template_response(request,'store/frontend/default/cart/checkout.html',locals())

def side_product_filter(request):
    # TODO: 产品筛选边
    return template_response(request,'store/frontend/default/include/inc_product_side_filter.html')

@cache_page(60 * 15)
def side_category_tree(request):
    # TODO: 左边的产品树
    return template_response(request,'store/frontend/default/include/inc_category_tree.html')


def checkout_result(request):
    return template_response(request,'store/frontend/default/cart/result.html')

def calculate_shipping_cost(request):
    return template_response(request,'store/frontend/default/include/inc_shipping_cost.html',locals())

def get_intrerested(request):
    return template_response(request,'store/frontend/default/include/inc_intrested.html',locals())

def get_latest_product(request):
    pm = ProductManager(request)
    products = pm.get_products(count=12)
    title = _("Latest Product")
    return template_response(request, 'store/frontend/default/include/inc_popular_products.html', locals())

def popular_products(request):
    pm = ProductManager(request)
    products = pm.get_products(count=4, order_by='-buy_count')
    title = _("Popular Product")
    return template_response(request, 'store/frontend/default/include/inc_popular_products.html', locals())

def cart_dialog(request):
    cm = CartManager(request)
    cart = cm.get_cart()
    return template_response(request,'store/frontend/default/include/inc_cart_dialog.html',locals())

def cart_footer(request):
    return template_response(request,'store/frontend/default/include/inc_cart_footer.html',locals())

@login_required()
def admin_change_plan(request):
    pm = PlanManager(request)
    plan = pm.get_user_plan(request.user)
    plans = pm.get_all_plans()
    return template_response(request, 'store/admin/select_plan.html', locals())

@login_required()
def admin_confirm_plan(request,id):
    if request.method == 'POST':
        if 'subscribe' in request.POST:
            # Create an order
            om = OrderManager(request)
            cm = CartManager(request)
            pm = PlanManager(request)

            plan_id = int(request.POST.get('plan_id'))
            promotion_id = int(request.POST.get('promotion'))
            if promotion_id == 0:
                promotion = None
            else:
                promotion = pm.get_promotion(promotion_id)

            quantity = int(request.POST.get('quantity'))

            coupon = request.POST.get("promo")
            plan = pm.get_plan(plan_id)
            prices = om.get_plan_final_price(plan,quantity=quantity,plan_promotion=promotion,coupon=coupon)
            order = om.create_order_from_plan(plan=plan,plan_promotion=promotion,quantity=quantity,coupon=coupon)
            return redirect(reverse('store_admin_pay_order',kwargs={'id':order.id}))
    else:
        pm = PlanManager(request)
        plan = pm.get_plan(id)
        return template_response(request,'store/admin/confirm_plan.html',locals())

@login_required()
def admin_pay_order(request,id):
    om = OrderManager(request)
    try:
        pm = PaymentManager(request)
        order = om.get_order(id)
        if request.method == 'POST':
            if 'token' in request.POST:
                payment_id = int(request.POST.get('payment_gateway'))
                order_id = int(request.POST.get('order_id'))

                token = request.POST.get('token')
                email = request.POST.get('email')

                order = om.get_order(order_id)
                payment_gateway = pm.get_payment_gateway(payment_id)
                handler = pm.get_payment_handler(payment_gateway.code)
                if handler:
                    plan_m = PlanManager(request)
                    up = plan_m.get_userplan(user=request.user)

                    params = json.loads(payment_gateway.settings)
                    params['token'] = token
                    params['email'] = email
                    params['userplan'] = up
                    result = handler.pay(order,params)
                    om.update_user_plan(order)
                    return redirect(reverse('store_admin_pay_result',kwargs={'id':order.id}))
                else:
                    return redirect(reverse('home'))
        else:
            handlers = pm.get_payment_gateways()
            gateways = []
            for handler in handlers:
                gateways.append(pm.get_payment_handler(handler.code,config=handler,order=order))
            return template_response(request, 'store/admin/pay_plan.html', locals())
    except Exception as ex:
        raise ex

@login_required()
def admin_pay_result(request,id):
    om = OrderManager(request)
    order = om.get_order(id)
    return template_response(request, 'store/admin/pay_result.html', locals())

@login_required()
def account_order_view(request,id):
    if request.method == "POST":
        if 'action' in request.POST:
            action = request.POST.get('action')
            if action == 'back':
                return return_to(request,reverse('store:account_order_list'))

    om = OrderManager(request)
    order = om.get_order(id)
    details = om.get_details(order)
    return template_response(request, 'store/account/order_view.html', locals())

@login_required()
def account_order_list(request):
    om = OrderManager(request)
    context = get_pager(om.get_orders(request.user), page=request.GET.get('page', 1))
    return template_response(request, 'store/account/order_list.html', context)

@login_required()
def account_payment_list(request):
    om = OrderManager(request)
    context = get_pager(om.get_payments(request.user),page=request.GET.get('page',1))
    return template_response(request, 'store/account/payment_list.html', context)

