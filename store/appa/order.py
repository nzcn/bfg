from bfg.appa.controllers.core import *

class OrderAppa(CompanyBaseAdmin):
    list_display = ['title','quantity','total','user','create_time']
    search_fields = ['title']
    def get_custom_filter(self,request=None):
        return Q(type='O')
