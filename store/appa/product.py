from bfg.appa.controllers.core import *
from landing.core.common import sort_list
from django import forms
from store.controller.product import ProductManager
from store.models import Product,ProductImage
from django.utils.translation import ugettext as _

class ProductForm(AppaForm):
    class Meta:
        model = Product
        fields = "__all__"

class ProductAppa(CompanyBaseAdmin):
    list_display = ['title','category','price','quantity','sku','enable']
    list_filter = ['category','quantity']
    search_fields = ['title','sku']
    def get_custom_filter(self,request=None):
        return Q(product_type='P')

    @atomic()
    def _changeform_view(self, request, object_id, form_url, extra_context):
        pm = ProductManager(request)
        product = pm.get_product(product_id=object_id)
        company = get_staff_company(request)
        tab = get_req_value(request,"tab")
        if request.method == "POST":
            action = get_req_value(request, "action")
            form = ProductForm(request.POST, request.FILES, instance=product)
            if action == 'save':
                if form.is_valid():
                    form.instance.save()
                    # Process Images
                    images = get_req_list(request,"images",parseInt=True)
                    # image_order = get_req_value(request, "image_pos")
                    if list(form.instance.images.all().values_list('id',flat=True)) != images:
                        form.instance.images.clear()
                        target_list = sort_list(list(Image.objects.filter(id__in=images)),"id",images)
                        pos = 0
                        for new_image in target_list:
                            ProductImage.objects.create(product=form.instance,image=new_image,pos=pos)
                            pos += 1
                    message_user(request, _("Save Product successfully."), messages.SUCCESS)
                else:
                    message_user(request, form.errors, messages.ERROR)
                    message_user(request, form.non_field_errors(), messages.ERROR)
            elif action == 'save_variants':
                form.save_related("variants")
                message_user(request, _("Save Variants successfully."), messages.SUCCESS)

        tab_page = "store/appa/product_edit.html"
        if tab:
            if tab == 'variants':
                tab_page = "store/appa/product_variants.html"
            elif tab == 'discount':
                tab_page = "store/appa/product_discount.html"
            elif tab == 'seo':
                tab_page = "store/appa/product_seo.html"
            elif tab == 'rules':
                tab_page = "store/appa/product_rules.html"

        return render(request,tab_page,locals())

