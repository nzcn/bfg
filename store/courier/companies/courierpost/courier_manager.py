from store.courier.core.freights import CourierBase
from django.utils import timezone
import requests


def get_channel_handler(token,request):
    return CourierPostFreight(token=token,request=request)

class CourierPostFreight(CourierBase):
    server = 'https://api.starshipit.com'

    def __get_headers(self):
        return {'StarShipIT-Api-Key': self.token['account'], 'Ocp-Apim-Subscription-Key': self.token['secret']}

    def __get_address_params(self, address):
        params = {
            'street': address.address,
            'post_code': address.postcode,
            'country': address.country,
            'suburb': address.suburb,
            'city': address.city,
        }
        return params

    def get(self, url, params):
        if url.startswith('/'):
            url = self.server + url
        return requests.get(url, params=params, headers=self.__get_headers())

    def post(self, url, params=None, headers=None, json=None):
        if url.startswith('/'):
            url = self.server + url
        if headers:
            headers.update(self.__get_headers())
        if json:
            return requests.post(url, params=params, headers=headers, json=json)
        else:
            return requests.post(url, params=params, headers=headers)

    def check_address(self, address):
        result = self.get('/api/address/validate', params=self.__get_address_params(address))
        if result.status_code == 200:
            result = result.json()
            if 'success' in result and 'valid' in result and result['success']:
                if result['valid']:
                    return True
                else:
                    return False
            else:
                return False
        else:
            raise Exception(result.reason)

    def get_rates(self, address, weight, options=None):
        result = self.get('/api/rates', params=self.__get_address_params(address))
        if result.status_code == 200:
            result = result.json()
            if 'success' in result and result['success']:
                return result['rates']
            else:
                return None
        else:
            raise Exception(result.reason)

    def get_order_json(self, fo, account=None, index=0, packages=None):
        return {}

    def __get_print_order_json(self, order_id):
        return {
            "order_id": order_id,
            "carrier_service_code": "CPOLE",
            "reprint": False
        }

    def check_result(self, result):
        return 'success' in result and result['success']

    def create_shipment(self, fo, account=None):
        header = {'Content-Type': 'application/json'}
        index = 0
        order_json = self.get_order_json(fo,account,index)
        if order_json:
            order = self.post('/api/orders', headers=header, json=order_json).json()
            while(not self.check_result(order)):
                if 'errors' in order and len(order['errors']) > 0:
                    if order['errors'][0]['message'] == 'Order Exists' or \
                            (order['errors'][0]['message'] == 'General Exception' and order['errors'][0]['details'] == 'Duplicate order number exists'):
                        print(order['errors'][0]['message'])
                        index += 1
                        order = self.post('/api/orders', headers=header, json=self.get_order_json(fo, account, index)).json()
                    else:
                        error_msg = ''
                        for error in order['errors']:
                            error_msg += '{msg}:{details}; '.format(msg=error['message'], details=error['details'])
                        raise Exception(error_msg)
                else:
                    return None
            return order['order']
        else:
            return None


    def print_label(self, id):
        header = {'Content-Type': 'application/json'}
        result = self.post('/api/orders/shipment', headers=header, json=self.__get_print_order_json(id)).json()
        return result

    def process_hook_request(self,request):
        pass
