from store.courier.core.freights import CourierBase
import urllib2
import json

SERVER = "http://api.gosweetspot.com"

def get_channel_handler(token,request):
    return GoSweetSpotFreight(token=token,request=request)

class GoSweetSpotFreight(CourierBase):
    def __get_request(self,path,json=False,body=None):
        headers = {"access_key": self.token}
        if json:
            headers.__setitem__("Content-Type","application/json; charset=utf-8")
        request = urllib2.Request(SERVER + path,
                                  headers=headers,data=body)
        return urllib2.urlopen(request).read()

    def get_rates(self,address):
        body = json.dumps(address)
        contents = self.__get_request("/api/rates",json=True,body=body)
        return json.loads(contents)

    def create_shipment(self,address):
        body = json.dumps(address)
        contents = self.__get_request("/api/shipments", json=True, body=body)
        return json.loads(contents)

    def get_label(self,cannote,filepath):
        path = "/api/labels?format=label_pdf&connote={c}".format(c=cannote)
        contents = self.__get_request(path)
        fh = open(filepath, "wb")
        fh.write(contents.decode('base64'))
        fh.close()
