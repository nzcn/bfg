from django.conf.urls import url
import store.courier.views as views

urlpatterns = [
    url(r'^get_rate$', views.get_rate, name='courier_get_rate'),
    url(r'^get_shipment$', views.get_rate, name='courier_get_shipment'),
    url(r'^hooks$', views.webhooks, name='webhooks'),
]
