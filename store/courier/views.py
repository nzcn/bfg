from django.http import JsonResponse,HttpResponseServerError
from store.courier.core.freights import CourierManager
from bfg.appa.core.common import *
from landing.core.common import *
from django.views.decorators.csrf import csrf_exempt
import json



def get_rate(request):
    address = json.loads(request.POST.get('body'))
    fm = CourierManager()
    token = company_account.account
    admin = fm.get_feight_admin(company_account.type,token)
    result = admin.get_rates(address)
    return JsonResponse(result,safe=False)

def get_shipment(request):
    if request.method == 'POST':
        if 'body' in request.POST:
            address = json.loads(request.POST.get('body'))
            company_id = get_user_company_id(request)
            fm = CourierManager()
            company_account = __get_company_account(fm, company_id, request)
            token = company_account.account
            admin = fm.get_feight_admin(company_account.type, token)
            return JsonResponse(admin.create_shipment(address))
        else:
            raise ValueError('body can not be found in POST')
    elif request.method == 'DELETE':
        pass
    else:
        pass

@csrf_exempt
def webhooks(request):
    account = get_req_value(request, "account")
    cm = CourierManager(request)
    account = cm.get_courier_account_by_account_id(account_id=account)
    courier = get_class_by_path(account.type)(request)
    courier.process_hook_request(request)
    return HttpResponse('Process webhooks successfully')
