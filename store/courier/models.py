from __future__ import unicode_literals
from bfg.appa.models import CompanyModelBase
from store.models import Country
from django.db import models


# Create your models here.

class Courier(models.Model):
    title = models.CharField(max_length=100, default=None)
    regex = models.CharField(max_length=100, default=None, null=True, blank=True)
    url = models.URLField(null=True, default=None, blank=True)
    image = models.CharField(max_length=200, null=True, blank=True)
    icon = models.URLField(null=True, default=None, blank=True)
    pos = models.PositiveSmallIntegerField(default=100, blank=True)
    address = models.CharField(max_length=100, default=None, null=True, blank=True)
    city = models.CharField(max_length=30, default=None, null=True, blank=True)
    country = models.ForeignKey(Country, default=None)
    enable = models.BooleanField(default=True)
    classpath = models.CharField(max_length=255, default=None, null=True, blank=True)

    def __str__(self):
        return self.title


class CourierAccount(CompanyModelBase):
    title = models.CharField(max_length=100, default=None, null=True, blank=True)
    code = models.CharField(max_length=10, default=None, null=True, blank=True)
    courier = models.ForeignKey(Courier, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    type = models.CharField(max_length=50, blank=True, null=True, default=None)
    account = models.CharField(max_length=60, blank=True, null=True, default=None)
    secrete = models.CharField(max_length=100, blank=True, null=True, default=None)
    create_time = models.DateTimeField(auto_now_add=True)
    pos = models.PositiveSmallIntegerField(default=100, blank=True)
    send_email = models.BooleanField(default=True, blank=True)
    contact_info = models.TextField(default=None, null=True, blank=True)
    length_difference = models.FloatField(default=0, blank=True)
    width_difference = models.FloatField(default=0, blank=True)
    height_difference = models.FloatField(default=0, blank=True)
    weight_difference = models.FloatField(default=0, blank=True)
    token = models.CharField(max_length=500, blank=True, null=True, default=None)
    regex = models.CharField(max_length=100, blank=True, null=True, default=None)
    token_expire_time = models.CharField(max_length=255, blank=True, null=True, default=None)
    max_pieces = models.SmallIntegerField(default=0, blank=True)
    max_piece_weight = models.FloatField(default=0, blank=True)
    max_piece_length = models.FloatField(default=0, blank=True)
    price_text = models.CharField(max_length=60, blank=True, null=True, default=None)
    price_calc = models.TextField(default=None, null=True, blank=True)
    rate = models.SmallIntegerField(default=0, blank=True)
    enable = models.BooleanField(default=True, blank=True)
    is_public = models.BooleanField(default=True, blank=True)

    def __str__(self):
        if self.code:
            return self.code
        elif self.title:
            return self.title
        elif self.courier:
            return self.courier.__str__()
        else:
            return 'Unknow Account'
