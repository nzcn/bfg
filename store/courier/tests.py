address = {
    "DeliveryReference": "ORDER123",
    "Destination": {
      "Id": 0,
      "Name": "DestinationName",
      "Address": {
        "BuildingName": "",
        "StreetAddress": "DestinationStreetAddress",
        "Suburb": "Avonside",
        "City": "Christchurch",
        "PostCode": "8061",
        "CountryCode": "NZ"
      },
    },
    "IsSaturdayDelivery": True,
    "IsSignatureRequired": True,
    "Packages": [
      {
        "Height": 20,
        "Length": 20,
        "Id": 0,
        "Width": 30,
        "Kg": 3.1,
        "Name": "GSS-DLE",
        "PackageCode": "DLE",
        "Type": "Box"
      }
    ]
  }

def test_courierpost(config):
  from store.courier.companies.courierpost.courier_manager import CourierPostFreight

  cpf = CourierPostFreight(token={
    'client_id':'a9ac1a856d974d8799b96038c682780e',
    'client_secret':'2EE94741C54B4F53887Ba0896e578ec3',
  })

  result = cpf.get_rates(address=address)
  print(result)


# def listCurrentPrinters():
#     conn = cups.Connection()
#     printers = conn.getPrinters()
#     currentPrinter =  None
#     for printer in printers:
#         if 'Canon' in printer:
#             currentPrinter = printer
#         print printer, printers[printer]["device-uri"]
#
#     if currentPrinter:
#         conn.printFile(currentPrinter,"/Users/yinxiangming/Downloads/import.pdf","PythonPrint",{})

def test_gosweetspot(config):
  from store.courier.companies.gosweetspot.courier_manager import GoSweetSpotFreight

  go = GoSweetSpotFreight('E79B07C5A4683DC3635CD6613756E465FC4F142442F4D93787')
  # go.get_label('ECE00001180','/Users/mark/Downloads/label.pdf')


  print(go.get_rates(address))

def test_webhooks(config):
  import requests
  result = requests.post("http://0.0.0.0:8098/store/courier/hooks?account=1",data=config)
  print(result.content)

test_webhooks('{"order_number":"PCL003443S PCL92038","carrier_name":"CourierPost","carrier_service":"CPOLE","shipment_date":"2020-05-06T17:12:29.9251474","tracking_number":"4871450000073101DUD006BS","tracking_status":"InTransit","last_updated_date":"2020-05-08T13:42:25.674763+00:00"}')

exit()