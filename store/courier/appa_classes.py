from django.contrib import admin
from bfg.appa.controllers.core import *
from bfg.appa.core.model import *

class CourierAccountAppa(CompanyBaseAdmin):
    list_display = ('title','code','courier','pos','send_email','type','account',
                    'regex','max_pieces', 'max_piece_weight', 'max_piece_length')
    search_fields = ('title','account','code')
    fieldsets = [
        (None, {'fields': ['title','code', 'courier','regex','pos','send_email', 'type',
                           'account', 'secrete', 'create_time','token','token_expire_time']}),
        ('Difference',
         {'fields': ['length_difference', 'width_difference', 'height_difference', 'weight_difference']}),
        ('Limitation',
         {'fields': ['max_pieces', 'max_piece_weight', 'max_piece_length','regex','rate']}),

    ]
    readonly_fields = ['create_time','token_expire_time','token']



