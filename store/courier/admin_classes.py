from django import forms
from django.forms import ModelForm, PasswordInput
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.core.cache import cache
from store.courier.models import *


class CourierAccountAdmin(admin.ModelAdmin):
    list_display = ('title', 'company', 'courier')
    search_fields = ('title',)
