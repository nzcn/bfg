import sys
from store.courier.models import *
from store.models import Shipment
from landing.models import Address

class CourierBase(object):
    '''
    Courier Base Class for all abstracted courier functions
    '''
    def __init__(self,token,request=None):
        self.token = token
        self.request =request

    def get_rates(self, address, weight, options=None, origin_address=None, volume=None, pieces=None):
        return 0

    def create_shipment(self,address):
        pass

    def create_label(self,shipment):
        pass

    def get_shipments(self,search):
        pass

    def download_label(self,id):
        pass

    def print_label(self,id):
        pass

    def check_shipment(self,shipment_id):
        pass

    def remove_shipment(self,shipment_id):
        pass

    def check_address(self,address):
        pass

    def search_address(self,address):
        pass

    def convert_address(self,address):
        pass

    def convert_rates(self,rates):
        pass

    def get_name(self):
        pass

    def create_orders_by_shipment(self,shipment):
        '''
        Create order and Set shipment properties
        :param shipment:
        :return:
        shipment.outer_id = order['order_number']
        shipment.courier_company = print_result['carrier_name']
        shipment.tracking_number = tracking_numbers[0]
        shipment.tracking_numbers = ''
        shipment.label_pdf_path =
        '''
        pass

    def get_tracking_link(self, tracking_number):
        '''
        Get tracking number full link.
        :param tracking_number:
        :return:
        '''
        pass

    def process_hook_request(self, request):
        pass


class CourierManager(object):

    def __init__(self,request=None):
        self.request =request

    def get_feight_admin(self,type,token):
        manager_module = self.get_channel_module(type)
        return manager_module.get_channel_handler(token,None)

    def get_channel_config(self, type, ext ='config'):
        config_module_name = "courier.companies.{addon}.{ext}".format(addon=type,ext=ext)
        if not config_module_name in sys.modules:
            __import__(config_module_name)
        module = sys.modules[config_module_name]
        return module

    def get_channel_module(self,type):
        return self.get_channel_config(type,'courier_manager')

    def get_couriers(self,country=None):
        return Courier.objects.all()

    def get_courier_account(self,company_id,courier_id):
        q = CourierAccount.objects.filter(company__id=company_id,courier__id=courier_id)
        if q.exists():
            return q.first()
        else:
            return None

    def get_courier_account_by_account_id(self,account_id):
        return CourierAccount.objects.get(pk=account_id)

    def get_shipment(self,order_id,tracking_number=None):
        q = Shipment.objects.filter(outer_id=order_id)
        if q.exists():
            return q.first()
        else:
            q = Shipment.objects.filter(tracking_numbers__icontains=tracking_number)
            if q.exists():
                return q.first()
            else:
                return None

    def get_shipment_by_id(self,id):
        try:
            return Shipment.objects.get(id=id)
        except Exception as ex:
            return None


