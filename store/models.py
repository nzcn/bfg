# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
from datetime import *
from decimal import Decimal

from django.shortcuts import *
from django.utils.translation import gettext_lazy as _
from payments import PurchasedItem
from payments.models import BasePayment

from bfg.appa.models import CompanyModelBase, Device, Company, Staff as AppaStaff
# from django.contrib.gis.db import models as gismodels
from landing.models import *

CONST_STORE_SITE_VARS = "STORE_VARS"


class Country(models.Model):
    abstraction = models.CharField(max_length=2, default=None)
    abstraction3 = models.CharField(max_length=3, default=None, null=True, blank=True)
    name = models.CharField(max_length=150, default=None)
    slug = models.CharField(max_length=30)
    phonecode = models.CharField(max_length=4, default=None, blank=True, null=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True, null=True)
    out_id = models.PositiveIntegerField(default=0, unique=True)
    enable = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Country")
        verbose_name_plural = _("Countries")


class State(models.Model):
    name = models.CharField(max_length=30, default=None, null=True, blank=True)
    slug = models.CharField(max_length=30)
    country = models.ForeignKey(Country, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    out_id = models.PositiveIntegerField(default=0, unique=True, blank=True)
    enable = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("State")
        verbose_name_plural = _("States")


class City(models.Model):
    name = models.CharField(max_length=30, default=None)
    slug = models.CharField(max_length=30)
    state = models.ForeignKey(State, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    out_id = models.PositiveIntegerField(default=0, unique=True)
    enable = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("City")
        verbose_name_plural = _("Cities")


class Suburb(models.Model):
    name = models.CharField(max_length=30, default=None)
    slug = models.CharField(max_length=30)
    city = models.ForeignKey(City, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    out_id = models.PositiveIntegerField(default=0, unique=True)
    enable = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Suburb")
        verbose_name_plural = _("Suburbs")


class Currency(models.Model):
    title = models.CharField(max_length=20)
    abstraction = models.CharField(max_length=5)
    sign = models.CharField(max_length=2)
    one_USD = models.FloatField(default=0, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    enable = models.BooleanField(default=True)

    def __str__(self):
        return self.abstraction

    class Meta:
        verbose_name = _("Currency")
        verbose_name_plural = _("Currencies")


class Tax(models.Model):
    title = models.CharField(max_length=20)
    tax = models.FloatField(default=0, blank=True)
    pos = models.SmallIntegerField(default=0, blank=True)

    class Meta:
        verbose_name = _("Tax")
        verbose_name_plural = _("Taxes")


class BankAccount(CompanyModelBase):
    title = models.CharField(max_length=50)
    key = models.CharField(max_length=10, default=None, null=True, blank=True)
    account = models.CharField(max_length=50)
    account_name = models.CharField(max_length=50, default=None, null=True, blank=True)
    swift = models.CharField(max_length=20, default=None, null=True, blank=True)
    bank = models.CharField(max_length=100, default=None, null=True, blank=True)
    details = models.CharField(max_length=200, default=None, null=True, blank=True)
    currency = models.ForeignKey(Currency, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    wechat_qr = models.ImageField(default=None, null=True, blank=True)
    icon = models.ImageField(default=None, null=True, blank=True, upload_to='upload/payment')
    pos = models.SmallIntegerField(default=0, blank=True)
    is_public = models.BooleanField(default=False)

    def __str__(self):
        return self.title if self.title else self.account


class Store(CompanyModelBase):
    title = models.CharField(max_length=50)
    url = models.CharField(max_length=100, unique=True)
    secure_url = models.CharField(max_length=100, unique=True)
    address = models.ForeignKey(Address, null=True, default=None, blank=True, on_delete=models.SET_DEFAULT)
    tax_rate = models.FloatField(default=0, blank=True)
    tax_name = models.CharField(default='GST', max_length=10)
    theme = models.CharField(max_length=20, default=None, null=True, blank=True)
    email_admin = models.EmailField(null=True, default=None, blank=True)
    email_sales = models.EmailField(null=True, default=None, blank=True)
    email_order = models.EmailField(null=True, default=None, blank=True)
    email_user = models.EmailField(null=True, default=None, blank=True)
    email_news_letter = models.EmailField(null=True, default=None, blank=True)

    class Meta:
        verbose_name = _("Store")
        verbose_name_plural = _("Stores")


class PaymentGateway(CompanyModelBase):
    title = models.CharField(max_length=20)
    code = models.CharField(max_length=20, default=None, null=True, blank=True)
    icon = models.ImageField(default=None, null=True, blank=True)
    description = models.TextField(default=None, null=True, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    settings = models.TextField(default=None, null=True, blank=True)
    test_settings = models.TextField(default=None, null=True, blank=True)
    test_mode = models.BooleanField(default=False)
    surcharge = models.FloatField(default=0, blank=True)
    enable = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("PaymentGateway")
        verbose_name_plural = _("PaymentGateways")


class PackType(CompanyModelBase):
    title = models.CharField(max_length=10)
    icon = models.ImageField(default=None, null=True, blank=True)
    description = models.TextField(default=None, null=True, blank=True)
    long = models.PositiveSmallIntegerField(default=0, blank=True)
    width = models.PositiveSmallIntegerField(default=0, blank=True)
    height = models.PositiveSmallIntegerField(default=0, blank=True)
    weight = models.PositiveSmallIntegerField(default=0, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    enable = models.BooleanField(default=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Pack Type")
        verbose_name_plural = _("Pack Types")


class ShippingArea(CompanyModelBase):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500, default=None, null=True, blank=True)
    countries = models.ManyToManyField(Country, blank=True)
    states = models.ManyToManyField(State, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    enable = models.BooleanField(default=True, blank=True)

    class Meta:
        verbose_name = _("ShippingArea")
        verbose_name_plural = _("ShippingAreas")

    def __str__(self):
        return self.title


class ShippingAreaCost(CompanyModelBase):
    area = models.ForeignKey(ShippingArea, on_delete=models.CASCADE)
    title = models.CharField(max_length=50, default=None, null=True, blank=True)
    cost = models.TextField(default=None, null=True, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)

    class Meta:
        verbose_name = _("ShippingAreaCost")
        verbose_name_plural = _("ShippingAreaCosts")


class Channel(CompanyModelBase):
    title = models.CharField(max_length=50)
    key = models.CharField(max_length=10, unique=True, null=True, blank=True, default=None)
    enable = models.BooleanField(default=True)
    admin = models.ForeignKey(AppaStaff, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    members = models.ManyToManyField('Customer', blank=True)
    create_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    active_time = models.DateTimeField(default=None, null=True, blank=True)
    wechat_mp_qr = models.FilePathField(default=None, null=True, blank=True)
    wechat_app_qr = models.FilePathField(default=None, null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Channel")
        verbose_name_plural = _("Channels")


class Customer(AdminModels.User):
    company = models.ForeignKey(Company, null=True, blank=True, default=None, related_name='customer_company',
                                on_delete=models.SET_DEFAULT)
    logo = models.ImageField(blank=True, null=True, default=None)
    picture = models.ImageField(null=True, default=None, blank=True)
    video = models.URLField(default=None, null=True, blank=True)
    gender = models.CharField(max_length=1, default='M', choices=GenderType)
    qrcode = models.ImageField(default=None, null=True, blank=True)
    customer_id = models.CharField(max_length=50, default=None, null=True, blank=True)
    from_channel = models.ForeignKey(Channel, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    sales = models.ForeignKey(AppaStaff, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    settings = models.ForeignKey(Profile, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    experience = models.PositiveIntegerField(default=0, blank=True)
    gold = models.FloatField(default=0, blank=True)
    silver = models.FloatField(default=0, blank=True)
    feedback_index = models.FloatField(default=3, blank=True)
    value_index = models.FloatField(default=3, blank=True)
    signature = models.TextField(null=True, default=None, blank=True)
    rank_up = models.PositiveSmallIntegerField(default=0, blank=True)
    rank_down = models.PositiveSmallIntegerField(default=0, blank=True)
    key = models.UUIDField(max_length=50, unique=True, editable=False, blank=True, default=uuid.uuid4)
    security_code = models.CharField(max_length=6, default=None, null=True, blank=True)
    agree_term_version = models.CharField(max_length=10, default=None, null=True, blank=True)
    agree_term_time = models.DateTimeField(default=None, null=True, blank=True)
    created_time = models.DateTimeField(null=True, blank=True, auto_now_add=True)
    last_modified_time = models.DateTimeField(null=True, blank=True, default=None)
    active_index = models.PositiveIntegerField(default=0, blank=True)
    email_request_update = models.BooleanField(default=True, blank=True)
    lat = models.FloatField(default=0, blank=True)
    lng = models.FloatField(default=0, blank=True)
    tel = models.CharField(max_length=20, default=None, null=True, blank=True)
    city = models.ForeignKey(City, null=True, default=None, blank=True, on_delete=models.SET_DEFAULT)
    suburb = models.ForeignKey(Suburb, null=True, default=None, blank=True, on_delete=models.SET_DEFAULT)

    sales_note = models.TextField(default=None, null=True, blank=True)
    bank_statement_reference = models.TextField(default=None, null=True, blank=True)
    customer_note = models.CharField(max_length=200, default=None, null=True, blank=True)

    referal = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)

    nickname = models.CharField(max_length=50, default=None, null=True, blank=True)

    account_facebook = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_googleplus = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_twitter = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_instgram = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_pintrest = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_wechat = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_qq = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_facetime = models.CharField(max_length=50, default=None, null=True, blank=True)
    account_whatsapp = models.CharField(max_length=50, default=None, null=True, blank=True)

    favorites = models.ManyToManyField('Product', blank=True)
    interests = models.ManyToManyField(Tag, blank=True)
    devices = models.ManyToManyField(Device, blank=True)
    comments = models.ManyToManyField(Comment, blank=True)

    verified = models.DateTimeField(default=None, null=True, blank=True)

    class Meta:
        verbose_name = _("Customer")
        verbose_name_plural = _("Customers")

    def __str__(self):
        if self.first_name:
            return self.get_full_name()
        else:
            return self.username


class Function(CompanyModelBase):
    title = models.CharField(max_length=50)
    content = RichTextUploadingField(null=True, default=None, blank=True)
    has_star = models.BooleanField(default=False, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    enable = models.BooleanField(default=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Function")
        verbose_name_plural = _("Functions")


CONTENT_STATUS = (
    ('A', 'Active'),
    ('H', 'Hidden'),
    ('D', 'Disabled'),
)

GROUP_TYPE = (
    ('C', 'Customers'),
    ('A', 'Administrators'),
)


class CustomerGroup(AdminModels.Group, CompanyModelBase):
    type = models.CharField(max_length=1, default='C', choices=GROUP_TYPE)
    status = models.CharField(max_length=1, default='A', choices=CONTENT_STATUS)

    class Meta:
        verbose_name = _("Customer Grooup")
        verbose_name_plural = _("Customer Groups")


class Category(CompanyModelBase):
    store = models.ForeignKey(Store, default=None, blank=True, null=True, on_delete=models.SET_DEFAULT)
    title = models.CharField(max_length=50)
    slug = models.CharField(blank=True, max_length=100)
    path = models.CharField(blank=True, max_length=500)
    meta_title = models.CharField(max_length=200, default=None, null=True, blank=True)
    description = models.CharField(max_length=200, default=None, null=True, blank=True)
    keywords = models.CharField(max_length=200, default=None, null=True, blank=True)
    content = RichTextUploadingField(null=True, default=None, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    status = models.CharField(max_length=1, default='A', choices=CONTENT_STATUS)
    picture = models.ImageField(max_length=255, null=True, default=None, blank=True)
    create_time = models.DateTimeField(auto_created=True, null=True, blank=True, default=None)
    last_modified_time = models.DateTimeField(auto_now=True, null=True, blank=True)
    parent = models.ForeignKey('self', null=True, blank=True, default=None, on_delete=models.CASCADE)
    show_header = models.BooleanField(default=False, blank=True)
    in_menu = models.BooleanField(default=True, blank=True)
    age_limit = models.SmallIntegerField(default=0, blank=True)
    age_verification = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        unique_together = (('store', 'path'), ('store', 'slug'))
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")


class Option(CompanyModelBase):
    title = models.CharField(max_length=50)
    is_global = models.BooleanField(default=False)
    settings = models.TextField(default=None, null=True, blank=True)
    pos = models.PositiveSmallIntegerField(default=0)


class Variant(CompanyModelBase):
    options = models.TextField(default=None, null=True, blank=True)
    title = models.CharField(max_length=250)
    is_global = models.BooleanField(default=False)
    thumb = models.ImageField(max_length=255, null=True, default=None, blank=True)
    picture = models.ImageField(max_length=255, null=True, default=None, blank=True)
    pos = models.PositiveSmallIntegerField(default=0)
    content = RichTextUploadingField(null=True, default=None)
    price = models.FloatField(default=0)
    weight = models.FloatField(default=0)
    sku = models.CharField(max_length=30, default=None, null=True, blank=True)
    height = models.FloatField(default=0)
    quantity = models.PositiveIntegerField(default=0)


class Discount(CompanyModelBase):
    title = models.CharField(max_length=50)
    min_quantity = models.PositiveIntegerField(default=0)
    max_quantity = models.PositiveIntegerField(default=0)
    discount = models.FloatField(default=1)
    price_off = models.FloatField(default=0)
    price = models.FloatField(default=0)
    enable = models.BooleanField(default=True)
    description = models.CharField(max_length=200, default=None, null=True, blank=True)
    pos = models.PositiveSmallIntegerField(default=0)
    from_date = models.DateTimeField(null=True, blank=True, default=None)
    to_date = models.DateTimeField(null=True, blank=True, default=None)
    groups = models.ManyToManyField(CustomerGroup, blank=True)

    class Meta:
        verbose_name = _("Discount")
        verbose_name_plural = _("Discounts")


OutOfStockAction = (
    ("N", "None"),
    ("B", "Buy in advance"),
    ("S", "Signup for notification"),
)

ZeroPriceAction = (
    ("R", "Do not allow customers add to cart"),
    ("P", "Allow to add to cart"),
    ("A", "Ask customer to enter the price"),
)


class Product(CompanyModelBase):
    site = models.ForeignKey(Site, null=True, default=None, blank=True, on_delete=models.SET_DEFAULT)
    title = models.CharField(max_length=300)
    user = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    local_title = models.CharField(max_length=300, default=None, null=True, blank=True)
    category = models.ForeignKey(Category, blank=True, null=True, default=None, related_name='main_category',
                                 on_delete=models.SET_DEFAULT)
    slug = models.SlugField(default=None, null=True, blank=True, max_length=500)
    quantity = models.PositiveIntegerField(default=0, blank=True)
    code = models.CharField(blank=True, max_length=100)
    sku = models.CharField(null=True, default=None, unique=True, blank=True, max_length=30)
    image = models.ImageField(default=None, null=True, blank=True, max_length=500)
    video = models.URLField(default=None, null=True, blank=True, max_length=500)
    thumb = models.ImageField(default=None, null=True, blank=True, max_length=500)
    create_time = models.DateTimeField(auto_created=True, null=True, blank=True, default=None)
    last_modified_time = models.DateTimeField(auto_now=True, null=True, blank=True)
    publish_time = models.DateTimeField(default=None, null=True, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    active_index = models.PositiveIntegerField(default=0, blank=True)
    enable = models.BooleanField(default=True)
    product_type = models.CharField(max_length=1, default='P', db_index=True, blank=True)
    status = models.CharField(max_length=1, default='A', choices=CONTENT_STATUS, blank=True)

    meta_title = models.CharField(max_length=500, default=None, null=True, blank=True)
    description = models.CharField(max_length=500, default=None, null=True, blank=True)
    keywords = models.CharField(max_length=500, default=None, null=True, blank=True)
    subtitle = models.TextField(blank=True, null=True, default=None)
    content = RichTextUploadingField(blank=True, null=True, default=None)

    price_text = models.CharField(max_length=200, default=None, null=True, blank=True)
    was_price = models.FloatField(default=0, blank=True)
    price = models.FloatField(default=0, blank=True)
    tax = models.ForeignKey(Tax, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    list_qty_count = models.SmallIntegerField(default=0, blank=True)
    low_qty_alert = models.PositiveIntegerField(default=0, blank=True)
    max_qty_in_box = models.SmallIntegerField(default=0, blank=True)
    min_qty_in_box = models.SmallIntegerField(default=0, blank=True)
    min_qty = models.SmallIntegerField(default=0, blank=True)
    max_qty = models.SmallIntegerField(default=0, blank=True)
    step_qty = models.SmallIntegerField(default=0, blank=True)
    return_days = models.SmallIntegerField(default=0, blank=True)

    out_of_stock_action = models.CharField(max_length=1, default='N', choices=OutOfStockAction, blank=True)
    zero_price_action = models.CharField(max_length=1, default='R', choices=ZeroPriceAction, blank=True)

    unit = models.CharField(max_length=20, default=None, null=True, blank=True)
    currency = models.ForeignKey(Currency, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)

    vote = models.FloatField(default=3, blank=True)
    view_count = models.PositiveSmallIntegerField(default=0, blank=True)
    buy_count = models.PositiveSmallIntegerField(default=0, blank=True)

    is_used = models.BooleanField(default=False)
    is_clearance = models.BooleanField(default=False)
    is_hot = models.BooleanField(default=False)
    is_service = models.BooleanField(default=False)
    is_online_product = models.BooleanField(default=True)
    is_free_shipping = models.BooleanField(default=False)

    weight = models.FloatField(default=0, verbose_name=_("Weight"), blank=True)
    height = models.FloatField(default=0, verbose_name=_("Height"), blank=True)
    width = models.FloatField(default=0, verbose_name=_("Width"), blank=True)
    length = models.FloatField(default=0, verbose_name=_("Length"), blank=True)
    shipping_freights = models.FloatField(default=0, blank=True)
    items_in_box = models.SmallIntegerField(default=0, blank=True)
    box_weight = models.FloatField(default=0, verbose_name=_("Weight"), blank=True)
    box_height = models.FloatField(default=0, verbose_name=_("Height"), blank=True)
    box_width = models.FloatField(default=0, verbose_name=_("Width"), blank=True)
    box_length = models.FloatField(default=0, verbose_name=_("Length"), blank=True)

    success_action = models.CharField(max_length=200, default=None, null=True, blank=True)
    template = models.CharField(max_length=200, default=None, null=True, blank=True)
    # For Export or Import Usage
    material = models.CharField(max_length=20, null=True, blank=True, default=None)
    brand = models.CharField(max_length=50, null=True, blank=True, default=None)
    usage = models.CharField(max_length=50, null=True, blank=True, default=None)

    variants = models.ManyToManyField(Variant, blank=True)
    options = models.ManyToManyField(Option, through='ProductOption', blank=True)
    subscribers = models.ManyToManyField(AdminModels.User, related_name='subscribers', blank=True)
    discounts = models.ManyToManyField(Discount, blank=True)
    images = models.ManyToManyField(Image, blank=True, through='ProductImage')
    files = models.ManyToManyField(File, blank=True, related_name='files')
    tags = models.ManyToManyField(Tag, blank=True)
    categories = models.ManyToManyField(Category, through='ProductCategory', blank=True)
    groups = models.ManyToManyField(CustomerGroup, blank=True)
    required = models.ManyToManyField('self', blank=True)

    class Meta:
        unique_together = ("company", "slug")
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def __str__(self):
        return self.title


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    pos = models.SmallIntegerField(default=0)


class ProductCategory(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    is_main = models.BooleanField(default=False)


class ProductOption(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    option = models.ForeignKey(Option, on_delete=models.CASCADE)
    title = models.CharField(max_length=30, default=None, null=True, blank=True)
    image = models.CharField(max_length=100, default=None, null=True, blank=True)
    thumb = models.CharField(max_length=100, default=None, null=True, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True)
    enable = models.BooleanField(default=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    price = models.FloatField(default=0, blank=True)
    quantity = models.PositiveIntegerField(default=0, blank=True)
    value = models.CharField(max_length=100, default=None, null=True, blank=True)


class StoreBranch(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    address = models.ForeignKey(Address, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    pos = models.PositiveSmallIntegerField(default=0)


class BranchInventory(models.Model):
    branch = models.ForeignKey(StoreBranch, on_delete=models.CASCADE)
    variant = models.ForeignKey(Variant, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=0)


class ProductInstance(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    code = models.CharField(max_length=100, default=None, null=True, blank=True)
    content = models.TextField(default=None, null=True, blank=True)
    create_time = models.DateTimeField(default=None, null=True, blank=True)
    sold_time = models.DateTimeField(default=None, null=True, blank=True)
    email_sent_time = models.DateTimeField(default=None, null=True, blank=True)


PricePlan_Unit = (
    ('W', 'Week'),
    ('M', 'Month'),
    ('F', 'Fortnight'),
    ('Y', 'Year'),
    ('D', 'Day'),
)

PricePlan_Type = (
    ('P', 'Plan'),
    ('A', 'Addon'),
)


class PlanFunction(models.Model):
    function = models.ForeignKey(Function, null=True, blank=True, default=None, on_delete=models.CASCADE)
    plan = models.ForeignKey('PricePlan', on_delete=models.CASCADE)
    title = models.CharField(max_length=50, default=None, null=True, blank=True)
    pos = models.PositiveSmallIntegerField(default=0)
    content = RichTextUploadingField(null=True, default=None, blank=True)

    def __str__(self):
        return self.plan.title + "-" + self.function.title


class PricePlan(CompanyModelBase):
    title = models.CharField(max_length=50)
    code = models.CharField(blank=True, max_length=10, unique=True)
    subtitle = models.TextField(blank=True, null=True, default=None)
    content = RichTextUploadingField(blank=True, null=True, default=None)
    style = models.CharField(blank=True, null=True, default=None, max_length=200)
    price_text = models.CharField(max_length=200, default=None, null=True, blank=True)
    upgrade_text = models.TextField(default=None, null=True, blank=True)
    image = models.ImageField(default=None, null=True, blank=True)
    thumb = models.ImageField(default=None, null=True, blank=True)
    price = models.FloatField(default=1000)
    was_price = models.FloatField(default=1000)
    tax = models.ForeignKey(Tax, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    quantity = models.PositiveIntegerField(default=0)
    unit = models.CharField(max_length=1, default='W', choices=PricePlan_Unit)
    type = models.CharField(max_length=1, default='P', choices=PricePlan_Type)
    currency = models.ForeignKey(Currency, blank=True, default=None, null=True, on_delete=models.SET_DEFAULT)
    features = models.ManyToManyField(PlanFunction, blank=True)
    enable = models.BooleanField(default=True)
    group = models.ForeignKey(AdminModels.Group, default=None, blank=True, null=True, on_delete=models.SET_DEFAULT)
    pos = models.PositiveSmallIntegerField(default=0)
    addons = models.ManyToManyField('PricePlan', related_name='children', blank=True)
    is_online_product = models.BooleanField(default=True)
    success_action = models.CharField(max_length=200, default=None, null=True, blank=True)

    def __str__(self):
        return self.title

    def get_days(self, quantity):
        if self.unit == 'M':
            return quantity * 30
        elif self.unit == 'Y':
            return quantity * 365
        elif self.unit == 'D':
            return quantity


class PlanPromotion(models.Model):
    plan = models.ForeignKey(PricePlan, related_name='promotions', on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    min_quantity = models.PositiveIntegerField(default=0)
    discount = models.FloatField(default=1)
    price_off = models.FloatField(default=0)
    pos = models.PositiveSmallIntegerField(default=0)


PURCHASE_STATUS = (
    ('T', 'Draft'),
    ('O', 'Open'),
    ('FM', 'Confirmed'),
    ('AU', 'Preauthed'),
    ('P', 'Paid'),
    ('PP', 'Partly Paid'),
    ('D', 'Processed'),
    ('C', 'Complete'),
    ('U', 'Finished'),
    ('CA', 'Cancel'),
    ('FA', 'Fail'),
    ('EX', 'Expire'),
    ('RE', 'Refund'),
    ('SH', 'Shipped'),
)


class CostGroup(CompanyModelBase):
    title = models.CharField(max_length=50, null=True, blank=True, default=None)

    def __str__(self):
        return self.title


class CostType(CompanyModelBase):
    title = models.CharField(max_length=100, null=True, blank=True, default=None)
    is_system = models.BooleanField(default=False)
    need_confirmed = models.BooleanField(default=False)
    need_pay = models.BooleanField(default=False)
    description = models.TextField(default=None, null=True, blank=True)
    pos = models.SmallIntegerField(default=100)
    code = models.CharField(max_length=30, null=True, blank=True, default=None)
    outer_code = models.CharField(max_length=10, null=True, blank=True, default=None)
    currency = models.ForeignKey(Currency, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    group = models.ForeignKey(CostGroup, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    include_tax = models.BooleanField(default=True, blank=True)
    amount = models.FloatField(default=0)
    min_amount = models.FloatField(default=0)
    max_amount = models.FloatField(default=0)
    unit = models.CharField(max_length=10, null=True, blank=True, default=None)
    discount = models.FloatField(default=0)

    def __str__(self):
        return str(self.title) if self.title else 'Unknown CostType'

    class Meta:
        unique_together = (('company', 'outer_code'))


class InvoiceTemplate(CompanyModelBase):
    title = models.CharField(max_length=50, null=True, blank=True, default=None)
    costs = models.ManyToManyField(CostType, blank=True)
    pos = models.SmallIntegerField(default=100)
    currency = models.ForeignKey(Currency, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    title_eval = models.CharField(max_length=100, null=True, blank=True, default=None)
    reference_eval = models.CharField(max_length=100, null=True, blank=True, default=None)
    code_eval = models.CharField(max_length=100, null=True, blank=True, default=None)
    description = models.TextField(null=True, blank=True, default=None)

    def __str__(self):
        return str(self.title) if self.title else 'Unknown InvoiceTemplate'


class Invoice(CompanyModelBase):
    title = models.CharField(max_length=100, null=True, blank=True, default=None)
    description = models.TextField(null=True, blank=True, default=None)
    creator = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, related_name='inv_creator',
                                on_delete=models.SET_DEFAULT)
    auditor = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, related_name='inv_auditor',
                                on_delete=models.SET_DEFAULT)
    payment = models.ForeignKey('Payment', null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    customer = models.ForeignKey(Customer, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    quantity = models.PositiveSmallIntegerField(default=1)
    discount = models.FloatField(default=1)
    price = models.FloatField(default=0)
    total = models.FloatField(default=0)
    total_paid = models.FloatField(default=0)
    shipping_cost = models.FloatField(default=0)
    tax = models.FloatField(default=0)
    snapshot = models.TextField(default=None, null=True)
    random = models.CharField(max_length=16, null=True, blank=True, default=None)
    key = models.UUIDField(null=True, blank=True, default=None, unique=True, auto_created=True)
    access_code = models.CharField(max_length=4, null=True, blank=True, default=None)
    access_expire = models.DateTimeField(default=None, null=True, blank=True)
    out_id = models.CharField(max_length=50, null=True, blank=True, default=None)
    invoice_file = models.FileField(null=True, blank=True, default=None)
    payment_proof = models.ImageField(null=True, blank=True, default=None)
    status = models.CharField(max_length=2, default='T', choices=PURCHASE_STATUS)

    reference = models.CharField(max_length=20, null=True, blank=True)
    create_time = models.DateTimeField(auto_now_add=True)
    start_date = models.DateField(default=None, null=True, blank=True)
    due_date = models.DateField(default=None, null=True, blank=True)
    expected_payment_date = models.DateField(default=None, null=True, blank=True)
    last_modified = models.DateTimeField(null=True, default=None, blank=True)
    audit_time = models.DateTimeField(null=True, default=None, blank=True)
    sent_time = models.DateTimeField(null=True, default=None, blank=True)

    files = models.ManyToManyField(File, blank=True, related_name='InvoiceFiles')
    comments = models.ManyToManyField(Comment, blank=True, related_name='InvoiceComments')
    payments = models.ManyToManyField('Payment', blank=True, related_name='InvoicePayments')

    buyer_fname = models.CharField(max_length=100, null=True, blank=True)
    buyer_lname = models.CharField(max_length=30, null=True, blank=True)
    buyer_mname = models.CharField(max_length=30, null=True, blank=True)
    buyer_email = models.CharField(max_length=50, null=True, blank=True)
    buyer_phone = models.CharField(max_length=50, null=True, blank=True)

    shipping_addr_fname = models.CharField(max_length=250, default=None, null=True, blank=True)
    shipping_addr_lname = models.CharField(max_length=250, default=None, null=True, blank=True)
    shipping_addr_building = models.CharField(max_length=150, default=None, null=True, blank=True)
    shipping_addr_street = models.CharField(max_length=200, default=None, null=True, blank=True)
    shipping_addr_street2 = models.CharField(max_length=200, default=None, null=True, blank=True)
    shipping_addr_suburb = models.CharField(max_length=150, default=None, null=True, blank=True)
    shipping_addr_phone = models.CharField(max_length=550, default=None, null=True, blank=True)
    shipping_addr_city = models.CharField(max_length=50, default=None, null=True, blank=True)
    shipping_addr_state = models.CharField(max_length=50, default=None, null=True, blank=True)
    shipping_addr_postcode = models.CharField(max_length=10, default=None, null=True, blank=True)
    shipping_addr_country = models.CharField(max_length=50, default=None, null=True, blank=True)

    billing_addr_fname = models.CharField(max_length=250, default=None, null=True, blank=True)
    billing_addr_lname = models.CharField(max_length=250, default=None, null=True, blank=True)
    billing_addr_building = models.CharField(max_length=150, default=None, null=True, blank=True)
    billing_addr_street = models.CharField(max_length=200, default=None, null=True, blank=True)
    billing_addr_street2 = models.CharField(max_length=200, default=None, null=True, blank=True)
    billing_addr_suburb = models.CharField(max_length=150, default=None, null=True, blank=True)
    billing_addr_phone = models.CharField(max_length=50, default=None, null=True, blank=True)
    billing_addr_city = models.CharField(max_length=50, default=None, null=True, blank=True)
    billing_addr_state = models.CharField(max_length=50, default=None, null=True, blank=True)
    billing_addr_postcode = models.CharField(max_length=10, default=None, null=True, blank=True)
    billing_addr_country = models.CharField(max_length=50, default=None, null=True, blank=True)

    def __str__(self):
        return self.title or 'Unknown Invoice'

    @property
    def lock(self):
        return self.status in ['FM', 'P']


class InvoiceDetail(models.Model):
    invoice = models.ForeignKey(Invoice, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    title = models.CharField(max_length=100, null=True, default=None, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True, default=None)
    cost_type = models.ForeignKey(CostType, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    create_time = models.DateTimeField(auto_now_add=True)
    quantity = models.FloatField(default=1)
    price = models.FloatField(default=0)
    tax = models.FloatField(default=0)
    discount = models.FloatField(default=1)
    subtotal = models.FloatField(default=0)
    pos = models.SmallIntegerField(default=100)


class Order(CompanyModelBase):
    title = models.CharField(verbose_name=_("Title"), max_length=100, null=True, blank=True, default=None)
    type = models.CharField(max_length=2, default='O')
    invoices = models.ManyToManyField(Invoice)
    creator = models.ForeignKey(AdminModels.User, null=True, blank=True,
                                default=None, related_name='creator', on_delete=models.SET_DEFAULT)
    user = models.ForeignKey(AdminModels.User, null=True, blank=True,
                             default=None, verbose_name=_("Customer"), on_delete=models.SET_DEFAULT)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name=_("Create Time"))
    plan = models.ForeignKey(PricePlan, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    quantity = models.PositiveSmallIntegerField(default=1, verbose_name=_("Quantity"))
    discount = models.FloatField(default=1, verbose_name=_("Discount"))
    price = models.FloatField(default=0, verbose_name=_("Price"))
    total = models.FloatField(default=0, verbose_name=_("Total Price"))
    currency = models.ForeignKey(Currency, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    shipping_cost = models.FloatField(default=0, verbose_name=_("Shipping Cost"))
    tax = models.FloatField(default=0, verbose_name=_("Tax"))
    success_fee = models.FloatField(default=0, verbose_name=_("Success Fee"))
    trading_fee = models.FloatField(default=0, verbose_name=_("Tradeing Fee"))
    weight = models.FloatField(default=0, verbose_name=_("Weight"))
    height = models.FloatField(default=0, verbose_name=_("Height"))
    width = models.FloatField(default=0, verbose_name=_("Width"))
    length = models.FloatField(default=0, verbose_name=_("Length"))
    geo_lat = models.FloatField(default=0)
    geo_lng = models.FloatField(default=0)
    snapshot = models.TextField(default=None, null=True, blank=True)
    order_ip = models.GenericIPAddressField(default=None, null=True, blank=True)

    buyer_note = models.TextField(default=None, null=True, blank=True)
    seller_note = models.TextField(default=None, null=True, blank=True)

    random = models.CharField(max_length=16, null=True, blank=True, default=None)
    access_code = models.CharField(max_length=4, null=True, blank=True, default=None)
    access_expire = models.DateTimeField(default=None, null=True, blank=True)

    status = models.CharField(max_length=2, default='T', choices=PURCHASE_STATUS)
    pay_time = models.DateTimeField(default=None, null=True, blank=True)
    reference = models.CharField(max_length=20, null=True, blank=True)
    image_url = models.CharField(max_length=100, null=True, blank=True, default=None)
    shipping_time = models.DateTimeField(null=True, blank=True, default=None)
    delivery_time = models.DateTimeField(null=True, blank=True, default=None)
    shipping_informed = models.BooleanField(default=False)
    tracking_no = models.CharField(max_length=20, null=True, blank=True, default=None)
    last_modified = models.DateTimeField(null=True, default=None)

    is_rural = models.BooleanField(default=False)
    is_pobox = models.BooleanField(default=False)
    supports_saturday_delivery = models.BooleanField(default=False)
    supports_evening_delivery = models.BooleanField(default=False)
    checked_address = models.TextField(default=None, null=True, blank=True)

    buyer_fname = models.CharField(max_length=100, null=True, blank=True)
    buyer_lname = models.CharField(max_length=30, null=True, blank=True)
    buyer_mname = models.CharField(max_length=30, null=True, blank=True)
    buyer_email = models.CharField(max_length=50, null=True, blank=True)
    buyer_phone = models.CharField(max_length=50, null=True, blank=True)

    shipping_addr_fname = models.CharField(max_length=250, default=None, null=True, blank=True)
    shipping_addr_lname = models.CharField(max_length=250, default=None, null=True, blank=True)
    shipping_addr_building = models.CharField(max_length=150, default=None, null=True, blank=True)
    shipping_addr_street = models.CharField(max_length=200, default=None, null=True, blank=True)
    shipping_addr_street2 = models.CharField(max_length=200, default=None, null=True, blank=True)
    shipping_addr_suburb = models.CharField(max_length=150, default=None, null=True, blank=True)
    shipping_addr_phone = models.CharField(max_length=550, default=None, null=True, blank=True)
    shipping_addr_city = models.CharField(max_length=50, default=None, null=True, blank=True)
    shipping_addr_state = models.CharField(max_length=50, default=None, null=True, blank=True)
    shipping_addr_postcode = models.CharField(max_length=10, default=None, null=True, blank=True)
    shipping_addr_country = models.CharField(max_length=50, default=None, null=True, blank=True)

    billing_addr_fname = models.CharField(max_length=250, default=None, null=True, blank=True)
    billing_addr_lname = models.CharField(max_length=250, default=None, null=True, blank=True)
    billing_addr_building = models.CharField(max_length=150, default=None, null=True, blank=True)
    billing_addr_street = models.CharField(max_length=200, default=None, null=True, blank=True)
    billing_addr_street2 = models.CharField(max_length=200, default=None, null=True, blank=True)
    billing_addr_suburb = models.CharField(max_length=150, default=None, null=True, blank=True)
    billing_addr_phone = models.CharField(max_length=50, default=None, null=True, blank=True)
    billing_addr_city = models.CharField(max_length=50, default=None, null=True, blank=True)
    billing_addr_state = models.CharField(max_length=50, default=None, null=True, blank=True)
    billing_addr_postcode = models.CharField(max_length=10, default=None, null=True, blank=True)
    billing_addr_country = models.CharField(max_length=50, default=None, null=True, blank=True)

    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")

    def _get_random_link(self):
        url = "/store/payorder/{code}".format(code=self.random)
        return url

    random_link = property(_get_random_link)

    def _get_fullname(self):
        return "{f} {l} ({email})".format(f=self.buyer_fname, l=self.buyer_lname, email=self.buyer_email)

    buyer_fullname = property(_get_fullname)


class OrderDetail(models.Model):
    order = models.ForeignKey(Order, default=None, null=True, blank=True, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    variant = models.ForeignKey(Variant, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    plan = models.ForeignKey(PricePlan, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    title = models.CharField(max_length=250, null=True, default=None, blank=True)
    title_en = models.CharField(max_length=250, null=True, default=None, blank=True)
    material = models.CharField(max_length=20, null=True, blank=True, default=None)
    material_en = models.CharField(max_length=120, null=True, blank=True, default=None)
    brand = models.CharField(max_length=50, null=True, blank=True, default=None)
    usage = models.CharField(max_length=50, null=True, blank=True, default=None)
    create_time = models.DateTimeField(auto_now_add=True)
    quantity = models.PositiveSmallIntegerField(default=1)
    price = models.FloatField(default=0)
    currency = models.ForeignKey(Currency, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    discount = models.FloatField(default=1)
    subtotal = models.FloatField(default=0)
    reference = models.CharField(max_length=20, null=True, blank=True)
    source_hs_code = models.CharField(max_length=50, null=True, blank=True, default=None)
    target_hs_code = models.CharField(max_length=50, null=True, blank=True, default=None)

    def __str__(self):
        if self.title:
            return self.title
        else:
            return "No Name"


class PromotionCode(CompanyModelBase):
    code = models.CharField(max_length=10)
    discount = models.FloatField(default=1)
    price_off = models.FloatField(default=0)
    expire_date = models.DateTimeField(default=None, null=True, blank=True)
    channel = models.ForeignKey(Channel, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    date = models.DateTimeField(default=None, null=True, blank=True)
    order = models.ForeignKey(Order, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)

    def __str__(self):
        if self.code:
            return self.code
        else:
            return "No Name Promotion Code"


ShipmentStatus = (
    ('D', 'Draft'),
    ('P', 'Pickup'),
    ('I', 'Processing'),
    ('T', 'Delivering'),
    ('F', 'Delivered'),
    ('C', 'Canceled'),
    ('H', 'Hold'),
    ('E', 'Failed'),
)

ShipmentType = (
    ('D', 'Delivery'),
    ('P', 'Pickup'),
    ('R', 'Return'),
)


class ShipmentItem(models.Model):
    status = models.CharField(max_length=1, choices=ShipmentStatus, blank=True, default='D')
    shipment = models.ForeignKey('Shipment', default=None, null=True, blank=True, on_delete=models.CASCADE)
    detail = models.OneToOneField(OrderDetail, default=None, null=True, blank=True)
    product = models.ForeignKey(Product, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    tn = models.CharField(max_length=50, default=None, null=True, blank=True, db_index=True)
    refence_id = models.PositiveIntegerField(default=None, null=True, blank=True)
    outer_id = models.CharField(max_length=50, default=None, null=True, blank=True)
    note = models.CharField(max_length=100, default=None, null=True, blank=True)
    weight = models.FloatField(default=0, verbose_name=_("Weight"), blank=True)
    height = models.FloatField(default=0, verbose_name=_("Height"), blank=True)
    width = models.FloatField(default=0, verbose_name=_("Width"), blank=True)
    length = models.FloatField(default=0, verbose_name=_("Length"), blank=True)
    quantity = models.PositiveIntegerField(default=1)
    content = models.CharField(max_length=150, default=None, null=True, blank=True)

    def __str__(self):
        if self.tn:
            return self.tn
        else:
            return "No Name Item"


class ShipmentGroup(CompanyModelBase):
    title = models.CharField(max_length=30, default=None, blank=True, null=True)
    key = models.CharField(max_length=20, default=None, blank=True, null=True, unique=True)
    courier_account = models.ForeignKey('CourierAccount', default=None, blank=True, null=True,
                                        on_delete=models.SET_DEFAULT)
    courier_company = models.CharField(max_length=20, default=None, blank=True, null=True)
    start_point = models.CharField(max_length=200, default=None, blank=True, null=True)
    end_point = models.CharField(max_length=200, default=None, blank=True, null=True)
    pickup_address = models.ForeignKey(Address, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)

    pieces = models.PositiveIntegerField(default=0, blank=True)
    volume = models.FloatField(default=0, blank=True)
    weight = models.FloatField(default=0, blank=True)
    has_tail_lift = models.BooleanField(default=False, blank=True)
    has_pallet_jack = models.BooleanField(default=False, blank=True)

    is_published = models.BooleanField(default=False, blank=True)
    publish_time = models.DateTimeField(default=None, null=True, blank=True)
    ordered_time = models.DateTimeField(default=None, null=True, blank=True)
    pickup_time = models.DateTimeField(default=None, null=True, blank=True)
    expect_deliver_time = models.DateTimeField(default=None, null=True, blank=True)

    view_time = models.DateTimeField(default=None, null=True, blank=True)
    delivered_time = models.DateTimeField(default=None, null=True, blank=True)
    paid_time = models.DateTimeField(default=None, null=True, blank=True)
    contact_info = models.CharField(max_length=50, default=None, blank=True, null=True)
    km = models.FloatField(default=0, blank=True)
    expect_minutes = models.PositiveIntegerField(default=0, blank=True)
    amount = models.FloatField(default=0, blank=True)
    comments = models.TextField(null=True, default=None, blank=True)
    lines = models.TextField(null=True, default=None, blank=True)
    driver_note = models.TextField(null=True, default=None, blank=True)

    class Meta:
        verbose_name = _("Manifest")
        verbose_name_plural = _("Manifests")

    def __str__(self):
        if self.title:
            return self.title
        elif self.key:
            return self.key
        else:
            return "No Name"

    @property
    def optimized_shipments(self):
        return self.shipment_set.order_by('pos')


class Shipment(CompanyModelBase):
    status = models.CharField(max_length=1, choices=ShipmentStatus, blank=True, default='D')
    type = models.CharField(max_length=1, choices=ShipmentType, blank=True, default='D')
    orders = models.ManyToManyField(Order, blank=True)
    pictures = models.ManyToManyField(Image, related_name='pictures', blank=True)
    deliver_pictures = models.ManyToManyField(Image, related_name='deliver_pictures', blank=True)
    deliver_signature = models.ForeignKey(Image, related_name='signature', default=None, blank=True, null=True,
                                          on_delete=models.SET_DEFAULT)
    deliver_barcodes = models.TextField(default=None, blank=True, null=True)
    deliver_lat = models.FloatField(default=0, blank=True)
    deliver_lng = models.FloatField(default=0, blank=True)
    deliver_note = models.CharField(max_length=100, default=None, blank=True, null=True)
    deliver_result = models.BooleanField(default=False, blank=True)
    operator = models.ForeignKey(AdminModels.User, default=None, null=True, blank=True,
                                 related_name="shipment_operator", on_delete=models.SET_DEFAULT)
    group = models.ForeignKey(ShipmentGroup, default=None, blank=True, null=True, on_delete=models.SET_DEFAULT)
    outer_id = models.CharField(max_length=50, default=None, blank=True, null=True)
    weight = models.FloatField(default=0, verbose_name=_("Weight"), blank=True)
    volume = models.FloatField(default=0, verbose_name=_("Volume"), blank=True)
    total = models.FloatField(default=0, verbose_name=_("Total"), blank=True)
    minutes = models.PositiveSmallIntegerField(default=0, verbose_name=_("Duration Minutes"), blank=True)
    distance = models.FloatField(default=0, verbose_name=_("Distance"), blank=True)
    arrive_time = models.DateTimeField(null=True, default=None, blank=True)
    pieces = models.PositiveSmallIntegerField(default=0, verbose_name=_("Pieces"))
    oversize_pieces = models.PositiveSmallIntegerField(default=0, verbose_name=_("Oversize Pieces"), blank=True)
    overweight_pieces = models.PositiveSmallIntegerField(default=0, verbose_name=_("Oversize Pieces"), blank=True)
    reference = models.CharField(max_length=50, default=None, blank=True, null=True)
    section = models.CharField(max_length=10, default=None, blank=True, null=True)
    tracking_number = models.CharField(max_length=50, default=None, blank=True, null=True)
    tracking_link = models.CharField(max_length=250, default=None, null=True, blank=True)
    tracking_numbers = models.TextField(default=None, blank=True, null=True)
    courier_account = models.ForeignKey('CourierAccount', default=None, blank=True, null=True,
                                        on_delete=models.SET_DEFAULT)
    courier_company = models.CharField(max_length=20, default=None, blank=True, null=True)
    driver_name = models.CharField(max_length=20, default=None, blank=True, null=True)
    ship_time = models.DateTimeField(null=True, default=None, blank=True)
    email_time = models.DateTimeField(null=True, default=None, blank=True)
    deliver_time = models.DateTimeField(null=True, default=None, blank=True)
    address_verified = models.BooleanField(default=False, blank=True)
    authorise_to_leave = models.BooleanField(default=False, blank=True)
    pos = models.SmallIntegerField(default=0, blank=True)
    tracking_info = models.TextField(default=None, null=True, blank=True)
    content_info = models.TextField(default=None, null=True, blank=True)
    comments = models.TextField(null=True, default=None, blank=True)
    driver_note = models.TextField(null=True, default=None, blank=True)
    label_print_time = models.DateTimeField(null=True, default=None, blank=True)
    label_pdf_path = models.FilePathField(default=None, null=True, blank=True)
    shipping_fname = models.CharField(max_length=250, default=None, null=True, blank=True)
    shipping_lname = models.CharField(max_length=250, default=None, null=True, blank=True)
    shipping_mname = models.CharField(max_length=250, default=None, null=True, blank=True)
    shipping_building = models.CharField(max_length=150, default=None, null=True, blank=True)
    shipping_street = models.CharField(max_length=200, default=None, null=True, blank=True)
    shipping_street2 = models.CharField(max_length=200, default=None, null=True, blank=True)
    shipping_suburb = models.CharField(max_length=150, default=None, null=True, blank=True)
    shipping_phone = models.CharField(max_length=550, default=None, null=True, blank=True)
    shipping_email = models.CharField(max_length=50, default=None, null=True, blank=True)
    shipping_city = models.CharField(max_length=50, default=None, null=True, blank=True)
    shipping_state = models.CharField(max_length=50, default=None, null=True, blank=True)
    shipping_postcode = models.CharField(max_length=10, default=None, null=True, blank=True)
    shipping_country = models.CharField(max_length=50, default=None, null=True, blank=True)
    shipping_instruction = models.CharField(max_length=200, default=None, null=True, blank=True)
    shipping_lng = models.FloatField(default=0, blank=True)
    shipping_lat = models.FloatField(default=0, blank=True)

    class Meta:
        verbose_name = _("Shipment")
        verbose_name_plural = _("Shipments")

    @property
    def is_delivered(self):
        return self.status in ['F']

    def __str__(self):
        if self.courier_account:
            return self.courier_account.__str__()
        elif self.courier_company:
            return self.courier_company
        elif self.group:
            return self.group.__str__()
        else:
            return "No Name"


class UserPlan(models.Model):
    order = models.ForeignKey(Order, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    user = models.ForeignKey(AdminModels.User, on_delete=models.CASCADE)
    expire_time = models.DateTimeField(default=None, null=True, blank=True)
    plan = models.ForeignKey(PricePlan, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    points = models.FloatField(default=0)
    customer_id = models.CharField(max_length=50, default=None, null=True, blank=True)


# Promotion and advertisement
class AdvertisementArea(CompanyModelBase):
    title = models.CharField(max_length=50, null=True, default=None, blank=True)
    plan = models.ForeignKey(PricePlan, null=True, default=None, blank=True, on_delete=models.SET_DEFAULT)
    content = RichTextUploadingField(blank=True, null=True, default=None)
    picture = models.ImageField(max_length=255, null=True, default=None, blank=True)
    pos = models.PositiveSmallIntegerField(default=0)
    can_share = models.BooleanField(default=False)
    max_share_count = models.PositiveSmallIntegerField(default=0)
    auto_extend = models.BooleanField(default=True)
    enable = models.BooleanField(default=True)


class Advertisement(CompanyModelBase):
    area = models.ForeignKey(AdvertisementArea, null=True, default=None, blank=True, on_delete=models.SET_DEFAULT)
    start_time = models.DateTimeField(null=True, default=None, blank=True)
    end_time = models.DateTimeField(null=True, default=None, blank=True)
    user = models.ForeignKey(AdminModels.User, on_delete=models.DO_NOTHING)


FeedbackType = (
    ('P', 'Positive'),
    ('N', 'Negative'),
    ('U', 'Nutural'),
)


class Feedback(CompanyModelBase):
    order = models.ForeignKey(Order, on_delete=models.SET_DEFAULT, default=None, blank=True, null=True)
    product = models.ForeignKey(Product, on_delete=models.SET_DEFAULT, default=None, blank=True, null=True)
    content = RichTextUploadingField(blank=True, null=True, default=None)
    reply = models.TextField(blank=True, null=True, default=None)
    status = models.CharField(max_length=1, default='O', choices=COMMENT_STATUS)
    type = models.CharField(max_length=1, default='P', choices=FeedbackType)
    create_time = models.DateTimeField(auto_now_add=True)
    reply_time = models.DateTimeField(null=True, blank=True, default=None)
    author = models.ForeignKey(Customer, null=True, blank=True, default=None, related_name='feedback_owner',
                               on_delete=models.SET_DEFAULT)
    replyer = models.ForeignKey(AdminModels.User, null=True, blank=True, default=None, related_name='replyer',
                                on_delete=models.SET_DEFAULT)
    ip = models.GenericIPAddressField(default=None, null=True, blank=True)
    modified_count = models.PositiveSmallIntegerField(default=0)
    reply_feedback_type = models.CharField(max_length=1, choices=FeedbackType, default='P')


class OperationLog(CompanyModelBase):
    title = models.CharField(max_length=500, default=None, null=True, blank=True)
    content = models.TextField(null=True, default=None, blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AdminModels.User, on_delete=models.DO_NOTHING)
    channel = models.ForeignKey(Channel, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)


class TransactionType(CompanyModelBase):
    code = models.CharField(max_length=30, null=True, blank=True, default=None)
    title = models.CharField(max_length=50, default=None, null=True, blank=True)
    style = models.CharField(max_length=50, default=None, null=True, blank=True)
    description = models.TextField(null=True, default=None, blank=True)
    pos = models.PositiveSmallIntegerField(default=0, blank=True, null=True)
    is_system = models.BooleanField(default=False)

    def __str__(self):
        return self.title if self.title else 'N/A'


class TransactionRecord(CompanyModelBase):
    customer = models.ForeignKey(Customer, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    type = models.ForeignKey(TransactionType, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    title = models.CharField(max_length=50, default=None, null=True, blank=True)
    content = models.TextField(null=True, default=None, blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    operator = models.ForeignKey(AppaStaff, default=None, null=True, blank=True, on_delete=models.SET_DEFAULT)
    gold = models.FloatField(default=0, blank=True)
    silver = models.FloatField(default=0, blank=True)
    origin_gold = models.FloatField(default=0, blank=True)
    origin_silver = models.FloatField(default=0, blank=True)
    from_account = models.CharField(max_length=100, default=None, null=True, blank=True)
    to_account = models.CharField(max_length=100, default=None, null=True, blank=True)
    reference = models.CharField(max_length=100, default=None, null=True, blank=True)
    code = models.CharField(max_length=100, default=None, null=True, blank=True)


class Payment(BasePayment, CompanyModelBase):
    gateway = models.ForeignKey(PaymentGateway, blank=True, null=True, default=None, on_delete=models.SET_DEFAULT)
    order = models.ForeignKey(Order, blank=True, null=True, default=None, on_delete=models.SET_DEFAULT)
    user = models.ForeignKey(AdminModels.User, blank=True, null=True, default=None, on_delete=models.SET_DEFAULT)
    bank_account = models.ForeignKey(BankAccount, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT)
    confirm_by = models.ForeignKey(AppaStaff, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT,
                                   related_name='PaymentConfirmBy')
    confirm_time = models.DateTimeField(null=True, blank=True, default=None)
    description = models.TextField(blank=True, null=True, default=None)
    billing_first_name = models.CharField(max_length=256, blank=True, null=True, default=None)
    billing_last_name = models.CharField(max_length=256, blank=True, null=True, default=None)
    billing_address_1 = models.CharField(max_length=256, blank=True, null=True, default=None)
    billing_address_2 = models.CharField(max_length=256, blank=True, null=True, default=None)
    billing_city = models.CharField(max_length=256, blank=True, null=True, default=None)
    billing_postcode = models.CharField(max_length=256, blank=True, null=True, default=None)
    billing_country_code = models.CharField(max_length=2, blank=True, null=True, default=None)
    billing_country_area = models.CharField(max_length=256, blank=True, null=True, default=None)
    billing_email = models.EmailField(blank=True, null=True, default=None)
    customer_ip_address = models.GenericIPAddressField(blank=True, null=True, default=None)

    def get_failure_url(self):
        return reverse("store:pay:result", kwargs={"token": self.token})

    def get_success_url(self):
        return reverse("store:pay:result", kwargs={"token": self.token})

    def get_purchased_items(self):
        from store.controller.order import OrderManager
        # you'll probably want to retrieve these from an associated order
        title = 'Product Purchase'
        quantity = 1
        sku = ''
        if self.order:
            om = OrderManager()
            details = om.get_details(self.order)
            result = []
            sku = ''
            for detail in details:
                if detail.product:
                    title = detail.prduct.title
                    sku = detail.product.sku
                elif detail.plan:
                    title = detail.plan.title
                    sku = detail.plan.code
            quantity = Decimal(self.order.quantity)
        else:
            title = ''
        yield PurchasedItem(name=title, sku=sku, quantity=quantity, price=Decimal(self.total), currency=self.currency)


class BankStatement(CompanyModelBase):
    signature = models.CharField(max_length=50, unique=True, verbose_name=_("Signature"))
    date = models.DateField(default=None, null=True, blank=True, verbose_name=_("Date"))
    account = models.CharField(max_length=100, default=None, null=True, blank=True, verbose_name=_("Account"))
    reference = models.CharField(max_length=100, default=None, null=True, blank=True, verbose_name=_("Reference"))
    amount = models.FloatField(default=0, verbose_name=_("Amount"))
    tax = models.FloatField(default=0, verbose_name=_("Tax"))
    gst = models.FloatField(default=0, verbose_name=_("GST"))
    invoice = models.ForeignKey(Invoice, default=None, null=True, blank=True, verbose_name=_("Invoice"),
                                on_delete=models.SET_DEFAULT)
    order_id = models.CharField(max_length=50, default=None, null=True, blank=True, verbose_name=_("Order ID"))
    create_time = models.DateTimeField(auto_now_add=True, verbose_name=_("Create Time"))
    processed = models.BooleanField(default=False, verbose_name=_("Processed"))

    def __str__(self):
        return "{d} {a}".format(d=self.date, a=self.account)

    class Meta:
        verbose_name = _("Bank Statement")
        verbose_name_plural = _('Bank Statements')


class Reservation(CompanyModelBase):
    create_time = models.DateTimeField(auto_now_add=True, verbose_name=_("Create Time"))
    processed = models.BooleanField(default=False, verbose_name=_("Processed"))
    reference = models.CharField(max_length=100, default=None, null=True, blank=True, verbose_name=_("Reference"))
    booking_time = models.DateTimeField(null=True, blank=True, default=None, verbose_name=_("Booking Time"))
    confirm_by = models.ForeignKey(AppaStaff, null=True, blank=True, default=None, on_delete=models.SET_DEFAULT,
                                   related_name='ReservationConfirmBy')
    confirm_time = models.DateTimeField(null=True, blank=True, default=None)
    description = models.TextField(blank=True, null=True, default=None)


from payments.signals import status_changed
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone


def payment_changed(sender, instance, **kwargs):
    if instance and instance.order:
        from store.controller.order import OrderManager
        om = OrderManager()

        order = instance.order
        if instance.status == "confirmed":
            order.status = 'P'
            order.pay_time = timezone.now()
            om.after_paid(order)

        elif instance.status == "waiting" or instance.status == "rejected":
            order.status = 'O'
        elif instance.status == "preauth":
            order.status = 'AU'
        elif instance.status == "refunded":
            order.status = 'RE'
        elif instance.status == "error":
            order.status = 'FA'
        else:
            order.status = 'T'
        order.save()


status_changed.connect(payment_changed, sender=Payment)


@receiver(post_save, sender=Product)
def post_save_product(sender, instance, **kwargs):
    if not instance.image.name and instance.images.count() > 0:
        instance.image = instance.images.first().image.url
        instance.thumb = instance.images.first().image.thumb
        instance.save()

# @receiver(pre_save, sender=TransactionRecord)
# def pre_save_transaction_record(sender, instance, **kwargs):
#     pass
