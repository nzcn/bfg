
name                ='Stripe'
icon                ='store/stripe.png'
description         ='Stripe is an American technology company operating in over 25 countries that allows both private individuals and businesses to accept payments over the Internet.'