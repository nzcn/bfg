from store.core.pay import PayBase
from config import *
import stripe
import json
from datetime import *
from django.http import JsonResponse
# noinspection PyPackageRequirements
from django.utils import timezone
from landing.core.common import get_action_result,get_class_by_path,get_class_by_name

def get_gateway():
    return StripeGateway()

class StripeGateway(PayBase):
    def set_configuration(self,config):
        if config:
            self.config = config
            configs = self.config.split(',')
            if len(configs) == 2:
                self.publish_key = configs[0]
                self.key = configs[1]
                stripe.api_key = self.key
            else:
                raise Exception('Settings Error. Two params needed.')

    def get_name(self):
        return name

    def pay(self,order,params=None):
        if order and params and 'secret_key' in params:
            if 'mode' in params and params['mode'] == 'test':
                stripe.api_key = params['secret_key_test']
            else:
                stripe.api_key = params['secret_key']
            if 'userplan' in params:
                up = params['userplan']
                if up.customer_id:
                    customer = stripe.Customer.retrieve(up.customer_id)
                else:
                    customer = stripe.Customer.create(
                        email = params['email'],
                        source = params['token'],
                    )
                    up.customer_id = customer.id
                    up.save()

                resp = stripe.Charge.create(
                    customer = customer,
                    amount= int(order.total * 100),
                    currency= order.plan.currency.abstraction,
                    description=order.snapshot
                )
            else:
                resp = stripe.Charge.create(
                    source=params['token'],
                    amount=int(order.total * 100),
                    currency=order.currency,
                    description=order.snapshot
                )
            if resp['paid'] == True:
                order.buyer_email = resp['source']['name']
                order.currency = resp['currency']
                order.country = resp['source']['country']
                order.pay_time = timezone.now()
                if order.plan:
                    order.to_time = timezone.now() + timedelta(days=order.plan.get_days(order.quantity))
                order.pay_id = resp.stripe_id
                order.pay_info = json.dumps(resp)
                order.status = 'P'
                order.save()
                return get_action_result(True,'Payment received.`')
            else:
                return get_action_result(False,'Payment failed.')
        else:
            raise ValueError('Can not find available payment')

    def create_secret(self, amount, currency, type, id):
        # customer = stripe.Customer.create()
        # ephemeralKey = stripe.EphemeralKey.create(
        #     customer=customer['id'],
        #     stripe_version='2020-08-27',
        # )
        paymentIntent = stripe.PaymentIntent.create(
            amount= int(amount * 100),
            currency= currency,
            # customer=customer['id'],
            payment_method_types= ["card"],
            metadata={
                'type': type,
                'id': id,
            }
        )
        result = {
            'paymentIntent':paymentIntent.client_secret,
            # 'ephemeralKey':ephemeralKey.secret,
            # 'customer':customer.id,
            'publishableKey':self.publish_key,
        }
        return result

    def get_metadata(self, request):
        result = json.loads(request.body)
        if 'type' in result and result['type'] == 'payment_intent.succeeded':
            # Call Process Result
            if 'data' in result and 'object' in result['data'] and 'metadata' in result['data']['object']:
                return result['data']['object']['metadata']
            else:
                return None
        else:
            return None

    def process_result(self, request):
        result = json.loads(request.body)
        if 'type' in result and result['type'] == 'payment_intent.succeeded':
            # Call Process Result
            if 'data' in result and 'object' in result['data'] and 'metadata' in result['data']['object']:
                payment_obj = result['data']['object']
                metadata = payment_obj['metadata']
                if 'type' in metadata and 'id' in metadata:
                    order_path = metadata['type'] or "store.models.Order"
                    # order_path = "packgo.models.DeclarationForm"
                    order_class = get_class_by_path(order_path)
                    order = order_class.objects.get(id=metadata['id'])
                    if 'charges' in payment_obj and 'data' in payment_obj['charges'] and len(payment_obj['charges']['data']) > 0:
                        charge_data = payment_obj['charges']['data'][0]
                        amount = round(charge_data['amount_captured'] / 100, 2)
                        currency = charge_data['currency']
                        order.process_payment(request=request, amount=amount, currency=currency, context=charge_data)
                        return JsonResponse({'success': True}, safe=False)
                    else:
                        raise Exception('Stripe returned data struct error')
                else:
                    raise Exception('Metadata property invalid')
            else:
                raise Exception('Metadata not available')
        else:
            raise Exception('Event is not success payment intent')