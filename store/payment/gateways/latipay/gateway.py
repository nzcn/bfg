from store.core.pay import PayBase
from .config import *
import json
from datetime import *
from django.utils import timezone
from landing.core.common import get_action_result

def get_gateway():
    return StripeGateway()

class LatipayGateway(PayBase):
    def get_name(self):
        return name

    def pay(self,order,params=None):
        if order and params and 'secret_key' in params:
            if 'mode' in params and params['mode'] == 'test':
                stripe.api_key = params['secret_key_test']
            else:
                stripe.api_key = params['secret_key']
            if 'userplan' in params:
                up = params['userplan']
                if up.customer_id:
                    customer = stripe.Customer.retrieve(up.customer_id)
                else:
                    customer = stripe.Customer.create(
                        email = params['email'],
                        source = params['token'],
                    )
                    up.customer_id = customer.id
                    up.save()

                resp = stripe.Charge.create(
                    customer = customer,
                    amount= int(order.total * 100),
                    currency= order.plan.currency.abstraction,
                    description=order.snapshot
                )
            else:
                resp = stripe.Charge.create(
                    source=params['token'],
                    amount=int(order.total * 100),
                    currency=order.currency,
                    description=order.snapshot
                )
            if resp['paid'] == True:
                order.buyer_email = resp['source']['name']
                order.currency = resp['currency']
                order.country = resp['source']['country']
                order.pay_time = timezone.now()
                if order.plan:
                    order.to_time = timezone.now() + timedelta(days=order.plan.get_days(order.quantity))
                order.pay_id = resp.stripe_id
                order.pay_info = json.dumps(resp)
                order.status = 'P'
                order.save()
                return get_action_result(True,'Payment received.`')
            else:
                return get_action_result(False,'Payment failed.')
        else:
            raise ValueError('Can not find available payment')

