from django.conf.urls import url, include
from django.contrib import admin

import store.payment.views as views

urlpatterns = [
    url(r'^order/(?P<random>\w+)/$', views.pay_order, name="pay_order"),
    url(r'^payment/(?P<payment_id>\d+)$', views.payment_details, name="pay_payment"),
    url(r'^result/(?P<token>[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12})/$', views.pay_result,
        name="result"),
    url(r'^paynow/$', views.pay_begin, name="payment_now"),
    url(r'^result/(?P<code>\w+)/$', views.pay_server_result, name="payment_server_result"),
    url(r'^secret/(?P<code>\w+)/(?P<id>\d+)$', views.pay_secret, name="payment_secret"),
    url(r'^plan_promo/$', views.plan_prom, name="payment_plan_promos"),
    url(r'^plan_price/$', views.plan_price, name="payment_plan_price"),
]
