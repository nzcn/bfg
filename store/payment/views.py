from __future__ import division

import django.dispatch
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from payments import RedirectNeeded
from landing.core.common import *
from landing.models import *
from store import *
from datetime import *
from store.controller.order import OrderManager
from store.controller.payment import PaymentManager
from store.controller.plan import PlanManager
from store.models import *
from payments.core import PAYMENT_VARIANTS
import json

order_event = django.dispatch.Signal(providing_args=["order"])


def get_provider_by_payment(payment):
    variants = getattr(settings, 'PAYMENT_VARIANTS', PAYMENT_VARIANTS)
    handler, config = variants.get(payment.variant, (None, None))
    setting = payment.gateway.test_settings if payment.gateway.test_mode else payment.gateway.settings
    if isinstance(setting, str):
        dict = json.loads(setting)
    else:
        dict = setting
    config.update(dict)
    if not handler:
        raise ValueError('Payment variant does not exist: %s' %
                         (payment.variant,))
    module_path, class_name = handler.rsplit('.', 1)
    module = __import__(
        str(module_path), globals(), locals(), [str(class_name)])
    class_ = getattr(module, class_name)
    handler = class_(**config)
    return handler


def payment_details(request, payment_id):
    try:
        pm = PaymentManager(request)
        payment = Payment.objects.get(id=int(payment_id))
        provider = get_provider_by_payment(payment)
        form = provider.get_form(payment, data=request.POST or None)
    except RedirectNeeded as redirect_to:
        return redirect(str(redirect_to))
    return template_response(request, 'store/frontend/default/cart/payment.html',
                             {'form': form, 'payment': payment})


def pay_result(request, token):
    '''
    Show Payment Result
    :param request:
    :return:
    '''
    if request.method == 'POST':
        print(request.POST)
        return JsonResponse(get_action_result(True, 'success'), safe=False)
    else:
        pm = PaymentManager(request)
        payment = pm.get_payment_by_token(token)
        if payment:
            order = payment.order
            gateway = payment.gateway
            om = OrderManager(request)
            details = om.get_details(order)
        return template_response(request, 'store/frontend/default/cart/result.html', locals())


@login_required
def pay_begin(request):
    id = request.GET.get("promo_id", None)
    if id:
        qty = request.GET.get("qty", 1)
        site = Site.objects.first()
        paypal_email = site.pay_paypal
        promo = PlanPromotion.objects.filter(pk=id).prefetch_related('plan').first()
        if qty < promo.min_quantity:
            qty = promo.min_quantity

        if promo.discount == 1 and promo.price_off > 0:
            price = promo.plan.price * qty - promo.price_off
        elif promo.discount < 1 and promo.price_off == 0:
            price = promo.plan.price * qty * promo.discount
        else:
            price = promo.plan.price * qty

        # Create new order
        order = Order(user=request.user)
        order.create_time = timezone.now()
        order.user = request.user
        order.plan = promo.plan
        order.quantity = qty
        order.discount = promo.discount
        order.total = price
        order.order_ip = request.META.get('REMOTE_ADDR')
        order.save()
        paypal_url = get_paypal_url(site)
        return render(request, 'landing/purchase/pay_paypal.html', locals())
    else:
        return Http404(ValueError("Invalid arguments"))


def pay_order(request, random):
    pm = PaymentManager(request)
    om = OrderManager(request)
    if request.method == 'POST':
        if 'token' in request.POST:
            payment_id = int(request.POST.get('payment_gateway'))
            order_id = int(request.POST.get('order_id'))

            token = request.POST.get('token')
            email = request.POST.get('email')

            order = om.get_order(order_id)
            payment_gateway = pm.get_payment_gateway(payment_id)
            handler = pm.get_payment_handler(payment_gateway.code)
            if handler:
                params = json.loads(payment_gateway.settings)
                params['token'] = token
                params['email'] = email
                result = handler.pay(order, params)
                return redirect(reverse('store:pay_result', kwargs={'random': random}))
            else:
                return redirect(reverse('home'))
    else:
        handlers = pm.get_payment_gateways()
        order = om.get_order_by_random(random)
        gateways = []
        for handler in handlers:
            gateways.append(pm.get_payment_handler(handler.code, config=handler, order=order))
    return template_response(request, 'store/frontend/default/cart/pay.html', locals())


@csrf_exempt
def pay_server_result(request, code=None):
    pm = PaymentManager(request=request)
    gateway = pm.get_payment_handler_by_gateway_code(code)
    return gateway.process_result(request)


@csrf_exempt
def pay_secret(request, code, id):
    pm = PaymentManager(request=request)
    gateway = pm.get_payment_handler_by_gateway_code(code)
    order_path = get_bfg_settings("order_class", "store.models.Order")
    order_class = get_class_by_path(order_path)
    order = order_class.objects.get(id=id)
    amount, currency, type, id = order.get_payable()
    result = gateway.create_secret(amount, currency=currency, type=type, id=id)
    return JsonResponse(result, safe=False)


def plan_prom(request):
    if request.method == 'POST':
        id = request.POST.get('id', PricePlan.objects.first().id)
        active_plan = PricePlan.objects.get(id=id)
        promotions = PlanPromotion.objects.filter(plan=active_plan).order_by('pos')
        result = []
        for p in promotions:
            result.append(
                {'id': p.id, 'title': p.title, 'price_off': p.price_off, 'discount': p.discount, 'min': p.min_quantity})
        return JsonResponse(result, safe=False)
    else:
        return JsonResponse({})


def plan_price(request):
    if request.method == 'POST':
        om = OrderManager(request)
        pm = PlanManager(request)
        plan_id = int(request.POST.get("plan_id"))
        promotion_id = int(request.POST.get("promotion_id"))
        quantity = int(request.POST.get("quantity"))
        coupon = request.POST.get("coupon")
        if coupon == '':
            coupon = None

        result = om.get_plan_final_price(plan=pm.get_plan(plan_id),
                                         quantity=quantity,
                                         plan_promotion=pm.get_promotion(promotion_id),
                                         coupon=coupon)
        return JsonResponse(result, safe=False)
    else:
        return JsonResponse({})
