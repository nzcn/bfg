# noinspection PyPackageRequirements
from django.conf.urls import url, include
# noinspection PyPackageRequirements
from django.contrib import admin
import store.views as views

urlpatterns = [
    url(r'^$', views.index, name="home"),
    url(r'^courier/', include('store.courier.urls', namespace='courier')),
    url(r'^category$', views.category, name="category_root"),
    url(r'^product/(?P<id>\d+).html$', views.product, name="product_detail"),
    url(r'^plan/(?P<id>\d+).html$', views.plan_detail, name="plan_detail"),
    url(r'^cart/$', views.cart, name="cart"),
    url(r'^cart/add_plan/(?P<id>\d+)/$', views.cart_add_plan, name="cart_add_plan"),
    url(r'^cart/add_product/(?P<id>\d+)/$', views.cart_add_product, name="cart_add_product"),
    url(r'^wishlist/add_product/(?P<id>\d+)/$', views.cart, name="wishlist_add_product"),
    url(r'^checkout/$', views.checkout, name="checkout"),
    url(r'^checkout_result/$', views.checkout_result, name="store_checkout_result"),
    url(r'^pay/', include('store.payment.urls', namespace="pay")),
    url(r'^admin_change_plan/$', views.admin_change_plan, name="store_admin_change_plan"),
    url(r'^admin_confirm_plan/(?P<id>\d+)$', views.admin_confirm_plan, name="store_admin_confirm_plan"),
    url(r'^admin_pay_order/(?P<id>\d+)$', views.admin_pay_order, name="store_admin_pay_order"),
    url(r'^admin_pay_result/(?P<id>\d+)$', views.admin_pay_result, name="store_admin_pay_result"),
    url(r'^account/order/list/$', views.account_order_list, name="account_order_list"),
    url(r'^account/order/list/(?P<id>\d+)$', views.account_order_view, name="account_order_view"),
    url(r'^account/payment/list/$', views.account_payment_list, name="account_payment_list"),
    url(r'^my/$', views.checkout, name="store_myaccount"),
]
