from bfg.appa.controllers.core import *
from store.appa import *
from store.courier.appa_classes import CourierAccountAppa
from django.utils.translation import ugettext as _, ugettext_lazy,gettext
from bfg.appa.controllers.company import AppaCompanyManager
from bfg.store.controller.payment import PaymentManager
from store.models import *
from datetime import *
import xlrd
from django import forms

class PaymentAppa(CompanyBaseAdmin):
    pass

class IventoryApp(CompanyBaseAdmin):
    pass

class ReservationAppa(CompanyBaseAdmin):
    list_display = ['booking_time','reference','description','confirm_by','processed']
    search_fields = ['reference','description']
    list_filter = ['booking_time','processed']
    ordering = ['-booking_time']


class PaymentGatewayAppa(CompanyBaseAdmin):
    list_display = ['title','code','surcharge','test_mode','enable','pos']
    search_fields = ['title']
    list_filter = ['enable']
    fieldsets = [
        (None, {'fields': ['title', 'code', 'description','surcharge','test_mode', 'enable','pos', ]}),
        ('Security', {
            'classes': ('collapse',),
            'fields': ['settings', 'test_settings']}
         ),
    ]

class CustomerAppa(CompanyBaseAdmin):
    list_display = ['first_name','last_name','email','gender',]
    is_user = True
    search_fields = ['first_name','last_name','email',]
    
    fieldsets = [
        (None, {'fields': ['first_name', 'last_name', 'email', 'gender', ]}),
        ('Security', {
            'classes': ('collapse',),
            'fields': ['username','password']}
         ),
    ]
    def pre_save(self, request, obj, form, change):
        if not form.cleaned_data['username']:
            obj.useranme = obj.email


class InvoiceAppa(CompanyBaseAdmin):
    list_display = ['title','price','status',]
    search_fields = ['title','reference']


def create_qr_code(modeladmin, request, queryset):
    from bfg.promotion.core.wechat.manager import WechatManager

    cm = AppaCompanyManager(request)
    company = cm.get_company()
    if not company.client_app or not company.client_app.app_key:
        message_user(request, _("Company Wechat App has not been set."), messages.ERROR)
        return

    wm_app = WechatManager(request, app_id=company.client_app.app_key)

    for channel in queryset:
        file_url = "companies/{cid}/channels/{key}.jpg".format(cid=channel.company.id,key=channel.key)
        try:
            file_url = wm_app.create_app_qr(scene="ch={id}".format(id=channel.id),file_url=file_url)
            if not channel.create_time:
                channel.create_time = timezone.now()
            channel.wechat_app_qr = file_url
            channel.save()
            message_user(request, _("Create QRCode %(channel)s successfully.") % {'channel': channel.title})
        except Exception as ex:
            message_user(request, _("Create QRCode %(channel)s failed.") % {'channel': channel.title})

def match_statement(modeladmin, request, queryset):
    cm = AppaCompanyManager(request)
    pm = PaymentManager(request)
    company = cm.get_company()
    for bs in queryset:
        if pm.check_bank_statement(bs):
            print('Match BS {tn}'.format(tn=bs.signature))

create_qr_code.short_description = ugettext_lazy("Create QR Code")
match_statement.short_description = ugettext_lazy("Match Statement")

class ChannelAppa(CompanyBaseAdmin):
    list_display = ['id','title','key','admin','wechat_field','enable']
    search_fields = ['title','key']
    list_display_links = ['title']
    company_filter_fields = ['admin','members']
    actions = [create_qr_code]
    fieldsets = [
        (None, {'fields': ['title', 'key', 'admin','members', 'enable']}),
    ]

    def wechat_field(self, obj):
        return get_html_from_template("store/appa/fields/channel_wechat_qr.html",{'channel':obj,'request':self.request})
    wechat_field.allow_tags = True
    wechat_field.short_description = ugettext_lazy("Wechat QRCode")

    def pre_save(self, request, obj, form, change):
        if not obj.create_time:
            obj.create_time = timezone.now()


class BankStatementAppa(CompanyBaseAdmin):
    list_display = ['date','account','reference','amount','invoice','order_id','processed']
    search_fields = ['reference','account']
    list_filter = ['processed','date']
    list_buttons = "store/appa/bank_statement_list_buttons.html"
    ordering = ['-date']
    actions = [match_statement]
    def has_delete_permission(self, request, obj=None):
        return False

    def action_view_all(self, request, form_url='', extra_context=None):
        action = get_req_value(request,'action')
        if action == 'import':
            if request.method == 'POST':
                filepath = "upload/temp_statements.xls"
                filepath = save_file_to_media(request, "file", filepath)
                if filepath:
                    pm = PaymentManager(request)
                    xl_workbook = xlrd.open_workbook(os.path.join(settings.MEDIA_ROOT, filepath))
                    columns = [
                        {'property': 'date', 'type': date, 'title': [u'Date'], 'format': '%Y/%m/%d'},
                        {'property': 'uniqueid', 'type': str, 'title': [u'Unique Id']},
                        {'property': 'trantype', 'type': str, 'title': [u'Tran Type']},
                        {'property': 'cheque', 'type': str, 'title': [u'Cheque Number']},
                        {'property': 'payee', 'type': str, 'title': [u'Payee']},
                        {'property': 'memo', 'type': str, 'title': [u'Memo']},
                        {'property': 'amount', 'type': float, 'title': [u'Amount']},
                    ]
                    xl_sheet = xl_workbook.sheet_by_index(0)
                    rows = self.import_xls(columns, xl_sheet, start_row=7)
                    pm.import_asb_bankstatements(rows, request)
                    message_user(request, _('Import bankstatements successfuly.'))
                else:
                    message_user(request, _('Import freight failed.'))
                return redirect(reverse("appa:packgo_freight_changelist") + "?state__exact=P")
        elif action == 'match':
            cm = AppaCompanyManager(request)
            pm = PaymentManager(request)
            company = cm.get_company()
            bss = BankStatement.objects.filter(processed=False, company=company, amount__gt=0)
            success_count = 0
            for bs in bss:
                if pm.check_bank_statement(bs):
                    success_count += 1
                    print('Match BS {tn}'.format(tn=bs.signature))
            return action_result(True,_('Match complete. {c} matched'.format(c=success_count)))


class ShipmentGroupAppa(CompanyBaseAdmin):
    list_display = ['title', 'courier_account', 'ordered_time', 'view_time', 'pickup_time', 'delivered_time',
                    'paid_time', 'has_tail_lift', 'has_pallet_jack']
    search_fields = ['title','courier_company']
    list_filter = ['courier_account','ordered_time','view_time']
    ordering = ['-ordered_time']
    fieldsets = [
        (None, {'fields': ['title', 'courier_account', 'comments', 'contact_info','driver_note','has_tail_lift','has_pallet_jack']}),
        ('Time',{'fields':['ordered_time','view_time','pickup_time','delivered_time','paid_time']})
    ]



class CostGroupAppa(CompanyBaseAdmin):
    list_display = ['title']
    fieldsets = [
        (None, {'fields':['title']})
    ]


class CostTypeAppa(CompanyBaseAdmin):
    list_display = ['title','code','outer_code','amount','unit','currency','discount','need_confirmed','need_pay','include_tax','is_system']
    search_fields = ['title','code']
    ordering = ['pos']
    fieldsets = [
        (None, {'fields':['title','code','outer_code','is_system','need_confirmed','need_pay']}),
        ('Advanced',{'fields':['group','pos','currency','amount','unit','discount','include_tax']}),
    ]
    show_none_data = True

class InvoiceTemplateAppa(CompanyBaseAdmin):
    list_display = ['title','pos','currency',]
    search_fields = ['title','pos']
    fieldsets = [
        (None, {'fields':['title','pos','currency','costs']}),
        ('Advanced', {'fields': ['title_eval', 'reference_eval', 'code_eval','description']}),
    ]

class TransactionRecordForm(forms.ModelForm):
    class Meta:
        model = TransactionRecord
        fields = "__all__"

class TransactionRecordAppa(CompanyBaseAdmin):
    list_display = ['title','type','customer','gold','silver','reference','operator','create_date']
    search_fields = ['content']
    readonly_fields = ['title','type','customer','gold','silver','reference']
    fieldsets = [
        (None, {'fields':['title','type','customer','gold','silver','reference']}),
    ]
    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def action_add_credit(self, request):
        pam = PaymentManager(request)
        if request.method == 'POST':
            form = TransactionRecordForm(request.POST,request.FILES)
            if form.is_valid():
                pam.add_customer_credit(form.instance)
                return action_result(True, _('Save Transacation Record Successfully'))
            else:
                raise Exception('Input not valid')
        else:
            types = pam.get_transaction_record_types()
            cm = AppaCompanyManager(request=request)
            customer = cm.get_customer(get_req_value(request,'customer_id'))
            return render(request, 'store/appa/dialog/dialog_edit_transaction_record.html',locals())

    def action_list_credit(self, request):
        pam = PaymentManager(request)
        if request.method == 'POST':
            pass
        else:
            cm = AppaCompanyManager(request=request)
            customer = cm.get_customer(get_req_value(request, 'customer_id'))
            list = pam.list_customer_credit(customer=customer)
            return render(request, 'store/appa/dialog/dialog_list_transaction_record.html',locals())
