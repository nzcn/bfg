from store.models import Channel,PromotionCode

class ChannelManager():

    def get_channel(self,key):
        q = Channel.objects.filter(key=key)
        if q.exists():
            return q.first()
        else:
            return None