from store.models import *

class CustomerManager(object):
    def __init__(self, request=None):
        self.request = request

    def get_customer(self,user=None):
        if not user and self.request:
            user = self.request.user
        q = Customer.objects.filter(id=user.id)
        if q.exists():
            return q.first()
        else:
            return None

    def get_customer_by_uid(self,uid):
        try:
            return Customer.objects.get(id=uid)
        except:
            return None




