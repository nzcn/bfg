from django.shortcuts import *
from store.controller.order import OrderManager
from django.core.cache import cache
from store.models import *
# noinspection PyPackageRequirements
from django.db import transaction
from landing.core.common import *
# noinspection PyPackageRequirements
import stripe


CART_KEY = 'order_id'

class CartManager(object):
    def __init__(self,request=None):
        self.request = request

    def get_order_id(self,resp,om = None):
        if om is None:
            om = OrderManager()
        if not CART_KEY in self.request.COOKIES:
            # Brand New Order
            order = om.create_order_from_cart()
            if order:
                set_cookie(resp, CART_KEY, order.id)
            else:
                raise ValueError('Cant create temperary order')
        return int(self.request.COOKIES[CART_KEY])

    def process(self):
        om = OrderManager()
        resp = HttpResponse()
        order = None
        order_id = self.get_order_id(resp,om)
        self.request.SESSION[CART_KEY] = order_id
        if self.request.method == 'POST' and 'action' in self.request.POST:
            action = self.request.POST.get('action')
            product_id = self.request.POST.get('product_id')
            quantity = self.request.POST.get('quantity',1)
            if order is None:
                order = om.get_order(order_id)
            if action == 'add':
                om.add_product(order,product_id,quantity)
            elif action == 'remove':
                om.remove_product(order,product_id,quantity)
            elif action == 'clear':
                om.clear_order(order)
        else:
            print('Invalid Cart Action')
        return resp

    def get_order(self):
        om = OrderManager()
        resp = HttpResponse()
        order_id = self.get_order_id(resp,om)
        return om.get_order(order_id)

    def set_cookie_str(self, resp, using_session, values):
        cart = json.dumps(values)
        if resp:
            resp.set_cookie('cart', cart)
        if using_session:
            self.request.session['cart'] = cart
        cache.set(CONST_STORE_SITE_VARS, None)
        return cart

    def add_product(self,resp,product_id,quantity,using_session = True, add=True):
        '''
        Add product to cart
        :param resp:
        :param product_id:
        :param quantity:
        :param using_session:
        :param add:
        :return:
        '''
        cart = self.get_cart_array()
        if 'products' in cart:
            products = cart['products']
        else:
            products = {}
        if product_id in products:
            if add:
                products[product_id] += quantity
            else:
                products[product_id] = quantity
        else:
            products[product_id] = quantity
        cart.update({'products':products})
        cart = self.set_cookie_str(resp, using_session, cart)
        return cart

    def del_product(self,resp,product_id,quantity,using_session = True):
        '''
        Delete a product from cart
        :param resp:
        :param product_id:
        :param quantity: 0 to remove the product.  Other quantity minus
        :param using_session:
        :return:
        '''
        cart = self.get_cart_array()
        if isinstance(quantity,str) and quantity.isdigit():
            quantity = int(quantity)
        if 'products' in cart:
            products = cart['products']
        else:
            products = {}
        if product_id in products :
            if quantity > 0:
                products[product_id] = quantity
            else:
                del products[product_id]
        cart['products']=products
        cart = self.set_cookie_str(resp, using_session, cart)
        return cart

    def add_plan(self,resp,plan_id,quantity=1,using_session = True,reference=None):
        '''
        Add plan to Cart. Only one plan can be selected in each Cart
        :param resp:
        :param plan_id:
        :param quantity:
        :param using_session:
        :param add:
        :return:
        '''
        cart = self.get_cart_array()
        if 'plans' in cart:
            plans = cart['plans']
        else:
            plans = {}
        if plan_id > 0:
            plan = PricePlan.objects.get(id=plan_id)
            plans.update({'plan_id':plan_id,'plan_qty':quantity,'currency_id':plan.currency_id,'reference':reference})
            cart.update({'plans': plans})
            return self.set_cookie_str(resp, using_session, cart)
        else:
            raise Exception(_('Invalid planid.'))


    def cancel_plan(self,resp,plan_id,quantity=0,using_session = True):
        '''
        Cancel Plan
        :param resp:
        :param plan_id:
        :param quantity:
        :param using_session:
        :return:
        '''
        cart = self.get_cart_array()
        if 'plans' in cart:
            plans = cart['plans']
        else:
            plans = {}
        if 'plan_id' in plans and 'plan_quantity' in plans:
            if quantity > 0:
                plans['plan_quantity'] -= quantity
            else:
                # Remove plan
                plans.pop("plan_id")
                plans.pop("plan_quantity")
        cart.update({'plans': plans})
        return self.set_cookie_str(resp, using_session, cart)

    def get_cart_array(self,key=None):
        if 'cart' in self.request.session:
            cart = self.request.session['cart']
        elif 'cart' in self.request.COOKIES:
            cart = self.request.COOKIES['cart']
        else:
            cart = '{}'
        cart = parse_dict(cart)
        if key:
            if key in cart:
                return cart[key]
            else:
                return {}
        else:
            return cart

    def get_cart(self):
        values = self.get_cart_array()
        if values is None:
            return None
        products = []
        addons = []
        total_price = 0
        total_count = 0
        plan_id = 0
        plan_quantity = 1
        currency = None
        need_delivery = False
        result = {}
        reference = None
        if 'products' in values:
            for v in values['products']:
                q = Product.objects.filter(id=v)
                if q.exists():
                    product = q.first()
                    quantity = int(values['products'][v])
                    subtotal = product.price * quantity
                    dict = {'id':int(v),'quantity':quantity,'product':product,'currency_id':product.currency_id,"subtotal":subtotal}
                    products.append(dict)
                    if not currency:
                        currency = product.currency
                    total_price += subtotal
                    total_count += 1
                    # If any product need delivery then the whole order need delivery
                    if not need_delivery:
                        if product.is_online_product:
                            need_delivery = True
                else:
                    print('Cant find product {id}'.format(id=v))
        result = {'products': products}

        if 'plans' in values:
            for v in values['plans']:
                if v == 'plan_id':
                    plan_id = values['plans'][v]
                    total_count += 1
                elif v == 'plan_qty':
                    plan_quantity = values['plans'][v]
                elif v == 'currency_id':
                    currency = Currency.objects.get(id=values['plans'][v])
                elif v == 'reference':
                    reference = values['plans'][v]
                else:
                    q = PricePlan.objects.filter(type='A',id=int(v))
                    if q.exists():
                        addon = q.first()
                        addons.append({'id': v, 'quantity': values['plans'][v], 'addon': addon,
                                         'currency_id': addon.currency_id})
                        total_count += 1

        plan_subtotal = 0
        if plan_id > 0:
            try:
                plan = PricePlan.objects.get(pk=plan_id)
            except:
                plan = None
            if plan:
                plan_subtotal = plan.price * plan_quantity
                if not need_delivery:
                    need_delivery = not plan.is_online_product
            total_price += plan_subtotal
        else:
            plan = None
            plan_subtotal = 0

        result.update({'total':total_price,
                       'count': total_count,
                       'plan_id':plan_id,
                       'plan':plan,
                       'plan_quantity':plan_quantity,
                       'plan_subtotal':plan_subtotal,
                       'reference': reference,
                       'addons': addons,
                       'currency':currency,
                       'need_delivery':need_delivery})
        return result

    def availabble_cart(self):
        cart = self.get_cart()
        if len(cart['products']) > 0 or ('plan' in cart and cart['plan']):
            return True
        else:
            return False

    def checkout(self,resp=None):
        '''
        Clean Cart and Made the order
        :param resp:
        :return:
        '''
        if self.request.method == 'POST':
            cart = self.get_cart()
            om = OrderManager(request=self.request)
            order = om.create_order_from_cart(cart)
            if resp:
                # Clean the shopping cart when payment processed.
                self.clean_cart(resp)
            return order
        else:
            raise ValueError('Can not find available payment')

    def clean_cart(self,resp):
        self.request.session['cart'] = None
        self.request.session.modified = True
        resp.set_cookie('cart','')
        cache.set(CONST_STORE_SITE_VARS, None)
        return resp