from django.shortcuts import HttpResponse
from store.controller.product import ProductManager
from landing.core.common import *
from store.models import *
from landing.controller.contents import ContentManager
from store.controller.plan import PlanManager
from bfg.appa.core.model import ModelManager
from django.utils.translation import gettext as _,ngettext
from datetime import *
import pytz


class OrderManager(object):
    def __init__(self,request=None):
        self.request = request

    def get_order(self,order_id):
        try:
            return Order.objects.get(id=order_id,user=self.request.user)
        except:
            return None

    def get_order_by_random(self,random):
        try:
            return Order.objects.get(random__iexact=random)
        except Exception as ex:
            print(ex.message)
            return None

    @atomic
    def create_order_from_cart(self, cart=None, request=None):
        if not request:
            request = self.request

        order = Order()
        if request and request.user.is_authenticated:
            order.user = request.user

        if cart:
            order.total = getvalue(cart,'total')
            order.quantity = cart['count']
            order.currency = cart['currency']
            # Save form data to fields
            mm = ModelManager(request,object_class=Order)
            mm.save_model(order)

        order.ip = request.META.get('REMOTE_ADDR')
        order.create_time = timezone.now()
        order.save()
        # Add products
        for detail in cart['products']:
            self.add_product(order,detail['id'],quantity=detail['quantity'])
        # Add plans
        if 'plan' in cart and cart['plan']:
            self.add_plan(order,cart['plan_id'],cart['plan_quantity'],cart['reference'])
        # Add addons
        for addon in cart['addons']:
            self.add_plan(order,addon['addon'],addon['quantity'])
        # Recalculate the total price order
        return self.caclulate(order)

    def create_invoice(self):
        invoice = Invoice(random=get_random_string(16), key=uuid.uuid4())
        return invoice

    def get_details(self,order,product=None):
        if product:
            return OrderDetail.objects.filter(order=order,product=product)
        else:
            return OrderDetail.objects.filter(order=order)

    def add_product(self,order,product_id,quantity=1):
        if quantity <= 0:
            return False
        pm = ProductManager()
        product = pm.get_product(product_id)
        if order and product:
            details = self.get_details(order,product)
            if details.exists():
                detail = details.first()
                detail.quantity += quantity
            else:
                detail = OrderDetail(order=order,product=product, quantity=quantity)
                detail.title = "Product:" + product.title
                detail.price = product.price
                detail.quantity = quantity
            detail.subtotal = product.price * detail.quantity
            detail.save()
            return True
        else:
            raise ValueError(_('Can not add product to order'))

    def add_plan(self,order,plan_id,quantity=1,reference=None):
        if quantity <= 0:
            return False
        pm = PlanManager(self.request)
        plan = pm.get_plan(plan_id)
        if order and plan:
            details = OrderDetail.objects.filter(order=order,plan=plan)
            if details.exists():
                detail = details.first()
                detail.quantity += quantity
            else:
                detail = OrderDetail(order=order,plan=plan,quantity=quantity)
                detail.title = "Plan Subscription:" + plan.title
                detail.price = plan.price
                detail.quantity = quantity
            detail.reference = reference
            detail.subtotal = plan.price * detail.quantity
            detail.currency = plan.currency
            detail.save()
            return True
        else:
            raise ValueError(_('Can not add plan to order'))

    def remove_product(self,order,product_id,quantity=1):
        '''
        Remove products from the order
        :param order:
        :param product_id: product to be removed
        :param quantity: 0 means remove all the details. greater than 0 measn minus quantity
        :return:
        '''
        if quantity < 0:
            return False
        pm = ProductManager()
        product = pm.get_product(product_id)
        if order and product:
            details = self.get_details(order, product)
            if details.exists():
                if quantity > 0:
                    detail = details.first()
                    detail.quantity -= quantity
                    if detail.quantity < 0:
                        detail.quantity = 0
                    detail.subtotal = product.price * detail.quantity
                    detail.save()
                else:
                    # Remote all the products
                    details.delete()
                self.caclulate(order)
            else:
                print('No product need to be removed')
            return True
        else:
            raise ValueError('Can not add product to order')

    def caclulate(self,order):
        details = self.get_details(order)
        total_price = 0
        total_quantity = 0
        for detail in details:
            if detail.product:
                total_price += detail.product.price * detail.quantity * detail.discount
            elif detail.plan:
                total_price += detail.plan.price * detail.quantity * detail.discount
            total_quantity += detail.quantity
            if order.title is None:
                order.title = detail.title
                count = len(details)
                if count > 0:
                    order.title = ngettext("%(title)s","%(title)s and other %(count)d items",count) % {"count":count,"title":order.title}
        order.price = total_price
        order.total = order.price + order.shipping_cost
        order.quantity = total_quantity
        order.save()
        return order

    def clear_order(self,order):
        self.get_details(order).delete()

    def after_paid(self,order):
        cm = ContentManager(self.request)
        site = cm.get_site_by_domain()
        details = OrderDetail.objects.filter(order=order)
        context = {'order':order,'details':details,'site':site}
        for detail in details:
            # Execute Product Action
            dict = {}
            try:
                if detail.product:
                   dict = execute_action(detail.product.success_action, detail)
                elif detail.plan:
                   dict = execute_action(detail.plan.success_action, detail)
            except Exception as ex:
                logger.error(ex)
            if dict:
                context.update(dict)
        # Send Email
        send_email_from_template(subject=_("Order #{{ order.id }} Payment received"),
                                 receivers=[order.buyer_email],
                                 context=context,
                                 template="store/backend/mail/html/new_order.html")

    def after_preauthed(self,order):
        send_email_from_template(subject=_("Order has been preauthed"),
                                 receivers=[order.buyer_email],
                                 context={"order": order},
                                 template="store/backend/mail/html/new_order.html")

    def after_refund(self,order):
        send_email_from_template(subject=_("Order has been refunded"),
                                 receivers=[order.buyer_email],
                                 context={"order": order},
                                 template="store/backend/mail/html/new_order.html")

    def create_order_from_plan(self,plan,quantity=0,plan_promotion=None,coupon=None):
        '''
        Create order from an plan
        :param user:
        :param plan:
        :param quantity:
        :param plan_promotion:
        :return:
        '''
        order = self.create_order_from_cart(request=self.request)
        order.plan = plan
        prices = self.get_plan_final_price(plan=plan,quantity=quantity,plan_promotion=plan_promotion,coupon=coupon)
        order.total = prices['price']
        order.discount = prices['save']
        order.quantity = quantity
        order.title = "{plan} x {qty}".format(plan = plan.title,qty = quantity)
        order.snapshot = "{plan} x {qty}. {promotion} promo:{p}".format(plan = plan.title,
                                                                        qty = quantity,
                                                                        promotion = plan_promotion,
                                                                        p = coupon)

        order.promotion_code = coupon
        order.order_ip = self.request.META['REMOTE_ADDR']
        order.save()

        detail = OrderDetail(order=order)
        detail.plan = plan
        detail.user = order.user
        detail.create_time = timezone.now()
        detail.quantity = quantity
        detail.discount = order.discount
        detail.subtotal = order.total
        detail.save()
        return order

    def get_promoted_price(self,price,discount,price_off):
        if discount and discount > 0 and discount < 1:
            final_price_discount = price * discount
        else:
            final_price_discount = price

        if price_off and price_off > 0:
            final_price_off = price - price_off
        else:
            final_price_off = price
        final_price = final_price_discount if final_price_discount < final_price_off else final_price_off
        return final_price

    def get_plan_final_price(self,plan,quantity=1,plan_promotion=None,coupon=None):
        total_price = plan.price * quantity
        final_price = total_price

        if plan_promotion and plan_promotion.plan == plan and quantity >= plan_promotion.min_quantity:
            final_price = self.get_promoted_price(total_price,plan_promotion.discount,plan_promotion.price_off)

        before_coupon_price = final_price
        # Get coupon price
        final_price = self.get_coupon_price(before_coupon_price,coupon)

        return {'price':final_price,
                'was':total_price,
                'save':total_price - final_price,
                'coupon_save':before_coupon_price - final_price,
                'discount_save':total_price - before_coupon_price}

    def get_coupon_price(self,price,coupon):
        final_price = price
        q =PromotionCode.objects.filter(code__iexact = coupon)
        if q.exists():
            pc = q.first()
            if (not pc.expire_date) or (pc.expire_date and pc.expire_date > timezone.now()):
                final_price = self.get_promoted_price(price,pc.discount,pc.price_off)
        return final_price

    def get_payments(self,user):
        if user:
            q = Q(status='confirmed') | Q(status='error') | Q(status='preauth') | Q(status='refunded')
            return Payment.objects.filter(order__user=user,status='confirmed').order_by('-id')
        else:
            return []

    def get_orders(self,user):
        if user:
            return Order.objects.filter(user=user).order_by('-create_time')
        else:
            return []

    def get_invoices(self,user):
        pass

    def update_user_plan(self,order):
        if order and order.user and order.plan:
            pm = PlanManager(self.request)
            original_plan = pm.get_user_plan(order.user)
            up = pm.subscribe_plan(order.user,order)
            return up,original_plan
        else:
            raise ValueError('Update user plan error')

