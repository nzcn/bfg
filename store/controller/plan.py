# noinspection PyPackageRequirements
from django.shortcuts import HttpResponse
from store.models import *
from store.controller.product import ProductManager
from landing.core.common import getvalue
from django.utils import timezone
from datetime import *


class PlanManager(object):

    def __init__(self,request=None):
        self.request = request

    def get_all_plans(self):
        plans = PricePlan.objects.filter(enable=True).order_by('pos').prefetch_related('features')
        return plans

    def subscribe_plan(self,user,order):
        up = self.get_userplan(user)
        if not up:
            up = UserPlan(user=user,order=order)
        up.plan = order.plan
        up.expire_time = order.to_time
        up.order = order
        up.save()
        return up

    def get_user_plan(self,user):
        q = UserPlan.objects.filter(user=user)
        if q.exists():
            return q.first().plan
        else:
            return None

    def get_userplan(self,user):
        q = UserPlan.objects.filter(user=user)
        if q.exists():
            return q.first()
        else:
            return None

    def add_free_plan(self,user):
        q = UserPlan.objects.filter(user=user)
        if q.exists():
            up = q.first()
        else:
            up = UserPlan(user=user)
        up.plan = self.get_free_plan()
        up.save()

    def get_free_plan(self):
        return PricePlan.objects.get(code='free')

    def get_plan(self,id):
        return PricePlan.objects.get(id=id)

    def get_promotion(self, id):
        if id > 0:
            return PlanPromotion.objects.get(id=id)
        else:
            return None

    def cancel_plan(self,user):
        pass

