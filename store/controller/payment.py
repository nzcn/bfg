from store.models import *
from django.conf import settings
from bfg.appa.controllers.company import AppaCompanyManager
from bfg.landing.core.common import *
from packgo.models import DeclarationForm
from packgo.controllers.parcels import ParcelManager
import os
import sys


class PaymentManager(object):

    def __init__(self, request=None):
        self.request = request

    def get_payment_gateways_path(self):
        return os.path.join(settings.BASE_DIR, 'store/payment/gateways')

    def get_payment_handler(self, name, config=None, order=None):
        module = self.get_gateway_module(name)
        handler = module.get_gateway()
        if handler:
            if config:
                handler.set_configuration(config)
            if order:
                handler.set_order(order)
            return handler
        else:
            raise ValueError('Can not find payment gateway.')

    def get_payment_gateway(self, id):
        return PaymentGateway.objects.get(id=id)

    def get_payment_gateway_by_code(self, code):
        return PaymentGateway.objects.get(code=code)

    def get_payment_by_token(self, token):
        try:
            return Payment.objects.get(token=token)
        except:
            return None

    def get_payment_handler_by_gateway_id(self, id):
        payment_gateway = self.get_payment_gateway(id)
        return self.get_payment_handler_by_gateway(payment_gateway)

    def get_payment_handler_by_gateway_code(self, code):
        payment_gateway = self.get_payment_gateway_by_code(code)
        return self.get_payment_handler_by_gateway(payment_gateway)

    def get_payment_handler_by_gateway(self, gateway):
        if not gateway.enable:
            raise Exception('Gateway is not enable.')
        return self.get_payment_handler(gateway.code, gateway.test_settings if gateway.test_mode else gateway.settings)

    def get_payment_gateways(self):
        q = PaymentGateway.objects.filter(enable=True).order_by('pos')
        if q.exists():
            return q
        else:
            return []

    def create_payment_by_gateway(self, order, gateway, amount=0):
        payment = Payment(order=order, gateway=gateway)
        if amount == 0:
            amount = order.total
        payment.currency = order.currency.abstraction
        payment.variant = gateway.code
        payment.total = amount
        payment.tax = order.tax
        payment.billing_first_name = order.buyer_fname
        payment.billing_last_name = order.buyer_lname
        payment.billing_address_1 = order.billing_addr_street
        payment.billing_address_2 = order.billing_addr_street2
        payment.billing_city = order.billing_addr_city
        payment.billing_email = order.buyer_email
        payment.billing_postcode = order.billing_addr_postcode
        payment.billing_country = order.billing_addr_country
        payment.save()
        return payment

    def get_transaction_record_types(self):
        return TransactionType.objects.filter(is_system=True).order_by('pos')

    def get_transaction_record_type(self, code):
        try:
            return TransactionType.objects.get(is_system=True, code=code)
        except:
            return None

    def get_gateway_config(self, name, ext='config'):
        config_module_name = "store.payment.gateways.{name}.{ext}".format(name=name, ext=ext)
        if not config_module_name in sys.modules:
            __import__(config_module_name)
        module = sys.modules[config_module_name]
        return module

    def get_gateway_module(self, name):
        return self.get_gateway_config(name, 'gateway')

    def get_gateway_handlers(self):
        gateways = []
        path = self.get_payment_gateways_path()
        for name in os.listdir(path):
            if os.path.isdir(os.path.join(path, name)):
                try:
                    handler = self.get_payment_handler(name)
                    gateways.append({'title': handler.name, 'handler': handler, 'code': name})
                except Exception as ex:
                    print(ex.message)
        return gateways

    @atomic
    def add_customer_credit(self, record):
        import hashlib
        record.content = '{title},{refer},{customer}'.format(title=record.title, refer=record.reference,
                                                             customer=record.customer)
        record.company = record.customer.company
        record.origin_gold = record.customer.gold
        record.origin_silver = record.customer.silver
        record.customer.gold += record.gold
        record.customer.silver += record.silver
        md5 = hashlib.md5()
        md5.update("{gold}{silver}{orig_gold}{orig_silver}".format(gold=record.gold, silver=record.silver,
                                                                   orig_silver=record.origin_silver,
                                                                   orig_gold=record.origin_gold))
        record.code = md5.hexdigest()
        record.save()
        record.customer.gold = round(record.customer.gold, 2)
        record.customer.silver = round(record.customer.silver, 2)
        record.customer.save()

    def list_customer_credit(self, customer, term=None, order_by='-id'):
        q = Q(customer=customer)
        if term:
            q = q & Q(reference__icontains=term)
        return TransactionRecord.objects.filter(q).order_by(order_by)

    def import_asb_bankstatements(self, rows, request):
        def import_bankstatement(row, company, retry):
            qs = BankStatement.objects.filter(signature=row['uniqueid'])
            if qs.exists():
                if retry:
                    bs = qs.first()
                else:
                    return None
            else:
                bs = BankStatement.objects.create(signature=row['uniqueid'], company=company)
            bs.date = row['date']
            bs.account = row['payee']
            bs.reference = row['memo']
            bs.amount = row['amount']
            bs.save()
            return bs

        success_count = 0
        cm = AppaCompanyManager(request)
        company = cm.get_company()
        import_only_new_records = get_req_value(request, 'import_extra') == 'on'
        for row in rows:
            try:
                bs = import_bankstatement(row, company, retry=not import_only_new_records)
                if bs:
                    success_count += 1
                    print('Imported Bank Statement {tn}'.format(tn=bs.signature))
            except Exception as ex:
                message_user(request, 'Import Row {tn} failed. Possible reason: {e}'.format(
                    tn=row['uniqueid'],
                    e=ex.__str__()
                ), level=messages.ERROR)
        return success_count

    def check_bank_statement(self, bs):
        # Find FO tracking number
        reg = '\w{3,4}(\d{6,})[S|F]{0,1}'
        fo_tracking_number = get_regex_group(bs.reference, reg)
        fo = None
        if fo_tracking_number:
            try:
                fo = DeclarationForm.objects.get(id=fo_tracking_number)
            except:
                fo = None

        if not fo:
            # Find Inv Number
            reg = 'inv[-|\s]{0,1}(\d+)'
            inv_number = get_regex_group(bs.reference, reg)
            if inv_number:
                inv_number = inv_number.lstrip("0").strip(" ")
                qs = DeclarationForm.objects.filter(id=inv_number, total=bs.amount)
                if qs.exists():
                    fo = qs.first()

        if not fo:
            # Find Customer Number
            inv_number = replace_regex(bs.reference, reg, '')
            reg = '(\d{5})'
            customer_number = get_regex_group(inv_number, reg)
            if customer_number:
                qs = DeclarationForm.objects.filter(applicant_number=customer_number, total=bs.amount,
                                                    create_time__range=[timezone.now() - timedelta(days=15),
                                                                        timezone.now()]).order_by('id')
                if qs.exists():
                    fo = qs.last()
                else:
                    qs = DeclarationForm.objects.filter(applicant_number=customer_number, total=bs.amount).order_by(
                        'id')
                    if qs.exists():
                        fo = qs.last()

        if fo:
            bs.order_id = fo.tracking_number
            bs.processed = True
            bs.save()
            if not fo.paid:
                pm = ParcelManager(self.request)
                pm.pay_fo(fo, bs.amount)
                pm.add_tracking_info(fo, 'Match Statement Success', customer_visible=False)
            return True
        else:
            return False

    def get_account_names(self, qs, excludes=None):
        result = qs.values_list('account', flat=True).annotate(c=Count('account')).order_by('-c')
        if excludes:
            for ele in excludes:
                result.remove(ele)
        return result

    def get_account_customer(self, account, company=None):
        '''
        Locate current account customer with existing clues. For example from matched BS records.
        :param account:
        :return:
        '''
        if not company:
            cm = AppaCompanyManager(self.request)
            company = cm.get_company()
        # Check matched BS records with existing account
        matched_bss = BankStatement.objects.filter(account=account, processed=True, company=company)
        result = None
        if matched_bss.exists():
            pm = ParcelManager(self.request)
            order_id = matched_bss.first().order_id
            fo = pm.get_freight_order_by_tracking_number(tracking_number=trace, is_staff=True)
            if fo:
                result = fo.customer
        return result

    def match_account_receive_bank_statements(self, account, company=None):
        '''
        Happened when all BS with reference checked
        :param account:
        :return:
        '''
        if not company:
            cm = AppaCompanyManager(self.request)
            company = cm.get_company()
        unprocessed_bss = BankStatement.objects.filter(account=account, amount__gt=0, processed=False, company=company)
        customers = Customer.objects.filter(bank_statement_reference__icontains=account)
        customer = customers.first()
        pm = ParcelManager(self.request)
        for bs in unprocessed_bss:
            fos = DeclarationForm.objects.filter(applicant_number=customer.applicant_number, create_time__lt=bs.date,
                                                 total=bs.amount)
            if fos.exists():
                fo = fos.last()
                pm.match_fo_bankstatement(fo, bs)
                print('Matched BS {bs} to {fo}'.format(bs=bs.signature, fo=fo.tracking_number))

    def match_account_unprocessed_bank_statements(self):
        '''
        After checking reference. All BS records have been checked. This method is used to find the unprocessed BS records according to the account
        and customer FO records. Only check by amount and customer and create date.
        :return:
        '''
        # To Do: To be finished.
        qs = DeclarationForm.objects.filter()
        cm = AppaCompanyManager(self.request)
        accounts = self.get_account_names(BankStatement.objects.filter(company=cm.get_company(), processed=False),
                                          excludes=['CREDIT'])

    def get_freights_invoices(self, freights):
        return Invoice.objects.filter(declarationform__freight__in=freights)
