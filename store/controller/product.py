from store.models import *
from landing.core.common import *

class ProductManager(object):

    def __init__(self,request=None):
        self.request =request

    def get_product(self,product_id):
        try:
            # Product Detail
            # Variants
            # Inventory
            return Product.objects.get(id=product_id)
        except:
            return None

    def get_product_by_slug(self,product_slug,category=None):
        try:
            q = Q(slug=product_slug)
            if category:
                q = q & Q(category=category)
            result = Product.objects.filter(q)
            if result.exists():
                return result.first()
            else:
                return None
        except:
            return None


    def get_products(self,term=None, category=None,pricerange=None,
                     variants=None, count=0, order_by='-id',
                     available=None,rating=None):
        q = Q(status='A')
        qs = Product.objects.filter(q).prefetch_related('images','categories','options','variants').order_by(order_by)
        if count > 0:
            return qs[0:count]
        else:
            return qs

    def get_categories(self,store=None):
        from store.api import CategorySerializer
        '''
        Retrun Leveled Categories in a Store
        :param store:
        :return:
        '''
        categories = self.get_category_queryset(store)
        list = CategorySerializer(categories,many=True).data
        forest = list_to_tree(list,parent_name='parent',order_name='pos',root_parent=None)
        return forest

    def get_category_queryset(self,store=None):
        q = Q(status='A')
        if store:
            q = q & Q(store=store)
        return Category.objects.filter(q).order_by('pos')

    def get_cateogry(self,category_id):
        return None

    def get_category_by_slug(self,slug):
        return Category.objects.get(slug=slug)

    def add_review(self,review,parent=None):
        return None

    def reply_review(self,review_id):
        return None

    def get_reviews(self,product_id,index=0):
        return None

    def add_to_wishlist(self,product_id):
        return None

    def get_related_products(self,product_id):
        return None

    def get_popular_products(self,count=8,order_by="-buy_count"):
        return self.get_products(count=count,order_by=order_by)


    