# noinspection PyPackageRequirements
from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets,permissions
from store.models import *
from bfg.promotion.core.wechat import *
from rest_framework import serializers, viewsets
import django_filters.rest_framework
from rest_framework.response import Response
import hmac
import hashlib
import urllib
from store.controller.order import OrderManager
from rest_framework.decorators import action
from landing.api import FileSerializer

def register_api(router):
    router.register(r'pay', PayViewSet, base_name="pay")
    router.register(r'products', ProductViewSet,base_name="product")
    router.register(r'cities', CityViewSet, base_name="city")
    router.register(r'categories', CategoryViewSet, base_name="category")

# Serializers define the API representation.
class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id','title', 'quantity','price','image','thumb')

class ProductDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

class SuburbSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = ('id','name')

class CitySerializer(serializers.ModelSerializer):
    suburb = SuburbSerializer(read_only=True, many=True)
    class Meta:
        model = City
        fields = ('id','name','pos','suburb')

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id','title','slug','path','pos','parent')

class OrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = '__all__'

class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = '__all__'

class CurrencyAbstractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ('abstraction','sign')


class InvoiceDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceDetail
        fields = '__all__'

class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'

class InvoiceListSerializer(serializers.ModelSerializer):
    invoicedetail_set = InvoiceDetailSerializer(read_only=True, many=True)
    invoice_file = serializers.FileField(allow_null=True,required=False)
    class Meta:
        model = Invoice
        fields = ('id','title','quantity','discount','price','total','shipping_cost','tax',
                  'create_time','due_date','sent_time','invoicedetail_set','invoice_file')


class PaymentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'

class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = '__all__'

# ViewSets define the view behavior.
class ProductViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return Product.objects.all()

    serializer_class = ProductSerializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    search_fields = ('title')

    # def update(self, request, *args, **kwargs):
    #     pass
    
    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ProductDetailSerializer
        else:
            return ProductSerializer

class CityViewSet(viewsets.ReadOnlyModelViewSet):
    def get_queryset(self):
        return City.objects.all().order_by('pos')
    serializer_class = CitySerializer

class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CategorySerializer
    permission_classes = [permissions.AllowAny]


class PayViewSet(viewsets.ViewSet):
    permission_classes = [permissions.IsAuthenticated]
    def list(self, request):
        return Response({})

    @action(methods=['get', 'post'], detail=False)
    def latipay(self,request):
        order_id = get_req_value(request, "oid")
        om = OrderManager(request)
        order = om.get_order(order_id)
        if order:
            if not 'HTTP_APPID' in request.META:
                raise Exception("AppID not found")
            app_id = request.META['HTTP_APPID']
            app = get_app(app_id)
            if not app:
                raise Exception("App with {id} not found".format(id=app_id))
            wechat = get_wechat_openid(order.user, app)
            if not wechat:
                raise Exception("OpenID not found".format(id=app_id))
            openid = wechat.openid
            data = {
                'user_id': 'U000003385',
                'wallet_id': 'W000004007',
                'amount': '0.01',
                'product_name': order.title,
                'notify_url': 'https://packns.wisdomin.com/store/pay/result/',
                'merchant_reference': get_random_string(6),
                'open_id': openid,
                'app_id': app_id,
            }
            API_SECRET = '5yCtaS'
            msg = ''
            for k in sorted(data):
                msg += "{k}={v}&".format(k=k, v=data[k])
            msg = msg.strip('&') + API_SECRET
            signature = hmac.new(
                str(API_SECRET),
                msg=msg,
                digestmod=hashlib.sha256
            ).hexdigest().lower()
            data.update({
                'signature': signature
            })
            headers = {'Content-type': 'application/json;charset=UTF-8'}
            response = requests.post(
                url="https://api.latipay.net/v2/miniapppay",
                data=json.dumps(data),
                headers=headers,
            )
            json_data = json.loads(response.text)

            latipayId = json_data.paydata.order_id
            payment = json_data.payment

            return JsonResponse({
                'nonceStr': payment.nonceStr,
                'paySign': payment.paySign,
                'signType': payment.signType,
                'timeStamp': payment.timeStamp,
                'package': payment.packageStr,
            }, safe=False)
        else:
            raise Exception("Order not found")