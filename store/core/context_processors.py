# noinspection PyPackageRequirements
from django.core.cache import cache
from store.controller.cart import CartManager
from store.controller.product import ProductManager
from store.controller.store_manager import StoreManager
from store.models import *

def store_var(request):
    content = cache.get(CONST_STORE_SITE_VARS,None)
    if not content:
        cm = CartManager(request)
        pm = ProductManager(request)
        sm = StoreManager(request)
        store = sm.get_store()
        categories = pm.get_categories(store)
        content = {'cart':cm.get_cart(),
                   'currencies':[],
                   'categories':categories,
                   'store':store,
                   }
        cache.set(CONST_STORE_SITE_VARS,content)
    return {'store':content}