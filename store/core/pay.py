
# from django.template import Template,Context
from landing.core.common import get_html_from_template

import json

class PayBase(object):
    '''
    Payment base class
    '''
    def get_name(self):
        return None

    def pay(self,order,params=None):
        pass

    def create_secret(self, amount, currency):
        pass

    def get_metadata(self, request):
        pass

    def process_result(self, request):
        pass

    def refund(self,order,amount = 0):
        pass

    def get_content(self,filename):
        context = {'handler': self}
        return get_html_from_template('store/gateways/{name}/{file}'.format(name=self.get_name().lower(),
                                                                            file=filename), context)
    def get_custom_js(self):
        return self.get_content("custom_js.html")

    def get_paynow_js(self):
        return self.get_content("paynow_js.html")

    def get_setting_html(self):
        return self.get_content("settings.html")

    def set_configuration(self,config):
        if config:
            self.config = config

    def set_order(self,order):
        self.order = order

    def settings(self):
        result = None
        if self.config:
            settings = json.loads(self.config.settings)
            return settings
        return result
