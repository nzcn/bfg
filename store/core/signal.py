from landing.core.singal import BfgSignal
from django.dispatch import Signal,receiver

after_shipment_create = BfgSignal(providing_args=["request", "shipment"])
after_start_shipment = BfgSignal(providing_args=["request", "shipment"])
after_intransit_shipment = BfgSignal(providing_args=["request", "shipment"])
after_outfordelivery_shipment = BfgSignal(providing_args=["request", "shipment"])
after_delivered_shipment = BfgSignal(providing_args=["request", "shipment"])
after_pickup_shipment = BfgSignal(providing_args=["request", "shipment"])
after_cancel_shipment = BfgSignal(providing_args=["request", "shipment"])
after_exception_shipment = BfgSignal(providing_args=["request", "shipment"])
after_waitingcollection_shipment = BfgSignal(providing_args=["request", "shipment"])

