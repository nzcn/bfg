from __future__ import division
import decimal
from store.models import *


def currency_convert(currency_from, currency_to, amount):
    if currency_from and currency_to:
        if isinstance(currency_from, str):
            currency_from = Currency.objects.get(abstraction=currency_from)
        if isinstance(currency_to, str):
            currency_to = Currency.objects.get(abstraction=currency_to)
        if isinstance(amount, str):
            if ',' in amount:
                amount = amount.replace(',', '')
        return float(amount) * (currency_from.one_USD / currency_to.one_USD)


def decimal2(value):
    return "{0:.2f}".format(value)


def decimal3(value):
    return "{0:.3f}".format(value)
