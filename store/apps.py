# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig
from django.conf import settings


class StoreConfig(AppConfig):
    name = 'store'
    notifications = [
        {
            'key':'after_order_paid',
            'subject': 'Thanks for your order',
            'template':'/store/backend/mail/new_order.html',
        },
        {
            'key': 'after_order_refund',
            'subject': 'Your order #{{ order.id }} has been refund',
            'template': '/store/backend/mail/refund.html',
        },
        {
            'key': 'after_order_sent',
            'subject': 'Your parcel is on the way.',
            'template': '/store/backend/mail/shipped.html',
        },
        {
            'key': 'new_customer',
            'subject': 'Thanks for signing up {{ landing.site.title }}',
            'template': '/store/backend/mail/new_customer.html',
        },
    ]

def init():
    if "store" in settings.INSTALLED_APPS:
        if 'store.core.context_processors.store_var' not in settings.TEMPLATES[0]['OPTIONS']['context_processors']:
            settings.TEMPLATES[0]['OPTIONS']['context_processors'].append('store.core.context_processors.store_var')
