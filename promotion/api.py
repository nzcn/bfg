# noinspection PyPackageRequirements
from django.db.models import Q
from django.utils.translation import gettext as _
from rest_framework import serializers, viewsets, permissions
from rest_framework.decorators import action

from .controller.promotion import PromotionManager
from landing.api import api_result
from landing.core.common import get_req_value
from django.utils import timezone
from bfg.promotion.models import *


def register_api(router):
    router.register(r'promotions', PromotionViewSet, base_name='promotions')


class PromotionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Promotion
        fields = ('id', 'title', 'description', 'logo', 'start_time', 'end_time',
                  'company', 'available_days', 'available_date', 'price', 'price_text', 'purchasable',)


class UserAttendanceSerializer(serializers.ModelSerializer):
    promotion = PromotionSerializer(many=False, read_only=True)

    class Meta:
        model = UserAttendance
        fields = ('id', 'available_count', 'create_time', 'expire_time', 'use_time', 'promotion')


class WechatAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = WechatApp
        fields = ['title', 'type', 'is_test', 'home_logo', 'forward_logo', 'quote_link', 'contact_link']


class PromotionViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = PromotionSerializer

    def get_queryset(self):
        cid = get_req_value(self.request, "cid")
        if not cid and self.request and self.request.user.is_authenticated:
            cmqs = Customer.objects.filter(id=self.request.user.id)
            if cmqs.exists():
                customer = cmqs.first()
                if customer.company:
                    cid = customer.company.id
        if cid:
            q = Q(company_id=cid) & (Q(start_time__lt=timezone.now()) | Q(start_time__isnull=True)) & (
                    Q(end_time__gt=timezone.now()) | Q(end_time__isnull=True))
            return Promotion.objects.filter(q).order_by("start_time", "id")
        else:
            return Promotion.objects.none()

    @action(methods=['get', 'post'], detail=False)
    def vouchers(self, request, **kwargs):
        pm = PromotionManager(request)
        if request.method == "POST":
            code = get_req_value(request, 'code')
            try:
                customer = Customer.objects.get(id=request.user.id)
                result = pm.use_promotion_code(None, code, customer)
                if isinstance(result, list):
                    return api_result(_("Use promotion code successfully."),
                                      obj=UserAttendanceSerializer(result, many=True).data, code=0)
                else:
                    return api_result(_("Use promotion code successfully."),
                                      obj=UserAttendanceSerializer(result, many=False).data, code=0)
            except Exception as ex:
                return api_result(ex.__str__(), code=1)
        else:
            qs = pm.get_user_attendances()
            return api_result(_("Get user vouchers successfully"), obj=UserAttendanceSerializer(qs, many=True).data)
