# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-02-03 03:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('promotion', '0013_auto_20190603_1643'),
    ]

    operations = [
        migrations.CreateModel(
            name='WechatMessageTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(blank=True, default=None, max_length=10, null=True)),
                ('template_id', models.CharField(blank=True, default=None, max_length=50, null=True)),
                ('app', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='promotion.WechatApp')),
            ],
        ),
    ]
