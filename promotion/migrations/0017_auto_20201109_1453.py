# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2020-11-09 01:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0060_auto_20201102_1527'),
        ('promotion', '0016_auto_20200726_1604'),
    ]

    operations = [
        # migrations.RemoveField(
        #     model_name='coupon',
        #     name='product_ptr',
        # ),
        migrations.AddField(
            model_name='promotion',
            name='check_code',
            field=models.TextField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='promotion',
            name='class_path',
            field=models.CharField(blank=True, default=None, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='promotion',
            name='code',
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
        migrations.DeleteModel(
            name='Coupon',
        ),
    ]
