# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import *
from bfg.promotion.core.wechat import *



@csrf_exempt
def index(request, app_id):
    wm = WechatManager(request,app_id=app_id)

    if settings.DEBUG:
        print(request.GET)
        print(request.body)

    if 'signature' in request.GET:
        if wm.is_valide():
            if 'echostr' in request.GET:
                return HttpResponse(request.GET.get("echostr"))
            else:
                xml = wm.reply()
                if settings.DEBUG:
                    print(xml)
                return HttpResponse(xml)
        else:
            return HttpResponse("Unautherized Access")
    else:
        return HttpResponse("success")
