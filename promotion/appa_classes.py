from django.db.models.aggregates import Sum
from django.utils.translation import ugettext as _, ugettext_lazy
from bfg.promotion.models import *
from bfg.appa.controllers.core import *
from bfg.promotion.core.wechat import *
from bfg.appa.controllers.company import AppaCompanyManager
from bfg.promotion.controller.promotion import PromotionManager
from django.contrib import admin


def set_menu(modeladmin, request, queryset):
    '''
    Set Wechat MP Menu
    :param modeladmin:
    :param request:
    :param queryset:
    :return:
    '''
    for app in queryset:
        wm = WechatManager(request, app.app_key)
        result = wm.set_menu()
        if result['errcode'] > 0:
            message_user(request, result['errmsg'], messages.WARNING)
        else:
            message_user(request, _("Set wechat menu successfully"), messages.SUCCESS)


set_menu.short_description = ugettext_lazy("Set Wechat Menu")


class WechatOpenIdInline(admin.TabularInline):
    model = WechatOpenId
    fields = ['user', 'openid', 'unionid']

class WechatMessageTemplateInline(admin.StackedInline):
    model = WechatMessageTemplate
    fields = ['code','template_id']

class WechatAppAppa(CompanyBaseAdmin):
    list_display = ['title', 'app_key', 'type', 'code', 'expired_time']
    actions = [set_menu]
    inlines = [WechatMessageTemplateInline]
    fieldsets = [
        (None, {'fields': ['title', 'type', 'app_key', 'app_secret', 'code', 'is_test']}),
        ('Basic Information', {
            'fields': ['token', 'aes_key', 'auto_reply',
                       'template_message1',
                       'template_message2',
                       'template_message3',
                       'template_message4',
                       'template_message5',
                       'menu']}),
        ('Advanced Information', {
            'fields': ['home_logo', 'forward_logo', 'quote_link', 'contact_link']}),
    ]

def copy_promotion(modeladmin, request, queryset):
    from bfg.promotion.controller.promotion import PromotionManager
    '''
    Copy promotions
    :param modeladmin: 
    :param request: 
    :param queryset: 
    :return: 
    '''
    pm = PromotionManager(request)
    for promotion in queryset:
        pm.copy_promotion(promotion)
    message_user(request,'Copy {count} promotions.'.format(count=queryset.count()),messages.SUCCESS)

def create_10_code(modeladmin, request, queryset):
    from bfg.promotion.controller.promotion import PromotionManager
    '''
    Create 10 Coupon
    :param modeladmin: 
    :param request: 
    :param queryset: 
    :return: 
    '''
    pm = PromotionManager(request)
    for promotion in queryset:
        pm.create_promotion_code(promotion,10)
    message_user(request,'Created {count} coupons'.format(count=10),messages.SUCCESS)

copy_promotion.description = ugettext_lazy("Copy Promotions")
create_10_code.description = ugettext_lazy("Create 10 Codes")
# check_promotion.description = ugettext_lazy("Check Code")


class CouponAppa(CompanyBaseAdmin):
    list_display = ['promotion','promocode','end_time','available_count','create_time','description']
    ordering = ['-create_time']
    search_fields = ['promocode','description']
    list_filter = ['promotion']
    readonly_fields = ['create_time']
    fieldsets = [
        (None, {
            'fields': ['promotion', 'promocode', 'end_time', 'available_count','description']}),
    ]

class PromotionConnectionInline(admin.TabularInline):
    model = PromotionConnection
    extra = 1
    fk_name = 'parent'

class PromotionAppa(CompanyBaseAdmin):
    list_display = ['title','code', 'available_count', 'start_time', 'end_time', 'available_days', 'available_date','class_path','pos']
    ordering = ['pos','-id']
    fieldsets = [
        (None, {
            'fields': ['title', 'code', 'price', 'price_text', 'purchasable', 'description', 'link', 'start_time', 'end_time',
                       'count', 'available_count', 'available_days','is_invisible_to_staff','is_private','is_unique',
                       'available_date', 'logo', 'icon','pos']}),
        ('Advanced', {
            'classes': ('collapse',),
            'fields': ['class_path','app_key','app_secret','style', 'price_code', 'use_code', 'settings','regex']}),
    ]
    actions = [create_10_code, copy_promotion]
    inlines = [PromotionConnectionInline]
    # ToDo: Add Check Code Later
    # def pre_save(self, request, obj, form, change):
    #     cm = AppaCompanyManager(request)
    #     try:
    #         cm.check_code(obj.price_code)
    #     except Exception as ex:
    #         message_user(request,"Price Code Error: " + ex.__str__(),messages.ERROR)
    #
    #     try:
    #         cm.check_code(obj.use_code)
    #     except Exception as ex:
    #         message_user(request,"Use Code Error: " + ex.__str__(),messages.ERROR)

    def action_view(self, request, object_id, form_url='', extra_context=None):
        action = get_req_value(request,'action')

    def action_view_all(self, request, form_url='', extra_context=None):
        action = get_req_value(request, 'action')
        if action == 'add_promotion':
            cm = AppaCompanyManager(request)
            prm = PromotionManager(request)
            promotion_id = get_req_value(request,'promotion')
            customer_id = get_req_value(request,'customer')
            qty = get_req_value(request,'qty',0)
            if promotion_id and customer_id and qty:
                promotion = prm.get_promotion(promotion_id)
                customer = cm.get_customer(customer_id)
                try:
                    attendance = prm.attendance(customer, promotion, count=qty,
                                                description=_('Operator add promotion.'))
                    ids = []
                    if isinstance(attendance,list):
                        for ua in attendance:
                            ids.append(ua.id)
                    else:
                        ids.append(attendance.id)
                    return JsonResponse(get_action_result(True,_('Add operation successfully'),ids=ids))
                except Exception as ex:
                    return JsonResponse(get_action_result(False, ex.message))
            else:
                return JsonResponse(get_action_result(False, _('Add operation faild. Invalid parameters.')))




