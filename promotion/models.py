# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from bfg.appa.models import Company,CompanyModelBase
from store.models import Customer,Invoice,Product
from django.contrib.auth import models as AdminModels
from django.utils.translation import gettext_lazy as _
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models

# Create your models here.
class Agent(Company):
    level1_commision_rate = models.FloatField(default=0)
    level2_commision_rate = models.FloatField(default=0)
    level3_commision_rate = models.FloatField(default=0)
    level4_commision_rate = models.FloatField(default=0)
    level5_commision_rate = models.FloatField(default=0)
    level6_commision_rate = models.FloatField(default=0)
    customers = models.ManyToManyField(Customer,blank=True,related_name='agent_customers',through='AgentCustomer')
    invoices = models.ManyToManyField(Invoice,blank=True)
    children = models.ManyToManyField('self')
    create_time = models.DateTimeField(auto_now_add=True)

class AgentCustomer(models.Model):
    agent = models.ForeignKey(Agent)
    customer = models.ForeignKey(Customer)
    discount = models.FloatField(default=1)
    note = models.CharField(max_length=200,default=None,blank=True,null=True)
    create_time = models.DateTimeField(auto_now_add=True)

class CustomerGrade(CompanyModelBase):
    title = models.CharField(max_length=100,default=None,blank=True,null=True)
    description = models.TextField(default=None,blank=True,null=True)
    min_experience_credits = models.PositiveIntegerField(default=0)
    max_experience_credits = models.PositiveIntegerField(default=0)
    logo = models.ImageField(default=None,blank=True,null=True)
    icon = models.ImageField(max_length=50,default=None,blank=True,null=True)
    style = models.CharField(max_length=50, default=None, blank=True, null=True)

PROMOTION_TRIGGER_TYPE = (
    ('A','Automatic'),
    ('M','Manual'),
    ('R','Register'),
)

class PromotionGroup(CompanyModelBase):
    title = models.CharField(max_length=100, default=None, blank=True, null=True)
    description = RichTextUploadingField(default=None, blank=True, null=True)
    logo = models.ImageField(default=None, blank=True, null=True)
    icon = models.ImageField(max_length=50, default=None, blank=True, null=True)
    regex = models.CharField(max_length=100, default=None, blank=True, null=True)
    class_path = models.CharField(max_length=100, default=None, blank=True, null=True)


class Promotion(CompanyModelBase):
    title = models.CharField(max_length=100, default=None, blank=True, null=True)
    description = RichTextUploadingField(default=None, blank=True, null=True)
    link = models.URLField(max_length=255, default=None, blank=True, null=True)
    group = models.ForeignKey(PromotionGroup, default=None, blank=True, null=True, on_delete=models.SET_DEFAULT)
    price_text = models.CharField(max_length=10, default=None, blank=True, null=True)
    price = models.FloatField(default=0)
    purchasable = models.BooleanField(default=False)
    logo = models.ImageField(default=None, blank=True, null=True)
    icon = models.ImageField(max_length=50, default=None, blank=True, null=True)
    style = models.CharField(max_length=50, default=None, blank=True, null=True)
    trigger_type = models.CharField(max_length=1, choices=PROMOTION_TRIGGER_TYPE,default='M')
    start_time = models.DateTimeField(default=None, blank=True, null=True)
    count = models.SmallIntegerField(default=1)
    available_count = models.SmallIntegerField(default=0)
    available_days = models.SmallIntegerField(default=30)
    available_date = models.DateTimeField(default=None, blank=True, null=True)
    end_time = models.DateTimeField(default=None, blank=True, null=True)
    create_time = models.DateTimeField(auto_now_add=True)
    regex = models.CharField(max_length=100, default=None, blank=True, null=True)
    code = models.CharField(max_length=50, default=None, blank=True, null=True)
    codes = models.TextField(default=None, blank=True, null=True)
    app_key = models.CharField(max_length=50, default=None, blank=True, null=True)
    app_secret = models.CharField(max_length=50, default=None, blank=True, null=True)
    class_path = models.CharField(max_length=100, default=None, blank=True, null=True)
    price_code = models.TextField(default=None, blank=True, null=True)
    use_code = models.TextField(default=None, blank=True, null=True)
    check_code = models.TextField(default=None, blank=True, null=True)
    settings = models.TextField(default=None, blank=True, null=True)
    active = models.BooleanField(default=True, blank=True)
    is_unique = models.BooleanField(default=True, blank=True)
    is_private = models.BooleanField(default=False, blank=True)
    is_invisible_to_staff = models.BooleanField(default=False, blank=True)
    pos = models.PositiveSmallIntegerField(default=0)
    promotions = models.ManyToManyField('self', through='PromotionConnection', blank=True, symmetrical=False, related_name='children')

    def __str__(self):
        return str(self.title) if self.title else 'Untitled Promotion'

class PromotionConnection(models.Model):
    parent = models.ForeignKey(Promotion, on_delete=models.CASCADE, related_name='parent')
    child = models.ForeignKey(Promotion, on_delete=models.CASCADE, related_name='child')
    count = models.SmallIntegerField(default=1)
    pos = models.PositiveSmallIntegerField(default=0)


class Coupon(CompanyModelBase):
    promotion = models.ForeignKey(Promotion, on_delete=models.CASCADE)
    promocode = models.CharField(max_length=10,unique=True)
    create_time = models.DateTimeField(auto_now_add=True)
    end_time = models.DateTimeField(default=None, blank=True, null=True)
    available_count = models.SmallIntegerField(default=0)
    description = models.CharField(max_length=50, default=None, blank=True, null=True)
    

class UserAttendance(CompanyModelBase):
    user = models.ForeignKey(AdminModels.User,related_name='user', on_delete=models.CASCADE)
    promotion = models.ForeignKey(Promotion, on_delete=models.CASCADE)
    available_count = models.SmallIntegerField(default=1,blank=True)
    create_time = models.DateTimeField(auto_now_add=True,blank=True)
    expire_time = models.DateTimeField(default=None, blank=True, null=True)
    use_time = models.DateTimeField(default=None, blank=True, null=True)
    operator = models.ForeignKey(AdminModels.User,related_name='promotion_operator',default=None,null=True,blank=True, on_delete=models.SET_DEFAULT)
    file = models.FileField(default=None, blank=True, null=True)
    description = models.CharField(max_length=50, default=None, blank=True, null=True)



class AdsPosition(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True, default=None)
    pos = models.PositiveSmallIntegerField(default=0)
    weekly_cost = models.FloatField(default=0)



class Ads(CompanyModelBase):
    title = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True, default=None)
    href = models.URLField(default=None, null=True, blank=True)
    position = models.ForeignKey(AdsPosition,default=None,null=True,blank=True, on_delete=models.SET_DEFAULT)
    start_time = models.DateTimeField(default=None,null=True,blank=True)
    end_time = models.DateTimeField(default=None,null=True,blank=True)
    paid = models.BooleanField(default=False)



class Badge(CompanyModelBase):
    title = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True, default=None)
    image = models.ImageField(default=None, null=True, blank=True)
    pos = models.PositiveSmallIntegerField(default=0)

APP_TYPE = (
    ('A','App'),
    ('M','MP'),
    ('W','Website'),
    ('O','Mobile'),
)

class WechatApp(CompanyModelBase):
    title = models.CharField(max_length=20)
    app_key = models.CharField(max_length=50)
    app_secret = models.CharField(max_length=50)
    access_token = models.CharField(max_length=200, default=None, blank=True, null=True)
    expired_time = models.DateTimeField(default=None, null=True, blank=True)
    token = models.CharField(max_length=100,blank=True)
    aes_key = models.CharField(max_length=250,default=None,blank=True,null=True)
    type = models.CharField(max_length=1,default='A',choices=APP_TYPE)
    menu = models.TextField(default=None, null=True, blank=True)
    auto_reply = models.TextField(default=None, null=True, blank=True)
    code = models.CharField(max_length=50, default=None, blank=True, null=True)
    is_test = models.BooleanField(default=False)
    home_logo = models.ImageField(default=None, null=True, blank=True)
    forward_logo = models.ImageField(default=None,null=True,blank=True)
    quote_link = models.URLField(default=None, blank=True, null=True)
    contact_link = models.URLField(default=None, blank=True, null=True)
    template_message1 = models.CharField(max_length=255, default=None, blank=True, null=True)
    template_message2 = models.CharField(max_length=255, default=None, blank=True, null=True)
    template_message3 = models.CharField(max_length=255, default=None, blank=True, null=True)
    template_message4 = models.CharField(max_length=255, default=None, blank=True, null=True)
    template_message5 = models.CharField(max_length=255, default=None, blank=True, null=True)

    def __str__(self):
        if self.title:
            return self.title
        else:
            return 'Unknown Wechat App'


    class Meta:
        verbose_name = _("Wechat App")
        verbose_name_plural = _("Wechat Apps")

class WechatMessageTemplate(models.Model):
    app = models.ForeignKey(WechatApp, on_delete=models.CASCADE)
    code = models.CharField(max_length=10, default=None, blank=True, null=True)
    template_id = models.CharField(max_length=50,default=None,blank=True,null=True)


class WechatOpenId(models.Model):
    user = models.ForeignKey(AdminModels.User, on_delete=models.CASCADE)
    openid = models.CharField(max_length=50)
    unionid = models.CharField(max_length=50,default=None,blank=True,null=True)
    code = models.CharField(max_length=50,default=None,blank=True,null=True)
    app = models.ForeignKey(WechatApp, on_delete=models.CASCADE)
    access_token = models.CharField(max_length=200,default=None,blank=True,null=True)
    expired_time = models.DateTimeField(default=None,null=True,blank=True)
    refresh_token = models.CharField(max_length=200, default=None, blank=True, null=True)
    refresh_token_expired_time = models.DateTimeField(default=None, null=True, blank=True)
    created_time = models.DateTimeField(null=True,blank=True,auto_now_add=True)
    leave_time = models.DateTimeField(default=None,null=True,blank=True)

    class Meta:
        unique_together = [('openid', 'app')]