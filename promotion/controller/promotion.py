from bfg.promotion.models import *
from django.db.models import Q
from django.utils import timezone
from bfg.promotion.core.signal import *
from landing.core.common import get_class_by_path
from store.controller.customer import CustomerManager
from packgo.core.promotion import DefaultPromotion


class PromotionManager(object):
    def __init__(self, request=None):
        self.request = request

    def get_customer_promotions(self, customer, purchasable=None, in_time_range=False):
        if not customer:
            cm = CustomerManager(self.request)
            customer = cm.get_customer()

        if customer:
            f = Q(company=customer.company)
            if not purchasable is None:
                f = f & Q(purchasable=purchasable)
            if in_time_range:
                f = f & (Q(start_time__lt=timezone.now()) | Q(start_time__isnull=True)) & (
                        Q(end_time__gt=timezone.now()) | Q(end_time__isnull=True))
            return Promotion.objects.filter(f)
        else:
            return Promotion.objects.none()

    def get_promotion(self, promotion_id):
        try:
            return Promotion.objects.get(id=promotion_id)
        except:
            return None

    def get_user_attendance(self, id):
        return UserAttendance.objects.get(id=id)

    def get_user_attendances(self, user=None, all=False):
        if not user:
            user = self.request.user
        q = Q(user=user) & Q(expire_time__gt=timezone.now())
        if not all:
            q = q & Q(available_count__gt=0)
        return UserAttendance.objects.filter(q).order_by('create_time').prefetch_related('promotion')

    def check_promotion_code(self, promotion, code):
        '''
        Check promotion code exists and available
        :param promotion:
        :param code:
        :return:
        '''
        if not promotion:
            promotion = self.find_promotion_by_code(code)
        if promotion:
            if promotion.class_path:
                promotion_cls = get_class_by_path(promotion.class_path)
                promotion_obj = promotion_cls(promotion)
                return promotion_obj.check_code(code)
        promotion_obj = DefaultPromotion(promotion)
        return promotion_obj.check_code(code)

    def find_promotion_by_code(self, code):
        from landing.core.common import match_regex
        qs = Coupon.objects.filter(promocode=code)
        if qs.exists():
            return qs.first().promotion
        else:
            promotions = Promotion.objects.filter(regex__isnull=False)
            for prom in promotions:
                if match_regex(code, prom.regex):
                    return prom
            return None

    def create_promotion_code(self, promotion, count=10):
        from landing.core.common import get_random_string
        for i in range(count):
            coupon = Coupon(promotion=promotion, company=promotion.company)
            coupon.promocode = get_random_string(4, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
            coupon.available_count = 1
            coupon.end_time = promotion.end_time or timezone.now() + timedelta(days=promotion.available_days)
            coupon.save()

    def copy_promotion(self, promotion):
        from landing.core.common import get_random_string
        new_promotion = Promotion()
        new_promotion.__dict__.update(promotion.__dict__)
        new_promotion.id = None
        new_promotion.code = promotion.code + get_random_string(6)
        new_promotion.start_time = timezone.now()
        new_promotion.end_time = timezone.now() + timedelta(days=30)
        if promotion.available_date:
            new_promotion.available_date = new_promotion.start_time + timedelta(days=30)
        new_promotion.save()
        if promotion.promotions:
            for promo in promotion.promotions.all():
                PromotionConnection(parent=new_promotion, child=promo).save()

    def attendance(self, customer, promotion, count=0, description=None):
        from bfg.promotion.core.signal import after_customer_attendance_create
        if promotion.is_unique and UserAttendance.objects.filter(user=customer, promotion=promotion).exists():
            raise Exception('User already attend this promotion.')
        if promotion.promotions.all().count() > 0:
            # For multi promotion set
            attendances = []
            for conn in PromotionConnection.objects.filter(parent=promotion).order_by('pos').all():
                # Create UA seperately.
                for i in range(conn.count):
                    ua = self.create_promotion_attendance(conn.child, customer)
                    if ua:
                        ua.operator = self.request.user
                        if description:
                            ua.description = description
                        ua.save()
                        attendances.append(ua)
            return attendances
        else:
            # For single promotion set
            ua = self.create_promotion_attendance(promotion, customer)
            ua.available_count = count or promotion.available_count
            if self.request:
                ua.operator = self.request.user
            if description:
                ua.description = description
            ua.save()
            return ua

    def create_promotion_attendance(self, promotion, customer, count=1, description=None):
        ua = UserAttendance(company=customer.company, user_id=customer.id, promotion=promotion)
        ua.available_count = count
        if promotion.available_date:
            ua.expire_time = promotion.available_date
        elif promotion.available_days:
            ua.expire_time = timezone.now() + timedelta(days=promotion.available_days)
        else:
            ua.expire_time = timezone.now() + timedelta(days=60)
        if description:
            ua.description = description
        ua.save()
        after_customer_attendance_create.send_robust(self.__class__, request=self.request, customer=customer,
                                                     attendance=ua)
        return ua

    def use_promotion_code(self, promotion, code, customer):
        from store.controller.customer import CustomerManager
        from bfg.appa.controllers.company import AppaCompanyManager
        from django.utils import timezone

        '''
        Use Promotion Code
        :param promotion:
        :param code:
        :return:
        '''
        if not promotion:
            promotion = self.find_promotion_by_code(code)

        cm = AppaCompanyManager(request=self.request)
        registed_customer = cm.get_customer(customer.id)
        if not registed_customer:
            raise Exception('You have to become registered Customer before using this function.')

        if promotion:
            if promotion.class_path:
                promotion_cls = get_class_by_path(promotion.class_path)
                promotion_obj = promotion_cls(promotion)
            else:
                promotion_obj = DefaultPromotion(promotion)
            promotion = promotion_obj.check_code(code)
            if not promotion:
                raise Exception('Promotion not found in check code')
            if promotion_obj.use_code(code, description='used by {id}:{name} at {dt}'.format(id=customer.id,
                                                                                             name=customer.get_full_name(),
                                                                                             dt=timezone.now().strftime(
                                                                                                 '%Y-%m-%d %H:%M'))):
                # Create User Vouchers
                if customer:
                    return self.attendance(customer, promotion, 1, code)
                else:
                    raise Exception('User not found')
            else:
                raise Exception('Code has already been used.')
        else:
            raise Exception('Promotion not found')

    def delete_user_atendance(self, ua):
        if ua and ua.declarationform_set.count() == 0:
            ua.delete()
        else:
            raise Exception('UA has been used.')

    def delete_user_atendances(self, ids):
        uas = UserAttendance.objects.filter(id__in=ids)
        count = 0
        for ua in uas:
            if ua.declarationform_set.count() == 0:
                if ua.delete():
                    count += 1
            else:
                raise Exception('UA has been used.')
        return count
