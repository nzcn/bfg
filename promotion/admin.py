# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# noinspection PyPackageRequirements
from django.contrib import admin
from bfg.promotion.models import *

# Register your models here.
admin.site.register(Promotion)
