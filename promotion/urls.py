from django.conf.urls import url, include
import bfg.promotion.views as views

urlpatterns = [
    url(r'^wx/(?P<app_id>\w+)/message$', views.index, name="wx_message"),
]
