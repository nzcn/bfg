from math import *
def amount_minus(total, target, minus, count=0):
    save = floor(total / target) * minus
    return total - save

def amount_plus(total, target, plus, count=0):
    save_count = floor(total/(plus + target)) + floor((total-floor(total/(plus + target)) * (target + plus))/target)
    if count > 0:
        save_count = min(save_count,count)
    save = save_count * plus
    return max(total - save, target * save_count)
