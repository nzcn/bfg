from django.utils.translation import ugettext as _
from rest_framework_jwt.serializers import *
from rest_framework_jwt.views import JSONWebTokenAPIView
from bfg.promotion.core.wechat.manager import WechatManager
from bfg.promotion.models import WechatApp, WechatOpenId
from landing.core.common import *

# Create your views here.
def get_app(appId):
    if appId:
        q = WechatApp.objects.filter(app_key=appId)
        if q.exists():
            return q.first()
        else:
            return None
    else:
        return None

def get_app_by_code(code,company):
    try:
        return WechatApp.objects.get(code=code,company=company)
    except:
        return None

def get_wechat_user(openId, app=None):
    if app:
        try:
            return WechatOpenId.objects.get(app=app,openid=openId)
        except:
            return None
    else:
        try:
            return WechatOpenId.objects.get(openid=openId)
        except:
            return None

def get_wechat_openid(user, app):
    try:
        return WechatOpenId.objects.get(app=app, user=user)
    except:
        return None

def get_wechat_by_user(user,company):
    return WechatOpenId.objects.filter(app__company=company, user=user)

@atomic
def complete_auth(backend, uid, user=None, social=None, *args, **kwargs):
    from social_django.models import UserSocialAuth
    app = get_app(settings.SOCIAL_AUTH_WEIXIN_KEY)
    wm = WechatManager(request=kwargs['request'],app_id=app.id)
    customer = wm.get_or_create_customer(app,
                                         openid=uid,
                                         user_info=kwargs['response'],
                                         request=kwargs['request'])
    provider = backend.name
    social = UserSocialAuth.objects.get(provider=provider, uid=uid)
    if customer and user and customer.id != user.id:
        social.user = customer
        social.save()
        # Remove user
        user.delete()
    customer.provider = provider
    return {'user': customer,
            'is_new': customer is None
            }


class WeChatAppBackend(object):
    def authenticate(self, request, code=None, **kwargs):
        if code:
            wm = WechatManager(request,app_id=get_req_value(request, 'appid'))
            openid, user_info, app = wm.get_user_info(code, request)
            customer = wm.get_or_create_customer(app, openid, user_info, request)
            return customer
        else:
            print('Code is None in Wechat authentication.')
            return None

class WeChatAppCdoeSerializer(Serializer):
    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(WeChatAppCdoeSerializer, self).__init__(*args, **kwargs)
        self.fields['code'] = serializers.CharField()
        self.request = kwargs['context']['request']

    def validate(self, attrs):
        credentials = {
            'code': attrs.get('code'),
        }

        if all(credentials.values()):
            user = authenticate(request=self.request, **credentials)
            if user:
                if isinstance(user,AdminModels.User) and user.is_active:
                    payload = jwt_payload_handler(user)
                    return {
                        'token': jwt_encode_handler(payload),
                        'user': user
                    }
                else:
                    raise serializers.ValidationError(_('Not a valid user. Unknown Error'))
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "code"')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)


class WeChatAppCodeJwt(JSONWebTokenAPIView):
    serializer_class = WeChatAppCdoeSerializer


