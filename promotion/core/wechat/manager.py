from bfg.promotion.models import WechatApp, WechatOpenId
import weixin
from django.db import transaction
from weixin.lib.wxcrypt import WXBizDataCrypt
from weixin.lib.WXBizMsgCrypt import WXBizMsgCrypt
from weixin.oauth2 import OAuth2AuthExchangeRequest
from packgo.core.signal import after_customer_create, after_visitor_create
from django.conf import settings
from landing.core.common import *
from landing.core.api import *
import xmltodict
from datetime import *
import pytz

from store.controller.customer import CustomerManager
from bfg.promotion.core.wechat.response import ProResponse
from bfg.appa.controllers.company import AppaCompanyManager
from store.models import Channel, Customer
from django.utils.translation import ugettext as _


class WechatManager(object):
    def __init__(self, request, app_id=None, app_secret=None, token=None):
        self.app_id = app_id
        self.request = request
        self.app = None
        self.access_token = None
        if app_id:
            q = WechatApp.objects.filter(app_key=app_id)
            if q.exists():
                app = q.first()
                self.app = app
                if not app_secret:
                    self.app_secret = app.app_secret
                else:
                    self.app_secret = app_secret
                if not token:
                    self.token = app.token
                else:
                    self.token = token
            else:
                raise Exception("App not registed.")

    def get_app(self, appId):
        if appId:
            q = WechatApp.objects.filter(app_key=appId)
            if q.exists():
                return q.first()
            else:
                return None
        else:
            return None

    def _get_args(self, request, token, is_message=False):
        params = request.GET
        if not params:
            raise Exception("invalid params")
        args = {
            'mp_token': token,
            'signature': params.get('signature'),
            'timestamp': params.get('timestamp'),
            'echostr': params.get('echostr'),
            'nonce': params.get('nonce'),
        }

        if is_message:
            args.update({
                'msg_signature': params.get('msg_signature'),
                'encrypt_type': params.get('encrypt_type'),
            })
        return args

    def _decrypt_xml(self, params, crypt, xml_str):
        nonce = params.get('nonce')
        msg_sign = params.get('msg_signature')
        timestamp = params.get('timestamp')
        ret, decryp_xml = crypt.DecryptMsg(xml_str, msg_sign,
                                           timestamp, nonce)
        return decryp_xml, nonce

    def _encryp_xml(self, crypt, to_xml, nonce):
        to_xml = to_xml
        ret, encrypt_xml = crypt.EncryptMsg(to_xml, nonce)
        return encrypt_xml

    def add_wo(self, openid, user, app, unionid=None):
        q = WechatOpenId.objects.filter(app=app, openid=openid, user=user)
        if q.exists():
            wo = q.first()
            if wo.user != user:
                wo.user = user
                wo.save()
            return wo
        else:
            oi = WechatOpenId.objects.create(app=app, openid=openid, user=user, unionid=unionid)
            return oi

    def _get_xml(self, data):
        post_str = data
        return post_str

    def get_wechat_user(self, openId, app=None):
        if app:
            try:
                return WechatOpenId.objects.get(app=app, openid=openId)
            except:
                return None
        else:
            try:
                return WechatOpenId.objects.get(openid=openId)
            except:
                return None

    def get_user_info(self, code, request):
        app = None
        if 'appid' in request.POST:
            app = self.get_app(self.app_id)
            if app:
                api = weixin.WXAPPAPI(appid=app.app_key, app_secret=app.app_secret)
            else:
                raise Exception("AppId Not found")
        else:
            api = weixin.WXAPPAPI(appid=settings.SOCIAL_AUTH_WEIXINAPP_KEY,
                                  app_secret=settings.SOCIAL_AUTH_WEIXINAPP_SECRET)

        session_info = api.exchange_code_for_session_key(code=code)
        openid = session_info.get("openid")

        if 'data' in request.POST and 'iv' in request.POST:
            encrypted_data = request.POST.get('data')
            iv = request.POST.get("iv")
            session_key = session_info.get('session_key')
            if app:
                crypt = WXBizDataCrypt(app.app_key, session_key)
            else:
                crypt = WXBizDataCrypt(settings.SOCIAL_AUTH_WEIXINAPP_KEY, session_key)
            user_info = crypt.decrypt(encrypted_data, iv)
        else:
            user_info = {}
        user_info.update(session_info)
        return openid, user_info, app

    def clean_name(self, user_info):
        try:
            if 'nickName' in user_info:
                name = user_info['nickName']
            elif 'nickname' in user_info:
                name = user_info['nickname']
            else:
                name = None
            return name
        except:
            return None

    def get_unionid(self, user_info):
        '''
        Get UnionId from user_info dictionary
        :param user_info:
        :return:
        '''
        if 'unionid' in user_info:
            unionid = user_info['unionid']
        elif 'unionId' in user_info:
            unionid = user_info['unionId']
        else:
            unionid = None
        return unionid

    @atomic
    def get_or_create_customer(self, app, openid, user_info, request):
        '''
        Create or get Wechat Customer When Wechat User Login
        :param app:
        :param openid:
        :param user_info:
        :param request:
        :return:
        '''
        if not openid:
            raise Exception("OpenId is needed")

        if not app:
            raise Exception("App is not available")

        customer_class = get_class_by_path(get_bfg_settings("wechat_customer", "store.models.Customer"))
        if not customer_class:
            raise Exception("wechat_customer class is needed")

        unionid = self.get_unionid(user_info)
        customer = None
        need_save = False
        create = False

        wechat_user_openid = self.get_wechat_user(openid, app)
        if wechat_user_openid and wechat_user_openid.user:
            q = customer_class.objects.filter(id=wechat_user_openid.user_id)
            if q.exists():
                customer = q.first()
            else:
                # The openid has attached to other types of user. Return None
                return wechat_user_openid.user

        hijack_user = get_req_value(request, 'hijack_user')
        if hijack_user:
            q = customer_class.objects.filter(username=hijack_user)
            if q.exists():
                customer = q.first()

        if not customer and unionid and app.company:
            cq = customer_class.objects.filter(customer_id=unionid, company=app.company)
            if cq.exists():
                customer = cq.first()

        if not customer:
            # Add "wx_" prefix to username to identify it is a wechat_user_openid from wx
            username = "wx_{app}_{openid}".format(app=app.app_key, openid=openid)
            find_user_by_username = AdminModels.User.objects.filter(username=username)
            find_customer_by_username = customer_class.objects.filter(username=username)
            if find_customer_by_username.exists():
                customer = find_customer_by_username.first()
            else:
                if find_user_by_username.exists():
                    base_user = find_user_by_username.first()
                    customer = customer_class(user_ptr_id=base_user.id)
                    customer.__dict__.update(base_user.__dict__)
                    customer.save()
                else:
                    customer = customer_class.objects.create_user(username=username,
                                                                  password=get_random_string(length=32))
                create = True

        # There must be a customer object by now.

        # Set Assocication
        if wechat_user_openid:
            if wechat_user_openid.user_id != customer.id:
                wechat_user_openid.user_id = customer.id
                wechat_user_openid.save()
        else:
            wechat_user_openid = self.add_wo(openid, customer, app, unionid=unionid)

        if not customer.company:
            if 'errcode' not in user_info:
                customer.first_name = self.clean_name(user_info)

            uid = get_req_value(request, "uid", type=int)
            user_chid = None
            if uid and isinstance(customer, customer_class):
                customer.referal_id = uid
                cm = CustomerManager(request)
                referal_customer = cm.get_customer_by_uid(uid)
                if referal_customer:
                    ch_filter = Q(members__in=[referal_customer])
                    ch_query = Channel.objects.filter(ch_filter)
                    if ch_query.exists():
                        user_chid = ch_query.first().id

            # Check wether channel id exists
            company = None
            chid = get_req_value(request, 'ch') or user_chid
            if chid:
                ch_query = Channel.objects.filter(id=chid)
                if ch_query.exists():
                    channel = ch_query.first()
                    customer.from_channel = channel
                    if channel.company:
                        company = channel.company
            else:
                cid = get_req_value(request, 'cid')
                if cid:
                    cm = AppaCompanyManager(self.request)
                    company = cm.get_company(cid)

            if not company:
                company = app.company
            customer.company = company

            # Customized Function
            if 'create_request' in dir(customer):
                customer.create_request(request)

        # Check UnionId
        if unionid and customer.customer_id != unionid:
            customer.customer_id = unionid
            need_save = True

        profile = customer.settings
        if not profile:
            profile_q = Profile.objects.filter(user=customer)
            if profile_q.exists():
                profile = profile_q.first()
            else:
                profile = Profile(user=customer)

            if 'gender' in user_info:
                profile.gender = 'M' if user_info['gender'] == 1 else 'F'
            elif 'sex' in user_info:
                profile.gender = 'M' if user_info['sex'] == 1 else 'F'
            else:
                profile.gender = 'U'
            if 'avatarUrl' in user_info:
                avartar_url = user_info['avatarUrl']
            elif 'headimgurl' in user_info:
                avartar_url = user_info['headimgurl']
            else:
                avartar_url = None

            if avartar_url and len(avartar_url) > 0:
                profile.picture = save_url_to_media(avartar_url,
                                                    path="customers/{name}".format(name=customer.username),
                                                    filename="logo.png")
                customer.logo = profile.picture
            if 'language' in user_info:
                profile.lang = user_info['language']
            profile.account_wechat = self.clean_name(user_info)
            profile.save()
            customer.settings = profile
            customer.nickname = profile.account_wechat
            need_save = True

        if not profile.social_wechat_settings:
            profile.social_wechat_settings = json.dumps(user_info)
            profile.save()

        # if not customer.wechat_mp and app.type == 'M':
        #     customer.wechat_mp = True
        #     need_save = True
        #
        # if not customer.wechat_app and app.type == 'A':
        #     customer.wechat_app = True
        #     need_save = True

        if need_save:
            customer.save()

        if create:
            after_visitor_create.send_robust(sender=self, request=request, customer=customer)

        # Add Hijack function
        if customer and customer.is_superuser and 'HTTP_KEY' in request.META:
            key = request.META['HTTP_KEY']
            qs = customer_class.objects.filter(key=key)
            if qs.exists():
                customer = qs.first()

        return customer

    def is_valide(self):
        args = self._get_args(self.request, self.token)
        api = weixin.WeixinMpAPI(**args)
        return api.validate_signature()

    def get_access_token(self, type="m"):
        if self.app:
            if not self.app.expired_time or self.app.expired_time < timezone.now():
                if self.app.type == 'M':
                    api = weixin.WeixinMpAPI(appid=self.app_id, app_secret=self.app_secret,
                                             grant_type='client_credential')
                elif self.app.type == 'A':
                    api = weixin.WXAPPAPI(appid=self.app_id, app_secret=self.app_secret, grant_type='client_credential')
                else:
                    api = None
                if api:
                    try:
                        req = OAuth2AuthExchangeRequest(api)
                        data = req.exchange_for_access_token()
                        self.access_token = data['access_token']
                        self.app.access_token = self.access_token
                        self.app.expired_time = timezone.now() + timedelta(seconds=data["expires_in"])
                        self.app.save()
                        return self.app.access_token
                    except Exception as ex:
                        print("Get Wechat Access Token Error: {ex}".format(ex=ex.__str__()))
                        return None
            elif self.app.access_token:
                return self.app.access_token
            else:
                return None
        else:
            return None

    def refresh_access_token(self):
        if self.app:
            self.app.access_token = None
            self.app.expired_time = None
            self.app.save()
            self.get_access_token()

    def get_api(self, access_token, type="m"):
        api = weixin.WeixinMpAPI(access_token=access_token)
        return api

    def mp_get_user_info(self, api, openid=None, code=None):
        if openid:
            url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={ACCESS_TOKEN}&openid={OPENID}&lang=zh_CN".format(
                ACCESS_TOKEN=api.access_token,
                OPENID=openid
            )
            response = requests.get(url)
            return json.loads(response.content)
        else:
            return None

    def mp_get_user(self):
        if 'openid' in self.request.GET:
            openid = self.request.GET.get("openid")
            q = WechatOpenId.objects.filter(openid=openid, app=self.app)
            unionid = None
            customer = None
            if q.exists():
                wo = q.first()
                customer = wo.user
            else:
                wo = None

            if not customer:
                # Set the user propertry
                cm = CustomerManager(self.request)
                # Check whether there is associate use exists
                api = self.get_api(access_token=self.get_access_token())
                user_info = self.mp_get_user_info(api, openid=openid)

                # Create new customer
                customer = self.get_or_create_customer(self.app,
                                                       openid=openid,
                                                       user_info=user_info,
                                                       request=self.request)

                if customer and not wo:
                    # Found the customer and create a new wo record
                    self.add_wo(openid, customer, self.app, unionid=unionid)

            # Return the associated user
            return customer
        else:
            return None

    def get_wechat_openid(self):
        if 'openid' in self.request.GET and self.app:
            openid = self.request.GET.get("openid")
            try:
                return WechatOpenId.objects.get(openid=openid, app=self.app)
            except:
                return None
        else:
            return None

    def get_openid_by_user(self, user):
        try:
            if isinstance(user, AdminModels.User):
                wo = WechatOpenId.objects.get(user=user, app=self.app)
                return wo.openid
            else:
                return WechatOpenId.objects.filter(user__in=user, app=self.app).values_list('openid', flat=True)
        except:
            return None

    def reply(self):
        xml_str = self._get_xml(self.request.body)
        decryp_xml = xml_str
        xml_dict = xmltodict.parse(decryp_xml)
        if self.app and self.app.code:
            response_class = get_class_by_path(self.app.code)
        else:
            response_class = get_class_by_path(
                get_bfg_settings("wechat_class") or "bfg.promotion.core.wechat.response.ProResponse")
        if response_class and issubclass(response_class, ProResponse):
            api = response_class(xml_dict, self)
        else:
            api = ProResponse(xml_dict, self)
            api.auto_reply_content = _("The response class has not been configured properly.")
        xml = api() if api else 'success'
        return xml

    def set_menu(self):
        if self.app and self.app.menu:
            url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token={ACCESS_TOKEN}".format(
                ACCESS_TOKEN=self.get_access_token()
            )
            response = requests.post(url, data=self.app.menu)
            result = json.loads(response.content)
            return result

    def convert_to_api_result(self, result, success_msg=None, fail_msg=None, obj=None):
        if result:
            if 'errcode' in result and result['errcode'] > 0:
                return api_result(fail_msg if fail_msg else result['errmsg'], code=result['errcode'])
            else:
                return api_result(success_msg if success_msg else 'success', code=0, obj=obj)
        else:
            raise Exception('Wechat result error')

    def send_template_message(self, user, template_id, context, detail_url='', app_id=None, app_url=None,
                              header_color='FF0000', retry=False, lang=None):
        if settings.DEBUG or settings.TESTING:
            # openid = self.get_openid_by_user(Customer.objects.filter(email__in=settings.ADMIN_EMAIL,company=user.company).first())
            # openid = self.get_openid_by_user(Customer.objects.filter(id=1736).first())
            openid = self.get_openid_by_user(user)
        else:
            openid = self.get_openid_by_user(user)
        result = {}
        if openid:
            if self.app.type == 'M':
                url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={ACCESS_TOKEN}".format(
                    ACCESS_TOKEN=self.get_access_token()
                )
                data = {
                    "template_id": template_id,
                    "url": detail_url,
                    "topcolor": header_color,
                    "data": context,
                }
                if app_id and app_url:
                    data.update({
                        "miniprogram": {
                            "appid": app_id,
                            "pagepath": app_url,
                        },
                    })
            else:
                url = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token={ACCESS_TOKEN}".format(
                    ACCESS_TOKEN=self.get_access_token()
                )
                data = {
                    "template_id": template_id,
                    "data": context,
                }
                if app_id and app_url:
                    data.update({
                        "miniprogram_state": app_id,
                        "page": app_url,
                    })
            if isinstance(openid, str):
                data.update({
                    "touser": openid,
                })
                response = requests.post(url, json=data)
                result = json.loads(response.content)
            elif isinstance(openid, list):
                for oid in openid:
                    data.update({
                        "touser": oid,
                    })
                    response = requests.post(url, json=data)
                    result = json.loads(response.content)

        if 'errcode' in result and result['errcode']:
            if result['errcode'] == 40001 and not retry:
                token = self.refresh_access_token()
                self.send_template_message(user=user, template_id=template_id, context=context,
                                           detail_url=detail_url, app_id=app_id, app_url=app_url,
                                           header_color=header_color, retry=True)
            print('Send Wechat Message Error > Data: {data} Result: {result}'.format(data=json.dumps(data),
                                                                                     result=json.dumps(result)))
        return result

    def send_wxapp_message(self, customer, weapp_template_msg, mp_template_msg, send_wxapp=False):
        openid = self.get_openid_by_user(customer)
        token = self.get_access_token(type='a')
        if openid:
            url = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send?access_token={ACCESS_TOKEN}". \
                format(ACCESS_TOKEN=token)
            data = {
                "touser": openid,
                "mpTemplateMsg": mp_template_msg,
            }
            if send_wxapp:
                data.update({
                    "weappTemplateMsg": weapp_template_msg,
                })
            response = requests.post(url, json=data)
            return json.loads(response.content)
        else:
            return None

    def add_wxapp_message_template(self, id, field_list):
        token = self.get_access_token(type='a')
        url = "https://api.weixin.qq.com/cgi-bin/wxopen/template/add?access_token={ACCESS_TOKEN}". \
            format(ACCESS_TOKEN=token)
        data = {
            "id": id,
            "keyword_id_list": field_list,
        }
        response = requests.post(url, json=data)
        return json.loads(response.content)

    def create_app_qr(self, scene, file_url):
        token = self.get_access_token(type=self.app.type.lower())
        url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token={ACCESS_TOKEN}". \
            format(ACCESS_TOKEN=token)
        data = {
            "scene": scene,
        }
        response = requests.post(url, json=data)
        filepath = os.path.join(settings.MEDIA_ROOT, file_url)
        check_dir(filepath)
        open(filepath, 'wb').write(response.content)
        return file_url
