# -*- coding: utf-8 -*-
import weixin
from weixin.reply import *
from weixin.reply import TextReply
from landing.core.common import get_bfg_settings
from datetime import *
from django.utils import *
import re

AUTO_REPLY_CONTENT = '欢迎关注！'

class ProResponse(weixin.WXResponse):
    def __init__(self, xml_dict, wm, *args, **kwargs):
        self.auto_reply_content = get_bfg_settings("wechat_autoreply") or AUTO_REPLY_CONTENT
        self.wm = wm
        self.handlers = []
        self.init()
        super(ProResponse, self).__init__(xml_dict=xml_dict)

    def _subscribe_event_handler(self):
        user = self.wm.mp_get_user()
        if self.wm.app and self.wm.app.auto_reply:
            self.reply_params['content'] = self.wm.app.auto_reply
        else:
            self.reply_params['content'] = self.auto_reply_content
        self.reply = TextReply(**self.reply_params).render()

    def _unsubscribe_event_handler(self):
        wo = self.wm.get_wechat_openid()
        if wo:
            wo.leave_time = timezone.now()
            wo.save()

    def init(self):
        pass

    def get_user(self):
        return self.wm.mp_get_user()

    def get_openid(self):
        return self.data.get("FromUserName")

    def register_handler(self,reg,func):
        self.handlers.append({"key":reg,"func":func})

    def _text_msg_handler(self):
        message = self.data['Content']
        for handler in self.handlers:
            matches = re.findall(handler['key'], message, re.MULTILINE)
            if len(matches) > 0:
                result = handler['func'](message, matches)
                self.reply = result
                return
            else:
                continue
        return None

