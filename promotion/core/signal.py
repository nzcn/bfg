from landing.core.singal import BfgSignal
from django.dispatch import Signal,receiver

after_customer_attendance_create = BfgSignal(providing_args=["request", "customer","attendance"])
after_customer_attendance_used = BfgSignal(providing_args=["request", "customer","attendance"])
